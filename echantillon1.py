from __future__ import print_function

from sympy import symbols

from in_equations import simplification_latex

x = symbols("x")
inequation = 3 * x - 4 > 5 * x - 3

print(simplification_latex(inequation))
