#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from sympy import sympify, symbols, Poly, latex, Add, Pow, Mul, sqrt, Rational, Eq

from random import choice
import __init__

from generateurTex import generer_corrige_ds
from compilation.compilateur import compileDocument
from donnees.donnees import decode_distribution_dm

from generateurTex import generer_corrige_ds
from secondDegre import (
    inequations,
    equations,
    corrige_equ_poly_snd_deg,
    corrige_ineq_poly_snd_deg,
)

from trigo import (
    generer_AnglePourMesPrinc,
    corrige_mesurePrincipale,
    generer_AnglesEgauxOuPresque,
    corrige_ApplicationPythagore,
    questionPlacerPointsImagesSurCercleTrace,
)

from secondDegre import equations


x = symbols("x")
t = symbols("t")
R = symbols("R")
z = symbols("z")

for i in xrange(10):
    angle = generer_AnglePourMesPrinc(choice(["-", "+"]), choice(["-", "+"]))
    print angle
    print corrige_mesurePrincipale(angle)
    print ""
    [
        "cosinus et sinus",
        "equations du second degr\xc3\xa9",
        "mesure principale",
        "Inequations du second degr\xc3\xa9",
        "equations trigonometriques",
        "angles egaux ",
    ]
if __name__ == "__main__":
    eleve = decode_distribution_dm("1STI2DdistriDM2.csv")[3]
    print eleve.keys()
    mesures_pour_mp = [
        generer_AnglePourMesPrinc(choice(["-", "+"]), choice(["-", "+"]))
        for k in xrange(eleve["mesure principale"])
    ]
    equations_snd = equations()[: eleve["equations du second degr\xc3\xa9"]]
    print equations_snd

    devoir = {
        "classe": "1 STI-2D",
        "num": 1,
        "date": "lundi 3 octobre 2016",
        "sujets": [
            {
                "exercices": [
                    [
                        corrige_equ_poly_snd_deg(R ** 2 - 4 * R + 6),
                        corrige_equ_poly_snd_deg(3 * t ** 2 - 13 * t - 10),
                        corrige_equ_poly_snd_deg(9 * x ** 2 - 12 * x + 4),
                    ],
                    [
                        corrige_ineq_poly_snd_deg(2 * t ** 2 - 4 * t + 3 > 0),
                        corrige_ineq_poly_snd_deg(81 - 4 * x ** 2 <= 0),
                    ],
                ]
            },
            {
                "exercices": [
                    [
                        corrige_equ_poly_snd_deg(x ** 2 - 6 * x + 8),
                        corrige_equ_poly_snd_deg(-3 * R ** 2 - 6 * R - 12),
                        corrige_equ_poly_snd_deg(-t ** 2 + 6 * t - 9),
                    ],
                    [
                        corrige_ineq_poly_snd_deg(36 - 25 * R ** 2 > 0),
                        corrige_ineq_poly_snd_deg(-3 * x ** 2 + 6 * x - 12 < 0),
                    ],
                ]
            },
        ],
    }
    # compileDocument('1STI2D_corrige_DS1',generer_corrige_ds(devoir))
print corrige_ApplicationPythagore("x", r"\sin", Rational(1, 3))
