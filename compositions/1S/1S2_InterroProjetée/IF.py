#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from random import choice

import os.path
import os

import sys

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
print dossier_actuel
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)


from compilation.compilateur import compileDocument


from trigo import *

if __name__ == "__main__":

    test_rapide = {
        "classe": "1S2",
        "date": u"Mardi 6 février 2018",
        "titre": "test rapide 1",
        "num": 1,
        "questions": [
            {
                "gauche": questionLectureMesureAngle([3, 4], point="E"),
                "droite": questionLectureMesureAngle([2, 3], point="F", positive=False),
                "temps": 20,
            },
            {
                "gauche": questionLecturePointImage([3, 3], nomsPoints="BCDEFA"),
                "droite": questionLecturePointImage([2, 4], nomsPoints="BCDEFGHA"),
                "temps": 20,
            },
            {
                "gauche": questionLecturePointImage([-2, 4], nomsPoints="BCDEFGHA"),
                "droite": questionLecturePointImage([-3, 3], nomsPoints="BCDEFA"),
                "temps": 20,
            },
            {
                "gauche": questionLectureMesureAngle([4, 3], point="C"),
                "droite": questionLectureMesureAngle([3, 4], point="B", positive=False),
                "temps": 20,
            },
            {
                "gauche": questionLectureMesureAngle([4, 6], point="C", positive=False),
                "droite": questionLectureMesureAngle([1, 2], point="B", positive=False),
                "temps": 20,
            },
            {
                "gauche": questionLectureMesurePrincipaleAngle([8, 6], point="C"),
                "droite": questionLectureMesurePrincipaleAngle([3, 4], point="B"),
                "temps": 20,
            },
            {
                "gauche": questionLectureMesurePrincipaleAngle([4, 6], point="C"),
                "droite": questionLectureMesurePrincipaleAngle([5, 4], point="B"),
                "temps": 20,
            },
            {
                "gauche": questionCosinus([1, 1]),
                "droite": questionCosinus([0, 1]),
                "temps": 20,
            },
            {
                "gauche": questionSinus([1, 6]),
                "droite": questionCosinus([1, 3]),
                "temps": 20,
            },
        ],
    }
    test_rapide2 = {
        "classe": "1S2",
        "date": u"Mercredi 7 février 2018",
        "num": 2,
        "questions": [
            {
                "gauche": questionLectureMesureAngle([2, 3], point="E"),
                "droite": questionLectureMesureAngle([3, 4], point="F"),
                "temps": 25,
            },
            {
                "gauche": questionLecturePointImage([5, 3], nomsPoints="BCDEFA"),
                "droite": questionLecturePointImage([-1, 4], nomsPoints="BCDEFGHA"),
                "temps": 25,
            },
            {
                "gauche": questionLecturePointImage([3, 4], nomsPoints="BCDEFGHA"),
                "droite": questionLecturePointImage([4, 3], nomsPoints="BCDEFA"),
                "temps": 25,
            },
            {
                "gauche": questionLectureMesureAngle([2, 4], point="C", positive=False),
                "droite": questionLectureMesureAngle([4, 6], point="B", positive=False),
                "temps": 25,
            },
            {
                "gauche": questionLectureMesurePrincipaleAngle([8, 6], point="C"),
                "droite": questionLectureMesurePrincipaleAngle([5, 4], point="B"),
                "temps": 25,
            },
            {
                "gauche": questionLectureMesurePrincipaleAngle([3, 4], point="C"),
                "droite": questionLectureMesurePrincipaleAngle([4, 6], point="B"),
                "temps": 25,
            },
            {
                "gauche": questionCosinus([0, 1]),
                "droite": questionCosinus([1, 1]),
                "temps": 25,
            },
            {
                "gauche": questionCosinus([1, 3]),
                "droite": questionCosinus([1, 2]),
                "temps": 25,
            },
            {
                "gauche": questionSinus([1, 4]),
                "droite": questionCosinus([1, 6]),
                "temps": 25,
            },
            {
                "gauche": questionSinus([1, 6]),
                "droite": questionCosinus([1, 4]),
                "temps": 25,
            },
        ],
    }
    test_rapide3 = {
        "classe": "1S2",
        "date": u"Mardi 13 février 2018",
        "num": 3,
        "questions": [
            {
                "gauche": questionCosinus([0, 1]),
                "droite": questionSinus([0, 1]),
                "temps": 25,
            },
            {
                "gauche": questionCosinus([1, 3]),
                "droite": questionCosinus([1, 4]),
                "temps": 25,
            },
            {
                "gauche": questionSinus([1, 4]),
                "droite": questionSinus([1, 6]),
                "temps": 25,
            },
            {
                "gauche": questionSinus([-1, 3]),
                "droite": questionCosinus([-1, 6]),
                "temps": 90,
            },
            {
                "gauche": questionCosinus([-1, 3]),
                "droite": questionSinus([-1, 6]),
                "temps": 90,
            },
            {
                "gauche": questionSinus([5, 6]),
                "droite": questionCosinus([4, 3]),
                "temps": 90,
            },
            {
                "gauche": questionCosinus([5, 6]),
                "droite": questionSinus([4, 3]),
                "temps": 90,
            },
            {
                "gauche": questionSinus([5, 4]),
                "droite": questionCosinus([3, 4]),
                "temps": 90,
            },
            {
                "gauche": questionCosinus([5, 4]),
                "droite": questionSinus([3, 4]),
                "temps": 90,
            },
        ],
    }
    compileDocument("an03Test3", generer_interro_flash_corrigee_tex(test_rapide3))
