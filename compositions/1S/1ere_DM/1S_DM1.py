#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from random import choice

import os.path
import os
import sys

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)

from generateurTex import generer_dm, generer_correction_dm
from compilation.compilateur import compileDocument
from donnees.d2017_2018.donnees import listings


from secondDegre import *


if __name__ == "__main__":
    blanc1 = "\n".join([ur"\ \\" for i in xrange(30)])
    blanc2 = "\n".join([ur"\ \\" for i in xrange(10)])
    exercice_de_recherche = {
        "enonce": ur"""
Un entrepreneur loue une benne pour évacuer de la terre d'un chantier. La partie à remplir a la forme schématisée ci-dessous : sa base est le rectangle $ABCD$ de 1,1 m sur 2 m, sa hauteur $AF$ est de 1,8 m et sa coupe selon un plan parallèle au plan $(ABF)$ est un trapèze rectangle identique à $ABGF$.\newline
\includegraphics{benne0}
\includegraphics{benne}
Sachant que l'entrepreneur prévoit d'évacuer $6 m^3$ de terre, déterminez la hauteur jusqu'à laquelle la benne sera remplie. (On donnera la valeur exacte du résultat puis sa valeur approchée à $10^{-2}$ près)
""",
        "correction": blanc1,
    }
    ### création de la liste des élèves
    # devoir = {'classe' : "1ere S2",
    #'num' : 1,
    #'date' : 'lundi 18 septembre 2018',
    #'eleves' : listings["1ere S2"]
    # }
    ### distribution des exercices
    # for el in devoir['eleves']:
    # el.update({'exercices':[
    # exercice_de_recherche,
    # exercice_premieres_equations(),
    ##exercice_signes_et_inequations(), pour une autre fois
    # exercice_canonisation(),
    # exercice_equations_discriminant()
    # ]})
    ##for el in devoir['eleves']:
    ##for ex in el['exercices']:
    ##ex.update({'correction':ex['correction']+blanc2})

    # compileDocument('1ere S2_DM1',generer_dm(devoir))
    # compileDocument('1ere S2_DM1_corr',generer_correction_dm(devoir))

    devoir = {"classe": "1ere S3", "num": 1, "date": "", "eleves": listings["1ere S3"]}
    ## distribution des exercices
    for el in devoir["eleves"]:
        el.update(
            {
                "exercices": [
                    exercice_de_recherche,
                    exercice_premieres_equations(),
                    # exercice_signes_et_inequations(), pour une autre fois
                    exercice_canonisation(),
                    exercice_equations_discriminant(),
                ]
            }
        )
    # for el in devoir['eleves']:
    # for ex in el['exercices']:
    # ex.update({'correction':ex['correction']+blanc2})

    compileDocument("1ere S3_DM1", generer_dm(devoir))
    compileDocument("1ere S3_DM1_corr", generer_correction_dm(devoir))
