#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from sympy import sympify, symbols, Poly, latex, Add, Pow, Mul, sqrt, Rational, Eq


from random import choice

import os.path
import os

import sys

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)

from generateurTex import generer_corrige_exos_livre
from compilation.compilateur import compileDocument


from secondDegre import *


if __name__ == "__main__":
    correction_sup = [
        {
            "ref": "1",
            "questions": [
                {"correction": corrige_canonisation(3 * R ** 2 - 7 - 20 * R)},
                {"correction": corrige_canonisation(5 * t ** 2 + 4 * t + 5)},
                {"correction": corrige_canonisation(3 * x ** 2 + 24 * x + 48)},
            ],
        }
    ]
    compileDocument("correction_alt", generer_corrige_exos_livre(correction_sup))
    corrige_DS_1 = [
        {
            "ref": "1",
            "questions": [
                {"correction": corrige_equ_poly_snd_deg(Eq(3 * R ** 2, 7 + 20 * R))},
                {"correction": corrige_equ_poly_snd_deg(Eq(4 * t, -5 * t ** 2 - 5))},
                {"correction": corrige_equ_poly_snd_deg(Eq(3 * x ** 2 + 24 * x + 48))},
            ],
        },
        {"ref": "2 Partie A", "questions": [{"correction": ""}]},
        {
            "ref": "2 Partie B",
            "questions": [
                {"correction": corrige_canonisation(3 * x ** 2 - 6 * x + 7)},
                {"correction": corrige_factorisation_fc(3 * (x - 1) ** 2 + 4)},
            ],
        },
        {
            "ref": "2 Partie C",
            "questions": [
                {"correction": corrige_canonisation(2 * x ** 2 + 2 * x - 1)},
                {"correction": corrige_factorisation_fc(2 * (x + 0.5) ** 2 - 1.5)},
            ],
        },
    ]
    # compileDocument('1S2_DS1', generer_corrige_exos_livre(corrige_DS_1))
    exercices_marie = [
        {"ref": "1", "questions": [{}]},
        {
            "ref": "3",
            "questions": [
                {
                    "correction": corrige_factorisation_racine(
                        x ** 3 - 4 * x ** 2 + 5 * x - 2, 2
                    )
                }
            ],
        },
        {
            "ref": "4",
            "questions": [
                {"nom": "f", "expr": x ** 2 - 4 * x - 7},
                {"nom": "g", "expr": 2 * x ** 2 + 4 * x},
                {"nom": "h", "expr": 2 * x ** 2 - 6 * x - 4},
                {"nom": "j", "expr": -x ** 2 - 5 * x + 1},
            ],
        },
        {
            "ref": "7",
            "questions": [
                {"equation": Eq(x ** 2 - 4 * x, 1)},
                {"equation": Eq(-2 * x + 3 * x ** 2 - 1, 0)},
                {"equation": Eq(x ** 2, 4 * x)},
                {"equation": Eq(2 * x ** 2 - 3 * x + 4, 0)},
                {"equation": Eq(2 * x ** 2 - 4 * sqrt(3) * x + 6, 0)},
                {"equation": Eq(x ** 2 + 4 * x + 4, 0)},
            ],
        },
        {
            "ref": "exemples cours",
            "questions": [
                {"equation": Eq(-2 * x ** 2 + 3 * x + 9, 0)},
                {"equation": Eq(4 * x ** 2 + 12 * x + 11, 0)},
                {"equation": Eq(-9 * x ** 2 - 18 * x - 9, 0)},
                {"equation": Eq((3 * (x - 2) ** 2).expand(), 0)},
            ],
        },
        {
            "ref": "13 p 25",
            "questions": [
                {"nom": "f", "expr": x ** 2 - 10 * x + 29},
                {"nom": "g", "expr": -x ** 2 + 4 * x + 3},
                {"nom": "g", "expr": x ** 2 + 2 * x},
                {"nom": "h", "expr": 2 * x ** 2 - 10 * x + 15},
                {"nom": "j", "expr": -2 * x ** 2 + 8 * x - 64},
                {"nom": "l", "expr": 9 * x ** 2 + 12 * x - 1},
            ],
        },
    ]

    ## correction des exercices.
    ## ! La liste d'exercices devra être mise à jour.
    for question in exercices_marie[2]["questions"]:
        question.update({"correction": corrige_canonisation(question["expr"])})
    for question in exercices_marie[3]["questions"]:
        question.update({"correction": corrige_equ_poly_snd_deg(question["equation"])})
    for question in exercices_marie[4]["questions"]:
        question.update({"correction": corrige_equ_poly_snd_deg(question["equation"])})
    for question in exercices_marie[5]["questions"]:
        question.update({"correction": corrige_canonisation(question["expr"])})
    ## construction du document
    # compileDocument('1ere_s1',generer_corrige_exos_livre(exercices_marie))''
    pas_a_pas = ""
    for question in exercices_marie[5]["questions"]:
        pas_a_pas += r"\item" + question["correction"]
    pas_a_pas = pas_a_pas.replace(r"\\" "\n=", r"\\$" "\n$=")
    print pas_a_pas
    formes_canoniques = [
        -5 / 6 * (x + 5) ** 2 + 30,
        -5 * (x + 9) ** 2,
        -11 * (x - 1) ** 2 - 121,
        sqrt(2) * (x - 5) ** 2 - sqrt(2),
        -1 * (x - 2) ** 2 - 3,
        6 * x ** 2 - 96,
    ]
    pas_a_pas_2 = r"\item".join([str(fc.factor()) for fc in formes_canoniques])
    print pas_a_pas_2
