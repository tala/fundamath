#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from sympy import sympify, symbols, Poly, latex, Add, Pow, Mul, sqrt, Rational, Eq


from random import choice

import os.path
import os

import sys

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)

from generateurTex import generer_corrige_exos_livre
from compilation.compilateur import compileDocument

from secondDegre import *
from equation_droites import *

if __name__ == "__main__":
    corrige_DS_1 = [
        {
            "ref": "1",
            "questions": [
                {"correction": corrige_equ_poly_snd_deg(Eq(R ** 2 - R - 12, 0))},
                {"correction": corrige_equ_poly_snd_deg(Eq(4 * t, -5 * t ** 2 - 5))},
                {"correction": corrige_equ_poly_snd_deg(Eq(3 * x ** 2 + 24 * x + 48))},
            ],
        },
        {
            "ref": "2 ",
            "questions": [
                {
                    "correction": corrige_equation_cartesienne_version_longue(
                        ["A", -2, 3], ["B", 3, 5]
                    )
                },
                {
                    "correction": corrige_equation_cartesienne_version_courte_point_vecteur(
                        ["C", -1, 4], ["v", 2, 3]
                    )
                },
            ],
        },
        {
            "ref": "3",
            "questions": [
                {"correction": corrige_canonisation(x ** 2 - 4 * x + 5)},
                {"correction": corrige_canonisation(2 * x ** 2 + 6 * x - 3)},
            ],
        },
        {"ref": "4", "questions": []},
        {
            "ref": "5",
            "questions": [
                {"correction": corrige_equ_poly_snd_deg(Eq(-x ** 2 + 4 * x + 60, 50))},
                {
                    "correction": corrige_canonisation(
                        Rational(-1, 2) * x ** 2 + 2 * x + 30
                    )
                },
            ],
        },
    ]
    compileDocument("1S2_DS1", generer_corrige_exos_livre(corrige_DS_1))
