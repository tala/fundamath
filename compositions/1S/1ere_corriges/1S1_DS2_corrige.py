#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function

import os
import os.path
import sys

from sympy import Rational as R
from sympy import symbols as S

dossier_actuel = os.path.realpath(os.curdir)
while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel
sys.path.append(DOSSIER_FUNDAMATH)

import secondDegre as snd  # isort:skip
import in_equations as inq  # isort:skip


x = S("x")

for triple in [[-2, -2, 12], [3, 42, 147], [R(1, 2), R(-1, 1), R(5, 2)]]:
    print(snd.signe_poly_snd_deg(triple[0] * x ** 2 + triple[1] * x + triple[2]))
print("ex02 :")
for inequation in [
    9 * x ** 2 - 6 * x + 5 >= 3,
    2 * x ** 2 - 5 * x - 3 > 0,
    -9 * x ** 2 >= 1 - 6 * x,
]:
    print(snd.corrige_ineq_poly_snd_deg(inequation))
print("ex03 :")
print(snd.elts_correction_ineq_quotient_poly((3 * x ** 2 - 14 * x + 8) / (x - 1) <= 0))
