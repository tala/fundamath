#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals, division, print_function

import os.path
import os

import sys

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)

from sympy import symbols
from sympy_to_latex import reformat_latex as latex

from compilation.compilateur import compileDocument
from generateurTex import generer_brouillon_diapo
from derivation import corrige_derivee
from equation_droites import corrige_ordonnee_origine

x = symbols("x")

f = x**2-3*x+7
x_0 = 3


def determiner_coeff_dir_tangente(fct, val_x):
    lignes =[]
    lignes.append(corrige_derivee(fct)['corrige']+r"\\")
    deriv = fct.diff(x)
    a = deriv.subs(x, val_x)
    val_y = fct.subs(x, val_x)
    lignes.append(r"Le coefficient directeur de la tangente, au point d'abscisse ${0}$, est donc : $f'\left({0}\right) = {1}$\\".format(latex(val_x), latex(a)))
    lignes.append(r"La tangente cherchée et la courbe représentative de $f$ se coupent au point $P\left({0}, f\left({0}\right)\right)$\\".format(latex(val_x)))
    lignes.append(r"Or, $f\left({0}\right) = {1}$.\\".format(latex(val_x), latex(val_y)))
    lignes.append(corrige_ordonnee_origine(a, ["P", val_x, val_y]))
    return "\n".join(lignes)


donnees = [
    {
        "titre": "Recherche de tangente",
        "corps": determiner_coeff_dir_tangente(f,x_0),
    }
    ]

compileDocument("brouillon1", generer_brouillon_diapo(brouillon=donnees))
