#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from sympy import sympify, symbols, Poly, latex, Add, Pow, Mul, sqrt, Rational, Eq

import os.path
import os

import sys

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)

from generateurTex import generer_corrige_exos_livre
from compilation.compilateur import compileDocument


from equation_droites import *

if __name__ == "__main__":
    print "ex 32 p 192"
    A = ["A", 1, 1]
    B = ["B", 2, 2]
    print latex(equation_cartesienne_droite(A, B))
    A = ["A", 3, 0]
    B = ["B", 1, -2]
    print latex(equation_cartesienne_droite(A, B))
    A = ["A", -1, 1]
    B = ["B", -2, 1]
    print latex(equation_cartesienne_droite(A, B))
    A = ["A", 2, 1]
    B = ["B", -1, 4]
    print latex(equation_cartesienne_droite(A, B))
    print "ex 33 p 192"
    A = ["A", Rational(1, 2), 1]
    B = ["B", Rational(1, 4), 0]
    print latex(equation_cartesienne_droite(A, B))
    A = ["A", Rational(1, 4), Rational(1, 4)]
    B = ["B", Rational(3, 4), -1]
    print latex(equation_cartesienne_droite(A, B))
    A = ["A", -Rational(3, 4), -Rational(1, 4)]
    B = ["B", Rational(1, 3), -Rational(4, 3)]
    print latex(equation_cartesienne_droite(A, B))
    A = ["A", Rational(2, 3), Rational(4, 3)]
    B = ["B", -1, 3]
    print latex(equation_cartesienne_droite(A, B))

    print ""
    A = ["A", 1, 3]
    B = ["B", 5, 4]
    print latex(equation_cartesienne_droite(A, B))
    import sys

    sys.exit(main(sys.argv))
