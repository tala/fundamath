#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from sympy import sympify, symbols, Poly, latex, Add, Pow, Mul, sqrt, Rational, Eq


from random import choice

import os.path
import os

import sys

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)

from generateurTex import generer_corrige_exos_livre
from compilation.compilateur import compileDocument

from geometrieAnalytique import *

if __name__ == "__main__":

    exercices_livre = [
        {
            "ref": "8 p 217",
            "questions": [
                {"points": [["A", 1, 2], ["B", 3, 3]]},
                {"points": [["A", 2, 4], ["B", 4, 5]]},
                {"points": [["A", 4, 0], ["B", 5, 2]]},
                {"points": [["A", -1, 2], ["B", 3, 4]]},
            ],
        },
        {
            "ref": "9 p 217",
            "questions": [
                {"points": [["A", 1, 2], ["B", -5, 3]]},
                {"points": [["A", 6, -1], ["B", -2, 0]]},
                {"points": [["A", 3, 2], ["B", -4, -1]]},
                {"points": [["A", -2, -7], ["B", 3, -3]]},
            ],
        },
        {
            "ref": "10 p 217",
            "questions": [{"points": [["E", 0, 2], ["F", 3, 4], ["G", 1, 2]]}],
        },
        {
            "ref": "11 p 217",
            "questions": [{"points": [["A", 3, 1], ["B", -1, 3], ["K", 3, 6]]}],
        },
        {
            "ref": "12 p 217",
            "questions": [
                {"points": [["A", 5, 9], ["B", 1, -3]]},
                {"points": [["A", -3, 5], ["B", 6, 7]]},
            ],
        },
        {
            "ref": "13 p 217",
            "questions": [
                {"points": [["A", 0, -4], ["B", 9, -1]]},
                {"points": [["A", 11, -12], ["B", 45, 22]]},
            ],
        },
        {
            "ref": "14 p 217",
            "questions": [{"points": [["A", 2, 1], ["C", -2, -2]]}, {}],
        },
    ]
    for exo in exercices_livre[:2]:
        for question in exo["questions"]:
            question.update({"correction": corrige_longueur(*question["points"])})
    question = exercices_livre[2]["questions"][0]
    question.update({"correction": corrige_longueurs(question["points"])})
    question = exercices_livre[3]["questions"][0]
    question.update(
        {
            "correction": corrige_longueur(question["points"][0], question["points"][2])
            + corrige_longueur(question["points"][1], question["points"][2])
        }
    )
    for exo in exercices_livre[4:6]:
        for question, nom_milieu in zip(exo["questions"], ["M", "N"]):
            question.update(
                {
                    "correction": corrige_coordonnees_milieu(
                        *question["points"] + [nom_milieu]
                    )
                }
            )
    question1, question2 = exercices_livre[6]["questions"]
    print exercices_livre
    print question1
    # compileDocument('2nde_corriges_s1',
    # generer_corrige_exos_livre(exercices_livre))
