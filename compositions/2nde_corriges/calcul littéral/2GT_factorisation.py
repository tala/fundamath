#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from sympy import sympify, symbols, Poly, latex, Add, Pow, Mul, sqrt, Rational, Eq


from random import choice

import os.path
import os

import sys

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)

from generateurTex import generer_corrige_exos_livre
from compilation.compilateur import compileDocument

from secondDegre import *

if __name__ == "__main__":

    exercices_livre = [
        {
            "ref": "78 p 76",
            "questions": [
                {"expr": 4 * x ** 2 + 4 * x + 1},
                {"expr": x ** 2 - 6 * x + 9},
                {"expr": 9 * x ** 2 - 4},
            ],
        },
        {
            "ref": "79 p 76",
            "questions": [
                {"expr": x * (x - 1) + 2 * x * (x - 3)},
                {"expr": x * (1 - x) + (x - 1) * (x + 2)},
                {"expr": (5 * x + 1) * (-3 * x + 4) + x * (10 * x + 2)},
            ],
        },
    ]
    for exo in exercices_livre:
        for question in exo["questions"]:
            try:
                question.update({"correction": corrige_factorisation(question["expr"])})
            except:
                pass
    print exercices_livre
    compileDocument(
        "2nde_corriges_factorisation", generer_corrige_exos_livre(exercices_livre)
    )
