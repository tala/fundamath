#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from sympy import sympify, symbols, Poly, latex, Add, Pow, Mul, sqrt, Rational, Eq

from random import choice, randint
import os.path
import os
import sys


dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)

from signes import separation, ligne_signe, signe_direct

from generateurTex import generer_corrige_exos_livre
from compilation.compilateur import compileDocument

x = symbols("x")

liste_expr = [
    choice([-1, 1]) * randint(1, 7) * x + choice([-1, 0, 1]) * randint(1, 7)
    for i in range(10)
]
liste_rac = [expr_1.as_poly().real_roots() for expr_1 in liste_expr]

liste_tableau = [signe_direct(expr_1) for expr_1 in liste_expr]

print separation(Add(1, 3), 2)

print separation(Pow(x, -1), 0)
print signe_direct((x + 2) / (3 * x - 1), valeurs=[Rational(1, 3), -2])
print liste_expr
print liste_rac
print liste_tableau[3]
