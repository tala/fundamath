#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

import os
import os.path
import sys
from random import choice, randint

from sympy import Add, Eq, Mul, Poly, Pow, Rational, latex, sqrt, symbols, sympify

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)

from geometrie_analytique import (  # isort:skip
    corrige_coordonnees_milieu,
    corrige_longueur,
    corrige_nature_triangle,
    corrige_symetrique,
)

A = ["A", -7, -1]
B = ["B", -3, -5]
C = ["C", 5, 3]
omega = [r"\Omega ", -1, 1]
D = ["D", -6, 5]
print(corrige_longueur(A, C))
print(corrige_nature_triangle(A, B, C))
print(corrige_coordonnees_milieu(A, C))
print(corrige_longueur(omega, D))
print(corrige_symetrique(B, omega, "E"))
