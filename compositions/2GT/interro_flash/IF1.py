#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from sympy import sympify, symbols, Poly, latex, Add, Pow, Mul, sqrt, Rational, Eq


from random import choice

import os.path
import os
import sys

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel
sys.path.append(DOSSIER_FUNDAMATH)

from generateurTex import generer_interro_flash_corrigee_tex_2
from compilation.compilateur import compileDocument

from secondDegre import question_variations_fc
from equation_droites import (
    question_point_droite,
    question_lecture_graphique_coeff_dir,
    question_calcul_coeff_dir,
)

x, y, z, t = symbols("x,y,z,t")

if __name__ == "__main__":
    interro = {
        "classe": r"2\up{nde} 9",
        "date": u"Mercredi 4 avril 2018",
        "titre": "interro rapide",
        "num": 1,
        "questions": [
            {
                "gauche": question_point_droite(["A", 1, 3], ["d_1", Eq(y, 4 * x - 1)]),
                "droite": question_point_droite(["A", 4, 3], ["d_1", Eq(y, 2 * x - 1)]),
                "temps": 30,
            },
            {
                "gauche": question_point_droite(
                    ["B", -2, 3], ["d_2", Eq(y, 4 * x + 5)]
                ),
                "droite": question_point_droite(
                    ["A", 2, -1], ["d_2", Eq(y, -2 * x + 3)]
                ),
                "temps": 30,
            },
            {
                "gauche": question_lecture_graphique_coeff_dir(
                    ["d_1", Eq(y, 2 * x + 1)]
                ),
                "droite": question_lecture_graphique_coeff_dir(["d_1", Eq(x, 2)]),
                "temps": 30,
            },
            {
                "gauche": question_variations_fc("p_1", 3 * (x + 7) ** 2 + 7),
                "droite": question_calcul_coeff_dir(["A", 2, 5], ["B", 4, -3]),
                "temps": 120,
            },
            {
                "gauche": question_calcul_coeff_dir(["A", -3, 5], ["B", -3, 4]),
                "droite": question_variations_fc("p_1", -2 * (x + 3) ** 2 + 5),
                "temps": 120,
            },
        ],
    }
    compileDocument("2nde9_IF1", generer_interro_flash_corrigee_tex_2(interro))
