#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from sympy import symbols, Eq, And


from random import choice

import os, os.path, sys

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel
sys.path.append(DOSSIER_FUNDAMATH)

from generateurTex import generer_interro_flash_corrigee_tex
from compilation.compilateur import compileDocument

from in_equations import (
    question_equation,
    question_ineq_to_interval,
    question_dbl_ineq_to_interval,
)

x, y, z, t = symbols("x,y,z,t")

if __name__ == "__main__":
    # interro = {'classe':r"2\up{nde} 8",'date':u'Lundi 24 septembre 2018',
    #'titre':'interro rapide', 'num':1,
    #'questions':[
    # {
    #'gauche': question_equation(Eq(3*x-5,4)),
    #'droite': question_equation(Eq(2*x-3,5)),
    #'temps' : 60
    # },
    # {
    #'gauche': question_equation(Eq(2*y-5,4*y+3)),
    #'droite': question_equation(Eq(3*y+2,5*y+4)),
    #'temps' : 60
    # },
    # {
    #'gauche': question_dbl_ineq_to_interval(And(t<=2,t>-1), order = 'ascending'),
    #'droite': question_dbl_ineq_to_interval(And(z<5,z>=-1), order = 'descending'),
    #'temps': 45
    # },
    # {
    #'gauche': question_dbl_ineq_to_interval(And(t<12,t>1.5), order = 'descending' ),
    #'droite': question_dbl_ineq_to_interval(And(z<=3.5,z>=-10), order = 'ascending'),
    #'temps': 45
    # },
    # {
    #'gauche': question_ineq_to_interval(z<=7),
    #'droite': question_ineq_to_interval(t>-2),
    #'temps': 45
    # },
    # ]}

    interro = {
        "classe": r"2\up{nde} 8",
        "date": u"Jeudi 26 septembre 2018",
        "titre": "interro rapide",
        "num": 2,
        "questions": [
            {
                "gauche": question_equation(Eq(3 * y + 2, 5 * y + 4)),
                "droite": question_equation(Eq(2 * y - 5, 4 * y + 3)),
                "temps": 60,
            },
            {
                "gauche": question_equation(2 * x + 2 < 3 * x - 3),
                "droite": question_equation(2 * x - 3 >= 3 * x - 5),
                "temps": 60,
            },
            {
                "gauche": question_ineq_to_interval(z >= -3),
                "droite": question_ineq_to_interval(t < 2),
                "temps": 45,
            },
            {
                "gauche": question_dbl_ineq_to_interval(
                    And(z <= 2.5, z >= 0), order="descending"
                ),
                "droite": question_dbl_ineq_to_interval(
                    And(t < 7, t > 3.5), order="ascending"
                ),
                "temps": 45,
            },
            {
                "gauche": question_dbl_ineq_to_interval(
                    And(z < 2.5, z >= -1), order="ascending"
                ),
                "droite": question_dbl_ineq_to_interval(
                    And(t <= 0, t > -1), order="descending"
                ),
                "temps": 45,
            },
        ],
    }
    print interro
    compileDocument("2nde8_IF2", generer_interro_flash_corrigee_tex(interro))
