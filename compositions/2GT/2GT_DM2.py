#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from sympy import sympify, symbols, Poly, latex, Add, Pow, Mul, sqrt, Rational, Eq


from random import choice

import os.path
import os

import sys

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)

from donnees.donnees import GT2
from generateurTex import generer_dm
from compilation.compilateur import compileDocument

from secondDegre import equations_college

ex1 = (
    "Déterminer les antécédents éventuels de 0 par les fonctions définies ci-dessous :"
)

eleves = [
    {"nom": eleve["nom"], "prenom": eleve["prenom"], "exercices": []} for eleve in GT2
]
print eleves

x = symbols("x")
t = symbols("t")
R = symbols("R")
z = symbols("z")


if __name__ == "__main__":

    devoir = {
        "classe": "2 GT",
        "num": 2,
        "date": "lundi 28 novembre 2016",
        "eleves": eleves,
    }
    compileDocument("2GT_DM_2", generer_dm(devoir))
