#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import os
import os.path
import sys

# from sympy import sympify, symbols, latex, Add, Pow, Mul, sqrt, Rational, Eq


dossier_actuel = os.path.realpath(os.curdir)
while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel
sys.path.append(DOSSIER_FUNDAMATH)

from compilation.compilateur import compileDocument  # isort:skip
from generateurTex import generer_revision_corrigee_tex  # isort:skip
from geometrie_analytique import (  # isort:skip
    question_coordonnees_milieu,
    question_longueur,
    question_symetrique,
    corrige_coordonnees_milieu,
    corrige_longueur,
    corrige_symetrique,
    milieu,
    corrige_nature_triangle,
    corrige_centre_cercle_circonscrit
)

# x = symbols("x")
def question_1(*args, **kwargs):
    return {
        "question": "",
        "enonce": question_coordonnees_milieu(*args, **kwargs),
        "corrige": corrige_coordonnees_milieu(*args, **kwargs),
    }


def question_2(*args, **kwargs):
    return {
        "question": "",
        "enonce": question_longueur(*args, **kwargs),
        "corrige": corrige_longueur(*args, **kwargs),
    }


def question_3(*args, **kwargs):
    return {
        "question": "",
        "enonce": question_symetrique(*args, **kwargs),
        "corrige": corrige_symetrique(*args, **kwargs),
    }



def question_4(*args, **kwargs):
    return {
        "question": "",
        "enonce": "Montrer que le triangle $ABC$ est rectangle isocèle en B.",
        "corrige": corrige_nature_triangle(*args, **kwargs),
    }
    
    
def question_5(*args, **kwargs):
    return {
        "question": "",
        "enonce": "Montrer que le point $M$ est le centre du cercle circonscrit au triangle $ABC$.",
        "corrige": corrige_centre_cercle_circonscrit(*args, **kwargs),
    }
    
    
def question_6(*args, **kwargs):
    return {
        "question": "",
        "enonce": "Montrer que le point $M$ est le centre du cercle circonscrit au triangle $ABC$.(plus court)",
        "corrige": corrige_coordonnees_milieu(*args, **kwargs),
    }
    
    
if __name__ == "__main__":
    # A = ["A", -2, 3]
    # B = ["B", 3, 4]
    # C = ["C", 5, 2]
    # M = ["M", 1.5, 2.5]
    # revision = {
        # "classe": "2\\up{nde} 8",
        # "num": 1,
        # "date": "vendredi 7 décembre 2018",
        # "questions": [
            # question_1(A, C, "M"),
            # question_2(A, B),
            # question_3(B, M, "D"),
            # {
                # "question": "",
                # "enonce": "Quelle est la nature du quadrilatère ABCD ?",
                # "corrige": r"D'après les questions précédentes, le quadrilatère $ABCD$ a ses diagonales qui se coupent en leur milieu $M$.\\ C'est donc un parallélogramme.",
            # },
        # ],
    # }
    # compileDocument("2nde_8_rev_1", generer_revision_corrigee_tex(revision))
    
    A = ["A", 1, 2]
    B = ["B", -3, 3]
    C = ["C", -2, 7]
    # M = ["M", -0.5, 4.5]
    M = milieu(A, C)
    print(M)
    revision = {
        "classe": "2\\up{nde} 8",
        "num": 1,
        "date": "Lundi 17 décembre 2018",
        "questions": [
            question_4(A, B, C),
            question_5(M, A, B, C),
            question_6(A, C, 'N')
        ],
    }
    compileDocument("2nde_8_rev_1", generer_revision_corrigee_tex(revision))
