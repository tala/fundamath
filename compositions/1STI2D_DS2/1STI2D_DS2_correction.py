#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from random import choice

from sympy import Add, Eq, Mul, Poly, Pow, Rational, latex, sqrt, symbols, sympify

import __init__
from compilation.compilateur import compileDocument
from generateurTex import generer_corrige_ds
from secondDegre import corrige_equ_poly_snd_deg, corrige_ineq_poly_snd_deg
from trigo import (
    corrige_anglesSontIlsEgaux,
    corrige_ApplicationPythagore,
    corrige_equationTrigo,
    corrige_mesurePrincipale,
    corrige_valeursTrigo,
)

x = symbols("x")


if __name__ == "__main__":

    devoir = {
        "classe": "1 STI-2D",
        "num": 1,
        "date": "lundi 14 novembre 2016",
        "sujets": [
            {
                "exercices": [
                    [
                        corrige_anglesSontIlsEgaux([13, 6], [31, 6]),
                        corrige_anglesSontIlsEgaux([16, 3], [-8, 3]),
                    ],
                    [
                        corrige_mesurePrincipale([38, 3]),
                        corrige_mesurePrincipale([15, 4]),
                    ],
                    [
                        corrige_valeursTrigo([2, 3]),
                        corrige_valeursTrigo([5, 4]),
                        corrige_valeursTrigo([-1, 6]),
                    ],
                    [
                        corrige_ApplicationPythagore(
                            fonction=r"\cos",
                            valeur=Rational(1, 7),
                            borneInf=[0, 1],
                            borneSup=[1, 1],
                        ),
                        corrige_ApplicationPythagore(
                            "b", valeur=0.7, borneInf=[1, 2], borneSup=[1, 1]
                        ),
                    ],
                    [
                        corrige_equationTrigo(-2, "sin", "t"),
                        corrige_equationTrigo(r"\dfrac{1}{2}", "cos", "a"),
                        corrige_equationTrigo(r"\dfrac{\sqrt{2}}{2}", "sin", "x"),
                    ],
                    [corrige_ineq_poly_snd_deg(-3 * x ** 2 + 20 * x - 25 >= 0)],
                ]
            },
            {
                "exercices": [
                    [
                        corrige_anglesSontIlsEgaux([13, 3], [11, 3]),
                        corrige_anglesSontIlsEgaux([16, 3], [31, 3]),
                    ],
                    [
                        corrige_mesurePrincipale([37, 4]),
                        corrige_mesurePrincipale([26, 3]),
                    ],
                    [
                        corrige_valeursTrigo([-1, 3]),
                        corrige_valeursTrigo([3, 4]),
                        corrige_valeursTrigo([7, 6]),
                    ],
                    [
                        corrige_ApplicationPythagore(
                            valeur=Rational(1, 3), borneInf=[0, 1], borneSup=[1, 2]
                        ),
                        corrige_ApplicationPythagore(
                            "b", r"\cos", valeur=0.8, borneInf=[-1, 2], borneSup=[0, 1]
                        ),
                    ],
                    [
                        corrige_equationTrigo(r"\dfrac{\sqrt{3}}{2}", "cos", "x"),
                        corrige_equationTrigo(3, "cos", "t"),
                        corrige_equationTrigo(r"\dfrac{\sqrt{2}}{2}", "sin", "a"),
                    ],
                    [corrige_ineq_poly_snd_deg(-3 * x ** 2 + 7 * x + 20 < 0)],
                ]
            },
        ],
    }
    compileDocument("1STI2D_corrige_DS1", generer_corrige_ds(devoir))
