#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

import os
import os.path
import sys
from itertools import chain, product
from random import choice, randint, sample, shuffle

from sympy import Add, Eq, Mul, Poly, Pow, Rational, latex, sqrt, symbols

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)

from compilation.compilateur import compileDocument  # isort:skip
from generateurTex import generer_corrige_ds  # isort:skip
from second_degre import (  # isort:skip
    corrige_equ_poly_snd_deg,  # isort:skip
    corrige_ineq_poly_snd_deg,  # isort:skip
)

x = symbols("x")
if __name__ == "__main__":
    equations_ex_1 = [
        Eq(2 * x ** 2 - 50 * x, 0),
        Eq(2 * x ** 2 - x, 10),
        Eq(x ** 2 - 8 * x + 16, 0),
    ]
    print(br"\underline{\textbf{ Exercice 2 :}}\\")
    print(r"\begin{enumerate}")
    for corr in map(corrige_equ_poly_snd_deg, equations_ex_1):
        print("\item", corr)
    print(r"\end{enumerate}")
    print(br"\underline{\textbf{ Exercice 3 :}}\\")
    print(r"\begin{enumerate}")
    print(r"\item", corrige_equ_poly_snd_deg(Eq(-x ** 2 + 10000 * x - 4750000, 0)))
    print(r"\end{enumerate}")
    expr = -x ** 2 + 10000 * x - 4750000
    print(expr.subs(x, 1000))
    print(expr.subs(x, 5000))
    print(expr.subs(x, 10000))
    inequation = (x - 3) * (-2 - x) > 0
    print(((x - 3) * (-2 - x)).expand())
    print(corrige_ineq_poly_snd_deg(inequation))
    # print corrige_equ_poly_snd_deg(Eq(-2*x**2+3*x+9,0))
    # print corrige_equ_poly_snd_deg(Eq(4*x**2+12*x+11,0))
    # print corrige_equ_poly_snd_deg(Eq(3*x**2-12*x+12,0))
    # print ""
    # exo3 = (x**2+2*x-35, 2*x**2+8*x+6, 5*x**2-3*x+2, 9*x**2-24*x+16,
    # -2*x**2+3*x-5, 3*x**2+12*x+9)

    # print '\n\n'.join([corrige_equ_poly_snd_deg(Eq(p,0)) for p in exo3])
    # print ""
    # print corrige_equ_poly_snd_deg(Eq(x**2-10*x+24,8))

    # ex_25_p49 = [Eq(x**2+x-6,0), Eq(4*x**2-12*x+9,0),
    # Eq(-2*x**2+3*x-5,0), Eq(3*x**2+2*x,40),
    # Eq(-x**2+3*x,2), Eq(-2*x**2+8*x-8)]
    # for equation in ex_25_p49 :
    # string = ur"\begin{frame}""\n"
    # string += corrige_equ_poly_snd_deg(equation)+r'\\''\n'
    # string += ur"\end{frame}"'\n'
    # print string
    # p = -x**2+60*x-500
    # print p.subs(x,22)
    # print p.subs(x,43)
    # print corrige_equ_poly_snd_deg(Eq(p,375))
    # print ""
    # print corrige_equ_poly_snd_deg(Eq(p,0))

    # couples = [
    # (a, b) for a in range(-6, -1) + range(1, 6) for b in range(-6, -1) + range(1, 6)
    # ]
    # polys1 = [(a * (x - 1 / a) * (x - b)).expand() for a, b in couples]
    # polys2 = [((x - a) * (x - b)).expand() for a, b in couples]
    # polys3 = [(a * (x + b) ** 2).expand() for a, b in couples]
    # polys4 = [(a * ((x + b) ** 2 + 1)).expand() for a, b in couples]
    # polys_puni = (
    # sample(polys1, 2) + sample(polys2, 2) + sample(polys3, 2) + sample(polys4, 2)
    # )
    # shuffle(polys_puni)
    # polys_exemples = [choice(polys1), choice(polys3), choice(polys4)]
    # # if and(map(lambda p: p in polys_puni))
    # while any(map(lambda p: p in polys_puni, polys_exemples)):
    # polys_exemples = [choice(polys1), choice(polys3), choice(polys4)]
    # print "exemples :"
    # for poly in polys_exemples:
    # print "\n\\item " + corrige_equ_poly_snd_deg(Eq(poly, 0))
    # print "sujet :"
    # print "\item " + (
    # "\n\\item".join(map(lambda eq: latex(eq, mode="inline"), polys_puni))
    # )

    # print "correction :"
    # for poly in polys_puni:
    # print "\n\\item " + corrige_equ_poly_snd_deg(Eq(poly, 0))
