#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

import os
import os.path
import sys

from sympy import symbols

dossier_actuel = os.path.realpath(os.curdir)
while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel
sys.path.append(DOSSIER_FUNDAMATH)


from compilation.compilateur import compileDocument  # isort:skip
from generateurTex import generer_brouillon_diapo  # isort:skip
from sympy_to_latex import reformat_latex as latex  # isort:skip


x = symbols("x")
