#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals, division, print_function

import os.path
import os

import sys

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
print dossier_actuel
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)

from sympy import symbols
from sympy_to_latex import reformat_latex as latex

from compilation.compilateur import compileDocument
from generateurTex import generer_brouillon_diapo


x = symbols("x")
