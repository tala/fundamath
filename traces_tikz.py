#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function, unicode_literals

from random import choice, randint

from sympy import (
    Add,
    Eq,
    Integer,
    Mul,
    Number,
    Pow,
    Rational,
    Reals,
    Symbol,
    solve,
    solveset,
    symbols,
)

x = symbols("x")


def arrondir_si_besoin(val, precision=4):
    if not isinstance(val, int) and not isinstance(val, Integer):
        return round(val, precision)
    else:
        return val


def formater_fct_pgfplots(fct, precision=4):
    return str(fct).replace("**", "^").replace("x", r"\x")


# def formater_fct_pgfplots(fct, precision=4):
# if isinstance(fct, Integer):
# return str(fct)
# elif isinstance(fct, Mul):
# return " * ".join(map(formater_fct_pgfplots, fct.args))
# elif isinstance(fct, Add):
# return " + ".join(map(formater_fct_pgfplots, fct.args))
# elif isinstance(fct, Number):
# return str(round(fct, precision))
# elif isinstance(fct, Symbol):
# return r"\{}".format(fct)
# elif isinstance(fct, Pow):
# return r"({0})^({1})".format(
# formater_fct_pgfplots(fct.args[0]), formater_fct_pgfplots(fct.args[1])
# )


def placer_point(
    x_point, y_point, etiquette=None, position="below", marque=r"$\times$", precision=4
):
    figure = ""
    val_x = arrondir_si_besoin(x_point, precision)
    val_y = arrondir_si_besoin(y_point, precision)
    if marque:
        figure += r"\draw ({0}, {1}) node {{{2}}};".format(val_x, val_y, marque) + "\n"
    if etiquette is not None:
        figure += (
            r"\draw ({0}, {1}) node [{3}] {{{2}}};".format(
                val_x, val_y, etiquette, position
            )
            + "\n"
        )
    return figure


def tracer_segment(x_0, y_0, x_1, y_1, style=None):
    if style:
        trace = (r"\draw[{4}] ({0}, {1}) -- ({2}, {3});" + "\n").format(
            x_0, y_0, x_1, y_1, style
        )
    else:
        trace = (r"\draw ({0}, {1}) -- ({2}, {3});" + "\n").format(x_0, y_0, x_1, y_1)
    return trace


def reperer_x(val_x, label_x, position="below", precision=4):
    return placer_point(
        val_x, 0, etiquette=label_x, position=position, precision=precision
    )


def reperer_y(val_y, label_y, position="left", precision=4):
    return placer_point(
        0, val_y, etiquette=label_y, position=position, precision=precision
    )


def dessiner_fleche(x_1, y_1, x_2, y_2, style=None, precision=4):
    x_1, y_1, x_2, y_2 = map(
        lambda t: arrondir_si_besoin(t, precision), [x_1, y_1, x_2, y_2]
    )
    if style:
        return (
            r"\draw[{4}, -{{Latex[length=5mm,width=2mm]}}] ({0}, {1}) -- ({2}, {3});".format(
                x_1, y_1, x_2, y_2, style
            )
            + "\n"
        )
    else:
        return (
            r"\draw[ -{{Latex[length=5mm,width=2mm]}}] ({0}, {1}) -- ({2}, {3});".format(
                x_1, y_1, x_2, y_2
            )
            + "\n"
        )


def dessiner_axe_x(xmin, xmax):
    figure = dessiner_fleche(xmin, 0, xmax, 0)
    figure += r"\draw (1,-0.1) -- (1,0.1);" "\n"
    figure += placer_point(1, 0, etiquette="$1$", marque=None)
    # figure += r"\draw (1,0) node[below] {$1$};" "\n"
    return figure


def dessiner_axe_y(ymin, ymax):
    figure = dessiner_fleche(0, ymin, 0, ymax)
    figure += r"\draw (-0.1,1) -- (0.1,1);" "\n"
    figure += placer_point(0, 1, etiquette="$1$", position="left", marque=None)
    return figure


def dessiner_axes(xmin, xmax, ymin, ymax):
    return dessiner_axe_x(xmin=xmin, xmax=xmax) + dessiner_axe_y(ymin=ymin, ymax=ymax)


def tracer_fct(fct, xmin=-5, xmax=5, ymin=-5, ymax=5, precision=4, style=None):
    """
    Primitive de base pour le tracé des courbes représentatives.
    Dépends des paquetages latex tikz et pgfplots.
    """
    if style:
        figure = r"\draw [{3}, smooth,domain={0}:{1}] plot(\x,{{{2}}});".format(
            xmin, xmax, formater_fct_pgfplots(fct, precision), style
        )
    else:
        figure = r"\draw [smooth,domain={0}:{1}] plot(\x,{{{2}}});".format(
            xmin, xmax, formater_fct_pgfplots(fct, precision)
        )
    return figure + "\n"


if __name__ == "__main__":
    # print(dessiner_fleche(1, 2, 3, 4))
    # print(formater_fct_pgfplots(1 / x))
    # print(tracer_fct(x ** 2))
    # print(placer_point(2, 4))
    print(tracer_segment(1, 2, 3, 4))
    print(tracer_segment(1, 2, 3, 4, style="dashed"))
    print(formater_fct_pgfplots(x ** 2 + x ** 3 - x))
    print(formater_fct_pgfplots(((x - 3) * (x + 2)).expand()))
    # print(tracer_fct(x ** 3 - 2 * x))
