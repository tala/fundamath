#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
from sympy.abc import x
from sympy import exp, Rational, cos, sin, ln
from derivation import (
    question_derivee,
    question_primitives,
    corrige_derivee,
    corrige_primitives,
)

from generateurTex import generer_interro_flash_corrigee_tex
from compilation.compilateur import compileDocument

interroFlash = {
    "classe": "TSTI2D",
    "date": "Mardi 2 mars 2016",
    "num": 8,
    "questions": [
        {
            "gauche": question_derivee(exp(3 * x + 7)),
            "droite": question_primitives(exp(x) + 2 * x),
            "temps": 30,
        },
        {
            "gauche": question_primitives(1 + exp(x)),
            "droite": question_derivee(x * exp(x)),
            "temps": 30,
        },
        {
            "gauche": question_derivee(exp(-x) + 2 * x),
            "droite": question_primitives(exp(x) - exp(-x)),
            "temps": 120,
        },
        {
            "gauche": question_primitives(exp(3 * x + 7)),
            "droite": question_derivee(x + 2 * exp(x)),
            "temps": 120,
        },
        {
            "gauche": question_derivee(x / exp(x)),
            "droite": question_primitives(exp(2 * x - 5)),
            "temps": 120,
        },
    ],
}
IF9 = {
    "classe": "TSTI2D",
    "date": "Jeudi 3 mars 2016",
    "num": 9,
    "questions": [
        {
            "gauche": question_primitives(exp(2 * x + 7)),
            "droite": question_derivee(exp(2 * x) + x ** 3),
            "temps": 60,
        },
        {
            "gauche": question_derivee(x * exp(2 * x)),
            "droite": question_primitives(exp(4 * x)),
            "temps": 60,
        },
        {
            "gauche": question_primitives(x * exp(x ** 2)),
            "droite": question_derivee(cos(x) - exp(-2 * x)),
            "temps": 60,
        },
        {
            "gauche": question_derivee(sin(x) - exp(-3 * x + 7)),
            "droite": question_primitives(x * exp(x ** 2)),
            "temps": 60,
        },
        {
            "gauche": question_primitives(exp(2 * x - 5) + x ** 2),
            "droite": question_derivee(x * exp(2 * x - 5)),
            "temps": 60,
        },
    ],
}
IF10 = {
    "classe": "TSTI2D",
    "date": "Mardi 8 mars 2016",
    "num": 10,
    "questions": [
        {
            "gauche": question_primitives(exp(5 * x + 7)),
            "droite": question_derivee(exp(3 * x) + x ** 4),
            "temps": 60,
        },
        {
            "gauche": question_derivee(2 * x * exp(2 * x)),
            "droite": question_primitives(exp(4 * x)),
            "temps": 60,
        },
        {
            "gauche": question_primitives(2 * x * exp(x ** 2)),
            "droite": question_derivee(ln(x) - exp(-2 * x)),
            "temps": 60,
        },
        {
            "gauche": question_derivee(ln(x) - exp(-2 * x - 5)),
            "droite": question_primitives(2 * x * exp(x ** 2)),
            "temps": 60,
        },
        {
            "gauche": question_primitives(exp(3 * x + 3) + x ** 3),
            "droite": question_derivee(2 * x * exp(5 * x)),
            "temps": 60,
        },
    ],
}
if __name__ == "__main__":
    compileDocument("TSTI2D_IF10", generer_interro_flash_corrigee_tex(IF10))
