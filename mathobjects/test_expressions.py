#!/usr/bin/python
# -*- coding: utf8 -*-
from __future__ import absolute_import

import unittest

from .expressions import *


class expressionsTestCase(unittest.TestCase):
    def test_parsing(self):

        formule = "2**3"
        puissance = Pow(Num(2), Num(3))
        self.assertEqual(str_to_math_expr(formule), puissance)

        formule = "2^3"
        puissance = Pow(Num(2), Num(3))
        self.assertEqual(str_to_math_expr(formule), puissance)

        expr1 = Add(Mult(Num(2), Symbol("x")), Symbol("x"))
        expr2 = str_to_math_expr("2*x + x")
        self.assertEqual(expr1, expr2)

        formule = "3^2"
        expr1 = Pow(Num(3), Num(2))
        expr2 = str_to_math_expr(formule)
        self.assertEqual(expr1, expr2)

        formule = "-1"
        expr1 = Num(-1)
        expr2 = str_to_math_expr(formule)
        self.assertEqual(expr1, expr2)

        formule = "(-1)^2"
        expr1 = Pow(Num(-1), Num(2))
        expr2 = str_to_math_expr(formule)
        self.assertEqual(expr1, expr2)

        formule = "5^2"
        expr1 = Pow(Num(5), Num(2))
        expr2 = str_to_math_expr(formule)
        self.assertEqual(expr1, expr2)

        formule = "2*(-1)^2"
        expr1 = Mult(Num(2), Pow(Num(-1), Num(2)))
        expr2 = str_to_math_expr(formule)
        self.assertEqual(expr1, expr2)

        formule = "2*x^2"
        expr1 = Mult(Num(2), Pow(Symbol("x"), Num(2)))
        expr2 = str_to_math_expr(formule)
        self.assertEqual(expr1, expr2)

    def test_equality(self):
        num1 = Num(3)
        num2 = Num(3)
        prod1 = Num(3) * 2
        prod2 = Num(3) * 2
        sum1 = Num(3) + 2
        prod3 = Symbol("x") * 2
        prod4 = 3 * Symbol("x")
        prod5 = 3 * Symbol("x")
        self.assertTrue(num1 == num2)
        self.assertTrue(prod1 == prod2)
        self.assertFalse(prod1 == sum1)
        self.assertFalse(prod3 == prod4)
        self.assertTrue(prod4 == prod5)

    def test_substitution(self):

        expr1 = str_to_math_expr("2*x^2+3*x-5")
        expr2 = str_to_math_expr("2*(-1)^2+3*(-1)-5")
        x = str_to_math_expr("x")
        self.assertEqual(expr1.subs(x, -1), expr2)

        expr1 = str_to_math_expr("x**2").subs(x, -1)
        expr2 = str_to_math_expr("(-1)**2")
        self.assertEqual(expr1, expr2)

        # expr1 = Add(Mul(Num(2),Symbol('x')),
        # Symbol(x))
        # expr1 = str_to_math_expr('2*x + x').subs(x,-1
        # expr2 = str_to_math_expr('2*(-1) + (-1)')
        # self.assertEqual(expr1, expr2)

        expr1 = str_to_math_expr("2*x + x").subs(x, 2)
        expr2 = str_to_math_expr("2*2 + 2")
        self.assertEqual(expr1, expr2)

        expr1 = str_to_math_expr("2*x + x").subs(x, -1)
        expr2 = str_to_math_expr("2*(-1) + (-1)")
        self.assertEqual(expr1, expr2)

        expr1 = str_to_math_expr("x**2 + x").subs(x, -1)
        expr2 = str_to_math_expr("(-1)**2 + (-1)")
        self.assertEqual(expr1, expr2)

        expr1 = str_to_math_expr("x**2 + x - 2").subs(x, -1)
        expr2 = str_to_math_expr("(-1)**2 + (-1) - 2")
        self.assertEqual(expr1, expr2)

        expr1 = str_to_math_expr("5*x**2 + x - 2").subs(x, -1)
        expr2 = str_to_math_expr("5*(-1)**2 + (-1) - 2")
        self.assertEqual(expr1, expr2)

        expr1 = str_to_math_expr("2*x**3 + 5*x**2 + x - 2").subs(x, -1)
        expr2 = str_to_math_expr("2*(-1)**3 + 5*(-1)**2 + (-1) - 2")
        self.assertEqual(expr1, expr2)

    def test_leftExpand(self):
        self.assertEqual(
            (Symbol("x") * (Num(3) + 4)).leftExpand(), Symbol("x") * 3 + Symbol("x") * 4
        )

    # def test_rightExpand(self):
    # self.assertEqual(Mult(Add(3,'x'),'x').rightExpand(),Add(Mult(3,'x'),Mult('x','x')))

    # def test_expand(self):
    # self.assertEqual(Mult(Add(3,'x'),'x').expand(),Add(Mult(3,'x'),Mult('x','x')))
    # self.assertEqual(Mult('x',Add(3,4)).leftExpand(),Add(Mult('x',3),Mult('x',4)))

    # def test_factorise(self):
    # self.assertEqual(Add(Mult('x',3),Mult('x',5)).factorise(),Mult('x',Add(3,5)))
    # self.assertEqual(Sub(Mult('x',3),Mult('x',5)).factorise(),Mult('x',Sub(3,5)))
    # self.assertEqual(Add(Mult('x',3),Mult(5,'x')).factorise(),Mult('x',Add(3,5)))
    # self.assertEqual(Add(Mult(3,'x'),Mult('x',5)).factorise(),Mult('x',Add(3,5)))
    # self.assertEqual(Add(Mult(3,'x'),Mult(5,'x')).factorise(),Mult('x',Add(3,5)))
    # self.assertEqual(Sub (Pow (Add (Mult (2, 'x'), 3), 2), Pow(5,2)).factorise(),Mult(Add(Add(Mult (2, 'x'), 3),5),Sub(Add(Mult (2, 'x'), 3),5)))

    # def test_toPower(self):
    # self.assertEqual(Mult(Pow(Add('x', 3),3),Add('x', 3)).toPower(),Pow(Add('x', 3),Add(3, 1)))
    # self.assertEqual(Mult(Add('x', 3),Pow(Add('x', 3),3)).toPower(),Pow(Add('x', 3),Add(3, 1)))
    # self.assertEqual(Mult(Add('x', 3),Add('x', 3)).toPower(),Pow(Add('x', 3),2))
    # self.assertEqual(Mult(3, 3).toPower(),Pow(3,2))
    # self.assertEqual(Mult(3, 'x').toPower(),Mult(3, 'x'))
    # self.assertEqual(Mult(Pow('x', 3),Pow('x',2)).toPower(),Pow('x',Add(3,2)))

    # def test_simplify(self):
    # self.assertEqual(Add(Frac(3,5),Frac(4,5)).simplify(),Frac(Add(3,4),5))

    # def test_simplify_sqrt(self):
    # self.assertEqual(Sqrt(Pow(3,2)).simplify(),3)
    # self.assertEqual(Sqrt(16).simplify(),Sqrt(Pow(4,2)))

    # def test_doubleDist(self):
    # A=Mult(Add(3,Mult(2,'x')),Sub(Mult(5,'x'),3))
    # B=Add(Sub(Mult(3,Mult(5,'x')),Mult(3,3)),Sub(Mult(Mult(2,'x'),Mult(5,'x')),Mult(Mult(2,'x'),3)))
    # self.assertEqual(A.doubleDist(),B)

    # somme6= Add (Mult (152, 'x'), 58)
    # print(somme6.show())
    # AS1=AlgebraicSum([Pow(3,2),Mult(2,Mult(3,'x')),Pow('x',2)])


if __name__ == "__main__":
    unittest.main()
