#!/usr/bin/env python
# -*- coding: utf-8 -*-
from generateurTex import (
    generer_correction_projetee_tex,
    generer_interro_flash_corrigee_tex,
)
from compilation.compilateur import compileDocument

donneesCorrection = {
    "classe": "1STI-2D",
    "date": "Vendredi 2 septembre",
    "exercices": [
        {
            "ref": "38 p 215",
            "questions": [
                {
                    "enonce": u"énoncé",
                    "corrige": [u"étape 1", u"étape 2"],
                    "conclusion": "conclusion",
                },
                {
                    "enonce": u"énoncé",
                    "corrige": [u"étape 1", u"étape 2"],
                    "conclusion": "conclusion",
                },
            ],
        }
    ],
}
# compileDocument('testCorrection',generer_correction_projetee_tex(donneesCorrection))

donneesInterrogationProjetee = {
    "classe": "1STI-2D",
    "date": "Vendredi 2 septembre",
    "objectifs": [{"titre": "objectif1"}, {"titre": "objectif2"}],
    "questions": [
        {
            "temps": 30,
            "gauche": {
                "enonce": u"énoncé G",
                "consigne": "consigne G",
                "corrige": [u"étape 1", u"étape 2"],
                "conclusion": "conclusion",
            },
            "droite": {
                "enonce": u"énoncé D",
                "consigne": "consigne D",
                "corrige": [u"étape 1", u"étape 2"],
                "conclusion": "conclusion",
            },
        },
        {
            "temps": 30,
            "gauche": {
                "enonce": u"énoncé G",
                "consigne": "consigne G",
                "corrige": [u"étape 1 G", u"étape 2 G"],
                "conclusion": "conclusion G",
            },
            "droite": {
                "enonce": u"énoncé D",
                "consigne": "consigne D",
                "corrige": [u"étape 1 D", u"étape 2 D"],
                "conclusion": "conclusion D",
            },
        },
    ],
}
# compileDocument('testInterroProj',generer_interro_flash_corrigee_tex(donneesInterrogationProjetee))
donneesDMPerso = {
    "classe": "1STI-2D",
    "date": "Vendredi 2 septembre",
    "objectifs": [{"titre": "objectif1"}, {"titre": "objectif2"}],
}
