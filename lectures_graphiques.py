#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

from random import choice, randint

from sympy import (
    Eq,
    Ge,
    Gt,
    Interval,
    Le,
    Lt,
    Number,
    Rational,
    Reals,
    solveset,
    symbols,
)

from sympy_to_latex import reformat_latex as latex
from traces_tikz import arrondir_si_besoin as round
from traces_tikz import (
    dessiner_axes,
    dessiner_fleche,
    placer_point,
    reperer_x,
    reperer_y,
    tracer_fct,
    tracer_segment,
)

x = symbols("x")


def lecture_image(
    fct,
    val_x,
    xmin=-5,
    xmax=5,
    ymin=-5,
    ymax=5,
    precision_dessin=4,
    precision_resultat="absolute",
):
    figure = r"\begin{tikzpicture}" "\n"
    figure += dessiner_axes(xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
    figure += tracer_fct(fct, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax) + "\n"
    val_y = fct.subs(x, val_x)
    if val_y < 0:
        position = "above"
    else:
        position = "below"
    figure += (
        reperer_x(
            val_x,
            "${}$".format(latex(val_x)),
            position=position,
            precision=precision_dessin,
        )
        + "\n"
    )
    figure += placer_point(val_x, val_y, precision=precision_dessin) + "\n"
    figure += dessiner_fleche(
        val_x, 0, val_x, val_y, style="dashed", precision=precision_dessin
    )
    figure += dessiner_fleche(
        val_x, val_y, 0, val_y, style="dashed", precision=precision_dessin
    )
    if val_x < 0:
        position = "right"
    else:
        position = "left"
    if precision_resultat is not "absolute":
        etiquette_y = "${}$".format(latex(round(val_y, precision_resultat)))
    else:
        etiquette_y = "${}$".format(latex(val_y))
    figure += reperer_y(val_y, etiquette_y, position=position) + "\n"
    figure += r"\end{tikzpicture}" "\n"
    return figure


def lecture_antecedents(
    fct,
    val_y,
    xmin=-5,
    xmax=5,
    ymin=-5,
    ymax=5,
    precision_dessin=3,
    precision_resultat=1,
):
    if precision_resultat is not "absolute":
        val_y_pratique = round(float(val_y), precision_dessin)
    else:
        val_y_pratique = val_y
    figure = r"\begin{tikzpicture}" "\n"
    figure += (r"\clip ({}, {}) rectangle ({}, {});" "\n").format(
        xmin, ymin, xmax, ymax
    )
    figure += dessiner_axes(xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
    figure += tracer_fct(fct, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
    figure += placer_point(
        0, val_y_pratique, etiquette="${}$".format(latex(val_y)), position="above left"
    )
    figure += tracer_segment(xmin, val_y, xmax, val_y, style="dashed")
    valeurs_x = [val for val in solveset(Eq(fct, val_y_pratique)) if val.is_real]
    valeurs_x.sort()
    for v in valeurs_x:
        figure += placer_point(v, val_y_pratique)
    if val_y < 0:
        position = "above"
    else:
        position = "below"
    for v in valeurs_x:
        figure += dessiner_fleche(v, val_y_pratique, v, 0, style="dashed")
        if precision_resultat is not "absolute":
            etiquette_x = "${}$".format(latex(round(v, precision_resultat)))
        else:
            etiquette_x = "${}$".format(latex(v))
        figure += placer_point(v, 0, etiquette=etiquette_x, position=position)
    figure += r"\end{tikzpicture}" "\n"
    return figure


def arrondir(val, arrondi=3):
    if isinstance(val, Number):
        val_arrondie = round(val.evalf(arrondi + 2), arrondi)
    else:
        val_arrondie = round(val, arrondi)
    if val_arrondie % 1 == 0:
        val_arrondie = int(val_arrondie)
    return val_arrondie


def lire_abscisses(
    val_y,
    valeurs_x,
    xmin=-5,
    xmax=5,
    ymin=-5,
    ymax=5,
    precision_dessin=3,
    precision_resultat=1,
):
    val_y = arrondir(val_y, precision_dessin)
    figure = tracer_segment(xmin, val_y, xmax, val_y, style="dashed")
    if val_y < 0:
        position = "above"
    else:
        position = "below"
    for val_x in valeurs_x:
        val_x = arrondir(val_x, precision_dessin)
    for val_x in valeurs_x:
        figure += placer_point(
            val_x,
            val_y,
            etiquette=None,
            position=position,
            marque=r"$\times$",
            precision=precision_dessin,
        )
        if abs(val_y) > 0.5:
            figure += dessiner_fleche(
                val_x, val_y, val_x, 0, style="dashed", precision=precision_dessin
            )
        figure += reperer_x(
            val_x,
            "${}$".format(latex(arrondir(val_x, precision_resultat))),
            position=position,
            precision=precision_dessin,
        )
    return figure


def charnieres_reelles(relation, arrondi=3, xmin=-5, xmax=5):
    valeurs_exactes = [
        val
        for val in solveset(Eq(*relation.args), domain=Interval(xmin, xmax))
        if val.is_real
    ]
    valeurs_exactes.sort()
    valeurs_arrondies = [arrondir(v, arrondi) for v in valeurs_exactes if v.is_real]
    return [valeurs_exactes, valeurs_arrondies]


# def resoudre_graphiquement(in_equ, xmin=-5, xmax=5, ymin=-5, ymax=5):
#     valeurs_charnieres = Eq(*in_equ.args), domain=Reals))
#     print(valeurs_charnieres)
#     valeurs_charnieres.sort()
#     print(*valeurs_charnieres)


def resoudre_ineq_image_simple(
    ineq, xmin=-5, xmax=5, ymin=-5, ymax=5, precision_dessin=3, precision_resultat=1
):
    """
    Graphically solving for simples inequations such as
    x**2 - 3*x>7
    """
    figure = r"\begin{tikzpicture}" "\n"
    figure += (r"\clip ({}, {}) rectangle ({}, {});" "\n").format(
        xmin, ymin, xmax, ymax
    )
    figure += dessiner_axes(xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
    if ineq.args[1].is_number:
        fct, val_y = ineq.args
    else:
        return resoudre_ineq_image_simple(
            ineq.reversed, xmin=-5, xmax=5, ymin=-5, ymax=5
        )
    figure += tracer_fct(fct, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
    if precision_resultat is not "absolute":
        val_y_pratique = round(float(val_y), precision_dessin)
    else:
        val_y_pratique = val_y
    figure += placer_point(
        0, val_y_pratique, etiquette="${}$".format(latex(val_y)), position="above left"
    )
    if isinstance(ineq, Le) or isinstance(ineq, Lt):
        figure += tracer_segment(0, ymin, 0, val_y, style="line width=2pt")
    elif isinstance(ineq, Ge) or isinstance(ineq, Gt):
        figure += tracer_segment(0, val_y, 0, ymax, style="line width=2pt")
    charnieres_exactes, charnieres_arrondies = charnieres_reelles(ineq)
    figure += lire_abscisses(
        val_y,
        charnieres_arrondies,
        xmin=xmin,
        xmax=xmax,
        ymin=ymin,
        ymax=ymax,
        precision_dessin=precision_dessin,
        precision_resultat=precision_resultat,
    )
    bornes_arrondies = list(set([xmin] + charnieres_arrondies + [xmax]))
    bornes_arrondies.sort()
    for a, b in zip(bornes_arrondies[:-1], bornes_arrondies[1:]):
        if ineq.subs(x, (a + b) / 2):
            figure += tracer_fct(
                fct, xmin=a, xmax=b, ymin=ymin, ymax=ymax, style="line width=2pt"
            )
    ## brouillon
    charnieres_exactes.sort()
    charnieres_arrondies.sort()
    val_init = charnieres_exactes[0]
    val_fin = charnieres_exactes[-1]
    for val_inf, val, val_sup in zip(
        charnieres_exactes, charnieres_exactes[1:], charnières_exactes[2:]
    ):
        pass
    ## fin brouillon
    for a, b in zip(bornes_arrondies[:-1], bornes_arrondies[1:]):
        if ineq.subs(x, (a + b) / 2):
            figure += tracer_segment(a, 0, b, 0, style="line width=2pt")
    figure += r"\end{tikzpicture}" "\n"
    return figure


if __name__ == "__main__":
    # print(lecture_image(x ** 2, 2, xmin=-3, xmax=3, ymin=-0.5, ymax=9))
    # print(
    #     lecture_image(
    #         x ** 2, 1.25, precision_resultat=1, xmin=-2, xmax=2, ymin=-0.5, ymax=4
    #     )
    # )
    # print(lecture_antecedents(x ** 2, 3, xmin=-2, xmax=2, ymin=-0.5, ymax=4))
    # print(lecture_antecedents(x ** 2, 1, xmin=-2, xmax=2, ymin=-0.5, ymax=4))
    # print(lecture_antecedents(x ** 2, 2, xmin=-2, xmax=2, ymin=-0.5, ymax=4))
    # print(
    #     lecture_antecedents(
    #         x ** 2, 2, xmin=-2, xmax=2, ymin=-0.5, ymax=4, precision_resultat=1
    #     )
    # )
    # resoudre_graphiquement(Eq(x ** 2, 2))
    # resoudre_graphiquement(2 <= (x ** 2 - 3) * (x - 2) * (x + 3))
    # print(resoudre_ineq_image_simple(x ** 2 - 2 * x > 7))
    # print(resoudre_ineq_image_simple(7 < x ** 2 - 2 * x))
    # print(charnieres_reelles(3 * x < 7))
    # print(charnieres_reelles(x ** 2 - 2 * x + 3 < 3))
    # print(charnieres_reelles(x ** 3 - 2 * x ** 2 + 2 * x < 3))
    # # print(charnieres_reelles(1 / x < 3))
    # print(
    #     lecture_antecedents(
    #         ((x - 2) * (x + 1) * (x - 3) / 2).expand(),
    #         1.5,
    #         precision_dessin=3,
    #         precision_resultat=1,
    #     )
    # )
    # print(
    #     lecture_antecedents(
    #         ((x - 2) * (x + 1) * (x - 3) / 2).expand(),
    #         0,
    #         precision_dessin=3,
    #         precision_resultat=1,
    #     )
    # )
    # print(
    #     lecture_antecedents(
    #         ((x - 2) * (x + 1) * (x - 3) / 2).expand(),
    #         Rational(1, 3),
    #         precision_dessin=3,
    #         precision_resultat=1,
    #     )
    # )
    # print(charnieres_reelles(Eq((x - 2) * (x + 1) * (x - 3) / 2, Rational(1, 3))))
    print(charnieres_reelles(Eq((x - 2) * (x + 1) * (x - 3) / 2, 0.3333)))
    print(
        lecture_antecedents(
            ((x - 2) * (x + 1) * (x - 3) / 2).expand(),
            Rational(1, 3),
            precision_dessin=3,
            precision_resultat=1,
        )
    )
    print(charnieres_reelles(Eq(x ** 2, 3)))
    print(charnieres_reelles(Eq(x ** 2 - 3 * x + 2, 3)))
    print(charnieres_reelles(Eq(x ** 3, 27)))
    print(charnieres_reelles(Eq(x ** 3, 3)))
    print(map(latex, charnieres_reelles(Eq(x ** 3, 3))[0]))
    print(map(latex, charnieres_reelles(Eq(x ** 2 - 2 * x + 2, 3))[0]))
    print(map(latex, charnieres_reelles(Eq(x ** 2 - 2 * x + 2, 3))[1]))
    print(lire_abscisses(2, [3, 4, 5]))
    print(resoudre_ineq_image_simple(x ** 2 > 3))
    print(resoudre_ineq_image_simple((x - 2) * (x + 1) * (x - 3) / 2 > 0.3333))
