#!/usr/bin/env python
# -*- coding: utf-8 -*-


from sympy import symbols

from second_degre import *
from sympy_to_latex import reformat_latex as latex

x = symbols("x")
polynomes = [
    x ** 2 + 2 * x - 3,
    x ** 2,
    2 * x ** 2,
    3 * x ** 2 - 2,
    -2 * x ** 2 + 4 * x - 3,
]
for p in polynomes:
    print(latex(p) + " : ")
    print(latex(forme_canonique(p)))
    print(corrige_racines_reelles_poly_snd_deg(p))
