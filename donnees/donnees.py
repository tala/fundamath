#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function
import csv
import sys
import os
import os.path

# import tinydb
# sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
# import generateurTex
# from compilation.compilateur import compileDocument
STI2D1 = []
if __name__ == "__main__":
    DOSSIER_FICHIERS = os.path.realpath(os.curdir)
else:
    # hypothèse est faite que l'import se fait depuis la racine du projet
    # nécessité d'accéder aux fichiers de données depuis les dossiers
    # situés dans fundamath
    dossier_actuel = os.path.realpath(os.curdir)
    while os.path.basename(dossier_actuel) != "fundamath":
        dossier_actuel = os.path.dirname(dossier_actuel)
    DOSSIER_FUNDAMATH = dossier_actuel
    DOSSIER_FICHIERS = os.path.join(DOSSIER_FUNDAMATH, "donnees")


def ouvre_ici(nom_fichier):
    return open(os.path.join(DOSSIER_FICHIERS, nom_fichier))


with ouvre_ici("listing1STI2D2.csv") as fichier_csv:
    lecteur = csv.DictReader(fichier_csv)
    for l in lecteur:
        STI2D1.append(
            {"nom": l["nom"].decode("utf-8"), "prenom": l["prenom"].decode("utf-8")}
        )
# print(STI2D1)
#'data optim':
# traitement_data_optimisation(
# l['optimisation']),
#'data poly':
# traitement_data_poly(l['polynome secret'])})

GT2 = []

with ouvre_ici("listing2GT.csv") as fichier_csv:
    lecteur = csv.DictReader(fichier_csv)
    for l in lecteur:
        GT2.append(
            {"nom": l["nom"].decode("utf-8"), "prenom": l["prenom"].decode("utf-8")}
        )

Donnees = {
    "année": "2016-2017",
    "classes": [{"nom": "1 STI-2D", "eleves": STI2D1}, {"nom": "2 GT", "eleves": GT2}],
}


def bilan_colore_latex(ev):
    if ev == "0":
        res = r"\cellcolor{red}"
    elif ev == "1":
        res = r"\cellcolor{yellow}"
    elif ev == "2":
        res = r"\cellcolor{blue}"
    elif ev == "3":
        res = r"\cellcolor{green}"
    elif ev == "":
        res = r"\cellcolor{gray}"
    else:
        res = ev
    return res


def recupere_et_presente_premiere_evaluation(fichier_csv, eleves):
    with ouvre_ici(fichier_csv) as fichier:
        lecteur = csv.DictReader(fichier)
        champs = lecteur.fieldnames[1:]
        # la première colonne n'est pas une compétence
        for evaluation, eleve in zip(lecteur, eleves):
            # les élèves doivent être dans le même ordre dans les deux
            # iterables.
            eleve["Savoir-faire"] = []
            for sf in champs:
                eleve["Savoir-faire"].append(
                    {
                        "description": sf.decode("utf-8"),
                        "evaluations": [
                            bilan_colore_latex(evaluation[sf].decode("utf-8"))
                        ],
                    }
                )
        return eleves


bilan1STI2D = Donnees["classes"][0]
# print(bilan1STI2D)
with ouvre_ici("1STI2DbilanIR1.csv") as fichier_csv:
    lecteur = csv.DictReader(fichier_csv)
    champs = lecteur.fieldnames[1:]
    for ir, eleve in zip(lecteur, bilan1STI2D["eleves"]):
        eleve["Savoir-faire"] = []
        for sf in champs:
            eleve["Savoir-faire"].append(
                {
                    "description": sf.decode("utf-8"),
                    "evaluations": [bilan_colore_latex(ir[sf].decode("utf-8"))],
                }
            )

with ouvre_ici("1STI2DbilanDM1.csv") as fichier_csv:
    lecteur = csv.DictReader(fichier_csv)
    champs = lecteur.fieldnames[1:]
    for dm, eleve in zip(lecteur, bilan1STI2D["eleves"]):
        for sf in champs:
            # les compétences ont été créées précédemment
            for savoir_faire in eleve["Savoir-faire"]:
                if savoir_faire["description"] == sf.decode("utf-8"):
                    savoir_faire["evaluations"].append(
                        bilan_colore_latex(dm[sf].decode("utf-8"))
                    )
                    #'prenom': l['prenom'].decode('utf-8')})

with ouvre_ici("1STI2DbilanDS1.csv") as fichier_csv:
    lecteur = csv.DictReader(fichier_csv)
    champs = lecteur.fieldnames[1:]
    for ds, eleve in zip(lecteur, bilan1STI2D["eleves"]):
        for sf in champs:
            # les compétences ont été créées précédemment
            for savoir_faire in eleve["Savoir-faire"]:
                if savoir_faire["description"] == sf.decode("utf-8"):
                    savoir_faire["evaluations"].append(
                        bilan_colore_latex(ds[sf].decode("utf-8"))
                    )

# for el in bilan1STI2D['eleves'] :
# for savoir_faire in el['Savoir-faire'] :
# savoir_faire['evaluations'].append(" ")

bilan2GT = Donnees["classes"][1]

with ouvre_ici("2GTbilanIR1.csv") as fichier_csv:
    lecteur = csv.DictReader(fichier_csv)
    champs = lecteur.fieldnames[1:]
    for ir, eleve in zip(lecteur, bilan2GT["eleves"]):
        eleve["Savoir-faire"] = []
        for sf in champs:
            eleve["Savoir-faire"].append(
                {
                    "description": sf.decode("utf-8"),
                    "evaluations": [bilan_colore_latex(ir[sf].decode("utf-8"))],
                }
            )

with ouvre_ici("2GTbilanDM1.csv") as fichier_csv:
    lecteur = csv.DictReader(fichier_csv)
    champs = lecteur.fieldnames[1:]
    for dm, eleve in zip(lecteur, bilan2GT["eleves"]):
        for sf in champs:
            # les compétences ont été créées précédemment
            for savoir_faire in eleve["Savoir-faire"]:
                if savoir_faire["description"] == sf.decode("utf-8"):
                    savoir_faire["evaluations"].append(
                        bilan_colore_latex(dm[sf].decode("utf-8"))
                    )
                    #'prenom': l['prenom'].decode('utf-8')})

with ouvre_ici("2GTbilanDS1.csv") as fichier_csv:
    lecteur = csv.DictReader(fichier_csv)
    champs = lecteur.fieldnames[1:]
    for ds, eleve in zip(lecteur, bilan2GT["eleves"]):
        for sf in champs:
            # les compétences ont été créées précédemment
            for savoir_faire in eleve["Savoir-faire"]:
                if savoir_faire["description"] == sf.decode("utf-8"):
                    savoir_faire["evaluations"].append(
                        bilan_colore_latex(ds[sf].decode("utf-8"))
                    )
                    #'prenom': l['prenom'].decode('utf-8')})

# for el in bilan2GT['eleves'] :
# for savoir_faire in el['Savoir-faire'] :
# savoir_faire['evaluations'].append(" ")


def decode_distribution_dm(fichier):
    res = []
    # hypothèse est faite que les deux premières colonnnes correspondent
    # aux noms et aux prénoms et que les autres colonnes contiennent le
    # nombre de questions de chaque type
    with ouvre_ici(fichier) as fichier_csv:
        lecteur = csv.DictReader(fichier_csv)
        exercices_type = lecteur.fieldnames[2:]
        for ligne in lecteur:
            for ex in exercices_type:
                ligne[ex] = int(ligne[ex])
            res.append(ligne)
    return res


bilan1STI2D_2 = recupere_et_presente_premiere_evaluation("1STI2DbilanDS2.csv", STI2D1)
if __name__ == "__main__":
    # print(recupere_et_presente_premiere_evaluation('2GTbilanIR2.csv',GT2))
    print(recupere_et_presente_premiere_evaluation("1STI2DbilanDS2.csv", STI2D1))
    # print(decode_distribution_dm('1STI2DdistriDM2.csv'))
    # print(bilan1STI2D['eleves'][2])
    # savoir_faire_derivation = []
    # with open('donnees/savoir-faire-derivation.csv') as fichier_csv:
    # lecteur = csv.DictReader(fichier_csv)
    # for l in lecteur:
    # savoir_faire_derivation.append(l['savoir-faire'].decode('utf-8'))

    # savoir_faire_tale = []
    # with open('donnees/savoir-faire-serie2.csv') as fichier_csv:
    # lecteur = csv.DictReader(fichier_csv)
    # for l in lecteur:
    # savoir_faire_tale.append(l['savoir-faire'].decode('utf-8'))

    ## accès à 1 STI2D par Donnees['classes'][0]
