#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import csv
import sys
import os
import os.path

listings = {}
if __name__ == "__main__":
    DOSSIER_FICHIERS = os.path.realpath(os.curdir)
else:
    # hypothèse est faite que l'import se fait depuis la racine du projet
    # nécessité d'accéder aux fichiers de données depuis les dossiers
    # situés dans fundamath
    dossier_actuel = os.path.realpath(os.curdir)
    while os.path.basename(dossier_actuel) != "fundamath":
        dossier_actuel = os.path.dirname(dossier_actuel)
    DOSSIER_FUNDAMATH = dossier_actuel
    DOSSIER_FICHIERS = os.path.join(DOSSIER_FUNDAMATH, "donnees", "d2017_2018")


def ouvre_ici(nom_fichier):
    return open(os.path.join(DOSSIER_FICHIERS, nom_fichier))


def nom_principal(nom_fichier):
    return nom_fichier.split(os.extsep)[0]


for fichier in os.listdir(DOSSIER_FICHIERS):
    if len(fichier.split(os.extsep)) == 2 and fichier.split(os.extsep)[1] == "csv":
        with ouvre_ici(fichier) as fichier_csv:
            lecteur = csv.DictReader(fichier_csv)
            classe = []
            for l in lecteur:
                classe.append(
                    {
                        "nom": l["nom"].decode("utf-8"),
                        "prenom": l["prenom"].decode("utf-8"),
                    }
                )
            listings.update({nom_principal(fichier).decode("utf-8"): classe})

if __name__ == "__main__":
    print listings["1ere S2"]
