#!/usr/bin/env python
# -*- coding: utf-8 -*-

from faker import Faker
from csv import DictWriter

fake = Faker(locale="fr_FR")


def generer_listing(classe):
    fichier_csv = open(classe + ".csv", "w")
    dict_writer = DictWriter(fichier_csv, ["nom", "prenom"])
    dict_writer.writeheader()
    for i in xrange(35):
        dict_writer.writerow(
            {"nom": fake.last_name(), "prenom": fake.first_name().encode("utf-8")}
        )
    fichier_csv.close()


if __name__ == "__main__":
    generer_listing("1ere E")
