#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, unicode_literals

from random import choice

from sympy import And, Eq, Ge, Gt, Le, Lt, Rational, latex, sqrt, symbols

from compilation.compilateur import compileDocument
from generateurTex import (
    generer_correction_projetee_tex,
    generer_interro_flash_corrigee_tex,
)
from sympy_to_latex import decimal_to_latex
from sympy_to_latex import reformat_latex as latex
from sympy_to_latex import set_to_latex

x = symbols("x")
t = symbols("t")
R = symbols("R")
z = symbols("z")


def dbl_ineq_to_frame_latex(dbl_ineq, order="ascending"):
    """
    return the "frame" corresponding to well-formed double-inequality
    like And(x>3,x<5)
    """
    var = dbl_ineq.free_symbols.pop()
    ineq1, ineq2 = dbl_ineq.args
    if order == "ascending":
        if isinstance(ineq1, Gt) or isinstance(ineq1, Ge):
            ineq1 = ineq1.reversed
        if isinstance(ineq2, Gt) or isinstance(ineq2, Ge):
            ineq2 = ineq2.reversed
    if order == "descending":
        if isinstance(ineq1, Lt) or isinstance(ineq1, Le):
            ineq1 = ineq1.reversed
        if isinstance(ineq2, Lt) or isinstance(ineq2, Le):
            ineq2 = ineq2.reversed
    if ineq1.args[0] == var:
        ineq = ineq2
        ineq2 = ineq1
        ineq1 = ineq
    return decimal_to_latex(ineq1)[:-1] + decimal_to_latex(ineq2)


def simplifier0(ineq):
    mbrG, mbrD = ineq.args
    cste = mbrG.as_coefficients_dict()[1]
    return ineq.func(mbrG - cste, mbrD - cste)


def simplifier1(ineq):
    variables = ineq.free_symbols
    var = variables.pop()
    mbrG, mbrD = ineq.args
    cste = mbrD.as_coefficients_dict()[1]
    monomes = mbrD - cste
    return ineq.func(mbrG - monomes, mbrD - monomes)


def simplifier2(ineq):
    variables = ineq.free_symbols
    var = variables.pop()
    mbrG, mbrD = ineq.args
    coeff = mbrG.as_coefficients_dict()[var]
    inv_rel = {Gt: Lt, Ge: Le, Lt: Gt, Le: Ge, Eq: Eq}
    if coeff > 0:
        return ineq.func(mbrG / coeff, mbrD / coeff)
    elif coeff < 0:
        relation = inv_rel[ineq.func]
        return relation(mbrG / coeff, mbrD / coeff)


def simplifier3(ineq):
    variables = ineq.free_symbols
    var = variables.pop()
    if ineq.func == Eq:
        return ineq
    else:
        return latex(var) + r"\in " + set_to_latex(ineq.as_set())


def simplifier(ineq):
    res = []
    res.append(ineq)
    if not ineq == simplifier0(ineq):
        res.append(simplifier0(ineq))
    if not res[-1] == simplifier1(res[-1]):
        res.append(simplifier1(res[-1]))
    if not res[-1] == simplifier2(res[-1]):
        res.append(simplifier2(res[-1]))
    if not res[-1] == simplifier3(res[-1]):
        res.append(simplifier3(res[-1]))
    return res


def corrige_equation(eq):
    print(simplifier(eq))
    reponse = {}
    reponse["rappel texte"] = ""
    ensemble = eq.as_set()
    correction = r"\\" "\n".join(
        [latex(etape, mode="inline") for etape in simplifier(eq)]
    )
    correction += r"\\" "\n"
    correction += "L'ensemble des solutions est : $S={}$".format(set_to_latex(ensemble))
    reponse["corrige"] = correction
    reponse["conclusion"] = "$S={}$".format(set_to_latex(ensemble))
    return reponse


def question_equation(eq):
    question = {}
    question["consigne"] = (
        "Déterminer les solutions éventuelles de " "l'équation suivante :"
    )
    question["enonce"] = latex(eq, mode="inline")
    question.update(corrige_equation(eq))
    return question


def corrige_ineq_to_interval(ineq):
    reponse = {}
    ensemble = ineq.as_set()
    variables = ineq.free_symbols
    var = variables.pop()
    reponse["rappel texte"] = latex(ineq, mode="inline")
    reponse["conclusion"] = "${} \in {}$".format(var, set_to_latex(ensemble))

    reponse["corrige"] = reponse["rappel texte"] + r"\\" "\n"
    reponse["corrige"] += reponse["conclusion"]

    return reponse


def question_ineq_to_interval(ineq):
    question = {}
    question["consigne"] = "Traduire la relation suivante grâce à un " "intervalle"
    question["enonce"] = latex(ineq, mode="inline")
    question.update(corrige_ineq_to_interval(ineq))
    return question


def corrige_dbl_ineq_to_interval(dbl_ineq, order="ascending"):
    reponse = {}
    ensemble = dbl_ineq.as_set()
    variables = dbl_ineq.free_symbols
    var = variables.pop()
    reponse["rappel texte"] = "${}$".format(dbl_ineq_to_frame_latex(dbl_ineq, order))
    reponse["conclusion"] = "${} \in {}$".format(var, set_to_latex(ensemble))

    reponse["corrige"] = reponse["rappel texte"] + r"\\" "\n"
    reponse["corrige"] += reponse["conclusion"]
    return reponse


def question_dbl_ineq_to_interval(dbl_ineq, order="ascending"):
    question = {}
    question["consigne"] = "Traduire la relation suivante grâce à un " "intervalle"
    question["enonce"] = "${}$".format(dbl_ineq_to_frame_latex(dbl_ineq, order))
    question.update(corrige_dbl_ineq_to_interval(dbl_ineq, order))
    return question


def question_encadrement_carre(expr, order="ascending"):
    symbols = expr.free_symbols
    var = symbols.pop()
    if isinstance(expr, And):
        formule = "${}$".format(dbl_ineq_to_frame_latex(expr, order))
    else:
        formule = latex(expr, mode="inline")
    return {
        "consigne": "Donner un encadrement pour ${{{0}}}^2$ si ${0}$ est "
        "tel que :".format(var),
        "enonce": formule,
    }


def corrige_encadrement_et_intervalle(expr, order="ascending"):
    if isinstance(expr, And):
        frame = "${}$".format(dbl_ineq_to_frame_latex(expr, order))
    else:
        frame = latex(expr, mode="inline")
    return {
        "enonce": frame,
        "corrige": ["${}$".format(set_to_latex(expr.as_set()))],
        "conclusion": "${}$".format(set_to_latex(expr.as_set())),
    }


def presente_correction_exercice_intervalle(ref, exprs):
    return {
        "ref": ref,
        "questions": [corrige_encadrement_et_intervalle(expr) for expr in exprs],
    }


def simplification_latex(ineq):
    relations = simplifier(ineq)
    formules = [r"$\phantom{{\iff}}{}$".format(latex(relations[0]))] + [
        r"$\iff {}$".format(latex(r)) for r in relations[1:]
    ]
    return (r"\\" + "\n").join(formules)


def presente_correction_inequation(ineq):
    return {
        "enonce": r"Résoudre dans $\mathbb{{R}}$ : ${}$".format(latex(ineq)),
        "corrige": [latex(etape, mode="inline") for etape in simplifier(ineq)],
        "conclusion": latex(simplifier(ineq)[-1], mode="inline"),
    }


def presente_correction_exercice_inequation(ref, inequations):
    return {
        "ref": ref,
        "questions": [presente_correction_inequation(ineq) for ineq in inequations],
    }


# IF1 = {'classe': "2GT", 'date': 'Vendredi 13 janvier 2016', 'num': 1,
#'questions': [
# {'gauche': question_ineq_to_interval(x>=4),
#'droite': question_ineq_to_interval(x<3),
#'temps': 10
# },
# {'gauche': question_dbl_ineq_to_interval(
# And(2>=x,x>1)),
#'droite': question_dbl_ineq_to_interval(
# And(1>x,x>-1),
#'descending'),
#'temps': 10
# },
# {'gauche': question_equation(Eq(x**2,9)),
#'droite': question_equation(Eq((x+3)*(3*x-7),0)),
#'temps': 10
# },
# {'gauche': question_equation(2*x+3<3*x-7),
#'droite': question_equation(4*x+7<2*x-5),
#'temps': 10
# },
# {'gauche': question_equation(Eq(x**2+3*x,0)),
#'droite': question_equation(Eq((2*x-5)*(3-7*x),
# (3-7*x)*(2-12*x))),
#'temps': 10
# }
# ]}

ex18 = [x - 6 > 8, 2 * x < 7, 8 - x <= 3, -2 * x >= 24]
ex19 = [
    4 * x - 7 <= 10 * x + 8,
    8 * x + 11 < 3 * x - 4,
    2 * x + 9 >= 3 * x - 2,
    -2 * x - 5 < -7 * x - 15,
]
# ex20 = [
# 5*x+13<8*x-2,
# 9-3*x >= -2,
# 3*x**5+2*x-7<3*x**5-8*x-10,
# -2*x+4>3*x-5
# ]
# ex9 = [
# And(0<=x,x<=sqrt(2)),
# And(-2<x,x<-1),
# x>=Rational(7,3),
# x>-3.5
# ]

# correction = {'classe': "2GT",
#'date': 'Vendredi 20 janvier 2016',
#'exercices' : [
# presente_correction_exercice_inequation('18 p 103',ex18),
# presente_correction_exercice_inequation('19 p 103',ex19),
# presente_correction_exercice_inequation('20 p 103',ex20)
# ]
# }

# correction2 = {'classe': "2GT",
#'date': 'Lundi 23 janvier 2016',
#'exercices' : [
# presente_correction_exercice_inequation('20 p 103',ex20),
# presente_correction_exercice_intervalle('9 p 102',ex9)
# ]
# }

if __name__ == "__main__":
    print("truc")
    print(dbl_ineq_to_frame_latex(And(x > 2, x < 5)))
    print(question_encadrement_carre(And(x > 2, x < 5)))
    print(And(x > 2, x < 5).free_symbols)
    for rel in ex18 + ex19:
        print(simplifier(rel))
    print(simplifier(Eq(3 * x - 4, 6 * x - 5)))
    # for expr in ex9 :
    # print(corrige_encadrement_et_intervalle(expr))
    # print(set_to_latex(expr.as_set()))
    # compileDocument('Corr_2GT',
    # generer_correction_projetee_tex(correction2))
    # compileDocument('IF1_2GT', generer_interro_flash_corrigee_tex(IF1))
