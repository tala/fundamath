#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import
import jinja2
import os
import os.path
from jinja2 import Template

# from .donnees.donnees import STI2D1, bilan1STI2D, bilan2GT, bilan1STI2D_2
from compilation.compilateur import compileDocument

if __name__ == "__main__":
    DOSSIER_FICHIERS = os.path.realpath(os.curdir)
else:
    # hypothèse est faite que l'import se fait depuis la racine du projet
    # nécessité d'accéder aux fichiers de données depuis les dossiers
    # situés dans fundamath
    dossier_actuel = os.path.realpath(os.curdir)
    while os.path.basename(dossier_actuel) != "fundamath":
        dossier_actuel = os.path.dirname(dossier_actuel)
    DOSSIER_FUNDAMATH = dossier_actuel
    DOSSIER_FICHIERS = DOSSIER_FUNDAMATH

latex_jinja_env = jinja2.Environment(
    block_start_string=r"\BLOCK{",
    block_end_string="}",
    variable_start_string=r"\VAR{",
    variable_end_string="}",
    comment_start_string="\#{",
    comment_end_string="}",
    line_statement_prefix="%%",
    line_comment_prefix="%#",
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(DOSSIER_FICHIERS),
)


def charge_gabarit_ici(gabarit):
    return latex_jinja_env.get_template(gabarit)


test_rapide = {
    "classe": "1STI2D",
    "date": "Lundi 4 novembre 2015",
    "num": 1,
    "questions": [
        {
            "gauche": {
                "consigne": "Consigne de gauche",
                "enonce": "Suite de la consigne",
            },
            "droite": {
                "consigne": "Consigne de droite",
                "enonce": "Suite de la consigne",
            },
            "temps": 10,
        }
    ],
}


def generer_corrige_exos_livre(exercices):
    template = charge_gabarit_ici("modeles/correctionExosLivreTPL.tex")
    return template.render(exercices=exercices).encode("utf-8")


def generer_interro_flash_tex(interro):
    for q in interro["questions"]:
        q["temps etoile"] = round(q["temps"] / 3, 3)
    template = charge_gabarit_ici("modeles/anciens/interroFlashTPL.tex")
    return template.render(interro=interro).encode("utf-8")


def generer_interro_flash_corrigee_tex(interro):
    for q in interro["questions"]:
        q["temps etoile"] = round(q["temps"] / 3, 3)
    template = charge_gabarit_ici("modeles/anciens/interroFlashCorrigeeTPL.tex")
    return template.render(interro=interro).encode("utf-8")


def generer_interro_flash_corrigee_tex_2(interro):
    for q in interro["questions"]:
        q["temps etoile"] = round(q["temps"] / 3, 3)
    template = charge_gabarit_ici("modeles/interroFlashCorrigeeTPL2.tex")
    return template.render(interro=interro).encode("utf-8")


def generer_interro_flash_corrigee_tex_RC(interro):
    for q in interro["questions"]:
        q["temps etoile"] = round(q["temps"] / 3, 3)
    template = charge_gabarit_ici("modeles/diaporamas/interroProjeteeCorrigeeTPL.tex")
    return template.render(interro=interro).encode("utf-8")


def generer_revision_corrigee_tex(revision):
    template = charge_gabarit_ici("modeles/revisions2TPL.tex")
    return template.render(revision=revision).encode("utf-8")


def generer_correction_projetee_tex(correction):
    template = charge_gabarit_ici("modeles/diaporamas/correctionProjeteeTPL.tex")
    return template.render(correction=correction).encode("utf-8")


def generer_fiche_methode(fiche):
    template = charge_gabarit_ici("modeles/ficheMethodeTPL.tex")
    return template.render(fiche=fiche).encode("utf-8")


def generer_dm_perso(classe, devoirs, savoir_faire):
    template = charge_gabarit_ici("modeles/DMPersonnaliseTPL.tex")
    for i, eleve in enumerate(classe["eleves"]):
        eleve["savoir-faire"] = savoir_faire
        eleve["exercices"] = [ex.decode("utf8") for ex in devoirs[i]]
        eleve["correction"] = ""
    return template.render(classe=classe).encode("utf-8")


def generer_correction_dm(devoir):
    template = charge_gabarit_ici("modeles/correctionDMTPL.tex")
    return template.render(devoir=devoir).encode("utf-8")


def generer_corrige_ds(devoir):
    template = charge_gabarit_ici("modeles/correctionDSTPL.tex")
    return template.render(devoir=devoir).encode("utf-8")


def generer_dm(devoir):
    template = charge_gabarit_ici("modeles/DM_TPL.tex")
    return template.render(devoir=devoir).encode("utf-8")


def generer_punitions(classe, devoirs):
    template = charge_gabarit_ici("modeles/DMPersonnaliseTPL.tex")
    for i, eleve in enumerate(classe["eleves"]):
        eleve["exercices"] = [ex.decode("utf8") for ex in devoirs[i]]
        eleve["correction"] = ""
    return template.render(classe=classe).encode("utf-8")


# def generer_dm_1_1STI2D():
# classe={'eleves' : STI2D1}
# template = charge_gabarit_ici('modeles/projetsSDAPS/1STI2Ddm1.tex')
# return template.render(classe=classe).encode('utf-8')


def generer_dm_1_1STI2D(devoirs):
    template = charge_gabarit_ici("modeles/1STI2Ddm1TPL.tex")
    return template.render(devoirs=devoirs).encode("utf-8")


def generer_dm_1_2GT(eleves):
    # peut-être un problème de gabarit "perdu" ?
    template = charge_gabarit_ici("modeles/2GTDM1TPL.tex")
    return template.render(eleves=eleves).encode("utf-8")


def generer_corrige_dm_1_1STI2D(eleves):
    template = charge_gabarit_ici("modeles/1STI2DDM1correctionTPL2.tex")
    return template.render(eleves=eleves).encode("utf-8")


def generer_bilan_1STI2D():
    template = charge_gabarit_ici("modeles/1STI2DBilan1TPL.tex")
    return template.render(eleves=bilan1STI2D["eleves"]).encode("utf-8")


def generer_bilan_2GT():
    template = charge_gabarit_ici("modeles/2GTBilan1TPL.tex")
    return template.render(eleves=bilan2GT["eleves"]).encode("utf-8")


def generer_corrige_dm_1_2GT(eleves):
    template = charge_gabarit_ici("modeles/2GT_DM1correctionTPL.tex")
    return template.render(eleves=eleves).encode("utf-8")


def generer_brouillon_diapo(brouillon):
    template = charge_gabarit_ici("modeles/diaporamas/brouillonDiaposTPL.tex")
    return template.render(brouillon=brouillon).encode("utf-8")


def generer_bilan(modele, eleves):
    template = charge_gabarit_ici(modele)
    return template.render(eleves=eleves).encode("utf-8")


if __name__ == "__main__":
    # compileDocument('1STI2Dbilan1',generer_bilan_1STI2D())
    print(bilan1STI2D_2)
    print(generer_bilan("modeles/1STI2DBilan1TPL.tex", bilan1STI2D_2))
    compileDocument(
        "1STI2Dbilan2", generer_bilan("modeles/1STI2DBilan1TPL.tex", bilan1STI2D_2)
    )
    # print(bilan2GT['eleves'][4])
    # fichier.close()
