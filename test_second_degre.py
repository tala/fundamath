#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

from sympy import Rational, symbols

from .second_degre import calcul_discriminant, wrapp_1, wrapp_2

x = symbols("x")


def test_wrapp_1():
    assert wrapp_1(2) == "2"
    assert wrapp_1(Rational(2, 3)) == r"\left(\dfrac{2}{3}\right)"
    assert wrapp_1(-2) == r"\left(-2\right)"


def test_wrapp_2():
    assert wrapp_2(2) == "2"
    assert wrapp_2(Rational(2, 3)) == r"\dfrac{2}{3}"
    assert wrapp_1(-2) == r"\left(-2\right)"


def test_calcul_discriminant():
    p = x ** 2 - 2 * x + 7
    correction = "\n".join(
        [
            r"$a = 1$ ; $b = -2$ ; $c = 7$\\",
            r"$\Delta =\left(-2\right)^{2}-4\times1\times7=-24$\\",
        ]
    )
    assert calcul_discriminant(p) == correction
