#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function, absolute_import

from random import shuffle, randint, choice
from sympy import (
    sympify,
    symbols,
    Poly,
    latex,
    Add,
    Pow,
    Mul,
    sqrt,
    Rational,
    Eq,
    Number,
)
from sympy.core.mul import Mul
from sympy.solvers.inequalities import solve_univariate_inequality

from mathobjects.expressions import str_to_math_expr

from signes import signe_produit_et_quotient
from sympy_to_latex import set_to_latex
from generateurTex import (
    generer_corrige_ds,
    generer_dm,
    generer_correction_dm,
    generer_dm_1_1STI2D,
)
from compilation.compilateur import compileDocument

# from .donnees.donnees import Donnees
from sympy_to_latex import reformat_latex as latex

x = symbols("x")
t = symbols("t")
R = symbols("R")
z = symbols("z")
y = symbols("y")
l = symbols("l")

VARIABLES = {x, t, R, z, y, l}


def wrapp_1(n):
    if n < 0 or isinstance(n, Rational):
        return r"\left({}\right)".format(latex(n))
    else:
        return latex(n)


def wrapp_2(n):
    if n < 0:
        return r"\left({}\right)".format(latex(n))
    else:
        return latex(n)


def presente_carre(val):
    return "{}^2".format(wrapp_1(val))


def presente_encadrement(xmin, x, xmax):
    pass


def variable(p):
    return VARIABLES.intersection(p.free_symbols).pop()


def coefficients_polynome(p):
    p_e = p.expand()
    var = variable(p)
    poly = Poly(p_e, variable(p_e))
    liste_coeffs = poly.all_coeffs()
    n = poly.degree()
    monomes = [var ** 2, var, 1]
    dico = dict(zip(monomes, liste_coeffs))
    # try :
    # dico[1]
    # except :
    # print(p)
    return dico


def fsd_ir3():
    """
    génère une identité remarquable développée du type A**2-B**2
    """
    v = choice([x, y, z, R, t])
    coeffs = [1, 2, 3]
    shuffle(coeffs)
    A = choice([-1, 1]) * coeffs[0] * v + choice([-1, 1]) * randint(1, 5)
    B = choice([-1, 1]) * coeffs[1] * v + choice([-1, 1]) * randint(1, 5)
    return A ** 2 - B ** 2


def fd_ir1_ir2():
    """
    génère une identité remarquable développée du type k(a+b)**2
    """
    v = choice([x, y, z, R, t])
    k = choice([-1, 1]) * randint(1, 5)
    A = choice([2, 3]) * v + choice([-1, 1]) * randint(1, 5)
    return (k * A ** 2).expand()


def fd_fc():
    """
    génère une forme développée comportant un facteur commun élémentaire
    """
    v = choice([x, y, z, R, t])
    k = choice([-1, 1]) * randint(1, 5) * v
    A = choice([2, 3]) * v + choice([-1, 1]) * randint(1, 5)
    return (k * A).expand()


def fc_nf():
    """
    génère une forme canonique non factorisable
    """
    v = choice([x, y, z, R, t])
    k = choice([-1, 1]) * randint(1, 5)
    A = v + choice([-1, 1]) * randint(1, 5)
    B = randint(1, 5)
    return Mul(A ** 2 + B, k)


def fc_f():
    """
    génère une forme canonique factorisable
    """
    v = choice([x, y, z, R, t])
    k = choice([-1, 1]) * randint(1, 5)
    A = v + choice([-1, 1]) * randint(1, 5)
    B = randint(1, 5)
    return Mul(A ** 2 - B ** 2, k)


def fsd_fc():
    """
    génère une forme semi-développée ayant un facteur commun
    """
    v = choice([x, y, z, R, t])
    A = choice([-1, 1]) * v + choice([-1, 1]) * randint(1, 5)
    coeffs = [2, 3]
    shuffle(coeffs)
    coeff1, coeff2 = coeffs
    B = coeff1 * v + choice([-1, 1]) * randint(1, 5)
    C = coeff2 * v + choice([-1, 1]) * randint(1, 5)
    facteurs_1 = [A, B]
    facteurs_2 = [A, C]
    map(shuffle, [facteurs_1, facteurs_2])
    E, F = facteurs_1
    G, H = facteurs_2
    return Mul(E, F, evaluate=False) - Mul(G, H, evaluate=False)


def fd_2r():
    """
    génère une forme développée ayant 2 racines réeles dont une entière
    """
    v = choice([x, y, z, R, t])
    k = choice([-1, 1]) * randint(1, 5)
    A = choice([2, 3]) * v + choice([-1, 1]) * randint(1, 5)
    B = v + choice([-1, 1]) * randint(1, 5)
    return (k * A * B).expand()


def fd_1r():
    """
    génère une forme développée ayant 1 racine double
    """
    v = choice([x, y, z, R, t])
    k = choice([-1, 1]) * randint(1, 5)
    A = choice([2, 3]) * v + choice([-1, 1]) * randint(1, 5)
    return (k * A ** 2).expand()


def fd_0r():
    """
    génère une forme développée n'ayant aucune racine réelle
    """
    v = choice([x, y, z, R, t])
    k = choice([-1, 1]) * randint(1, 5)
    A = choice([2, 3]) * v + choice([-1, 1]) * randint(1, 5)
    B = randint(1, 5)
    return (k * (A ** 2 + B)).expand()


def ineq1():
    """
    génère une inéquation dont l'ensemble des solutions est un
    intervalle ou un singleton
    """
    v = choice([x, y, z, R, t])
    return choice(
        [
            # k(ax +- 1)(x +-b)<0
            (
                (
                    (randint(1, 5) * v + choice([-1, 1]) * randint(1, 5))
                    * (v + choice([-1, 1]) * randint(1, 5))
                ).expand()
                < 0
            ),
            # -k(ax +- 1)(x +-b)>0
            (
                (
                    -1
                    * (randint(1, 5) * v + choice([-1, 1]) * randint(1, 5))
                    * (v + choice([-1, 1]) * randint(1, 5))
                ).expand()
                > 0
            ),
            # k(ax +- 1)(x +-b)<=0
            (
                (
                    (randint(1, 5) * v + choice([-1, 1]) * randint(1, 5))
                    * (v + choice([-1, 1]) * randint(1, 5))
                ).expand()
                <= 0
            ),
            # -k(ax +- 1)(x +-b)>=0
            (
                (
                    -1
                    * (randint(1, 5) * v + choice([-1, 1]) * randint(1, 5))
                    * (v + choice([-1, 1]) * randint(1, 5))
                ).expand()
                >= 0
            ),
        ]
    )


def ineq2():
    """
    génère une inéquation dont l'ensemble des solutions est la réunion
    de deux intervalles disjoints ou R
    """
    v = choice([x, y, z, R, t])
    return choice(
        [
            (
                (
                    (randint(1, 5) * v + choice([-1, 1]) * randint(1, 5))
                    * (v + choice([-1, 1]) * randint(1, 5))
                ).expand()
                > 0
            ),
            (
                (
                    -1
                    * (randint(1, 5) * v + choice([-1, 1]) * randint(1, 5))
                    * (v + choice([-1, 1]) * randint(1, 5))
                ).expand()
                < 0
            ),
            (
                (
                    (randint(1, 5) * v + choice([-1, 1]) * randint(1, 5))
                    * (v + choice([-1, 1]) * randint(1, 5))
                ).expand()
                >= 0
            ),
            (
                (
                    -1
                    * (randint(1, 5) * v + choice([-1, 1]) * randint(1, 5))
                    * (v + choice([-1, 1]) * randint(1, 5))
                ).expand()
                <= 0
            ),
        ]
    )


def ineq3():
    """
    génère une inéquation dont l'ensemble des solutions est R privé
    d'une valeur (??).
    """
    v = choice([x, y, z, R, t])
    return choice(
        [
            # (k(kx +-1))**2>0
            ((randint(1, 5) * v + choice([-1, 1]) * randint(1, 5)) ** 2).expand() > 0,
            # -(k(kx +-1))**2>=0
            -((randint(1, 5) * v + choice([-1, 1]) * randint(1, 5)) ** 2).expand() >= 0,
        ]
    )


def ineq4():
    """
    génère une inéquation dont l'ensemble des solutions est R.
    """
    v = choice([x, y, z, R, t])
    A = choice([1, 2]) * v + choice([-1, 1]) * randint(1, 5)
    B = randint(1, 5)
    return choice([(A ** 2 + B).expand() >= 0, -(A ** 2 + B).expand() <= 0])


def ineq5():
    """
    génère une inéquation dont l'ensemble des solutions est vide.
    """
    v = choice([x, y, z, R, t])
    A = choice([1, 2]) * v + choice([-1, 1]) * randint(1, 5)
    B = randint(1, 5)
    return choice([(A ** 2 + B).expand() <= 0, -(A ** 2 + B).expand() >= 0])


def transforme0_relation(rel):
    """
    transforme ax**2+bx+c ? 0 en ax**2+bx ? - c ou son reflet
    """
    p = rel.args[0]
    var = variable(p)
    coeffs = coefficients_polynome(p)
    c = coeffs[1]
    lance = choice([0, 1])
    if lance:
        return rel.func(p - c, -c)
    else:
        return rel.func(-c, p - c)


def transforme1_relation(rel):
    """
    transforme ax**2+bx+c ? 0 en ax**2+c ? -bx ou son reflet
    """
    p = rel.args[0]
    var = variable(p)
    coeffs = coefficients_polynome(p)
    b = coeffs[var]
    c = coeffs[1]
    lance = choice([0, 1])
    if lance:
        return rel.func(p - b * var, -b * var)
    else:
        return rel.func(-b * var, p - b * var)


def transforme2_relation(rel):
    """
    transforme ax**2+bx+c ? 0 en ax**2 ? -bx - c ou son reflet
    """
    p = rel.args[0]
    var = variable(p)
    coeffs = coefficients_polynome(p)
    a = coeffs[var ** 2]
    lance = choice([0, 1])
    if lance:
        return rel.func(-a * var ** 2, p - a * var ** 2)
    else:
        return rel.func(p - a * var ** 2, -a * var ** 2)


def equation1(p):
    """
    génère une équation du type a*x**2+b*x=-c ou son reflet
    """
    sbls = p.free_symbols
    var = variable(p)
    coeffs = coefficients_polynome(p)
    c = coeffs[1]
    lance = choice([0, 1])
    if lance:
        return Eq(p - c, -c)
    else:
        return Eq(-c, p - c)


def equation2(p):
    """
    génère une équation du type a*x**2+c=-b*x ou son reflet
    """
    var = variable(p)
    coeffs = coefficients_polynome(p)

    b = coeffs[var]
    c = coeffs[1]
    lance = choice([0, 1])
    if lance:
        return Eq(p - b * var, -b * var)
    else:
        return Eq(-b * var, p - b * var)


def equation3(p):
    """
    génère une équation du type a*x**2=-b*x-c ou son reflet
    """
    var = variable(p)
    coeffs = coefficients_polynome(p)

    a = coeffs[var ** 2]
    lance = choice([0, 1])
    if lance:
        return Eq(-a * var ** 2, p - a * var ** 2)
    else:
        return Eq(p - a * var ** 2, -a * var ** 2)


def equations_2nde_1():
    polynomes = [fsd_ir3(), fd_ir1_ir2(), fd_fc(), fc_nf(), fc_f(), fsd_fc()]
    equations = list(map(lambda p: Eq(p, 0, evaluate=False), polynomes))
    shuffle(equations)
    return equations


def equations_2nde_2():
    polynomes = [fsd_ir3(), fd_fc(), fc_f(), fsd_fc()]
    equations = list(map(lambda p: Eq(p, 0, evaluate=False), polynomes))
    shuffle(equations)
    return equations


def polys_a_canoniser():
    polynomes = list(map(lambda p: p.expand(), [fc_f(), fc_nf(), fc_f(), fc_f()]))
    shuffle(polynomes)
    return polynomes


def equations_1ere():
    equs = []
    polynomes = [fd_2r(), fd_1r(), fd_0r(), fd_2r()]
    shuffle(polynomes)
    for i, p in enumerate(polynomes):
        if i % 3 == 0:
            equs.append(transforme0_relation(Eq(p, 0)))
        elif i % 3 == 1:
            equs.append(transforme1_relation(Eq(p, 0)))
        else:
            equs.append(transforme2_relation(Eq(p, 0)))
    shuffle(equs)
    return equs


def inequations():
    ineqs = []
    ineqs0 = [ineq1(), ineq2(), ineq3(), ineq4()]
    shuffle(ineqs0)
    for i, ineq in enumerate(ineqs0):
        if i % 2 == 0:
            ineqs.append(transforme0_relation(ineq))
        else:
            ineqs.append(transforme2_relation(ineq))
    shuffle(ineqs)
    return ineqs


def inequs():
    inequations = [ineq1(), ineq2(), ineq3(), ineq4()]
    shuffle(inequations)
    return [latex(ineq) for ineq in inequations]


def exercice_equations(_equations):
    pass


def exercice_inequations(_inequations):
    pass


def exercice_premieres_equations():
    exo = {"equations": equations_2nde_1()}
    enonce = (
        r"Résoudre si possible les équations suivantes sans "
        "utiliser les"
        r" outils de première (donc sans calcul du discriminant) :"
    )
    enonce += "\n"
    enonce += r"\begin{enumerate}" + "\n"
    for eq in exo["equations"]:
        enonce += r"\item {{${}$}}".format(latex(eq)) + "\n"
    enonce += r"\end{enumerate}" + "\n"

    correction = r"\begin{enumerate}" + "\n"
    for eq in exo["equations"]:
        correction += r"\item {}".format(corrige_factorisation(eq.args[0])) + r"\\" "\n"
        correction += presente_solutions(eq)
    correction += r"\end{enumerate}" + "\n"
    exo.update({"enonce": enonce, "correction": correction})
    return exo


def exercice_canonisation():
    exo = {"polynomes": polys_a_canoniser()}
    enonce = (
        r"Écrire les polynômes du second degré "
        "sous leur formes canoniques (sans utiliser "
        "les discriminants):" + "\n"
    )
    enonce += r"\begin{multicols}{2}" + "\n"
    enonce += r"\begin{enumerate}" + "\n"
    for p in exo["polynomes"]:
        enonce += r"\item {{${}$}}".format(latex(p)) + "\n"
    enonce += r"\end{enumerate}" + "\n"
    enonce += r"\end{multicols}" + "\n"
    correction = ""
    correction += r"\begin{enumerate}" + "\n"
    for p in exo["polynomes"]:
        correction += r"\item {}".format(corrige_canonisation(p)) + "\n"
    correction += r"\end{enumerate}" + "\n"
    exo.update({"enonce": enonce, "correction": correction})
    return exo


def exercice_equations_discriminant():
    exo = {"equations": equations_1ere()}
    enonce = r"Résoudre chacune des équations suivantes : " + "\n"
    enonce += r"\begin{multicols}{2}" + "\n"
    enonce += r"\begin{enumerate}" + "\n"
    for eq in exo["equations"]:
        enonce += r"\item {{${}$}}".format(latex(eq)) + "\n"
    enonce += r"\end{enumerate}" + "\n"
    enonce += r"\end{multicols}" + "\n"
    correction = ""
    correction += r"\begin{enumerate}" + "\n"
    for eq in exo["equations"]:
        correction += r"\item {}".format(corrige_equ_poly_snd_deg(eq))
        correction += "\n"
    correction += r"\end{enumerate}" + "\n"
    exo.update({"enonce": enonce, "correction": correction})
    return exo


def calcul_discriminant(p):
    lignes = []
    var = variable(p)
    coeffs = coefficients_polynome(p)
    a = coeffs[var ** 2]
    b = coeffs[var]
    c = coeffs[1]
    lignes.append(r"$a = {}$ ; $b = {}$ ; $c = {}$\\".format(*map(latex, [a, b, c])))
    delta = b ** 2 - 4 * a * c
    lignes.append(
        r"$\Delta ={}-{}={}$\\".format(
            latex(Pow(b, 2, evaluate=False)),
            r"4\times{}\times{}".format(wrapp_2(a), wrapp_2(c)),
            latex(delta),
        )
    )
    return "\n".join(lignes)


def corrige_racines_reelles_poly_snd_deg(p):
    lignes = []
    var = variable(p)
    coeffs = coefficients_polynome(p)
    a = coeffs[var ** 2]
    b = coeffs[var]
    c = coeffs[1]
    delta = b ** 2 - 4 * a * c
    lignes.extend(calcul_discriminant(p).split("\n"))
    if delta > 0:
        lignes.append(
            r"Le polynôme ${}$ admet exactement deux racines réelles :\\".format(
                latex(p)
            )
        )
        lignes.append(
            r"${0}_1=\dfrac{{{1}-\sqrt{{{2}}}}}{{{3}}}=\dfrac{{{1}-{4}}}{{{5}}}={6}$\\".format(
                latex(var),
                latex(-b),
                latex(delta),
                latex(Mul(2, a, evaluate=False), mul_symbol="times"),
                latex(sqrt(delta)),
                latex(2 * a),
                latex((-b - sqrt(delta)) / (2 * a)),
            )
        )
        lignes.append(
            r"${0}_2=\dfrac{{{1}+\sqrt{{{2}}}}}{{{3}}}=\dfrac{{{1}+{4}}}{{{5}}}={6}$\\".format(
                latex(var),
                latex(-b),
                latex(delta),
                latex(Mul(2, a, evaluate=False), mul_symbol="times"),
                latex(sqrt(delta)),
                latex(2 * a),
                latex((-b + sqrt(delta)) / (2 * a)),
            )
        )
    elif delta < 0:
        lignes.append(
            r"Le polynôme ${}$ n'admet aucune racine réelle.\\".format(latex(p))
        )
    else:
        lignes.append(
            r"Le polynôme ${}$ admet exactement une racine réelle :\\".format(latex(p))
        )
        lignes.append(
            r"${0}_0=\dfrac{{{1}}}{{{2}}}=\dfrac{{{1}}}{{{3}}}={4}$\\".format(
                latex(var),
                latex(-b),
                latex(Mul(2, a, evaluate=False), mul_symbol="times"),
                latex(2 * a),
                latex((-b) / (2 * a)),
            )
        )
    return "\n".join(lignes)


def signe_poly_snd_deg(p):
    lignes = []
    var = variable(p)
    coeffs = coefficients_polynome(p)
    a = coeffs[var ** 2]
    b = coeffs[var]
    c = coeffs[1]
    delta = b ** 2 - 4 * a * c
    lignes.append(corrige_racines_reelles_poly_snd_deg(p))

    lignes.append(r"\begin{tikzpicture}")
    lignes.append(r"\tkzTabInit[lgt=3,espcl=1.5]")
    lignes.append(r"{{${}$ /1,signe de ${}$/1}}".format(latex(var), latex(p)))
    if delta > 0:
        x1, x2 = p.as_poly().real_roots()
        lignes.append(r"{{$-\infty$,${}$,${}$,$+\infty$}}".format(latex(x1), latex(x2)))
        if a > 0:
            lignes.append(r"\tkzTabLine{ , + , z , - , z , + , }")
        else:
            lignes.append(r"\tkzTabLine{ , - , z , + , z , - , }")
    elif delta == 0:
        x0 = p.as_poly().real_roots()[0]
        lignes.append(r"{{$-\infty$,${}$,$+\infty$}}".format(latex(x0)))
        if a > 0:
            lignes.append(r"\tkzTabLine{ , + , z , + , }")
        else:
            lignes.append(r"\tkzTabLine{ , - , z , - , }")
    else:
        lignes.append(r"{$-\infty$,$+\infty$}")
        if a > 0:
            lignes.append(r"\tkzTabLine{ , + , }")
        else:
            lignes.append(r"\tkzTabLine{ , - , }")
    lignes.append(r"\end{tikzpicture}\\")
    return "\n".join(lignes)


def canon_cas_simple(p):
    lignes = []
    var = variable(p)
    coeffs = coefficients_polynome(p)
    a = coeffs[var ** 2]
    b = coeffs[var]
    c = coeffs[1]
    # Affreux bricolage pour éviter l'évaluation auto de la commande
    # latex():
    formule = (
        latex((var + b / 2) ** 2) + "-" + latex(Pow(abs(b / 2), 2, evaluate=False))
    )
    if c > 0:
        formule += "+" + latex(c)
    elif c < 0:
        formule += latex(c)
    lignes.append(formule)
    lignes.append(latex(Add((var + b / 2) ** 2 - (b / 2) ** 2, c, evaluate=False)))
    lignes.append(latex((var + b / 2) ** 2 - (b / 2) ** 2 + c))
    return lignes


def corrige_canonisation(p):
    res = r"$\phantom{=}" + latex(p) + r"\\" + "\n"
    var = variable(p)
    coeffs = coefficients_polynome(p)
    a = coeffs[var ** 2]
    b = coeffs[var]
    c = coeffs[1]
    if a != 1:
        res += r"={}\left({}\right)\\".format(latex(a), latex(p / a)) + "\n"
        for ligne in canon_cas_simple(p / a):
            res += r"={}\left({}\right)\\".format(latex(a), ligne) + "\n"
        res += r"=\mathbf{{{}}}".format(
            latex(a * (var + b / (2 * a)) ** 2 - b ** 2 / (4 * a) + c)
        )
    else:
        for ligne in canon_cas_simple(p)[:-1]:
            res += "=" + ligne + r"\\" + "\n"
        res += r"=\mathbf{" + canon_cas_simple(p)[-1] + "}"
    return res + r"$\\" "\n"


def solutions_ineq(ineq):
    sbls = ineq.free_symbols
    var = sbls.pop()
    return (
        r"Solutions de l'inéquation : $\mathbf{"
        + set_to_latex(solve_univariate_inequality(ineq, var, relational=False))
        + "}$."
    )


def forme_canonique(p):
    var = variable(p)
    coeffs = coefficients_polynome(p)
    a = coeffs[var ** 2]
    b = coeffs[var]
    c = coeffs[1]
    alpha = -Rational(b, 2 * a)
    beta = p.subs(var, alpha)
    return a * (x - alpha) ** 2 + beta


def presente_solutions(eq):
    """
    l'équation
    """
    correction = ""
    if eq.args[0] != 0 and eq.args[1] != 0:
        eq = Eq(eq.args[0] - eq.args[1], 0)
    elif eq.args[1] != 0:
        eq = Eq(eq.args[1], 0)
    try:
        racines = list(set(eq.args[0].as_poly().real_roots()))
    except:
        return ""
    var = variable(eq.args[0])
    if racines == []:
        correction += (
            r"\textbf{L'équation n'admet aucune "
            r"solution sur $\mathbf{\mathbb{R}}$.}"
        )
    elif len(racines) == 1:
        correction += r"\textbf{La solution de l'équation est : "
        correction += r"$\mathbf{{{}={}}}$.}}".format(latex(var), latex(racines[0]))
    else:
        correction += r"\textbf{Les solutions de l'équation sont : " + ", ".join(
            [
                r"$\mathbf{{{}={}}}$".format(latex(var), latex(racine))
                for racine in racines[:-1]
            ]
        )
        correction += r" ou $\mathbf{{{}={}}}$.}}".format(
            latex(var), latex(racines[-1])
        )
    return correction


def corrige_ineq_poly_snd_deg(ineq):
    lignes = [r"inéquation : $" + latex(ineq) + r"$.\\"]
    if ineq.args[1] == 0:
        lignes.append(signe_poly_snd_deg(ineq.args[0]))
        lignes.append(solutions_ineq(ineq))
    elif ineq.args[0] == 0:
        lignes.append(signe_poly_snd_deg(ineq.args[1]))
        lignes.append(solutions_ineq(ineq))
    else:
        lignes.append(r"Cette inéquation équivaut à :\\")
        ineq = ineq.__class__(ineq.args[0] - ineq.args[1], 0)
        lignes.append("$" + latex(ineq) + r"$.\\")
        lignes.append(signe_poly_snd_deg(ineq.args[0]))
        lignes.append(solutions_ineq(ineq))
    return "\n".join(lignes)


def elts_correction_ineq_prod_poly(ineq):
    lignes = [r"inéquation : $" + latex(ineq) + r"$.\\"]
    premier_facteur = ineq.args[0].args[0]
    deuxieme_facteur = ineq.args[0].args[1]
    if True:  # du boulot à faire pour le signe de ax+b
        lignes.append(r"signe de $" + latex(premier_facteur) + r"$ :\\")
        lignes.append(signe_poly_snd_deg(premier_facteur))
    if True:  # du boulot à faire pour le signe de ax+b
        lignes.append(r"signe de $" + latex(deuxieme_facteur) + r"$ :\\")
        lignes.append(signe_poly_snd_deg(deuxieme_facteur))
    lignes.append(solutions_ineq(ineq))
    return "\n".join(lignes)


def get_denominateur(quotient):
    if isinstance(quotient.args[0], Pow) and quotient.args[0].args[1] == -1:
        return quotient.args[0].args[0]
    elif isinstance(quotient.args[1], Pow) and quotient.args[1].args[1] == -1:
        return quotient.args[1].args[0]


def get_numerateur(quotient):
    if isinstance(quotient.args[0], Pow) and quotient.args[0].args[1] == -1:
        return quotient.args[1]
    elif isinstance(quotient.args[1], Pow) and quotient.args[1].args[1] == -1:
        return quotient.args[0]


def elts_correction_ineq_quotient_poly(ineq):
    lignes = [r"inéquation : $" + latex(ineq) + r"$.\\"]
    numerateur = get_numerateur(ineq.args[0])
    denominateur = get_denominateur(ineq.args[0])
    lignes.append(r"signe de $" + latex(numerateur) + r"$ :\\")
    lignes.append(signe_poly_snd_deg(numerateur))
    lignes.append(r"signe de $" + latex(denominateur) + r"$ :\\")
    lignes.append(signe_poly_snd_deg(denominateur))
    lignes.append(solutions_ineq(ineq))
    return "\n".join(lignes)


def corrige_equ_poly_snd_deg(eq):
    lignes = [r"équation : $" + latex(eq) + r"$.\\"]
    if eq.args[0] != 0 and eq.args[1] != 0:
        eq = Eq(eq.args[0] - eq.args[1], 0)
        lignes.append("Cette équation équivaut à : $" + latex(eq) + r"$.\\")
    elif eq.args[1] != 0:
        eq = Eq(eq.args[1], 0)
        lignes.append("Cette équation équivaut à : $" + latex(eq) + r"$.\\")
    lignes.append(corrige_racines_reelles_poly_snd_deg(eq.args[0]))
    correction = "\n".join(lignes)
    correction += presente_solutions(eq)
    return correction


def corrige_factorisation_fact_comm_1(som):
    # implicitement les sommes sont du type kF-kG
    # il faudrait généraliser
    correction = r"Je reconnais un facteur commun.\\" "\n"
    formule = r"$\phantom{=}" + latex(som) + r"\\" "\n" + "="
    A = som.args[0].args[0]
    B = som.args[0].args[1]
    C = som.args[1].args[0]
    D = som.args[1].args[1]
    E = som.args[1].args[2]
    if A in [C, D, E]:
        fact_commun = A
        if A == C:
            formule += latex(Mul(A, Add(B, Mul(D, E), evaluate=False), evaluate=False))
            complement = B + D * E
        elif A == D:
            formule += latex(Mul(A, Add(B, Mul(C, E), evaluate=False), evaluate=False))
            complement = B + C * E
        else:  # A=E
            formule += latex(Mul(A, Add(B, Mul(C, D), evaluate=False), evaluate=False))
            complement = B + C * D
    else:
        fact_commun = B
        if B == C:
            formule += latex(Mul(B, Add(A, Mul(D, E), evaluate=False), evaluate=False))
            complement = A + D * E
        elif B == D:
            formule += latex(Mul(B, Add(A, Mul(C, E), evaluate=False), evaluate=False))
            complement = A + C * E
        else:  # B=E
            formule += latex(Mul(B, Add(A, Mul(C, D), evaluate=False), evaluate=False))
            complement = A + C * D
    formule += r"\\" "\n"
    formule += "=" + latex(fact_commun * complement) + "$" "\n"
    correction += formule
    return correction


def corrige_factorisation_fact_comm_2(p):
    # implicitement les sommes sont du type ax**2+b*x
    # il faudrait généraliser
    lignes = r"Je reconnais un facteur commun.\\" "\n"
    var = variable(p)
    coeffs = coefficients_polynome(p)
    a = coeffs[var ** 2]
    b = coeffs[var]
    formule = r"$\phantom{=}" + latex(p) + r"\\" "\n"
    formule += "=" + latex(Mul(var, (a * var + b), evaluate=False)) + r"\\" "\n"
    formule += "=" + latex(p.factor())
    formule += "$"
    lignes += formule
    return lignes


def corrige_factorisation_IR1_IR2(ir):
    r"""
    factorisation pas à pas de ax**2+-2abx+ab**2
    fc.
    """
    var = variable(ir)
    coeffs = coefficients_polynome(ir)
    a = coeffs[var ** 2]
    coeffs_norm = coefficients_polynome(ir / a)
    b = coeffs_norm[var] / 2
    lignes = r"$\phantom{=}" + latex(ir) + r"\\" "\n"
    if a != 1:
        lignes += "=" + latex(Mul(a, ir / a, evaluate=False)) + r"\\" "\n"
        lignes += "=" + latex(
            Mul(
                a,
                Add(
                    var ** 2,
                    Mul(2, b, var, evaluate=False),
                    Pow(b, 2, evaluate=False),
                    evaluate=False,
                ),
                evaluate=False,
            ),
            mul_symbol="times",
        )
        lignes += "=" + latex(
            Add(
                var ** 2,
                Mul(2, b, var, evaluate=False),
                Pow(abs(b), 2, evaluate=False),
                evaluate=False,
            ),
            mul_symbol="times",
        )
    lignes += "$" + r"\\" "\n"
    lignes += r"Je reconnais une identité remarquable. \\" "\n"
    lignes += "$" + latex(ir)
    if a != 1:
        lignes += "=" + latex(a * (var + b) ** 2)
    else:
        lignes += "=" + latex((var + b) ** 2)
    lignes += "$"
    return lignes


def corrige_factorisation_IR3(ir):
    r"""
    factorisation pas à pas de A**2-B**2.
    """
    # Hypothèse est faite que ir = A**2-B**2.*
    var = variable(ir)
    # On étudie le premier terme
    if isinstance(ir.args[0], Number) and ir.args[0] < 0:
        B = sqrt(-ir.args[0])
    elif isinstance(ir.args[0], Number):
        A = sqrt(ir.args[0])
    elif isinstance(ir.args[0], Pow):  # c'est donc un carré explicite
        A = ir.args[0].args[0]
    elif isinstance(ir.args[0], Mul):  # c'est donc -B**2
        B = ir.args[0].args[1].args[0]  # args[0] = (* -1 ,(** B, 2)
    # On étudie le deuxième terme
    if isinstance(ir.args[1], Number) and ir.args[1] < 0:
        B = sqrt(-ir.args[1])
    elif isinstance(ir.args[1], Number):
        A = sqrt(ir.args[1])
    elif isinstance(ir.args[1], Pow):  # c'est donc un carré explicite
        A = ir.args[1].args[0]
    elif isinstance(ir.args[1], Mul):  # c'est donc -B**2
        B = ir.args[1].args[1].args[0]  # args[0] = (* -1 ,(** B, 2)
    # bricolages pour éviter les erreurs de parenthésage et l'ordre
    # discutable des sorties latex

    # lignes = r'${}\left({}\right)^2-\left({}\right)^2$\\''\n'\
    # .format(latex(ir),latex(A),latex(B))
    lignes = r"Je reconnais une identité remarquable. \\" "\n"

    lignes += (
        r"$\phantom{{=}}\left({}\right)^2-\left({}\right)^2".format(latex(A), latex(B))
        + r"\\"
        "\n"
    )

    formule = r"=\left[\left({0}\right)-\left({1}\right)\right] "
    formule += r"\times \left[\left({0}\right)+\left({1}\right)\right]"
    lignes += formule.format(latex(A), latex(B)) + r"\\" "\n"
    if isinstance(B, Number) or coefficients_polynome(B)[var] > 0:
        formule = r"=\left({0}{1}\right)\left({0}+{2}\right)"
    else:  # hypothèse est faite que B = -k*var+p
        formule = r"=\left({0}+{1}\right)\left({0}{2}\right)"
    formule += r"\\" "\n"
    lignes += formule.format(latex(A), latex(-B), latex(B))
    formule = r"=\left({0}\right)\left({1}\right)" + r"\\" "\n"
    lignes += formule.format(latex(A - B), latex(A + B))
    lignes += "=" + latex(Mul(A - B, A + B, evaluate=False))
    lignes += "$"
    return lignes


def get_alpha_beta_fc(fc):
    r"""
    renvoie le couple (coefficient alpha, coefficient beta)
    """
    if isinstance(fc, Pow):
        beta = 0
        try:
            alpha = -fc.args[0].args[0]
        except:
            alpha = 0
    elif isinstance(fc, Mul):
        beta = 0
        try:
            alpha = -fc.args[1].args[0].args[0]
        except:
            alpha = 0
    else:
        beta = fc.args[0]
        alpha = get_alpha_beta_fc(fc.args[1])[0]

    return (alpha, beta)


def corrige_extremum_fc(fc):
    r"""
    lecture de l'extremum d'un polynôme du second degré écrit sous forme
    canonique.
    """
    lignes = "$" + latex(fc) + "$" + r"\\" "\n"
    lignes += r"Je reconnais une forme canonique. \\" "\n"
    alpha, beta = get_alpha_beta_fc(fc)
    var = variable(fc)
    a = coefficients_polynome(fc)[var ** 2]
    if a > 0:
        signe_a = "positif"
        type_extremum = "minimum"
    else:
        signe_a = "négatif"
        type_extremum = "maximum"
    lignes += (
        r"Je constate que le coefficient des ${}^2$ "
        "($a = {}$) est {}"
        "\n".format(latex(var), latex(a), signe_a)
    )
    lignes += (
        r"La fonction admet donc un {} : ${}$ "
        "atteint pour ${}={}$"
        "\n".format(type_extremum, *map(latex, (beta, var, alpha)))
    )
    return lignes


def corrige_factorisation_fc(fc):
    r"""
    factorisation pas à pas quand c'est possible de la forme canonique
    fc.
    """
    lignes = "$" + latex(fc) + "$" + r"\\" "\n"
    if isinstance(fc, Pow) or isinstance(fc, Mul):
        lignes += r"Cette expression est déjà factorisée " r"sur $\mathbb{R}$.\\" "\n"
        return lignes
    else:
        lignes += r"Je reconnais une forme canonique.\\" "\n"
    var = variable(fc)
    polynome = Poly(fc, var)
    if polynome.real_roots() == []:
        if fc.subs(var, 0) < 0:
            extremum_type = "maximum"
        else:
            extremum_type = "minimum"
        extremum = fc.as_coeff_Add()[0]
        lignes += r"Ce polynôme du second degré a pour {} ${}$. ".format(
            extremum_type, extremum
        )
        lignes += r"Il ne s'annule donc pas sur $\mathbb{{R}}$."
        # if fc.subs(var,0) < 0 :
        # signe = u'négative'
        # else :
        # signe = 'positive'
        # ligne = r"Cette expression est strictement {} sur "\
        # r"$\mathbb{{R}}$. Elle ne s'annule pas sur $\mathbb{{R}}$,"
        # lignes += ligne.format(signe)
        # lignes += ur' donc cette expression ne se factorise pas sur '\
        #'$\mathbb{R}$.'
    else:
        lignes += r"Cette expression se factorise sur $\mathbb{R}$ :\\"
        lignes += "\n"
        formule = r"$\phantom{=}" + latex(fc) + r"\\" "\n"
        c = fc.args[0]
        if isinstance(fc.args[1], Mul):
            a = fc.args[1].args[0]
            carre = fc.args[1].args[1]
            formule += (
                "="
                + latex(Mul(a, Add(carre, c / a, evaluate=False), evaluate=False))
                + r"\\"
                "\n"
            )
            # bricolage pour éviter l'autoévaluation
            formule += "=" + latex(a) + r"\left(" + latex(carre)
            formule += "-"
            formule += latex(Pow(sqrt(-c / a), 2, evaluate=False)) + r"\right)\\" "\n"
            formule += (
                "="
                + latex(
                    Mul(
                        a,
                        Add(carre.args[0], sqrt(-c / a), evaluate=False),
                        Add(carre.args[0], -sqrt(-c / a), evaluate=False),
                        evaluate=False,
                    )
                )
                + r"\\"
                "\n"
            )
            c1 = carre.args[0].args[0]
            if Add(c1, sqrt(-c / a), evaluate=False) != c1 + sqrt(-c / a):
                formule += "=" + latex(
                    Mul(
                        a,
                        var + c1 - sqrt(-c / a),
                        var + c1 + sqrt(-c / a),
                        evaluate=False,
                    )
                )
            formule += "$"
        else:  # Le cas des carrés a été traité plus haut.
            carre = fc.args[1]
            # bricolage pour éviter l'auto-évaluation
            formule += "=" + latex(carre)
            formule += "-"
            formule += latex(Pow(sqrt(-c), 2, evaluate=False)) + r"\\" "\n"
            formule += "=" + latex(
                Mul(
                    Add(carre.args[0], sqrt(-c), evaluate=False),
                    Add(carre.args[0], -sqrt(-c), evaluate=False),
                    evaluate=False,
                )
            )
            c1 = carre.args[0].args[0]
            if Add(c1, sqrt(-c), evaluate=False) != c1 + sqrt(-c):
                formule += r"\\" "\n=" + latex(
                    Mul(var + c1 - sqrt(-c), var + c1 + sqrt(-c), evaluate=False)
                )
            formule += "$"
        lignes += formule
    return lignes


def corrige_factorisation_racine(polynome, racine):
    """
    Corrige la factorisation d'un polynôme dont on connait une racine
    """
    var = variable(polynome)
    degre = polynome.as_poly(var).degree()
    # Je traduit le polynome et la variable vers mon calcul formel perso
    math_expr = str_to_math_expr(str(polynome))
    var_m_e = str_to_math_expr(str(var))
    calcul = math_expr.subs(var_m_e, racine)
    reponse = "$" + calcul.to_infix().replace("*", r"\times")
    while not calcul == calcul.evaluate_one_step():
        calcul = calcul.evaluate_one_step()
        reponse += " = " + calcul.to_infix().replace("*", r"\times") + "$"
    reponse += r"\\" "\n"
    a, b, c = symbols("a,b,c")
    facteur_poly = Add(
        *[
            coeff * monome
            for coeff, monome in zip([a, b, c], [var ** 2, var, 1][3 - degre :])
        ]
    )
    reponse += (
        r"${0}$ est bien une racine de ${1}$, donc ${1}"
        r"=\left({2}\right)\left({3}\right)$.\\"
        "\n".format(
            latex(racine), latex(polynome), latex(var - racine), latex(facteur_poly)
        )
    )
    reponse += r"En développant le second membre on obtient :\\" "\n"
    produit = ((var - racine) * facteur_poly).as_poly(var)
    reponse += "${}$\\" "\n".format(latex(produit.as_expr(), order="grlex"))
    coeffs1 = produit.all_coeffs()
    coeffs2 = polynome.as_poly(var).all_coeffs()
    reponse += (
        r"En identifiant les coefficients des deux polynômes "
        r"égaux, on obtient les équations :\\"
        "\n"
    )
    for coeff1, coeff2 in zip(coeffs1, coeffs2):
        reponse += "${}={}$\n".format(latex(coeff1), latex(coeff2))
    reponse += r"Ce qui donne finalement :" "\n"
    quotient = (polynome / (var - racine)).simplify()
    coeffs3 = Poly(quotient, var).all_coeffs()
    egalites = [
        "${}={}$".format(inconnue, valeur)
        for inconnue, valeur in zip([a, b, c], coeffs3)
    ]
    reponse += ", ".join(egalites[:-1]) + " et " + egalites[-1] + "\\" "\n"
    reponse += r"Par conséquent : ${}=\left({}\right)\left({}\right)$".format(
        *map(latex, [polynome, var - racine, quotient])
    )
    return reponse


def est_monome_deg(monome, var):
    if monome == var:
        return True
    elif isinstance(monome, Pow) and monome.args[0] == var:
        return True
    elif isinstance(monome, Mul) and (
        est_monome_deg(monome.args[0], var) or est_monome_deg(monome.args[1], var)
    ):
        return True
    else:
        return False


def a_facteur_commun_deg_1(somme):
    var = variable(somme)
    res = True
    for arg in somme.args:
        res = res and est_monome_deg(arg, var)
    return res


def est_carre(expr):
    if isinstance(expr, Pow) and expr.args[1] == 2:
        return True
    elif isinstance(expr, Number) and expr > 0:
        return True
    else:
        return False


def est_difference(expr):
    if not isinstance(expr, Add):
        return False
    elif isinstance(expr.args[1], Mul) and expr.args[1].args[0] == -1:
        return True
    elif isinstance(expr.args[0], Mul) and expr.args[0].args[0] == -1:
        return True
    elif isinstance(expr.args[0], Number) and expr.args[0] < 0:
        return True
    elif isinstance(expr.args[1], Number) and expr.args[1] < 0:
        return True
    else:
        return False


def est_IR3(expr):
    if not est_difference(expr):
        return False
    elif est_carre(expr.args[0]):
        if isinstance(expr.args[1], Number) and expr.args[1] < 0:
            return True
        elif est_carre(expr.args[1].args[1]):
            return True
        else:
            return False
    elif est_carre(expr.args[1]):
        if isinstance(expr.args[0], Number) and expr.args[0] < 0:
            return True
        elif est_carre(expr.args[0].args[1]):
            return True
        else:
            return False
    else:
        return False


def corrige_factorisation(somme):
    var = variable(somme)
    coeffs = coefficients_polynome(somme)
    coeff_deg_0 = coeffs[1]
    # des différences de carrés ou les constantes s'annulent amenent
    # des "surdétection" de facteurs communs.
    if a_facteur_commun_deg_1(somme):
        return corrige_factorisation_fact_comm_2(somme)
    elif (
        isinstance(somme, Add)
        and isinstance(somme.args[0], Mul)
        and isinstance(somme.args[0], Mul)
        and set(somme.args[0].args).intersection(set(somme.args[0].args)) != set()
    ):
        return corrige_factorisation_fact_comm_1(somme)
    elif (coeffs[var] / (2 * coeffs[var ** 2])) ** 2 == coeffs[1] / coeffs[var ** 2]:
        return corrige_factorisation_IR1_IR2(somme)
    elif est_IR3(somme):
        return corrige_factorisation_IR3(somme)
    else:  # hypothèse est faite que somme est une forme canonique
        return corrige_factorisation_fc(somme)


def elements_correction_factorisation(p):
    # lignes=[r" équation : ${}=0$\\".format(latex(p))]
    lignes = []
    var = variable(p)
    # try :
    # p = Poly(p,var)
    # except :
    # print('erreur de Poly')
    # print(p)
    if p.expand() != 0:
        racines = set(p.as_poly().real_roots())
        if racines != set([]):
            lignes.append(r" factorisation :\\")
            # for i in xrange(8):
            # lignes.append(r"\ \\")
            lignes.append(r"${}=0$\\".format(latex(p.factor())) + r"\\")
        else:
            # for i in xrange(8):
            # lignes.append(r"\ \\")
            if p.subs(var, 0) > 0:
                signe = "positif"
            else:
                signe = "négatif"
            lignes.append(
                r" Le polynôme ${}$ est toujours strictement {} sur $\mathbb{{R}}$.\\".format(
                    latex(p), signe
                )
            )
        lignes.append(
            r" ensemble des solutions réelles : ${}$\\".format(latex(racines)) + r"\\"
        )
        # lignes.append(r"Résolution :")
        # for i in xrange(6):
        # lignes.append(r"\ \\")
    else:
        lignes.append(r"L'équation est vérifiée sur $\mathbb{R}$")
    return "\n".join(lignes)


def corrige_forme_factorisee(nom_poly, racines):
    correction = "Je connais les racines "
    return correction


def tableau_variations_fc(nom_fc, fc):
    lignes = []
    var = variable(fc)
    a = coefficients_polynome(fc)[var ** 2]
    alpha, beta = get_alpha_beta_fc(fc)
    lignes.append(r"\begin{tikzpicture}")
    lignes.append(
        r"\tkzTabInit[lgt=2,espcl=1]"
        r"{{ ${}$ / 1, variations de ${}$/ 2}}".format(var, nom_fc)
    )

    lignes.append(r"{{ $-\infty$  , ${}$, $+\infty$ }}".format(latex(alpha)))
    if a > 0:
        lignes.append(r"\tkzTabVar{{+/ ,-/ ${}$, +/}}".format(latex(beta)))
    else:
        lignes.append(r"\tkzTabVar{{-/ ,+/ ${}$, -/}}".format(latex(beta)))
    lignes.append(r"\end{tikzpicture}")
    return "\n".join(lignes)


def question_image_fct_carree(val):
    q = {}
    q["consigne"] = r"Déterminer l'image par la fonction carrée de : "
    q["enonce"] = latex(val, mode="inline", fold_short_frac=False)
    q["corrige"] = latex(val * val, mode="inline", fold_short_frac=False)
    return q


def question_comparaison_images_fct_carree(val1, val2):
    q = {}
    q["consigne"] = r"Sans effectuer aucun calcul, comparer : "
    q["enonce"] = "${}$ et ${}$".format(presente_carre(val1), presente_carre(val2))
    if val1 ** 2 > val2 ** 2:
        q["corrige"] = "${} > {}$".format(presente_carre(val1), presente_carre(val2))
    else:
        q["corrige"] = "${} < {}$".format(presente_carre(val1), presente_carre(val2))
    return q


def question_encadrement_carre(xmin=None, xmax=None):
    q = {}
    q["consigne"] = r"Sachant que {} que peut-on dire de $x^2$ ?".format(
        presente_encadrement(xmin, x, xmax)
    )
    q["enonce"] = ""
    return q


def question_variations_fc(nom_fc, fc):
    q = {}
    var = variable(fc)
    q["consigne"] = r"Construire le tableau de variations du polynôme" " suivant :"
    q["enonce"] = r"${}\left({}\right)={}$".format(
        nom_fc, var, latex(fc, fold_short_frac=False)
    )
    q["corrige"] = tableau_variations_fc(nom_fc, fc)
    return q


# def question_extremum(forme_canonique):
# q={}
# q['consigne'] = r""
# q['enonce']=''
# return q

if __name__ == "__main__":
    for k in range(5):
        print(latex(fsd_fc()))
    # print(get_alpha_beta_fc(3*(x+Rational(2,3))**2-6))
    # print(get_alpha_beta_fc(x**2-5))
    # print(get_alpha_beta_fc(x**2))
    # print(get_alpha_beta_fc((x-2)**2+5))
    print(latex(3 * (x + Rational(2, 3)) ** 2 - 6))
    print(corrige_extremum_fc(3 * (x + Rational(2, 3)) ** 2 - 6))
    print(corrige_extremum_fc(-3 * (x + Rational(2, 3)) ** 2 - 6))
    print(corrige_extremum_fc(x ** 2 - 5))
    print(corrige_extremum_fc(x ** 2))
    print(exercice_premieres_equations()["enonce"])
    polynomes = [
        x ** 2 + 2 * x - 3,
        x ** 2,
        2 * x ** 2,
        3 * x ** 2 - 2,
        -2 * x ** 2 + 4 * x - 3,
    ]
    for p in polynomes:
        print(latex(p) + " : ")
        print(latex(forme_canonique(p)))
        print(corrige_racines_reelles_poly_snd_deg(p))
    # print(question_image_fct_carree(Rational(2,3)))
    # print(question_comparaison_images_fct_carree(Rational(2,3),Rational(5,3)))
    # print(r"\begin{enumerate}")
    # print(r"\item" + '$' + latex(1-2*x<=(3/(x+2))) + '$')
    # print("Signe de " + "$" + latex(2*x**2+3*x+1) + "$")
    # print(signe_poly_snd_deg(2*x**2+3*x+1))
    # print("")
    # print(solutions_ineq(1-2*x<=(3/(x+2))))
    # print(r"\item" + '$' + latex(1/(x+1)>(2/(x+2))) + '$')
    # print("Signe de " + "$" + latex(x**2+3*x+2) + "$")
    # print(signe_poly_snd_deg(x**2+3*x+2))
    # print("")
    # print(solutions_ineq(1/(x+1)>(2/(x+2))))
    # print(r"\item" + '$' + latex(x/(x-1)>((2*x-1)/(x+3))) + '$')
    # print("Signe de " + "$" + latex(-x**2+6*x-1) + "$")
    # print(signe_poly_snd_deg(-x**2+6*x-1))
    # print("Signe de " + "$" + latex(x**2+4*x+3) + "$")
    # print(signe_poly_snd_deg(-x**2+6*x-1))
    # print("")
    # print(solutions_ineq(x/(x-1)>((2*x-1)/(x+3))))
    # print(r"\end{enumerate}")
    # print(r"\begin{enumerate}")
    # print(r"\item " + elts_correction_ineq_quotient_poly()
    # (2*x**2-x-1)/(x**2-x-6)>0)
    # print(r"\item " + elts_correction_ineq_quotient_poly()
    # x**2/(x**2+6*x-2)>0)
    # print(r"\item " + signe_poly_snd_deg(x**2-3*x+3))
    # print(r"\end{enumerate}")

    # print(r"\begin{enumerate}")
    # print(r"\item "+corrige_ineq_poly_snd_deg(x**2+6*x-1>=0))
    # print(r"\item "+corrige_ineq_poly_snd_deg(3*x**2-x+3<=6*x+1))
    # print(r"\item "+corrige_ineq_poly_snd_deg(5*x**2-6*x-20<-x**2+6*x+4))
    # print(r"\item "+elts_correction_ineq_prod_poly()
    # (3*x**2+15*x-18)*(x**2+12*x+36)>0)
    # print(r"\item"+elts_correction_ineq_prod_poly()
    # (x**2-x-2)*(9*x**2-9*x-54)<0)
    # print(r"\end{enumerate}")
    # print(corrige_canonisation(-Rational(1,2)*l**2+2*l+30))
    # print(est_difference(3-x))
    # print(est_difference(x-3))
    # print(est_IR3((3+x)**2-x))
    # print(est_IR3((3+x)**2-16))
    # print(est_IR3((3+x)**2+5))
    # print(est_IR3(5-(3+x)**2))
    # print(est_IR3((5-3*x)**2-(3+x)**2))
    # print(est_carre((3+x)**2))
    # print(est_monome_deg(3,x))
    # print(est_monome_deg(3*x,x))
    # print(est_monome_deg(3*x**2,x))
    # print(est_monome_deg(3+x,x))
    # print(a_facteur_commun_deg_1(3+x))
    # print(a_facteur_commun_deg_1(x**2-3*x))
    # print(a_facteur_commun_deg_1(3*x**2-x))
    # print(coefficients_polynome(z**2-3*z))
    # print(coefficients_polynome(2*t**2))
    # print(corrige_factorisation(z**2-3*z))
    # print(corrige_factorisation(z**2-6*z+9))
    # print(corrige_factorisation((x+2)**2-(2*x+2)**2))
    # p=(x+2)**2-(2*x+2)**2
    # expression = str_to_math_expr(str(p))
    # print(expression.to_infix())
    # var = str_to_math_expr('x')
    # print((expression.subs(var,-2)).to_infix())
    # p=((2*x-1)*(x+2)).expand()
    # print(p)
    # print(corrige_factorisation_racine(p,-2))
    # p=((x+1)*(2*x-1)*(x+2)).expand()
    # print(p)
    # print(corrige_factorisation_racine(p,-1))
    # print(corrige_factorisation((t - 5)**2 - 25))
    # print(corrige_factorisation(17-(t - 5)**2))
    # print(corrige_factorisation((t - 5)**2 - (2*t-7)**2))
    # print(corrige_factorisation(27*R**2 + 90*R + 75))
    # print(latex((y-1)*(3*y-5)-(y-1)*(2*y-4)))
    # print(corrige_factorisation((y-1)*(3*y-5)-(y-1)*(2*y-4)))
    # print(corrige_canonisation(2*y**2-3*y+2))
    # print(corrige_equ_poly_snd_deg(Eq(-Rational(1,2)*l**2+2*l+30,25)))
    ### !!! corrige_factorisation(-(y-1)*(2*y-4)+(y-1)*(3*y-5)) provoque
    ### une erreur
    # for k in xrange(12) :
    # ir = fsd_ir3()
    # print(corrige_factorisation_IR3(ir))
    # p = fd_fc()
    # print(corrige_factorisation_fact_comm_2(p))
    # p = fsd_fc()
    # print(corrige_factorisation_fact_comm_1(p))
    # print(corrige_factorisation_IR1_IR2(4*x**2-40*x+100))
    # print(corrige_factorisation_IR1_IR2(3*x**2+12*x+12))
    # print(corrige_factorisation_IR1_IR2(x**2+2*x+1))
    # print(corrige_factorisation_fc((x+3)**2))
    # print(corrige_factorisation_fc(2*(x+3)**2-5))
    # print(corrige_factorisation_fc((x+3)**2-16))
    # print(corrige_factorisation_fc(2*(x+3)**2-50))
    # a,b,c = symbols('a,b,c')
    # p = a*R**2+b*R+c
    # print(coefficients_polynome(p))
    # p = a*R**2+2*sqrt(3)*R+4
    # print(coefficients_polynome(p))
    # p = a*R**2+2*sqrt(3)*R+4-2*R
    # print(coefficients_polynome(p))
    # print(corrige_canonisation(-36*z**2 - 120*z - 112))
    # print(corrige_canonisation(3*x**2+2*x-5))
    # print(corrige_canonisation(x**2+3*x-5))
    # print(set_to_latex()
    # solve_univariate_inequality(R>0,R,relational=False))
    # compileDocument('1STI2D_corrige_DS1',generer_corrige_ds(devoir))
    # STI2D1 = Donnees['classes'][0]
    # dm={'num':1,'classe':u'1ère E','date' : 'Lundi 11 septembre 2017'}
    # dm['eleves']=[]
    # for el in STI2D1['eleves'] :
    # _equations = equations()
    # _inequations = inequations()
    # dm['eleves'].append({'prenom': el['prenom'], 'nom': el['nom'],
    #'equations': [latex(eq,mode='inline') \
    # for eq in _equations],
    #'inequations': [latex(ineq,mode='inline') \
    # for ineq in _inequations]})
    # compileDocument('1S_DM1',generer_dm_1_1STI2D(dm))
    # print(corrige_racines_reelles_poly_snd_deg(x**2+2.2*x-6))
    # print(equations())
    # print(inequations())
    # print('INEQ3 :')
    # for k in xrange(10) :
    # rel = ineq3()
    ##print(rel)
    ##print(signe_poly_snd_deg(rel.args[0]))
    # print(solutions_ineq(rel))
    # print('INEQ4 :')
    # for k in xrange(10) :
    # rel = ineq4()
    ##print(rel)
    ##print(signe_poly_snd_deg(rel.args[0]))
    # print(solutions_ineq(rel))
    # print('INEQ5 :')
    # for k in xrange(10) :
    # rel = ineq5()
    ##print(rel)
    ##print(signe_poly_snd_deg(rel.args[0]))
    # print(solutions_ineq(rel))
