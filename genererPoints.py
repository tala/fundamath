#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
from random import randint, shuffle, choice, randrange
from fractions import Fraction, gcd as pgcd


def InitPoints(minimum=-6.1, maximum=6.1, nbval=3):
    dY = []
    # une tangente horizontale, des montantes et des descendantes
    directions = [(-1) ** i for i in range(nbval - 1)]
    directions.append(0)
    shuffle(directions)
    for i in range((nbval - 1) // 2):
        while True:
            a, b = randrange(1, 5), randrange(1, 5)
            # a et b sont premiers entre eux et compris entre 1 et 4
            # (1,2) ; (1,3) ; (1,4) ; (2,3) ; (3,2) ; (3,4) ; (4,3).
            if pgcd(a, b) == 1 and a % b != 0:
                dY.append(Fraction(a, b))
                break
    for i in range(nbval - 1 - len(dY)):
        # on ajoute des entiers entre 1 et 4
        dY.append(randrange(1, 5))
    print(dY)
    shuffle(dY)
    for i in range(nbval):
        if directions[i] == 0:
            dY.insert(i, 0)
        else:
            dY[i] = dY[i] * directions[i]
    # on ajoute des entiers entre une pente nulle au début et une autre
    # à la fin
    dY.insert(0, 0)
    dY.append(0)
    print(dY)
    #  Que font les lignes suivantes ???
    redo = True
    while redo:
        lX = [
            int(minimum)
            + 1
            + int(1.0 * i * (maximum - minimum - 2) / nbval)
            + randrange(4)
            for i in range(nbval)
        ]
        for i in range(len(lX) - 1):
            if lX[i + 1 :].count(lX[i]):
                redo = True
                break
            else:
                redo = False
    lX.insert(0, minimum)
    lX.append(maximum)
    lX.sort()
    lY = [randrange(-4, 5)]
    for i in range(1, len(lX)):
        inf = lY[-1] - 4 * int(lX[i] - lX[i - 1])
        sup = lY[-1] + 4 * int(lX[i] - lX[i - 1])
        nY = randrange(int(max(-4, inf)), int(min(5, sup)))
        lY.append(nY)
        # ======================================================================
        # while True:
        #     lg = randrange(1, 4) * (-1) ** randrange(2) * int(lX[i] - lX[i - 1])
        #     nY = lY[-1] + lg
        #     if -4 <= nY <= 4 :
        #         lY.append(nY)
        #         break
        # ======================================================================
    return zip(lX, lY, dY)

if __name__ == "__main__":
    print(InitPoints(-5, 5, 4))
