#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
from sympy.abc import x

from sympy import latex, Poly, ln, Add, Mul, Pow, oo
from sympy.solvers.inequalities import (
    solve_poly_inequality,
    solve_poly_inequalities,
    PolynomialError,
)

from sympy import latex, log

from random import sample, choice, randint, shuffle

premiers = [2, 3, 5, 7, 11, 13, 17]

SAVOIRS_FAIRES = [
    "Déterminer le domaine de définition d'une expression variable comprenant un logarithme",
    "",
]


def domaine_de_definition(expr):
    domaine = None
    if expr.func == ln:
        expr2 = expr.args[0]
        try:
            domaine = solve_poly_inequality(Poly(expr2), ">")
        except PolynomialError:
            if (
                expr2.func == Mul
                and expr2.args[0].func == Pow
                and expr2.args[0].args[1] == -1
            ):
                A = expr2.args[1]
                B = expr2.args[0].args[0]
                # L'expression est du type ln(A/B)
                domaine = solve_poly_inequality(Poly(A) * Poly(B), ">")
    elif expr.func == Add:
        A = expr.args[0]
        B = expr.args[1]
        dA = domaine_de_definition(A)
        dB = domaine_de_definition(B)
        if len(dA) == 1 and len(dB) == 1:
            domaine = [dA[0].intersection(dB[0])]
    return domaine


def intervalle_to_latex(intervalle):
    a = intervalle.start
    b = intervalle.end
    if a in intervalle:
        formule = r"\left[" + latex(a)
    else:
        formule = r"\left]" + latex(a)
    if b == oo:
        borneSup = r"+\infty"  # petit désaccord avec sympy
    else:
        borneSup = latex(b)
    if b in intervalle:
        formule += ";" + borneSup + r"\right]"
    else:
        formule += ";" + borneSup + r"\right["
    return formule


def liste_intervalles_to_latex(liste):
    return r"\bigcup".join(map(intervalle_to_latex, liste))


def exercice_inequations():
    lignes = []
    lignes.append(
        "Résoudre les équations et inéquations suivantes (penser aux domaines de définition):"
    )
    expressions = []
    affine1 = choice([1, -1]) * randint(1, 5) * x + choice([1, -1]) * randint(1, 5)
    expressions.append(r"$\ln\left(" + latex(affine1) + r"\right)\leqslant 0$")
    # lignes.append(liste_intervalles_to_latex(domaine_de_definition(ln(affine1))))
    affine2 = choice([1, -1]) * randint(1, 5) * x + choice([1, -1]) * randint(1, 5)
    while affine2 == affine1:
        affine2 = choice([1, -1]) * randint(1, 5) * x + choice([1, -1]) * randint(1, 5)
    expressions.append(r"$\ln\left(" + latex(affine2) + r"\right)\geqslant 1$")
    a = -1 * randint(1, 5)
    b = randint(1, 5)
    poly1 = ((x - a) * (x - b)).expand()
    termD = poly1.replace(x, 0)
    termG = poly1 - termD
    expressions.append(
        r"$\ln\left("
        + latex(termG)
        + r"\right)= \ln\left("
        + latex(-termD)
        + r"\right)$"
    )
    shuffle(expressions)
    lignes.append(" ; ".join(expressions[:-1]) + " et " + expressions[-1])
    return (r"\\" + "\n").join(lignes)


def exercice_derivees():
    lignes = []
    lignes.append(
        "Pour chacune des fonctions suivantes, déterminer son domaine de définition puis calculer sa dérivée :"
    )
    expressions = []
    affine1 = choice([1, -1]) * randint(1, 5) * x + choice([1, -1]) * randint(1, 5)
    n1 = randint(2, 4)
    expr1 = affine1 ** n1
    expressions.append(latex(expr1) + "$")
    affine2 = choice([1, -1]) * randint(1, 5) * x + choice([1, -1]) * randint(1, 5)
    n2 = randint(2, 4)
    expr2 = affine2 ** -n2
    expressions.append(latex(expr2) + "$")
    affine3 = choice([1, -1]) * randint(1, 5) * x + choice([1, -1]) * randint(1, 5)
    expr3 = ln(affine3)
    expressions.append(r"\ln\left(" + latex(affine3) + r"\right)$")
    expressions.append(
        choice([r"\dfrac{\ln (x)}{x}$", r"x \ln (x)$", r"\cos(x)\ln(x)$"])
    )
    shuffle(expressions)
    fonctions = ["$f(x)", "$g(x)", "$h(x)", "$l(x)"]
    couples = list(zip(fonctions, expressions))
    enumeration = (
        ", ".join(["=".join(cpl) for cpl in couples[:-1]])
        + " et "
        + "=".join(couples[-1])
    )
    lignes.append(enumeration)
    return (r"\\" + "\n").join(lignes)


def exercice_a_b():
    a, b = sample(premiers, 2)
    lna = round(float(log(a)), 3)
    lnb = round(float(log(b)), 3)
    ab = a * b
    lnab = round(float(log(ab)), 3)
    k = randint(2, 6)
    ak = a ** k
    lnak = round(float(log(ak)), 3)
    aqb = a / b
    lnaqb = round(float(log(aqb)), 3)
    inva = 1 / a
    lninva = round(float(log(inva)), 3)
    n = randint(2, 6)
    invbn = 1 / (b ** n)
    lninvbn = round(float(log(invbn)), 3)
    lignes = []
    lignes.append(
        r"On admet que $\ln({})\approx {}$ et $\ln({})\approx {}$.".format(
            a, lna, b, lnb
        )
    )
    lignes.append(
        r'En déduire, en détaillant vos calculs et sans utiliser la touche "ln" de votre calculatrice, les logarithmes suivants:'
    )
    questions_reponses = [
        [r"$\ln\left(" + str(a) + r"^" + str(k) + r"\right)$", lnak],
        [r"$\ln\left(" + str(ab) + r"\right)$", lnab],
        [r"$\ln\left(\dfrac{1}{" + str(a) + r"}\right)$", lninva],
        [r"$\ln\left(\dfrac{" + str(a) + r"}{" + str(b) + r"}\right)$", lnaqb],
        [r"$\ln\left(\dfrac{1}{" + str(b) + r"^" + str(n) + r"}\right)$", lninvbn],
    ]
    shuffle(questions_reponses)
    lignes.append(
        ", ".join([qr[0] for qr in questions_reponses[:-1]])
        + " et "
        + questions_reponses[-1][0]
    )
    return (r"\\" + "\n").join(lignes)


def corrige_exercice_a_b(a, b, k, n):
    rla = round(log(a), 3)
    rlb = round(log(b), 3)
    reponses = [
        r"$\ln\left({0}^{1}\right)={1}\times\ln\left({0}\right)"
        r"\approx {1}\times {2} \approx {3}$".format(
            a, k, round(log(a), 3), round(k * log(a), 3)
        ),
        r"$\ln\left({0}\right)=\ln\left({1}\times{2}\right)="
        r"\ln\left({1}\right)+\ln\left({2}\right)\approx {3}+{4}"
        r"\approx {5}$".format(a * b, a, b, rla, rlb, rla + rlb),
        r"$\ln\left(\dfrac{{1}}{{{0}}}\right)=-\ln\left({0}\right)"
        r"\approx - {1}$".format(a, rla),
        r"$\ln\left(\dfrac{{{0}}}{{{1}}}\right)=\ln\left({{{0}}}\right)"
        r"-\ln\left({{{1}}}\right)\approx {2}-{3} \approx {4}$".format(
            a, b, rla, rlb, rla - rlb
        ),
        r"$\ln\left(\dfrac{{1}}{{{0}^{1}}}\right)=-\ln\left({0}^{1}\right)"
        r"=-{1}\ln\left({0}\right)\approx -{1} \times {2}\approx {3}".format(
            b, n, rlb, -n * rlb
        ),
    ]
    return reponses


def exercice_primitives():
    lignes = []
    lignes.append("Pour chacune des fonctions suivantes, proposer une primitive :")
    expressions = []
    affine1 = choice([1, -1]) * randint(1, 5) * x + choice([1, -1]) * randint(1, 5)
    n1 = randint(2, 4)
    expr1 = affine1 ** n1
    expressions.append(latex(expr1) + "$")
    affine2 = choice([1, -1]) * randint(1, 5) * x + choice([1, -1]) * randint(1, 5)
    n2 = randint(2, 4)
    expr2 = affine2 ** -n2
    expressions.append(latex(expr2) + "$")
    affine3 = choice([1, -1]) * randint(1, 5) * x + choice([1, -1]) * randint(1, 5)
    expr3 = affine3 ** -1
    expressions.append(latex(expr3) + "$")
    poly1 = randint(1, 5) * x ** 2 + randint(1, 5)
    expr4 = x / poly1
    expressions.append(latex(expr4) + "$")

    shuffle(expressions)
    fonctions = ["$f(x)", "$g(x)", "$h(x)", "$l(x)"]
    couples = list(zip(fonctions, expressions))
    enumeration = (
        ", ".join(["=".join(cpl) for cpl in couples[:-1]])
        + " et "
        + "=".join(couples[-1])
    )
    lignes.append(enumeration)
    return (r"\\" + "\n").join(lignes)


def domaine_inequation(expr1, expr2):
    domaine = (-oo, oo)
    if hasattr(expr1, "func") and expr1.func == ln:
        domaine1 = domaine_de_definition(expr1)
    if hasattr(expr2, "func") and expr2.func == ln:
        domaine2 = domaine_de_definition(expr2)
        if len(domaine1) == 1 and len(domaine2) == 1:
            domaine = domaine1[0].intersection(domaine2[0])
    else:
        domaine = domaine1
    return domaine


def corrige_inequation(expr1, expr2):
    domaine = domaine_inequation(expr1, expr2)
    return domaine


devoirs = [
    [exercice_a_b(), exercice_inequations(), exercice_derivees(), exercice_primitives()]
    for i in range(30)
]
if __name__ == "__main__":
    for rep in corrige_exercice_a_b(7, 2, 6, 6):
        print(rep)
    print(corrige_inequation(ln(2 * x + 3), ln(3 - x)))
    print("")
    print(corrige_inequation(ln(2 * x + 3), 7))
    # print(devoirs)
    # print(exercice_a_b())
    # print(exercice_inequations())
    # print(exercice_derivees())
    # print(liste_intervalles_to_latex(
    # domaine_de_definition(ln((x-3)/(x+4)))))
