#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import
from .logarithmes import *
from sympy import ln, Interval, oo
from sympy.abc import x
import unittest


class logarithmesTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def test_domaine_de_definition(self):
        self.assertEqual(domaine_de_definition(ln(x + 2)), [Interval(-2, oo, True)])
        self.assertEqual(
            domaine_de_definition(ln(x ** 2 - 16)),
            [Interval(-oo, -4, True, True), Interval(4, oo, True)],
        )
        self.assertEqual(
            domaine_de_definition(ln((3 - x) * (x + 4))), [Interval(-4, 3, True, True)]
        )
        self.assertEqual(
            domaine_de_definition(ln((x - 3) * (x + 4))),
            [Interval(-oo, -4, True, True), Interval(3, oo, True)],
        )
        self.assertEqual(
            domaine_de_definition(ln((x - 3) / (x + 4))),
            [Interval(-oo, -4, True, True), Interval(3, oo, True)],
        )
        self.assertEqual(
            domaine_de_definition(ln(x - 3) + ln(x + 4)), [Interval(3, oo, True)]
        )

    def test_intervalle_to_latex(self):
        self.assertEqual(
            intervalle_to_latex(Interval(4, +oo, True)), r"\left]4;+\infty\right["
        )

    def test_liste_intervalles_to_latex(self):
        self.assertEqual(
            liste_intervalles_to_latex([Interval(4, +oo, True)]),
            r"\left]4;+\infty\right[",
        )
        self.assertEqual(
            liste_intervalles_to_latex([Interval(-oo, -2), Interval(4, +oo, True)]),
            r"\left]-\infty;-2\right]\bigcup\left]4;+\infty\right[",
        )


if __name__ == "__main__":
    unittest.main()
