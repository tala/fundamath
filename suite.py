#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

#  sans titre.py
#
#  Copyright 2017 talabardon <talabardon@talabardon-700T1C>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
from sympy import latex, symbols

n = symbols("n")


def terme(formule_explicite, indice):
    reponse = ur"U_{{{}}}".format
    return formule_explicite.subs(n, indice)


def main(args):
    return 0


if __name__ == "__main__":
    print terme(n ** 2, 4)
    import sys

    sys.exit(main(sys.argv))
