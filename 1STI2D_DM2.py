#!/usr/bin/python
# -*- coding: utf-8 -*-
from sympy import Rational
from random import choice
from secondDegre import inequations, equations
from trigo import (
    generer_AnglePourMesPrinc,
    corrige_mesurePrincipale,
    generer_AnglesEgauxOuPresque,
    corrige_ApplicationPythagore,
    questionPlacerPointsImagesSurCercleTrace,
)

for i in xrange(10):
    angle = generer_AnglePourMesPrinc(choice(["-", "+"]), choice(["-", "+"]))
    print angle
    print corrige_mesurePrincipale(angle)
    print ""

print corrige_ApplicationPythagore("x", r"\sin", Rational(1, 3))
