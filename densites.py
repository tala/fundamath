#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
from random import sample, randint, shuffle, choice
from sympy import latex

from generateurTex import generer_interro_flash_tex, generer_interro_flash_corrigee_tex
from compilation.compilateur import compileDocument

# lois uniformes


def question_loi_uniforme(a, b, c, d):
    question = {}
    question["consigne"] = (
        ur"La variable aléatoire $X$ suit la loi"
        + ur" uniforme sur $\left[{};{}\right]$.".format(latex(a), latex(b))
    )
    question["enonce"] = ur"Déterminez la probabilité de" + ur" l'événement :\\"
    if c and d:
        complement = ur"${}\leq X\leq {}$".format(latex(c), latex(d))
    elif c:
        complement = ur"${}\leq X $".format(latex(c))
    elif d:
        complement = ur"$X\leq {}$".format(latex(d))
    question["enonce"] += complement
    question.update(corrige_loi_uniforme(a, b, c, d))
    return question


def corrige_loi_uniforme(a, b, c, d):
    reponse = {}
    reponse["rappel texte"] = (
        u"$X$ suit la loi uniforme sur "
        + ur"$\left[{};{}\right]$.".format(latex(a), latex(b))
    )
    if c and d:
        reponse["corrige"] = (
            ur"$P\left({0} \leq X \leq {1}\right)"
            + ur"=\int_{0}^{1}\frac{{1}}{{{3}-{2}}}dx$"
        ).format(latex(c), latex(d), latex(a), latex(b))
    elif c:
        reponse["corrige"] = (
            ur"$P\left({0} \leq X\right)" + ur"=\int_{0}^{2}\frac{{1}}{{{2}-{1}}}dx$"
        ).format(latex(c), latex(a), latex(b))
    elif d:
        reponse["corrige"] = (
            ur"$P\left(X \leq {0}\right)" + ur"=\int_{1}^{0}\frac{{1}}{{{2}-{1}}}dx$"
        ).format(latex(d), latex(a), latex(b))
    return reponse


# lois exponentielles


def question_loi_exponentielle(lambd, c, d):
    question = {}
    question["consigne"] = (
        ur"La variable aléatoire $X$ suit la loi"
        + ur" exponentielle de paramètre ${}$".format(latex(lambd))
    )
    question["enonce"] = ur"Déterminez la probabilité de" + ur" l'événement :\\"
    if c and d:
        complement = ur"${}\leq X\leq {}$".format(latex(c), latex(d))
    elif c:
        complement = ur"${}\leq X $".format(latex(c))
    elif d:
        complement = ur"$X\leq {}$".format(latex(d))
    question["enonce"] += complement
    question.update(corrige_loi_exponentielle(lambd, c, d))
    return question


def corrige_loi_exponentielle(lambd, c, d):
    reponse = {}
    reponse[
        "rappel texte"
    ] = u"$X$ suit la loi" + ur" exponentielle de paramètre ${}$".format(latex(lambd))
    if c and d:
        reponse["corrige"] = (
            ur"$P\left({0} \leq X \leq {1}\right)" + ur"=\int_{0}^{1}{2}e^{{-{2}x}}dx$"
        ).format(latex(c), latex(d), latex(lambd))
    elif c:
        reponse["corrige"] = (
            ur"$P\left({0} \leq X\right)" + ur"=1-\int_{{0}}^{0}{1}e^{{-{1}x}}dx$"
        ).format(latex(c), latex(lambd))
    elif d:
        reponse["corrige"] = (
            ur"$P\left(X \leq {0}\right)" + ur"=\int_{{0}}^{0}{1}e^{{-{1}x}}dx$"
        ).format(latex(d), latex(lambd))
    return reponse


# lois normales


def question_loi_normale(mu, sigma, c, d):
    question = {}
    question["consigne"] = (
        ur"La variable aléatoire $X$ suit la loi"
        + ur" normale de moyenne ${}$ et d'écart-type ${}$.".format(
            latex(mu), latex(sigma)
        )
    )
    question["enonce"] = ur"Déterminez la probabilité de l'événement :\\"
    if c and d:
        complement = ur"${}\leq X\leq {}$".format(latex(c), latex(d))
    elif c:
        complement = ur"${}\leq X $".format(latex(c))
    elif d:
        complement = ur"$X\leq {}$".format(latex(d))
    question["enonce"] += complement
    question.update(corrige_loi_normale(mu, sigma, c, d))
    return question


def corrige_loi_normale(mu, sigma, c, d):
    reponse = {}
    reponse["rappel texte"] = (
        ur"$X$ suit la loi normale de moyenne" + ur" ${}$ et d'écart-type ${}$."
    ).format(latex(mu), latex(sigma))
    if c and d:
        reponse["corrige"] = (
            ur"$P\left({0} \leq X \leq {1}\right)"
            + ur"=\int_{0}^{1}\frac{{1}}{{{3}-{2}}} dx$"
        ).format(latex(c), latex(d), latex(mu), latex(sigma))
    elif c:
        reponse["corrige"] = (
            ur"$P\left({0} \leq X\right)" + ur"=\int_{0}^{2}\frac{{1}}{{{2}-{1}}}dx$"
        ).format(latex(c), latex(mu), latex(sigma))
    elif d:
        reponse["corrige"] = (
            ur"$P\left(X \leq {0}\right)" + ur"=\int_{1}^{0}\frac{{1}}{{{2}-{1}}}dx$"
        ).format(latex(d), latex(mu), latex(sigma))
    return reponse


IF10 = {
    "classe": "TSTI2D",
    "date": "Mardi 2 mars 2016",
    "num": 10,
    "questions": [
        {
            "gauche": question_loi_uniforme(1, 6, 4, 5),
            "droite": question_loi_uniforme(1, 6, 4, 5),
            "temps": 60,
        },
        {
            "gauche": question_loi_uniforme(1, 6, 4, None),
            "droite": question_loi_uniforme(1, 6, None, 5),
            "temps": 60,
        },
        {
            "gauche": question_loi_normale(1, 6, 4, None),
            "droite": question_loi_normale(1, 6, None, 5),
            "temps": 60,
        },
        {
            "gauche": question_loi_exponentielle(2, 4, None),
            "droite": question_loi_exponentielle(2, None, 5),
            "temps": 60,
        },
    ],
}
if __name__ == "__main__":
    compileDocument("TSTI2D_IF10", generer_interro_flash_corrigee_tex(IF10))
