#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division

from sympy import exp, ln, sqrt, pi, Rational

mem = dict()


def coeff_binomial(n, p):
    global mem
    if p == 0 or n == p:
        return 1
    try:
        return mem[(n, p)]
    except KeyError:
        v = coeff_binomial(n - 1, p) + coeff_binomial(n - 1, p - 1)
        mem[(n, p)] = v
    return v


def triangle_de_pascal(n):
    lignes = []
    for i in range(n):
        ligne = ["" for k in range(n)]
        for j in range(i + 1):
            if j == 0 or j == i:
                ligne[j] = 1
            elif j < i:
                print (ligne[j])
                ligne[j] = lignes[-1][j - 1] + lignes[-1][j]
        lignes.append(ligne)
    return lignes


def animation_triangle_pascal(n):
    res = r"\begin{{tabular}}{{{}}}".format("c" * n) + "\n"
    res += "\n".join(
        [
            r"\onslide<+->{" + " & ".join(map(str, l)) + r"}\\"
            for l in triangle_de_pascal(n)
        ]
    )
    res += "\n" r"\end{tabular}"
    return res


def proba_binom_egal(n, p, k):
    return coeff_binomial(n, k) * p ** k * (1 - p) ** (n - k)


def proba_binom_inf(n, p, k):
    res = 0
    for i in xrange(k):
        res = res + proba_binom_egal(n, p, i)
    return res


def proba_binom_inf_egal(n, p, k):
    res = 0
    for i in xrange(k + 1):
        res = res + proba_binom_egal(n, p, i)
    return res


def proba_binom_sup(n, p, k):
    res = 0
    for i in xrange(k + 1, n + 1):
        res = res + proba_binom_egal(n, p, i)
    return res


def proba_binom_sup_egal(n, p, k):
    res = 0
    for i in xrange(k, n + 1):
        res = res + proba_binom_egal(n, p, i)
    return res


def proba_exp_inf(l, x):
    return 1 - exp(-l * x)


def proba_exp_sup(l, x):
    return exp(-l * x)


def proba_exp_intervalle(l, a, b):
    if a < b:
        return exp(-l * a) - exp(-l * b)
    else:
        return 0


def inv_exp_inf(l, p):
    return ln(1 - p) / (-l)


def pgaussred(x):
    """pgaussred(x): probabilité qu'une variable aléatoire distribuée selon une loi normale centrée réduite soit inférieure à x"""
    if x == 0:
        return 0.5
    if x >= 7.56:
        return 1.0
    if x <= -7.56:
        return 0.0
    u = abs(x)
    n = int(u * 2000)
    du = u / n
    k = 1 / sqrt(2 * pi)
    u1 = 0
    f1 = k
    p = 0.5
    for i in xrange(0, n):
        u2 = u1 + du
        f2 = k * exp(-0.5 * u2 * u2)
        p = p + (f1 + f2) * du * 0.5
        u1 = u2
        f1 = f2
    if x > 0:
        return p
    else:
        return 1.0 - p


def xgaussred(p):
    """xgaussred(p): renvoie la valeur de la variable x, distribuée selon la LNR, correspondant à une probabilité donnée p"""
    if p == 0.5:
        return 0.0
    if p == 0.0:
        return -7.56
    if p == 1.0:
        return +7.56
    if p > 0.5:
        pc = p
    else:
        pc = 1 - p
    du = 1 / 2000
    eps = 1.0e-15
    k = 1 / sqrt(2 * pi)
    u1 = 0.0
    f1 = k
    p1 = 0.5
    while True:
        u2 = u1 + du
        f2 = k * exp(-0.5 * u2 * u2)
        s = (f1 + f2) * 0.5 * du
        p2 = p1 + s
        if abs(p2 - pc) < eps:
            break
        if ((p1 < pc) and (p2 < p1)) or ((p1 > pc) and (p2 > p1)):
            # =on est en train de s'éloigner du point recherché
            du = -du / 2
        u1 = u2
        f1 = f2
        p1 = p2
    if p > 0.5:
        return u2
    else:
        return -u2


def pgaussredR(x, arrondi=4):
    return round(pgaussred(x), arrondi)


def formaterNombre(n):
    return str(n).replace(".", ",")


def CalculDirectNormale(mu, sigma, a, b, arrondi=3):
    a2 = round((a - mu) / sigma, arrondi)
    b2 = round((b - mu) / sigma, arrondi)
    return round(pgaussredR(-a2) + pgaussredR(b2) - 1, arrondi)


def CalculToleranceNormale(mu, sigma, prob, arrondi=3):
    res = {"p": 0, "t": 0, "h": 0}
    res["p"] = (prob + 1) / 2
    res["t"] = round(xgaussred(res["p"]), 2)
    res["h"] = round(res["t"] * sigma, arrondi)
    return res


def CalculAltPGauss(mu, sigma, b, arrondi=3):
    b = (b - mu) / sigma
    h = b / 10000
    I = 1 + exp(-b ** 2 / 2)
    x = 0
    for k in xrange(1, 10000):
        x = x + h / 2
        I = I + 4 * exp(-x ** 2 / 2)
        x = x + h / 2
        I = I + 2 * exp(-x ** 2 / 2)
    x = x + h / 2
    I = I + 4 * exp(-x ** 2 / 2)
    I = 0.5 + I * h / (6 * sqrt(2 * pi))
    return round(I, arrondi)


def CalculAltDirectNormale(mu, sigma, a, b, arrondi=3):
    a = (a - mu) / sigma
    b = (b - mu) / sigma
    h = (b - a) / 1000
    I = exp(-a ** 2 / 2) + exp(-b ** 2 / 2)
    x = a
    for k in xrange(1, 1000):
        x = x + h / 2
        I = I + 4 * exp(-x ** 2 / 2)
        x = x + h / 2
        I = I + 2 * exp(-x ** 2 / 2)
    x = x + h / 2
    I = I + 4 * exp(-x ** 2 / 2)
    I = I * h / (6 * sqrt(2 * pi))
    return round(I, arrondi)


def CalculInterFluctNormale(mu, sigma, prob, arrondi=3):
    res = {"p": 0, "t": 0, "h": 0, "min": 0, "max": 0}
    intermed = CalculToleranceNormale(mu, sigma, prob, arrondi)
    p = intermed["p"]
    t = intermed["t"]
    h = intermed["h"]
    res["p"] = p
    res["t"] = t
    res["h"] = h
    res["min"] = round(mu - h, arrondi)
    res["max"] = round(mu + h, arrondi)
    return res


def CalculSigmaNormale(mu, tol, prob, arrondi=3):
    return tol / (round(xgaussred((prob + 1) / 2), 2))


if __name__ == "__main__":
    # print 0.5 - CalculAltDirectNormale(100, 0.8, 98, 100, arrondi=4)
    # print "lois binomiales :"

    # print proba_binom_egal(4, Rational(1,6), 0)
    # print round(proba_binom_egal(4, Rational(1,6), 0),3)
    # print proba_binom_egal(4, Rational(1,6), 1)
    # print round(proba_binom_egal(4, Rational(1,6), 1),3)
    # print proba_binom_egal(4, Rational(1,6), 2)
    # print round(proba_binom_egal(4, Rational(1,6), 2),3)
    # print proba_binom_egal(4, Rational(1,6), 3)
    # print round(proba_binom_egal(4, Rational(1,6), 3),3)

    # print proba_binom_egal(4, Rational(1,6), 4)
    # print round(proba_binom_egal(4, Rational(1,6), 4),3)

    # print proba_binom_egal(20,0.2, 1)
    # print round(proba_binom_egal(20, 0.2, 1),3)

    # print proba_binom_egal(6, 0.4, 0)
    # print proba_binom_inf_egal(6, 0.4, 2)
    # print proba_binom_egal(6, 0.6, 6)
    # print proba_binom_inf_egal(6, 0.6, 2)
    # print proba_binom_sup(6, 0.6, 1)
    # print "lois exponentielles :"
    # print proba_exp_inf(0.05, 20)
    # print 1 - proba_exp_inf(0.05, 40)
    # print proba_exp_inf(0.05, 35)
    # print proba_exp_inf(0.05, 25)
    # print proba_exp_intervalle(0.05, 25, 35)
    # print inv_exp_inf(0.002, 0.3)
    # print "lois normales :"
    ## print CalculDirectNormale(100,0.43,0,101,9)
    # print CalculAltDirectNormale(100, 0.43, 0, 101, 9)
    # print CalculAltDirectNormale(20, 5, 20, 28, 9) + 0.5
    # print 0.5 - CalculAltDirectNormale(20, 5, 12, 20, 9)
    # print CalculAltDirectNormale(20, 5, 12, 28, 9)
    # print "ex 10"
    # print 0.5 - CalculAltDirectNormale(10, 2, 8, 10, 3)
    # print 0.5 + CalculAltDirectNormale(10, 2, 8, 10, 3)
    # print CalculAltDirectNormale(10, 2, 9, 12, 3)
    # print CalculAltDirectNormale(10, 2, 7, 14, 3)
    # print "ex 11"
    # print 0.5 - CalculAltDirectNormale(100, 12, 80, 100, 3)
    # print 0.5 + CalculAltDirectNormale(100, 12, 80, 100, 3)
    # print CalculAltDirectNormale(100, 12, 90, 120, 3)
    # print CalculAltDirectNormale(100, 12, 70, 110, 3)
    # print "ex 12"
    # print 0.5 - CalculAltDirectNormale(8.5, 1.2, 7.5, 8.5, 3)
    # print 0.5 + CalculAltDirectNormale(8.5, 1.2, 7.5, 8.5, 3)
    # print CalculAltDirectNormale(8.5, 1.2, 9, 10, 3)
    # print CalculAltDirectNormale(8.5, 1.2, 7, 8, 3)
    def triangle_de_pascal(n):
        lignes = []
        for i in range(1, n + 1):
            ligne = " & ".join([str(coeff_binomial(i, j)) for j in range(i + 1)])
            ligne += " &".join(["" for j in range(n + 1 - i)]) + r" \\"
            lignes.append(ligne)
        print "\n".join(lignes)

    triangle_de_pascal(8)
    for i in range(8):
        print 11 ** i
