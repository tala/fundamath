#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division

from random import choice, randint, sample, shuffle

from sympy import Eq, Pow, Rational, cos, exp, latex, ln, oo, sin, sqrt, symbols

from compilation.compilateur import compileDocument
from donnees.donnees import STI2D1
from generateurTex import (
    generer_interro_flash_corrigee_tex,
    generer_interro_flash_tex,
    generer_revision_corrigee_tex,
)
from genererPoints import InitPoints
from interpolation import hermiteParMorceauxC1
from signes import ligne_signe, separation, signe

# from genererPoints import InitPoints
from sympy_to_latex import fpmEtTangentesTikz, fpmToTikz


def variations(fct, notation="f", vmin=-oo, vmax=oo):
    var = fct.free_symbols.pop()
    derivee = fct.diff().factor()
    etude = r"\onslide<+->" + "\n"
    etude += (ur"${}\left({}\right)={}$\\" "\n").format(notation, var, latex(fct))
    etude += r"\onslide<+->" + "\n"
    etude += (
        ur"La dérivée de la fonction ${0}$ est la fonction ${0}'$,"
        ur" définie par ${0}'\left({1}\right)={2}$\\"
        "\n"
    ).format(notation, var, latex(derivee))
    vals_notables = Eq(derivee, 0).as_set()
    ## ajout des valeurs interdites des facteurs au dénominateur
    for arg in derivee.args:
        if isinstance(arg, Pow) and arg.args[1] == -1:
            vals_notables = vals_notables.union(Eq(arg.args[0], 0).as_set())
    vals_notables = list(vals_notables)
    vals_notables.sort()
    etude += r"\onslide<+->" + "\n"
    etude += r"\begin{tikzpicture}" + "\n"
    etude += r"\tkzTabInit[lgt=4,espcl=1.5]" + "\n"
    etude += "{{${}$ /1,".format(latex(var)) + "\n"
    etude += (
        ur" Signe de ${0}'\left({1}\right)={2}$ /1,".format(
            notation, var, latex(derivee)
        )
        + "\n"
    )
    etude += ur" Variations de ${}$ /1}}".format(notation) + "\n"
    if vals_notables == []:
        etude += "{$-\infty$, $+\infty$}" + "\n"
    else:
        etude += (
            "{{$-\infty$, {}, $+\infty$}}".format(
                ", ".join([latex(r, mode="inline") for r in vals_notables])
            )
            + "\n"
        )
    etude += r"\onslide<+->{" + ligne_signe(derivee, vals_notables) + "}\n"
    etude += r"\end{tikzpicture}\\" "\n"
    return etude


x = symbols("x")
y = symbols("y")
t = symbols("t")


def equation_tangente(fct, x0):
    a = fct.diff().subs(x, x0)
    b = fct.subs(x, x0) - a * x0
    return Eq(y, a * x + b)


def question_integrale(fct, a, b, var=x, notation="I"):
    question = {}
    question["consigne"] = (
        u"Déterminer la valeur exacte de l'intégrale" + u" suivante :"
    )
    question["enonce"] = r"${}=\int_{}^{} {} d{}$".format(
        notation, latex(a), latex(b), latex(fct), var
    )
    question.update(corrige_integrale(fct, a, b, var, notation))
    return question


def corrige_integrale(fct, a, b, var, notation):
    reponse = {}
    reponse["rappel texte"] = r"${}=\int_{}^{} {} d{}$".format(
        notation, latex(a), latex(b), latex(fct), var
    )
    F = fct.integrate()
    corrige = r"${}=\left[{}\right]_{}^{}".format(
        notation, latex(F), latex(a), latex(b)
    )
    if F.subs(var, a) > 0:
        corrige += r"={}-{}".format(latex(F.subs(var, b)), latex(F.subs(var, a)))
    else:
        corrige += r"={}-\left({}\right)".format(F.subs(var, b), F.subs(var, a))
    corrige += r"={} $".format(latex(fct.integrate((var, a, b))))
    reponse["corrige"] = corrige
    return reponse


def question_primitives(fct):
    question = {}
    question["consigne"] = (
        u"Déterminer les primitives de la" + u" fonction $f$ définie par :"
    )
    question["enonce"] = r"$f\left(x\right)={}$".format(latex(fct))
    question.update(corrige_primitives(fct))
    return question


def question_derivee(fct):
    question = {}
    question["consigne"] = (
        u"Déterminer la fonction dérivée de la" + u" fonction $f$ définie par :"
    )
    question["enonce"] = r"$f\left(x\right)={}$".format(latex(fct))
    question.update(corrige_derivee(fct))
    return question


def exercice_aire_minimale_pave():
    volume = randint(1, 4)
    dimension = randint(1, 3)
    lignes = [
        "On se propose de construire un réservoir en tôle"
        " ayant la forme d'un parallélépipède rectangle et dont le volume"
        r" intérieur doit être de ${}\ m^3$.\\".format(volume),
        "Sachant que l'un des côtés de ce réservoir doit mesurer ${}$ m, ".format(
            dimension
        )
        + "déterminez les dimensions du réservoir pour que son aire totale"
        " soit minimale.",
    ]
    return "\n".join(lignes)


def exercice_volume_maximal_pave():
    aire = randint(6, 12)
    dimension = randint(1, 3)
    lignes = [
        "On se propose de construire un réservoir en tôle"
        " ayant la forme d'un parallélépipède rectangle et dont l'aire"
        r" totale devra être de ${}\ m^2$.\\".format(aire),
        "Sachant que l'un des côtés de ce réservoir doit mesurer ${}$ m, ".format(
            dimension
        )
        + "déterminez les dimensions du réservoir pour que son volume "
        "intérieur soit maximal.",
    ]
    return "\n".join(lignes)


def polynome_secret(P, x3):
    y0 = P.replace(x, 0)
    deriv = P.diff()
    coeff = deriv.replace(x, x3)
    y3 = P.replace(x, x3)
    mbrD = coeff * x + (y3 - coeff * x3)
    lignes = []
    lignes.append(
        r"Je suis un polynôme du second degré."
        + r" Ma représentation graphique dans un certain repère coupe l'axe "
        + r"des ordonnées au point de coordonnées  $\left(0;{}\right)$.".format(y0)
        + "De plus, la droite d'équation $y={}$ est tangente à cette courbe".format(
            latex(mbrD)
        )
        + " au point d'abscisse ${}$.".format(x3)
    )
    return "\n".join(lignes)


def genere_polynome_secret():
    a = choice([-1, 1]) * randint(1, 3)
    x1 = choice([-1, 1]) * randint(1, 3)
    x2 = choice([-1, 1]) * randint(1, 3)
    return a * (x - x1) * (x - x2).expand()


def corrige_polynome_secret(y0, mbrD, x3):
    lignes = []
    a = symbols("a")
    b = symbols("b")
    c = symbols("c")
    lignes.append(
        r"Notons $P$ le polynôme cherché.\\"
        + r"Comme il est de degré 2, il existe trois réels "
        + r"$a$,$b$ et $c$ tels que :\\"
        + r"$P(x) = a x^2 + b x + c$.\\"
    )
    lignes.append(
        r"D'après la deuxième phrase, on peut affirmer que" + r" $P(0)={}$\\".format(y0)
    )
    lignes.append(
        r"Par ailleurs, la fonction dérivée de $P$ est" + r" $P'(x)=2 a x + b$.\\"
    )
    nb = mbrD.replace(x, 1) - mbrD.replace(x, 0)
    lignes.append(
        r"Le coefficient directeur de la tangente à la"
        + r" courbe représentative de $P$ au point d'abscisse ${}$ est ${}$.".format(
            x3, nb
        )
        + r" Donc $P'\left({}\right)={}$\\".format(x3, nb)
    )
    lignes.append(
        r"Ce qui nous donne l'équation : ${}={}$.\\ ".format(latex(2 * x3 * a + b), nb)
    )
    lignes.append(
        r"De plus, le point d'abscisse ${}$ de la courbe".format(x3)
        + r" est aussi sur la tangente."
    )
    lignes.append(r"Son ordonnée est donc :  ${}$. ".format(mbrD.replace(x, x3)))
    lignes.append(r"(On remplace $x$ par ${}$ dans ${}$.)".format(x3, latex(mbrD)))
    lignes.append(
        r"On obtient donc l'équation : ${}={}$.\\ ".format(
            latex(x3 ** 2 * a + x3 * b + c), mbrD.replace(x, x3)
        )
    )
    c = y0
    lignes.append(
        r"Comme $c=P(0)={}$,".format(y0)
        + r" les deux autres équations constituent le système :\\"
    )
    lignes.append(r"$(s)\left\{")
    lignes.append(r"\begin{array}{r@{}l@{}r@{}l@{}c@{~}l@{~}}")
    coeff11 = x3 ** 2
    if coeff11 == 1:
        aff11 = ""
    elif coeff11 == -1:
        aff11 = "-"
    else:
        aff11 = str(coeff11)
    coeff12 = x3
    if coeff12 == 1:
        aff12 = "+"
    elif coeff12 == -1:
        aff12 = "-"
    elif coeff12 > 0:
        aff12 = "+" + str(coeff12)
    else:
        aff12 = str(coeff12)
    lignes.append(
        r"{}& a    & {}& b    & = & {}\\".format(aff11, aff12, mbrD.replace(x, x3) - y0)
    )
    coeff21 = 2 * x3
    if coeff21 == 1:
        aff21 = ""
    elif coeff21 == -1:
        aff21 = "-"
    else:
        aff21 = str(coeff21)
    lignes.append(r"{}& a    & +& b    & = & {}\\".format(coeff21, nb))
    lignes.append(r"\end{array}")
    lignes.append(r"\right.$\\")
    lignes.append(r"Ce système se résoud facilement par combinaison.")
    coeff3 = -x3
    if coeff3 == 1:
        aff3 = "+"
    elif coeff3 == -1:
        aff3 = "-"
    elif coeff3 > 0:
        aff3 = "+" + str(coeff3)
    else:
        aff3 = str(coeff3)
    lignes.append(
        r"En effet, en notant $L_1$ la première équation"
        + r" et $L_2$ la deuxième, la combinaison $L_1{}\times L2$ élimine".format(aff3)
        + r" l'inconnue $b$.\\ On obtient :"
    )
    lignes.append(
        r"${}={}.$".format(
            latex((coeff11 - coeff21 * x3) * a), mbrD.replace(x, x3) - y0 - x3 * nb
        )
    )
    a = (mbrD.replace(x, x3) - y0 - x3 * nb) / (coeff11 - coeff21 * x3)
    lignes.append(
        r"Donc $a={}$".format(latex(a)) + r" et donc $b={}$.\\ ".format(nb - 2 * x3 * a)
    )
    b = nb - 2 * x3 * a
    lignes.append(r"Finalement, $P(x)={}$.\\".format(latex(a * x ** 2 + b * x + c)))
    return "\n".join(lignes)


def exercice_polynome_secret():
    return polynome_secret(genere_polynome_secret(), choice([-1, 1]) * randint(1, 3))


def exercice_lecture_graphique_nb_derive():
    lignes = []
    liste = InitPoints(-6, 6, 4)
    lignes.append(
        "La courbe sur le dessin ci-dessous est la "
        + r"représentation graphique d'une certaine fonction $f$."
    )
    lignes.append("Déterminez graphiquement : ")
    lignes.append(
        ", ".join([r"$f'\left({}\right)$".format(elt[0]) for elt in liste[1:-2]])
        + " et "
        + r"$f'\left({}\right)$".format(liste[-2][0])
        + "."
    )
    lignes.append(
        fpmEtTangentesTikz(hermiteParMorceauxC1(liste), [elt[0] for elt in liste[1:-1]])
    )
    return "\n".join(lignes)


def exercice_lecture_signe_derive():
    liste = InitPoints(-6, 6, 4)
    lignes = []
    fpmEtTangentesTikz(hermiteParMorceauxC1(liste))
    return "\n".join(lignes)


def exercice_etude_fonction_homographique():
    lignes = []
    a = choice([-1, 1]) * randint(1, 3)
    b = choice([-1, 1]) * randint(1, 3)
    c = choice([-1, 1]) * randint(1, 3)
    d = choice([-1, 1]) * randint(1, 3)
    vi = -d / c
    xmin = int(-d / c) + 1
    xmax = xmin + 3 + randint(1, 3)
    if a > 0:
        num = a * x + b
    else:
        num = b + a * x
    if c > 0:
        den = c * x + d
    else:
        den = d + c * x
    lignes.append(
        "Déterminez en les justifiant les variations de la "
        r"fonction $g$, définie sur $\left[{};{}\right]$".format(xmin, xmax)
        + r"par $g(x)=\dfrac{{{}}}{{{}}}$".format(latex(num), latex(den))
    )
    return "\n".join(lignes)


def exercice_etude_cubique():
    lignes = []
    a = choice([-1, 1]) * randint(1, 3)
    x1 = choice([-1, 1]) * randint(1, 3)
    x2 = choice([-1, 1]) * randint(1, 3)
    p1 = a * (x - x1) * (x - x2).expand()
    p = p1.integrate() + choice([-1, 1]) * randint(1, 3)
    xmin = 2 * min(x1, x2) - max(x1, x2)
    xmax = 2 * max(x1, x2) - min(x1, x2)
    lignes.append(
        "Déterminez en les justifiant les variations de la "
        + r"fonction $f$, définie sur $\left[{};{}\right]$ par $f(x)={}$".format(
            xmin, xmax, latex(p)
        )
    )
    return "\n".join(lignes)


def corrige_optimisation_pour_aire_donnee(aire_donnee, cote):
    h = symbols("h")
    l = symbols("l")
    L = symbols("L")

    lignes = [
        "Notons respectivement L,l et h la longueur, la largeur"
        + " et la hauteur du réservoir, V son volume et A l'aire totale de"
        + " ses faces. Nous avons :"
    ]
    volume = h * l * L
    lignes.append("$V =" + latex(volume) + "$")
    aire = 2 * (h * l + h * L + l * L)
    lignes.append("$A =" + latex(aire) + "$")
    lignes.append("D'après le texte :")
    lignes.append("$l=" + str(cote) + "$")
    lignes.append("$A=" + str(aire_donnee) + "$")
    volume = volume.replace(l, cote)
    aire = aire.replace(l, cote)
    l = cote
    lignes.append("Donc :")
    lignes.append("$V =" + latex(volume) + "$")
    lignes.append("$" + str(aire_donnee) + "=" + latex(aire) + "$")
    lignes.append(
        "En isolant progressivement l'inconnues L dans la"
        + " deuxième équation, on obtient : "
    )
    mbrg = aire_donnee - 2 * l * h
    mbrd = aire - 2 * l * h
    lignes.append("$" + latex(mbrg) + "=" + latex(mbrd) + "$")
    mbrd = mbrd.factor()
    lignes.append("$" + latex(mbrg) + "=" + latex(mbrd) + "$")
    mbrd = mbrd / (l + h)
    mbrg = mbrg / (l + h)
    lignes.append("$" + latex(mbrg) + "=" + latex(mbrd) + "$")
    mbrd = mbrd / 2
    mbrg = (mbrg / 2).simplify()
    lignes.append("$" + latex(mbrg) + "=" + latex(mbrd) + "$")
    L = mbrg
    volume = h * l * L
    lignes.append(
        "Je peux ainsi exprimmer le volume en fonction d'un" " seul paramètre :"
    )
    lignes.append("$V=" + latex(volume.expand()) + "$")
    lignes.append(latex(volume.diff().factor()))
    print "\n".join(lignes)


def corrige_derivee(fct):
    reponse = {}
    reponse["rappel texte"] = (
        u"La fonction dérivée de la fonction $f$ "
        + ur"définie par $f\left(x\right)={}$".format(latex(fct))
        + u" est la fonction $f'$ définie par : "
    )
    reponse["corrige"] = ur"$f'\left(x\right)={}$".format(latex(fct.diff().factor()))
    return reponse


def corrige_primitives(fct):
    reponse = {}
    reponse["rappel texte"] = (
        u"Les primitives de la fonction $f$ "
        + ur"définie par $f\left(x\right)={}$".format(latex(fct))
        + u" sont les fonctions $F$ définies par : "
    )
    reponse["corrige"] = (
        ur"$F\left(x\right)={}+k$ où $k$ est une ".format(latex(fct.integrate()))
        + u"constante réelle quelconque."
    )
    return reponse


def corrige_optimisation_pour_volume_donnee(volume, cote):
    pass


if __name__ == "__main__":
    print (variations(2 * x ** 2 + 7 * x - 3))
    print (variations(-x ** 3 + 3 * x ** 2 + 9 * x + 2, notation="g", vmin=0, vmax=10))
    print (variations((2 * x + 3) / (2 - x), notation="h", vmin=2))

    # corrige_optimisation_pour_aire_donnee(12, 1)

    # print (question_derivee(3 * x))
    # print (question_derivee(3 * x / (7 * x - 3)))
    # # print(corrige_polynome_secret(-5,8*x-14,3))
    # # devoirs= [[
    # # exercice_lecture_graphique_nb_derive(),
    # # choice([exercice_etude_fonction_homographique(),
    # # exercice_etude_cubique()]),
    # # choice([exercice_volume_maximal_pave(),
    # # exercice_aire_minimale_pave()]),
    # # exercice_polynome_secret(),
    # # ] for i in range(26)]
    # interroFlash = {
    #     "classe": "1STI2D",
    #     "date": "Lundi 16 janvier 2016",
    #     "num": 3,
    #     "questions": [
    #         {
    #             "gauche": question_derivee(x ** 2),
    #             "droite": question_derivee(2 * x),
    #             "temps": 10,
    #         },
    #         {
    #             "gauche": question_derivee(7 - 3 * x),
    #             "droite": question_derivee(2 * x ** 3),
    #             "temps": 20,
    #         },
    #         {
    #             "gauche": question_derivee(x - sin(x)),
    #             "droite": question_derivee(1 / x),
    #             "temps": 30,
    #         },
    #         {
    #             "gauche": question_derivee(3 * x ** 3),
    #             "droite": question_derivee(2 - cos(x)),
    #             "temps": 30,
    #         },
    #         {
    #             "gauche": question_derivee(2 - 1 / x),
    #             "droite": question_derivee(2 * x ** 2 - 3 * x + 7),
    #             "temps": 30,
    #         },
    #         {
    #             "gauche": exercice_lecture_graphique_nb_derive(),
    #             "droite": exercice_lecture_graphique_nb_derive(),
    #             "temps": 30,
    #         },
    #     ],
    # }

    # test = {
    #     "classe": "STS TSMA1",
    #     "date": "Mardi 15 mars 2016",
    #     "num": "0",
    #     "questions": [
    #         {
    #             "gauche": question_derivee(x ** 2),
    #             "droite": question_derivee(2 * x + cos(x)),
    #             "temps": 60,
    #         },
    #         {
    #             "gauche": question_primitives(7 - 3 * x),
    #             "droite": question_derivee(2 * x ** 3),
    #             "temps": 60,
    #         },
    #         {
    #             "gauche": question_derivee(x - sin(x)),
    #             "droite": question_primitives(1 / x),
    #             "temps": 60,
    #         },
    #         {
    #             "gauche": question_primitives(3 * x ** 3),
    #             "droite": question_derivee(2 - cos(x)),
    #             "temps": 60,
    #         },
    #         {
    #             "gauche": question_derivee(2 - 1 / x),
    #             "droite": question_primitives(2 * x ** 2 - 3 * x + 7),
    #             "temps": 60,
    #         },
    #     ],
    # }

    # corrige = {
    #     "classe": "STS TSMA1",
    #     "date": "Mardi 22 mars 2016",
    #     "num": "xx",
    #     "questions": [
    #         {
    #             "gauche": question_primitives((2 * t - 1) / (t ** 2 - t - 6)),
    #             "droite": question_derivee(exp(2 * t - 7)),
    #             "temps": 60,
    #         },
    #         {
    #             "gauche": question_primitives((4 * x + 6) / (x ** 2 + 3 * x - 10)),
    #             "droite": question_derivee(cos(4 * t) + sin(4 * t)),
    #             "temps": 60,
    #         },
    #         {
    #             "gauche": question_integrale(2 * x + 2, Rational(1, 3), 3),
    #             "droite": question_derivee(sqrt(4 * x + 2)),
    #             "temps": 60,
    #         },
    #         {
    #             "gauche": "",
    #             "droite": question_derivee(sqrt(2) * ln(2 * x + 3)),
    #             "temps": 60,
    #         },
    #         {
    #             "gauche": "",
    #             "droite": question_primitives(x ** 2 - 2 * x + sin(x)),
    #             "temps": 60,
    #         },
    #         {"gauche": "", "droite": question_primitives(3 * x - 1 / x), "temps": 60},
    #         {
    #             "gauche": "",
    #             "droite": question_primitives((3 * x - 3) * exp(2 * x ** 2 - 4 * x + 5)),
    #             "temps": 60,
    #         },
    #         {
    #             "gauche": "",
    #             "droite": question_primitives((30 * x + 10) / (3 * x ** 2 + 2 * x + 1)),
    #             "temps": 60,
    #         },
    #     ],
    # }

    # revision = {
    #     "classe": "Tale STI-2D",
    #     "date": "Mardi 26 avril 2016",
    #     "num": u"révisions 1",
    #     "questions": [
    #         {
    #             "gauche": question_derivee((3 * x + 2) ** 3),
    #             "droite": question_primitives((30 * x + 10) / (3 * x ** 2 + 2 * x + 1)),
    #             "temps": 60,
    #         },
    #         {
    #             "gauche": question_derivee(ln(3 * x + 2)),
    #             "droite": question_primitives((3 * x + 3) / (x ** 2 + 2 * x + 1) ** 2),
    #             "temps": 60,
    #         },
    #         {
    #             "gauche": question_primitives((-5 * x + Rational(5, 2)) / (x ** 2 - x + 7)),
    #             "droite": question_derivee((2 * x + 7) ** 4),
    #             "temps": 60,
    #         },
    #     ],
    # }
    # revision2 = {
    #     "classe": "Tale STI-2D",
    #     "date": "Vendredi 29 avril 2016",
    #     "num": u"2",
    #     "questions": [
    #         question_derivee((3 * x + 2) ** 3),
    #         question_derivee(ln(3 * x + 2)),
    #         question_primitives((3 * x + 3) / (x ** 2 + 2 * x + 1) ** 2),
    #         question_primitives((-5 * x + Rational(5, 2)) / (x ** 2 - x + 7)),
    #         question_derivee((2 * x + 7) ** 4),
    #     ],
    # }
    # print('ça ne devrait pas')
    # liste_x = map(lambda k: k / 2, sample(range(-10, 10), 5))
    # liste_x.sort()
    # liste_y_hasard = map(lambda k: k / 2, [randint(-10, 10) for i in range(3)])

    # liste_y = [liste_y_hasard[0]]
    # for i, y in enumerate(liste_y_hasard[1:]):
    #     # i est l'indice du terme précédent
    #     liste_y.append(int(y + liste_y_hasard[i]) / 2)
    #     liste_y.append(y)

    # liste_pentes_choisies = [1, 2 / 3, 2, 0.5, 1.5]
    # shuffle(liste_pentes_choisies)

    # liste_dy = [0] * len(liste_x)

    # for i, y in enumerate(liste_y):
    #     if i > 0 and i < len(liste_y) - 1:
    #         if (liste_y[i - 1] - y) * (liste_y[i + 1] - y) > 0:
    #             liste_dy[i] = 0
    #         else:
    #             if liste_y[i + 1] - liste_y[i - 1] > 0:
    #                 liste_dy[i] = liste_pentes_choisies.pop()
    #             elif liste_y[i + 1] - liste_y[i - 1] < 0:
    #                 liste_dy[i] = -liste_pentes_choisies.pop()
    #             else:
    #                 liste_dy[i] = 0

    # print (liste_x)
    # print (liste_y_hasard)
    # print (liste_y)
    # print (liste_dy)

    # print (zip(liste_x, liste_y, liste_dy))

    # print (hermiteParMorceauxC1(zip(liste_x, liste_y, liste_dy)))

    # fonctions = [
    #     -x ** 2 - x + 2,
    #     -t ** 3 - 3 * t ** 2 + 9,
    #     (2 * x + 1) / (x + 1),
    #     (4 * x) / (x ** 2 + x + 1),
    # ]
    # for fct in fonctions:
    #     print r"\begin{frame}"
    #     print variations(fct)
    #     print r"\end{frame}"
    # print variations(-t ** 3 - 3 * t ** 2 + 9)
    # compileDocument('IF3_STI2D', generer_interro_flash_corrigee_tex(interroFlash))
    # compileDocument('revision2', generer_revision_corrigee_tex(revision2))
# compileDocument('TSMA1_IF0',generer_interro_flash_corrigee_tex(test))
# polynomes_secrets =[[el['prenom'].encode('utf-8'),
# el['nom'].encode('utf-8'),
# el['data poly']] for el in STI2D1]
# fichier = open('resultats.tex','wb')
# for data in polynomes_secrets :
# try :
# fichier.write(data[0]+' '+data[1]+r'\\'+'\n'+corrige_polynome_secret(data[2][0],data[2][1],data[2][2]))
# except :
# pass
# fichier.close()
# for k in range(40):
# P=genere_polynome_secret()
# xA=choice([-1,1])*randint(1,3)
# y0=P.replace(x,0)
# yA=P.replace(x,xA)
# derive=P.diff()
# coeff=derive.replace(x,xA)
# mbrD=coeff*x+yA-coeff*xA
# print(polynome_secret(P,xA))
# print(corrige_polynome_secret(y0,mbrD,xA))
# InitPoints(-6,6,4)
##print(fpmEtTangentesTikz(hermiteParMorceauxC1(liste),[elt[0] for elt in liste[1:-1]]))
