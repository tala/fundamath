#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division

# from random import sample, randint,shuffle,choice
from sympy import symbols, latex, cos, sin, exp, sqrt, ln, Rational, I, pi, oo

from compilation.compilateur import compileDocument
from generateurTex import (
    generer_interro_flash_tex,
    generer_interro_flash_corrigee_tex,
    generer_revision_corrigee_tex,
)

from derivation import question_derivee, question_primitives
from limites import question_limite_reference, question_limite_vers_asymptote
from complexes import (
    question_developpement,
    question_conjugue,
    question_forme_algebrique,
    question_forme_trigo,
    question_quotient,
    question_argument,
    question_module,
    question_lecture_affixe,
    question_forme_exponentielle,
    question_inverse,
    question_produit_et_formes_exponentielles,
    question_quotient_et_formes_exponentielles,
)

x = symbols("x")
t = symbols("t")
revision2 = {
    "classe": "Tale STI-2D",
    "date": "Vendredi 29 avril 2016",
    "num": u"2",
    "questions": [
        question_derivee((x ** 2 + 2) ** 2),
        question_derivee(x * ln(x)),
        question_derivee((x - 2) / (2 * x + 3)),
        question_primitives((x + 2) / (x ** 2 + 4 * x + 1) ** 2),
        question_primitives((-x + 2) / (x ** 2 - 4 * x + 7)),
        question_derivee((2 * x + 7) ** 4),
    ],
}
revision3 = {
    "classe": "Tale STI-2D",
    "date": "Lundi 2 mai 2016",
    "num": "3",
    "questions": [
        question_limite_reference(x ** 2 + 2 * x - 5),
        question_limite_reference(x ** 2 + 2 * x - 5, xlim=2),
        question_limite_reference((x - 2) / (x + 2), xlim=2),
        question_limite_reference((x - 2) / (x + 2), xlim=2, dir="-"),
        question_limite_reference((x - 2) / (x + 2), xlim=-2),
        question_limite_reference((x - 2) / (x + 2), xlim=-2, dir="-"),
        question_limite_vers_asymptote(),
        question_limite_vers_asymptote(fct="g", equa=["x", 0]),
        question_limite_vers_asymptote(fct="g", equa=["y", 2]),
        question_limite_vers_asymptote(fct="g", equa=[3, 2]),
    ],
}
revision4 = {
    "classe": "Tale STI-2D",
    "date": "Lundi 2 mai 2016",
    "num": "4",
    "questions": [
        question_developpement((2 + I) ** 2),
        question_conjugue(3 - sqrt(2) * I),
        question_lecture_affixe(3 - 2 * I),
        question_argument(-2 * I),
        question_module(1 + I),
        question_forme_trigo(2 - 2 * sqrt(3) * I),
        question_forme_trigo(-3 - 3 * I),
        question_limite_reference((x - 2) / (x + 2), xlim=2, dir="-"),
        question_limite_reference((x - 2) / (x + 2), xlim=-2),
        question_limite_reference((x - 2) / (x + 2), xlim=-2, dir="-"),
        question_limite_vers_asymptote(),
        question_limite_vers_asymptote(fct="g", equa=["x", 0]),
        question_limite_vers_asymptote(fct="g", equa=["y", 2]),
        question_limite_vers_asymptote(fct="g", equa=[3, 2]),
    ],
}
revision5 = {
    "classe": "Tale STI-2D",
    "date": "Mardi 10 mai 2016",
    "num": "5",
    "questions": [
        question_forme_exponentielle(-2 * I),
        question_forme_exponentielle(2 - 2 * I),
        question_produit_et_formes_exponentielles(
            2 * exp(I * pi / 6), 3 * exp(-I * pi / 3)
        ),
        question_derivee((2 * x - 3) ** 4),
        question_derivee(1 / (3 - x)),
        question_primitives(x ** 2 + 2 * x - 3),
        question_primitives((3 * x - 3) / (x ** 2 - 2 * x + 7)),
        question_limite_reference(x + 1 / x + exp(x), xlim=oo),
        question_limite_reference(x + 1 / x + exp(x), xlim=-oo),
        question_limite_vers_asymptote(fct="g", equa=["x", 2]),
    ],
}
if __name__ == "__main__":
    # compileDocument('revision2',generer_revision_corrigee_tex(revision2))
    # compileDocument('revision3',generer_revision_corrigee_tex(revision3))
    compileDocument("revision5", generer_revision_corrigee_tex(revision5))
    pass
