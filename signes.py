#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function, absolute_import

from sympy import symbols, latex, Eq, Pow, Mul, oo, zoo

from sympy_to_latex import set_to_latex

x = symbols("x")

### Fonction auxiliaires


def signe(expr, val):
    try:
        var = expr.free_symbols.pop()
        res = expr.subs(var, val)
    except:
        res = expr
    if res > 0:
        return "+"
    else:
        return "-"


def separation(expr, val):
    try:
        var = expr.free_symbols.pop()
        res = expr.subs(var, val)
    except:
        return "t"
    if res == 0:
        return "z"
    elif res in [oo, -oo, zoo]:
        return "d"
    else:
        return "t"


def ligne_signe(expr, valeurs):
    if valeurs == []:
        ligne = r"\tkzTabLine { ,"
        ligne += signe(expr, 0)
        ligne += ", }"
    else:
        ligne = r"\tkzTabLine { ,"
        ligne += signe(expr, valeurs[0] - 1)
        ligne += ","
        ligne += separation(expr, valeurs[0])
        ligne += ","
        for val1, val2 in zip(valeurs, valeurs[1:]):
            ligne += signe(expr, (val1 + val2) / 2)
            ligne += ","
            ligne += separation(expr, val2)
            ligne += ","
        ligne += signe(expr, valeurs[-1] + 1)
        ligne += ", }"
    return ligne


### Fonctions principales


def signe_direct(expr, valeurs=None):
    try:
        var = expr.free_symbols.pop()
        if not valeurs:
            valeurs = expr.as_poly().real_roots()
    except:
        var = x
        valeurs = []
    valeurs.sort()
    res = r"\begin{tikzpicture}" "\n"
    res += r"\tkzTabInit[lgt=4,espcl=1.5]" "\n"
    res += "{{${}$ /1,".format(latex(var)) + "\n"
    res += " Signe de ${}$ /1}}".format(latex(expr)) + "\n"
    res += (
        r"{{$-\infty$, {}, $+\infty$}}".format(
            ", ".join(["$" + latex(r) + "$" for r in valeurs])
        )
        + "\n"
    )
    res += ligne_signe(expr, valeurs) + "\n"
    res += r"\end{tikzpicture}"
    return res


def signe_produit_et_quotient(expr):
    # etude = r" Signe de ${}$\\".format(latex(expr)) + '\n'
    etude = r"\onslide<+->"
    if not isinstance(expr, Mul):
        etude += (
            r" On factorise : \onslide<+-> {{${}={}$}}\\".format(
                latex(expr), latex(expr.factor())
            )
            + "\n"
        )
        expr = expr.factor()
    var = expr.free_symbols.pop()
    vals_notables = Eq(expr, 0).as_set()
    ## ajout des valeurs interdites des facteurs au dénominateur
    for arg in expr.args:
        if isinstance(arg, Pow) and arg.args[1] == -1:
            vals_notables = vals_notables.union(Eq(arg.args[0], 0).as_set())
    vals_notables = list(vals_notables)
    vals_notables.sort()
    etude += r"""\begin{tikzpicture}
\onslide<+->
\tkzTabInit[lgt=4,espcl=1.5]
"""
    etude += "{{${}$ /1,".format(latex(var)) + "\n"
    facteurs = list(expr.args)
    facteurs.reverse()
    for facteur in facteurs:
        if isinstance(facteur, Pow) and facteur.args[1] == -1:
            facteur = facteur.args[0]
        etude += " Signe de ${}$ /1,".format(latex(facteur)) + "\n"
    etude += " Signe de ${}$ /1}}".format(latex(expr)) + "\n"
    etude += (
        r"{{$-\infty$, {}, $+\infty$}}".format(
            ", ".join(
                [latex(r, mode="inline", fold_short_frac=False) for r in vals_notables]
            )
        )
        + "\n"
    )
    for facteur in facteurs:
        if isinstance(facteur, Pow) and facteur.args[1] == -1:
            facteur = facteur.args[0]
        etude += r"\onslide<+->{" + ligne_signe(facteur, vals_notables) + "}\n"
    etude += r"\onslide<+->{" + ligne_signe(expr, vals_notables) + "}\n"
    etude += r"\end{tikzpicture}\\" "\n"
    return etude


def resolution_inequation_produit_et_quotient(ineq):
    resolution = r"\onslide<+->{}" + "\n"
    if ineq.args[0] == 0:
        expr = ineq.args[1]
    elif ineq.args[1] == 0:
        expr = ineq.args[0]
    else:
        a = ineq.args[0]
        b = ineq.args[1]
        ineq2 = ineq.func(a - b, 0)
        resolution += (
            r"\onslide<+->{{${}$ équivaut à ${}$}}".format(latex(ineq), latex(ineq2))
            + "\n"
        )
        expr = a - b
    # resolution = latex(ineq,mode = 'inline')+'\n'
    resolution += signe_produit_et_quotient(expr)
    var = expr.free_symbols.pop()
    resolution += r"\onslide<+->{{${} \iff {} \in {}$}}".format(
        latex(ineq), latex(var), set_to_latex(ineq.as_set())
    )
    return resolution


def main():
    ex35 = [
        (-2 * x + 3) * (-3 * x - 5),
        (2 * x + 14) * (6 * x + 24),
        (5 * x - 65) * (7 - 2 * x),
        (-3 * x - 72) * (-4 * x - 96),
    ]
    # for pdt in ex35 :
    # print(r'\begin{frame}')
    # print(signe_produit_et_quotient(pdt))
    # print(r'\end{frame}')
    ex = [
        (2 * x + 3) * (-3 * x - 5) > 0,
        (x ** 2 + 3) * (2 * x + 5) <= 0,
        (5 * x - 65) * (7 - 2 * x) >= 0,
        ((3 * x - 4) * (x + 1)) < 0,
    ]
    ex38 = [
        (x + 6) ** 2 - 25,
        (5 * x - 3) ** 2 - (x - 4) ** 2,
        x ** 2 - 7 * x,
        (-3 * x + 8) * (7 * x - 4) - (-3 * x + 8) * (5 - 2 * x),
    ]
    ex39 = [
        x ** 2 > 16,
        16 * x ** 2 + 8 * x + 1 > 0,
        64 * x ** 2 - 121 > 0,
        49 - (3 + x) ** 2 <= 0,
    ]
    ex41 = [
        (8 * x - 1) ** 2 * (2 - 7 * x),
        (x - 4) * (9 * x + 2) * (5 - x),
        -3 * (5 * x - 1) * (x + 1) * (4 - 6 * x),
    ]
    for expr in ex38:

        print(r"\begin{{frame}}{{Signe de ${}$}}".format(latex(expr)))
        print(signe_produit_et_quotient(expr))
        print(r"\end{frame}")
    for ineq in ex39:
        print(r"\begin{{frame}}{{${}$}}".format(latex(ineq)))
        print(resolution_inequation_produit_et_quotient(ineq))
        print(r"\end{frame}")
    for expr in ex41:

        print(r"\begin{{frame}}{{Signe de ${}$}}".format(latex(expr)))
        print(signe_produit_et_quotient(expr))
        print(r"\end{frame}")
    # for ineq in ex :
    # print(r'\begin{{frame}}{{{}}}'.format()
    # latex(ineq,mode = 'inline'))
    # print(r'\pause')
    # print(resolution_inequation_produit_et_quotient(ineq))
    # print(r'\end{frame}')
    ineq = (2 * x - 3) / (4 - 5 * x) >= 0
    print(r"\begin{{frame}}{{{}}}".format(latex(ineq, mode="inline")))
    print(r"\pause")
    print(resolution_inequation_produit_et_quotient(ineq))
    print(r"\end{frame}")


if __name__ == "__main__":
    main()
