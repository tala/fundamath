#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import
from .compilateur import compileDocument
import unittest
from os import remove


class compilationTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def test_minimal(self):
        source = r"""
\documentclass[11pt,a4paper]{beamer}
\usepackage[utf8]{inputenc}
\author{N.Talabardon}
\begin{document}
blah
\end{document}
"""
        erreur = False
        try:
            compileDocument("test_minimal", source)
        except:
            erreur = True
        self.assertEqual(erreur, False)


if __name__ == "__main__":
    unittest.main()
