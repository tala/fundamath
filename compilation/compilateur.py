#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division
import sys
from subprocess import call
from platform import system
from os import remove, listdir, chdir, mkdir, curdir
from os.path import pardir, exists

if system() == "Windows":
    from os import startfile


def affiche_pdf(fichier):
    """
    extrait modifié d'actimaths
    """
    # Cas de Windows
    if system() == "Windows":
        startfile(fichier)
    # Cas de Mac OS X
    elif system() == "Darwin":
        call(["open", fichier])
    # Cas de Linux
    elif system() == "Linux":
        call(["xdg-open", fichier])


def compileDocument(nom, source):
    try:
        chdir("documents")
    except OSError:
        t, e = sys.exc_info()[:2]
        if e.errno == 2:
            mkdir("documents")
            chdir("documents")
    if exists(nom + ".tex"):
        i = 0
        while exists(nom + "_" + str(i) + ".tex"):
            i = i + 1
        nom = nom + "_" + str(i)
    with open(nom + ".tex", "w") as fichier:
        fichier.write(source)
    fichier.close()
    for i in range(2):
        call(
            [
                "pdflatex",
                "-shell-escape",
                "-synctex=1",
                "-interaction=nonstopmode",
                fichier.name,
            ]
        )
    prenom = fichier.name.split(".")[0]
    nomPdf = prenom + ".pdf"
    for ext in [
        ".aux",
        ".dvi",
        ".out",
        ".ps",
        ".nav",
        ".snm",
        ".toc",
        ".log",
        ".tkzfct.table",
        ".synctex.gz",
        ".tkzfct.gnuplot",
    ]:
        if prenom + ext in listdir(curdir):
            remove(prenom + ext)
    # call(['evince',nomPdf])
    # call(["xdg-open",nomPdf])
    affiche_pdf(nomPdf)
    chdir(pardir)
