#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

from random import choice, randint

import sympy
from sympy import QQ, Eq, Poly, Rational, Reals, solve, solveset, symbols

x = symbols("x")


def diffDiv(Z):
    Diff = []
    Div = []
    i = 0
    l = 0
    while i < len(Z):
        p = 1
        multiplicite = len(Z[i])
        while p < multiplicite:
            Div.append(Z[i][0])
            ligne = []
            fact = 1
            c = 0
            while c < p:
                ligne.append(sympy.Rational(Z[i][c + 1], 1) / fact)
                c = c + 1
                fact = c * fact
            if l != 0:
                while c < len(Diff[-1]) + 1:
                    ligne.append(
                        (ligne[c - 1] - Diff[-1][c - 1]) / (Div[l] - Div[l - c])
                    )
                    c = c + 1
            Diff.append(ligne)
            p = p + 1
            l = l + 1
        i = i + 1
    Res = []
    Res.append(Div)
    Res.append(Diff)
    return Res


def lagrange(X, Y):
    Z = [[X[i], Y[i]] for i in range(len(X))]
    Div = diffDiv(Z)[0]
    Diff = diffDiv(Z)[1]
    P = 1
    Res = 0
    for k in range(len(Diff)):
        Res = Res + Diff[k][k] * P
        P = P * (x - Div[k])
    return Res


def hermite(Z):
    """
    Prend en argument une liste ordonnée (selon x_i) de listes du type :
    [[x_i, y(x_i), y'(x_i), ...]
    Renvoie un polynôme vérifiant les contraintes ci-dessus
    (reste à le développer)
    """
    Div = diffDiv(Z)[0]
    Diff = diffDiv(Z)[1]
    P = 1
    Res = 0
    for k in range(len(Diff)):
        Res = Res + Diff[k][k] * P
        P = P * (x - Div[k])
    return Res


def hermiteParMorceaux(Z):
    """
    Prend en argument une liste ordonnée selon x_i de listes du type :
    [[x_i, y(x_i), y'(x_i), ...]
    Renvoie un liste du type :
    [[[x_min, xmax], [y_min, y_max], P(x)], ...]
    """
    m = len(Z) - 1
    Res = []
    for k in range(m):
        if Z[k][0] == Z[k + 1][0]:
            # nécessité de pouvoir imposer une dérivée à gauche différente
            # de la dérivée à droite
            pass
        else:
            Res.append(
                [
                    [Z[k][0], Z[k + 1][0]],
                    [Z[k][1], Z[k + 1][1]],
                    hermite([Z[k], Z[k + 1]]).expand(),
                ]
            )
    return Res


def hermiteParMorceauxC1(Z):
    m = len(Z) - 1
    Res = []
    for k in range(m):
        if k > 0 and len(Z[k]) == 2:
            # le nb derive en x_k n'est pas imposé. On s'assure donc de
            # la continuité de la dérivée.
            pente = Res[-1][2].diff().replace(x, Z[k][0])
            Z[k].append(pente)
            h = hermite([Z[k], Z[k + 1]]).expand()
        else:
            h = hermite([Z[k], Z[k + 1]]).expand()
        Res.append([[Z[k][0], Z[k + 1][0]], [Z[k][1], Z[k + 1][1]], h])
    return Res


def deriveHpm(hpm):
    # reste à déterminer ymax et xmax pour cette dérivée
    res = [[z[0], z[1], z[2].diff()] for z in hpm]
    return res


def primitiveSympy(f, x0=0, c=0):
    # Hypothese est faite que l'unique variable est x.
    F = f.integrate()
    c0 = F.replace(x, x0)
    return F - c0 + c


def integrateHpm(hpm, x0=0, c=0):
    # FAUTIF pour l'instant
    # reste à déterminer ymax et xmax pour cette primitive+ PB intervalle
    # contenant 0.
    z = hpm[0]
    res = [[z[0], z[1], primitiveSympy(z[2], x0=x0, c=c)]]
    for i, z in enumerate(hpm[1:]):
        Q = res[-1]
        res += [
            [z[0], z[1], primitiveSympy(z[2], x0=Q[0][1], c=Q[2].replace(x, Q[0][1]))]
        ]
    return res


def trace_hpm_c1_tkzfct(hh):
    ymin = min([h[1][0] for h in hh]) - 1
    ymax = max([h[1][1] for h in hh]) + 1
    res = r"""\begin{{tikzpicture}}
\tkzInit[xmin={},xmax={},ymin={},ymax={}]
\tkzGrid
\tkzAxeXY
""".format(
        hh[0][0][0] - 1, hh[-1][0][1] + 1, ymin, ymax
    )
    for h in hh:
        res += r"\draw[very thick,domain={}:{}] plot function{{{}}};" "\n".format(
            h[0][0], h[0][1], formate_poly_gnuplot(h[-1])
        )
    res += r"\end{tikzpicture}" "\n"
    return res


def formate_poly_gnuplot(expr):
    ensemble = expr.free_symbols
    var = ensemble.pop()
    p = expr.as_poly()
    coefficients = p.all_coeffs()
    coefficients.reverse()
    res = 0
    for k, coeff in enumerate(coefficients):
        # if isinstance(coeff, Rational):
        #     res += coeff * var ** k
        # else :
        res += float(coeff) * var ** k
    return str(res)


def createPoly():
    c = randint(1, 3) * choice([-1, 1])
    s = randint(1, 3) * choice([-1, 1])
    t = randint(1, 3) * choice([-1, 1])
    x0 = choice([k for k in range(-3, 3) if k != 0])
    P = hermite([[0, c], [x0, s, t]])
    return Poly(P.expand())


def solve_hpm_val(hpm, val):
    res = []
    for fct in hpm:
        # if fct[1][0] <= val <= fct[1][1] :
        res.extend(
            [
                sol
                for sol in solveset(Eq(fct[2], val), x, domain=Reals)
                if fct[0][0] <= sol <= fct[0][1]
            ]
        )
    return set(res)


def illustre_ineq_hpm(hpm, val, ineq="<="):
    pass


if __name__ == "__main__":
    print(hermite([[-1, 0], [0, 2], [2, 0], [4, -2]]))
    # for i in range(20):
    # P = createPoly()
    # print(P.domain)
    # while P.degree() != 2 or P.domain == QQ:
    # P = createPoly()
    # print(P.expand())
    # print(hermite([[0, 0, 0], [1, 1, 2], [3, 9, 6]]).expand())
    # print(hermite([[0, 0, 0], [1, 1, 2], [3, 9, 6]]) == x**2)
    # print(hermite([[0, 0], [1, 1], [3, -4]]).expand())
    # print(hermiteParMorceaux([[0, 0], [1, 1, 0], [3, -4], [4, 5, 0]]))
    # hh = hermiteParMorceauxC1([[0, 0], [1, 1, 0], [3, -4, 0], [4, 5, 0]])
    # print(hh)
    # # mise en forme du tracé de la fonction C1 et hermite-par-morceaux
    # # pour un tracé avec tkz-fct
    # prologue = r"\begin{tikzpicture}" "\n"
    # print(trace_hpm_c1_tkzfct(hh))
    # print(
    #     trace_hpm_c1_tkzfct(
    #         hermiteParMorceauxC1(
    #             [[-3, 2], [-2, 3, 0], [-1, 0], [0, -2, 0], [2, 0], [3, 5]]
    #         )
    #     )
    # )
    # print(
    #     trace_hpm_c1_tkzfct(
    #         hermiteParMorceauxC1([[0, 2, -1.5], [1, 1, 0], [4, 4, 0], [6, 0], [7, -4]])
    #     )
    # )
    # print(
    #     trace_hpm_c1_tkzfct(
    #         hermiteParMorceauxC1(
    #             [
    #                 [-5, -5],
    #                 [-4, 0],
    #                 [-2, 8.2, 0],
    #                 [0, 4],
    #                 [1, 0],
    #                 [3, -4],
    #                 [5, 0],
    #                 [7, 3.8, 0],
    #                 [8, 1.5],
    #             ]
    #         )
    #     )
    # )
    # hpm = hermiteParMorceauxC1(
    #     [[-3, 2], [-2, 3, 0], [-1, 0], [0, -2, 0], [2, 0], [3, 5]]
    # )
    # print(hpm)
    # print(solve_hpm_val(hpm, 0))
    # print(solve_hpm_val(hpm, 1))
