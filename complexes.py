# !/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports de la librairie standart de python
from __future__ import division

# Imports de sympy
from sympy import latex, I, re, im, Abs, arg, sqrt, Rational, pi, sin, cos, exp
from sympy.core.mul import Mul
from sympy.core.add import Add

# Imports de fonctions développées dans d'autres modules
from generateurTex import (
    generer_interro_flash_tex,
    generer_interro_flash_corrigee_tex,
    generer_correction_projetee_tex,
)
from compilation.compilateur import compileDocument

anglesDeReference = [0, pi, pi / 6, pi / 4, pi / 3, pi / 2]


# vocabulaire de base


def question_partie_reelle(expr):
    question = {}
    question["consigne"] = u"Déterminer la partie réelle du nombre" + u" complexe :"
    question["enonce"] = r"$z={}$".format(latex(expr))
    question.update(corrige_partie_reelle(expr))
    return question


def corrige_partie_reelle(expr):
    reponse = {}
    reponse["rappel texte"] = r"$z={}$".format(latex(expr))
    reponse["corrige"] = ur"$\Re(z)={}$".format(latex(re(expr)))
    return reponse


def question_partie_imaginaire(expr):
    question = {}
    question["consigne"] = u"Déterminer la partie imaginaire du nombre" + u" complexe :"
    question["enonce"] = r"$z={}$".format(latex(expr))
    question.update(corrige_partie_imaginaire(expr))
    return question


def corrige_partie_imaginaire(expr):
    reponse = {}
    reponse["rappel texte"] = r"$z={}$".format(latex(expr))
    reponse["corrige"] = ur"$\Im(z)={}$".format(latex(im(expr)))
    return reponse


def question_conjugue(expr):
    question = {}
    question["consigne"] = u"Déterminer le conjugué du nombre" + u" complexe :"
    question["enonce"] = r"$z={}$".format(latex(expr))
    question.update(corrige_conjugue(expr))
    return question


def corrige_conjugue(expr):
    reponse = {}
    reponse["rappel texte"] = r"$z={}$".format(latex(expr))
    reponse["corrige"] = ur"$\bar{{z}}={}$".format(latex(expr.conjugate()))
    return reponse


# forme module, argument et forme trigonométrique


def question_module(expr):
    question = {}
    question["consigne"] = u"Déterminer le module du nombre" + u" complexe :"
    question["enonce"] = r"$z={}$".format(latex(expr))
    question.update(corrige_module(expr))
    return question


def corrige_module(expr):
    reponse = {}
    reponse["rappel texte"] = r"$z={}$".format(latex(expr))
    reponse["corrige"] = ur"$|z|={}$".format(latex(Abs(expr)))
    return reponse


def question_argument(expr):
    question = {}
    question["consigne"] = (
        u"Déterminer la mesure principale de" + u" l'argument du nombre complexe :"
    )
    question["enonce"] = r"$z={}$".format(latex(expr))
    question.update(corrige_argument(expr))
    return question


def corrige_argument(expr):
    reponse = {}
    reponse["rappel texte"] = r"$z={}$".format(latex(expr))
    reponse["corrige"] = ur"$arg(z)={}$".format(latex(arg(expr)))
    return reponse


def question_forme_trigo(expr):
    question = {}
    question["consigne"] = (
        u"Déterminer une forme trigonométrique" + u" du nombre complexe :"
    )
    question["enonce"] = r"$z={}$".format(latex(expr))
    question["rappel texte"] = r"$z={}$".format(latex(expr))
    question.update(corrige_forme_trigo(expr))
    return question


def corrige_forme_trigo(expr):
    reponse = {}
    reponse["rappel texte"] = r"$z={}$".format(latex(expr))
    if Abs(expr) == 1:
        reponse["corrige"] = ur"$z=\cos\left({}\right)+i \sin\left({}\right)$".format(
            latex(arg(expr))
        )
    else:
        reponse["corrige"] = (
            ur"$z={0}\left(\cos\left({1}\right)" ur"+i \sin\left({1}\right)\right)$"
        ).format(latex(Abs(expr)), latex(arg(expr)))
    return reponse


def recherche_module(z):
    lignes = []
    lignes.append(ur"Le module de ${}$ est : ".format(latex(z)))
    if hasattr(re(z), "is_Atom") and not re(z).is_Atom or isinstance(re(z), Rational):
        a = r"\left({}\right)".format(latex(re(z)))
    else:
        a = latex(re(z))
    if hasattr(im(z), "is_Atom") and not im(z).is_Atom or isinstance(im(z), Rational):
        b = r"\left({}\right)".format(latex(im(z)))
    else:
        b = latex(im(z))
    lignes.append(ur"$\sqrt{{{}^2+{}^2}}={}$\\".format(a, b, latex(Abs(z))))
    return "\n".join(lignes)


def recherche_argument_complexe_unitaire(z):
    lignes = []
    lignes.append(ur"Soit $\theta$ un argument de ${}$.\\".format(latex(z)))
    lignes.append(
        (
            ur"Le point image d'affixe ${}$ est sur "
            ur"le cercle trigonométrique. Donc son abscisse est"
            ur" $\cos \theta$ et son ordonnée est $\sin \theta$."
            ur"\\"
        ).format(latex(z))
    )
    lignes.append(ur"$\theta$ est donc solution du système :\\")
    lignes.append(ur" $\left\{")
    lignes.append(ur"\begin{array}{rcl}")
    lignes.append(ur"\cos \theta & = & {} \\".format(latex(re(z))))
    lignes.append(ur"\sin \theta & = & {} \\".format(latex(im(z))))
    lignes.append(ur"\end{array}")
    lignes.append(ur"\right.$\\")
    if arg(z) in anglesDeReference:
        lignes.append(
            (
                ur"On reconnaît $\cos\left({0}\right)$ et" ur" $\sin\left({0}\right)$."
            ).format(latex(arg(z)))
        )
        lignes.append(ur"$\theta = {}$ convient.".format(latex(arg(z))))
    elif pi - arg(z) in anglesDeReference:
        lignes.append(
            (
                ur"${0}=-\cos\left({2}\right)$ et" ur" ${1}=\sin\left({2}\right)$."
            ).format(latex(re(z)), latex(im(z)), latex(arg(z) + pi))
        )
        lignes.append(
            ur"$\theta = \pi-{}={}$ convient.".format(latex(pi - arg(z)), latex(arg(z)))
        )
    elif -arg(z) in anglesDeReference:
        lignes.append(
            (
                ur"${0}=\cos\left({2}\right)$ et" ur" ${1}=-\sin\left({2}\right)$."
            ).format(latex(re(z)), latex(im(z)), latex(arg(z) + pi))
        )
        lignes.append(ur"$\theta = {}$ convient.".format(latex(arg(z))))
    elif arg(z) + pi in anglesDeReference:
        lignes.append(
            (
                ur"${0}=-\cos\left({2}\right)$ et" ur" ${1}=-\sin\left({2}\right)$."
            ).format(latex(re(z)), latex(im(z)), latex(arg(z) + pi))
        )
        lignes.append(
            ur"$\theta = {}-\pi={}$ convient.".format(latex(pi + arg(z)), latex(arg(z)))
        )
    return "\n".join(lignes)


def recherche_argument(z):
    lignes = []
    lignes.extend(recherche_module(z).split("\n"))
    if Abs(z) == 1:
        lignes.extend(recherche_argument_complexe_unitaire(z).split("\n"))
    else:
        lignes.append(
            ur"$|z|\neq 1$. Donc le point $M$, image de $z$"
            ur" dans le plan complexe n'est pas sur"
            ur" le cercle trigonométrique.\\"
        )
        lignes.append(
            ur"Par contre le point d'affixe $\dfrac{z}{|z|}$"
            ur" est sur le cercle trigonométrique et il a"
            ur" les même arguments que $z$.\\"
        )
        lignes.append(
            ur"On cherche donc un argument de"
            ur" $\dfrac{{z}}{{|z|}}=\dfrac{{{}}}{{{}}}={}$.\\".format(
                latex(z), latex(Abs(z)), latex(re(z / Abs(z)) + I * im(z / Abs(z)))
            )
        )
        lignes.extend(
            recherche_argument_complexe_unitaire(
                re(z / Abs(z)) + I * im(z / Abs(z))
            ).split("\n")
        )
    lignes.append(ur"$\arg(z)={}+2k\pi $ où $k \in \mathbb{{Z}}$".format(latex(arg(z))))
    return "\n".join(lignes)


def question_forme_algebrique(module, argument):
    question = {}
    question["consigne"] = (
        u"Déterminer la forme algébrique du"
        + ur" nombre complexe de module $\rho$ et d'argument $\theta$ :"
    )
    question["enonce"] = r"$\rho={}$ ; $\theta={}$".format(
        latex(module), latex(argument)
    )
    question.update(corrige_forme_algebrique(module, argument))
    return question


def corrige_forme_algebrique(module, argument):
    reponse = {}
    reponse["rappel texte"] = ur"$|z|=\rho={}$ ; $\arg(z)=\theta={} $".format(
        latex(module), latex(argument)
    )
    reponse["corrige"] = (
        ur"$z="
        ur"\rho\left(\cos(\theta)+i\sin(\theta)\right)$\\$="
        ur"{0}\left(\cos({1})+i\sin({1})\right)$\\$="
        ur"{0}\left({2}+{3}\right)={4}$"
    ).format(
        latex(module),
        latex(argument),
        latex(cos(argument)),
        latex(sin(argument) * I),
        latex(module * (cos(argument) + sin(argument) * I)),
    )
    return reponse


# associations nombre complexe et point


def dessine_points_images(points, affixes):
    instructions = [ur"\begin{tikzpicture}[scale=0.5]"]
    instructions.append(ur"\tkzInit[xmin=-5,ymin=-5,xmax=5,ymax=5]")
    instructions.append(ur"\tkzGrid[sub,subxstep=0.5,subystep=0.5]")

    instructions.append(ur"\draw[very thick,-latex] (0,0) -- (0,1);")
    instructions.append(ur"\draw[very thick,-latex] (0,0) -- (1,0);")
    instructions.append(ur"\draw (0.5,0) node[below] {$\vec u$};")
    instructions.append(ur"\draw (0,0.5) node[left] {$\vec v$};")
    instructions.append(ur"\draw (0,0) node[below left] {O};")

    instructions.append(ur"\tkzDrawXY")
    for p, z in zip(points, affixes):
        instructions.append(ur"\draw ({},{}) node {{$\times$}};".format(re(z), im(z)))
        instructions.append(
            ur"\draw ({},{}) node[below right] {{{}}};".format(re(z), im(z), p)
        )
    instructions.append(ur"\end{tikzpicture}")
    return "\n".join(instructions)


def repere_modules_arguments(points, affixes):
    instructions = [ur"\begin{tikzpicture}[scale=0.5]"]
    instructions.append(ur"\tkzInit[xmin=-5,ymin=-5,xmax=5,ymax=5]")
    instructions.append(ur"\tkzGrid[sub,subxstep=0.5,subystep=0.5]")

    instructions.append(ur"\draw[very thick,-latex] (0,0) -- (0,1);")
    instructions.append(ur"\draw[very thick,-latex] (0,0) -- (1,0);")
    instructions.append(ur"\draw (0.5,0) node[below] {$\vec u$};")
    instructions.append(ur"\draw (0,0.5) node[left] {$\vec v$};")
    instructions.append(ur"\draw (0,0) node[below left] {O};")

    instructions.append(ur"\tkzDrawXY")
    for p, z in zip(points, affixes):
        instructions.append(ur"\draw ({},{}) node {{$\times$}};".format(re(z), im(z)))
        instructions.append(
            ur"\draw ({},{}) node[below right] {{{}}};".format(re(z), im(z), p)
        )
        instructions.append(ur"\draw (0,0) -- ({},{}) ;".format(re(z), im(z)))
        instructions.append(
            ur"\draw[thick,->,>=latex] (0:1) arc (0:{}:1);".format((arg(z) / pi) * 180)
        )
    instructions.append(ur"\end{tikzpicture}")
    return "\n".join(instructions)


def question_lecture_affixe(expr):
    question = {}
    question["consigne"] = (
        u"Dans le repère"
        + ur" $\left(O;\vec u,\vec v\right)$ représenté ci-dessous,"
        + ur"lire l'affixe du point M."
    )
    question["enonce"] = dessine_points_images(["M"], [expr])
    question.update(corrige_lecture_affixe(expr))
    return question


def corrige_lecture_affixe(expr):
    reponse = {}
    reponse["rappel texte"] = r"\ "
    reponse["corrige"] = (
        dessine_points_images(["M"], [expr])
        + r"\\"
        + "\n"
        + ur"L'affixe du point M est le nombre complexe "
        + ur"$z={}$".format(latex(expr))
    )
    return reponse


# calculs élémentaires


def question_developpement(expr):
    question = {}
    question["consigne"] = u"Déterminer la forme algébrique" + u" du nombre complexe :"
    question["enonce"] = r"$z={}$".format(latex(expr))
    question.update(corrige_developpement(expr))
    return question


def corrige_developpement(expr):
    reponse = {}
    reponse["rappel texte"] = r"$z={}$".format(latex(expr))
    reponse["corrige"] = ur"$z={}$".format(latex(expr.expand()))
    return reponse


def formate(op, A, B):
    if op == "__add__":
        return r"$z=\left({}\right)+\left({}\right)$".format(latex(A), latex(B))
    elif op == "__sub__":
        return r"$z=\left({}\right)-\left({}\right)$".format(latex(A), latex(B))
    elif op == "__mul__":
        if isinstance(A, Add):
            mbrg = r"\left({}\right)".format(latex(A))
        else:
            mbrg = latex(A)
        if isinstance(B, Add):
            mbrd = r"\left({}\right)".format(latex(B))
        else:
            mbrd = latex(B)
        return r"$z={}{}$".format(mbrg, mbrd)


def question_simplifie(op, A, B):
    question = {}
    question["consigne"] = u"Déterminer la forme algébrique" + u" du nombre complexe :"
    question["enonce"] = formate(op, A, B)
    question.update(corrige_simplifie(op, A, B))
    return question


def corrige_simplifie(op, A, B):
    reponse = {}
    reponse["rappel texte"] = formate(op, A, B)
    resultat = getattr(A, op)(B)
    resultat = resultat.expand()
    reponse["corrige"] = ur"$z={}$".format(latex(resultat))
    return reponse


# inverse et quotient


def question_inverse(expr):
    question = {}
    question["consigne"] = (
        u"Déterminer la forme algébrique" + u" de l'inverse du nombre complexe :"
    )
    question["enonce"] = r"$z={}$".format(latex(expr))
    question.update(corrige_inverse(expr))
    return question


def corrige_inverse(expr):
    reponse = {}
    reponse["rappel texte"] = "inverse de $z={}$ :".format(latex(expr))
    resultat = 1 / (expr)
    resultat = re(resultat) + I * im(resultat)
    reponse["corrige"] = ur"$z^{{-1}}=\dfrac{{1}}{{z}}={}$".format(latex(resultat))
    return reponse


def question_quotient(z1, z2):
    question = {}
    question["consigne"] = u"Déterminer la forme algébrique" + u" du quotient suivant :"
    question["enonce"] = r"$z=\dfrac{{{}}}{{{}}}$".format(latex(z1), latex(z2))
    question.update(corrige_quotient(z1, z2))
    return question


def corrige_quotient(z1, z2):
    reponse = {}
    reponse["rappel texte"] = (
        ur"forme algébrique de"
        + ur"$z=\dfrac{{{}}}{{{}}}$".format(latex(z1), latex(z2))
    )
    resultat = z1 / z2
    resultat = re(resultat) + I * im(resultat)
    reponse["corrige"] = ur"$z={}$".format(latex(resultat))
    return reponse


def question_quotient_et_formes_exponentielles(z1, z2):
    question = {}
    question["consigne"] = (
        u"Soient les nombres complexes $z_1$ et $z_2$"
        + u" définis par : $z_1={}$ et $z_2={}$.".format(latex(z1), latex(z2))
    )
    question["enonce"] = (
        ur"Déterminez la forme exponentielle de " + ur"$ \dfrac {z_1} {z_2}$."
    )
    question.update(corrige_quotient_et_formes_exponentielles(z1, z2))
    return question


def corrige_quotient_et_formes_exponentielles(z1, z2):
    reponse = {}
    reponse["rappel texte"] = ur"$z_1={}$ et $z_2={}$.".format(latex(z1), latex(z2))
    module = (Abs(z1) / Abs(z2)).simplify()
    argument = arg(z1) - arg(z2)
    print latex(argument)
    resultat = module * exp(I * argument)
    reponse["corrige"] = ur"$\dfrac {{z_1}} {{z_2}}={}$".format(latex(resultat))
    return reponse


def question_produit_et_formes_exponentielles(z1, z2):
    question = {}
    question["consigne"] = (
        u"Soient les nombres complexes $z_1$ et $z_2$"
        u" définis par : $z_1={}$ et $z_2={}$.".format(latex(z1), latex(z2))
    )
    question["enonce"] = (
        ur"Déterminez la forme exponentielle de " + ur"$ z_1 \times z_2 $."
    )
    question.update(corrige_produit_et_formes_exponentielles(z1, z2))
    return question


def corrige_produit_et_formes_exponentielles(z1, z2):
    reponse = {}
    reponse["rappel texte"] = ur"$z_1={}$ et $z_2={}$.".format(latex(z1), latex(z2))
    module = (Abs(z1) * Abs(z2)).simplify()
    argument = arg(z1) + arg(z2)
    resultat = module * exp(I * argument)
    reponse["corrige"] = ur"$z_1 \times z_2={}$".format(latex(resultat))
    return reponse


def question_forme_exponentielle(z):
    question = {}
    question["consigne"] = u"Soient le nombre complexe $z={}$.".format(latex(z))
    question["enonce"] = ur"Déterminez la forme exponentielle de " + ur"$ z $."
    question.update(corrige_forme_exponentielle(z))
    return question


def corrige_forme_exponentielle(z):
    reponse = {}
    reponse["rappel texte"] = ur"$z={}$.".format(latex(z))
    module = Abs(z).simplify()
    argument = arg(z)
    resultat = latex(module) + "e^{{{}}}".format(latex(I * argument))
    reponse["corrige"] = ur"$z={}$".format(resultat)
    return reponse


IF10 = {
    "classe": "1STI2D",
    "date": "Mardi 7 mars 2016",
    "num": 10,
    "questions": [
        {
            "gauche": question_partie_reelle(2 * I + 3),
            "droite": question_partie_imaginaire(2 * I + 3),
            "temps": 20,
        },
        {
            "gauche": question_partie_imaginaire(I * (2 * I + 3)),
            "droite": question_partie_reelle(I * (2 * I + 3)),
            "temps": 30,
        },
        {
            "gauche": question_developpement((-3 * I + 4) * (2 * I)),
            "droite": question_simplifie("__mul__", (2 * I - 5), (3 * I)),
            "temps": 60,
        },
        {
            "gauche": question_simplifie("__mul__", (-2 * I + 3), (3 * I)),
            "droite": question_developpement((-2 * I - 5) * (-2 * I)),
            "temps": 60,
        },
        {
            "gauche": question_developpement((-2 * I - 5) * (2 * I - 5)),
            "droite": question_developpement((-3 * I + 4) * (3 * I + 4)),
            "temps": 60,
        },
    ],
}
IF11 = {
    "classe": "1STI2D",
    "date": "Jeudi 10 mars 2016",
    "num": 11,
    "questions": [
        {
            "gauche": question_lecture_affixe(-2 + 3 * I),
            "droite": question_partie_imaginaire((-2 + 3 * I) * I),
            "temps": 30,
        },
        {
            "gauche": question_partie_imaginaire((3 - 2 * I) * I),
            "droite": question_lecture_affixe(3 - 2 * I),
            "temps": 30,
        },
        {
            "gauche": question_simplifie("__sub__", 2 - 5 * I, 7 - I),
            "droite": question_simplifie("__mul__", 2 - 5 * I, 7 - I),
            "temps": 60,
        },
        {
            "gauche": question_developpement((7 + 2 * I) * (7 - 2 * I)),
            "droite": question_simplifie("__sub__", 2 - 5 * I, 7 - 2 * I),
            "temps": 60,
        },
        {
            "gauche": question_simplifie("__mul__", 3 - I, 5 - 2 * I),
            "droite": question_developpement((2 + 3 * I) * (2 - 3 * I)),
            "temps": 60,
        },
    ],
}
IF12 = {
    "classe": "1STI2D",
    "date": "Lundi 14 mars 2016",
    "num": 12,
    "questions": [
        {
            "gauche": question_lecture_affixe(-2 - I),
            "droite": question_conjugue(7 - 5 * I),
            "temps": 30,
        },
        {
            "gauche": question_conjugue(-6 + 3 * I),
            "droite": question_lecture_affixe(-3 + 4 * I),
            "temps": 30,
        },
        {
            "gauche": question_partie_imaginaire(I * (5 - 2 * I)),
            "droite": question_partie_reelle(I * (5 - 2 * I)),
            "temps": 30,
        },
        {
            "gauche": question_partie_reelle((2 * I) * (2 + 3 * I)),
            "droite": question_partie_imaginaire((3 * I) * (2 - 2 * I)),
            "temps": 30,
        },
        {
            "gauche": question_developpement((3 + 2 * I) * (2 - 5 * I)),
            "droite": question_developpement((5 - 2 * I) * (5 + 2 * I)),
            "temps": 120,
        },
        {
            "gauche": question_developpement((-3 + I) * (-3 - I)),
            "droite": question_developpement((3 + 2 * I) * (7 - 2 * I)),
            "temps": 120,
        },
    ],
}
IF13 = {
    "classe": "1STI2D",
    "date": "Jeudi 17 mars 2016",
    "num": 13,
    "questions": [
        {
            "gauche": question_partie_imaginaire(2 * I * (3 * I - 5)),
            "droite": question_conjugue(3 * I - 5),
            "temps": 30,
        },
        {
            "gauche": question_conjugue(3 - I),
            "droite": question_partie_imaginaire(3 * I * (5 - I)),
            "temps": 30,
        },
        {
            "gauche": question_developpement((4 + 3 * I) * (4 - 3 * I)),
            "droite": question_developpement((2 - 5 * I) * (2 + 5 * I)),
            "temps": 60,
        },
        {
            "gauche": question_developpement((3 + 2 * I) * (2 - I)),
            "droite": question_inverse(2 + I),
            "temps": 120,
        },
        {
            "gauche": question_inverse(1 - 2 * I),
            "droite": question_developpement((2 + 3 * I) * (1 - 2 * I)),
            "temps": 120,
        },
    ],
}
IF14 = {
    "classe": "1STI2D",
    "date": "Mardi 22 mars 2016",
    "num": 14,
    "questions": [
        {
            "gauche": question_developpement((2 * I) * (3 - 5 * I)),
            "droite": question_developpement((3 - 2 * I) * (3 + 2 * I)),
            "temps": 60,
        },
        {
            "gauche": question_developpement((2 + 3 * I) * (2 - 3 * I)),
            "droite": question_developpement((3 * I) * (1 + 5 * I)),
            "temps": 60,
        },
        {
            "gauche": question_inverse(-I + 2),
            "droite": question_conjugue(3 * I - 5),
            "temps": 90,
        },
        {
            "gauche": question_conjugue(-5 * I - 2),
            "droite": question_inverse(3 * I - 1),
            "temps": 90,
        },
        {
            "gauche": question_argument(-5 * I),
            "droite": question_module(3 * I - 1),
            "temps": 90,
        },
        {
            "gauche": question_module(3 - 2 * I),
            "droite": question_argument(-3),
            "temps": 90,
        },
        {
            "gauche": question_forme_algebrique(3, pi / 3),
            "droite": question_forme_algebrique(3, pi),
            "temps": 90,
        },
    ],
}
# exemples_formes_trigo ={'classe':"1STI2D",'date':'Mardi 22 mars 2016','num': 'XX',
# 'questions':[

# {'gauche': question_forme(2*I*(3*I-5)),
# 'droite': question_conjugue(3*I-5),
# 'temps' : 30
# },
# {'gauche': question_conjugue(3-I),
# 'droite': question_partie_imaginaire(3*I*(5-I)),
# 'temps' : 30
# },
# {'gauche': question_developpement((4+3*I)*(4-3*I)),
# 'droite': question_developpement((2-5*I)*(2+5*I)),
# 'temps' : 60
# },
# {'gauche': question_developpement((3+2*I)*(2-I)),
# 'droite': question_inverse(2+I),
# 'temps' : 120
# },
# {'gauche': question_inverse(1-2*I),
# 'droite': question_developpement((2+3*I)*(1-2*I)),
# 'temps' : 120
# }
# ]}
IFT0 = {
    "classe": "TSTI2D",
    "date": "Lundi 9 mai 2016",
    "num": 0,
    "objectifs": [
        {"id": "CP101", "titre": u"Écrire un quotient sous sa forme algébrique."}
    ],
    "questions": [
        {
            "gauche": question_quotient_et_formes_exponentielles(
                2 * exp(-I * pi / 4), Rational(1, 2) * exp(I * pi / 6)
            ),
            "droite": question_produit_et_formes_exponentielles(
                2 * exp(-I * pi / 4), Rational(2, 3) * exp(I * pi / 6)
            ),
            "temps": 90,
        },
        {
            "gauche": question_forme_exponentielle(2 + 2 * I),
            "droite": question_forme_exponentielle(Rational(3, 2) - (sqrt(3) / 2) * I),
            "temps": 90,
        },
    ],
}
correction = {
    "classe": "TSTI2D",
    "date": "Lundi 9 mai 2016",
    "exercices": [
        {
            "ref": "38 p 183",
            "questions": [
                {
                    "enonce": "Quel est la couleur du cheval blanc d'henri IV ?",
                    "corrige": ["blanc ", u"évidement"],
                },
                {
                    "enonce": "Pourquoi les murs de La Rochelle sont-ils blancs ?",
                    "corrige": ["parce que transatlantique"],
                },
            ],
        }
    ],
}

if __name__ == "__main__":
    print repere_modules_arguments(["M"], [-1 - I])
    print question_quotient_et_formes_exponentielles(
        2 * exp(-I * pi / 4), Rational(1, 2) * exp(I * pi / 6)
    )
    # print recherche_module(sqrt(3)/2+Rational(1,2)*I)
    # print ""
    # print recherche_argument(1/sqrt(2)+(1/sqrt(2))*I)
    # print ""
    # print recherche_argument(sqrt(3)/2+Rational(1,2)*I)
    # print ""
    # print recherche_argument(-sqrt(3)/2+Rational(1,2)*I)
    # print ""
    # print recherche_argument(sqrt(3)/2-Rational(1,2)*I)
    # print ""
    # print recherche_argument(-sqrt(3)/2-Rational(1,2)*I)
    # compileDocument('1STI2D_IF10',generer_interro_flash_corrigee_tex(IF10))
    # compileDocument('1STI2D_IF11',generer_interro_flash_corrigee_tex(IF11))
    # compileDocument('1STI2D_IF13',generer_interro_flash_corrigee_tex(IF13))
    # compileDocument('1STI2D_IF14',generer_interro_flash_corrigee_tex(IF14))
    # compileDocument('TSTI2D_IF25', generer_interro_flash_corrigee_tex(IFT0))
    compileDocument("TSTI2D_cp0", generer_correction_projetee_tex(correction))
