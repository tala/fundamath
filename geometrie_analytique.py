#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals, division

import string
from itertools import cycle, islice, product
from random import choice, sample, shuffle

from sympy import Add, Mul, Rational, cos, gcd, pi, sin, sqrt

from compilation.compilateur import compileDocument
from donnees.donnees import Donnees
from generateurTex import (
    generer_correction_dm,
    generer_corrige_exos_livre,
    generer_dm,
    generer_dm_1_2GT,
)
from sympy_to_latex import reformat_latex as latex

GT2 = Donnees["classes"][1]


LETTRES = string.ascii_uppercase.replace("O", "").replace("I", "").replace("J", "")

COORDONNEES_ENTIERES_1 = [coord for coord in product(range(-5, 6), range(-5, 6))]

DENOMINATEURS = range(1, 7)


def fractions_irreductibles(_min, _max):
    """
    retourne la liste des fractions du type a/b telles que a soit
    premier avec b, que b soit un des dénominateurs autorisé et que la
    fraction a/b soit comprise entre _min et _max.
    """
    res = []
    for b in DENOMINATEURS:
        res.extend(
            [Rational(a, b) for a in range(_min * b, _max * b + 1) if gcd(a, b) == 1]
        )
    return res


def rotation(centre, angle, point):
    """
    Retourne le point image du point donné par la rotation de centre
    donné et d'angle donné.
    """
    xA = centre[1]
    yA = centre[2]
    x = point[1]
    y = point[2]
    res = [
        point[0] + "'",
        xA + cos(angle) * (x - xA) - sin(angle) * (y - yA),
        yA + sin(angle) * (x - xA) + cos(angle) * (y - yA),
    ]
    return res


def format_coordonnees(x, y):
    return r"$\left({};{}\right)$".format(latex(x), latex(y))


def format_segment(A, B):
    return r"$[{}{}]$".format(A, B)


def presente_points_coordonnees(points):
    texte_nom = ", ".join([point[0] for point in points[:-1]]) + " et " + points[-1][0]
    texte_coordonnees = (
        ", ".join([format_coordonnees(point[1], point[2]) for point in points[:-1]])
        + " et "
        + format_coordonnees(points[-1][1], points[-1][2])
    )
    texte = (r"Soient les points {} de coordonnées respectives {}.").format(
        texte_nom, texte_coordonnees
    )
    return texte


def milieu(pointA, pointB, nomMilieu="M"):
    return [nomMilieu, (pointA[1] + pointB[1]) / 2, (pointA[2] + pointB[2]) / 2]


def longueur(pointA, pointB):
    return (
        pointA[0] + pointB[0],
        sqrt((pointA[1] - pointB[1]) ** 2 + (pointA[2] - pointB[2]) ** 2),
    )


def longueurs(points):
    if len(points) > 2:
        boucle = list(islice(cycle(points), 1, len(points) + 1))
        return list(map(longueur, points, boucle))
    elif len(points) == 2:
        return longueur(*points)
    else:
        return "erreur : une longueur nécessite au moins deux points"


def question_coordonnees_milieu(pointA, pointB, nomMilieu):
    liste = [pointA, pointB]
    shuffle(liste)
    point1, point2 = liste
    texte = (
        r"Soient les points {} et {} de coordonnées " r"respectives {} et {}"
    ).format(
        point1[0],
        point2[0],
        format_coordonnees(point1[1], point1[2]),
        format_coordonnees(point2[1], point2[2]),
    )
    texte += "\n"
    texte += r"Déterminer les coordonnées du point ${}$, milieu du segment ".format(
        nomMilieu[0]
    )
    texte += format_segment(pointA[0], pointB[0])
    texte += "."
    return texte


def question_symetrique(pointA, pointB, symetrique):
    liste = [pointA, pointB]
    shuffle(liste)
    point1, point2 = liste
    texte = (
        r"Soient les points {} et {} de coordonnées " r"respectives {} et {}"
    ).format(
        point1[0],
        point2[0],
        format_coordonnees(point1[1], point1[2]),
        format_coordonnees(point2[1], point2[2]),
    )
    texte += "\n"
    texte += r"Déterminer les coordonnées du point ${}$, symétrique de  ${}$ par rapport à ${}$.".format(
        symetrique[0], pointA[0], pointB[0]
    )
    return texte


def question_longueur(pointA, pointB):
    liste = [pointA, pointB]
    shuffle(liste)
    point1, point2 = liste
    texte = (
        r"Soient les points {} et {} de coordonnées " r"respectives {} et {}"
    ).format(
        point1[0],
        point2[0],
        format_coordonnees(point1[1], point1[2]),
        format_coordonnees(point2[1], point2[2]),
    )
    texte += "\n"
    texte += r"Déterminer la longueur  ${}{}$.".format(pointA[0], pointB[0])
    return texte


def configuration_carre(pointA, pointB, nomC, nomD, direct=True):
    """
    Renvoie la liste des quatre sommets d'un carré ayant pour premiers
    sommets pointA et pointB. Les sommets étants pris soit dans le sens
    direct, si direct vaut True, soit dans le sens indirect.
    """
    x_A, y_A = pointA[1:]
    x_B, y_B = pointB[1:]
    if direct:
        pointD = [nomD, x_A + y_A - y_B, y_A + x_B - x_A]
    else:
        pointD = [nomD, x_A + y_B - y_A, y_A + x_A - x_B]
    x_D, y_D = pointD[1:]
    pointC = [nomC, x_D + x_B - x_A, y_D + y_B - y_A]
    return [pointA, pointB, pointC, pointD]


def configuration_losange_1(pointA, nomB, nomC, nomD, vecteur_diagonal, k):
    """
    Renvoie la liste des sommets d'un losange ayant pour premier sommet
    pointA, comme premier vecteur diagonal vecteur_diagonal de
    coordonnées (a,b) et de deuxième vecteur diagonal le vecteur k(-b,a)
    """
    x_A, y_A = pointA[1:]
    a, b = vecteur_diagonal
    pointC = [nomC, x_A + a, y_A + b]
    x_C, y_C = pointC[1:]
    pointB = [nomB, x_A + Rational(a - k * b, 2), y_A + Rational(b + k * a, 2)]
    x_B, y_B = pointB[1:]
    pointD = [nomD, x_C + x_A - x_B, y_C + y_A - y_B]
    return [pointA, pointB, pointC, pointD]


def exercice_parallelogramme(pointA, pointB, pointC, nomD, nomO):
    liste = [pointA, pointB, pointC]
    texte = presente_points_coordonnees(liste)
    question1 = (
        r"Calculer les coordonnées du point ${}$, "
        r"milieu du segment $\left[{};{}\right]$."
    ).format(nomO, pointA[0], pointC[0])
    question2 = (
        r"Déterminer les coordonnées du point {} "
        r"tel que {} soit aussi le milieu du segment "
        r"$\left[{}{}\right]."
    ).format(nomD, nomO, pointB[0], nomD)
    question3 = r"Quelle est la nature du quadrilatère ${}{}{}{}$ ?".format(
        pointA[0], pointB[0], pointC[0], nomD
    )
    return {"enonce": texte, "questions": [question1, question2, question3]}


def exercice_parallelogramme2(pointA, pointB, pointC, nomD):
    pointD = [
        nomD,
        pointC[1] + pointA[1] - pointB[1],
        pointC[2] + pointA[2] - pointB[2],
    ]
    liste = [pointA, pointB, pointC, pointD]
    texte = presente_points_coordonnees(liste)
    question = (
        "Déterminez, en la justifiant la nature " "du quadrilatère ${}{}{}{}$"
    ).format(pointA[0], pointB[0], pointC[0], nomD)
    return {"enonce": texte, "questions": [question]}


def exercice_triangle_rectangle(pointA, pointB, nomC, k):
    pointAux = rotation(pointA, pi / 2, pointB)
    pointC = [nomC] + [
        pointA[1] + k * (pointAux[1] - pointA[1]),
        pointA[2] + k * (pointAux[2] - pointA[2]),
    ]
    liste = [pointA, pointB, pointC]
    shuffle(liste)
    texte = presente_points_coordonnees(liste)
    texte += "\n"
    couples = [(liste[0], liste[1]), (liste[0], liste[2]), (liste[1], liste[2])]

    texte += r"Déterminer les longueur  ${}{}$, ${}{}$ et ${}{}$.\\".format(
        liste[0][0], liste[1][0], liste[0][0], liste[2][0], liste[1][0], liste[2][0]
    )
    texte += "\n"
    texte += (
        r"Que pouvez-vous en déduire pour la nature du "
        r"triangle {}{}{} ?".format(*[pt[0] for pt in liste])
    )
    return texte


def exercice_triangle_rectangle_isocele(pointA, pointB, nomC):
    pointC = [nomC] + rotation(pointA, pi / 2, pointB)[1:]
    liste = [pointA, pointB, pointC]
    shuffle(liste)
    texte = presente_points_coordonnees(liste)
    texte += "\n"
    couples = [(liste[0], liste[1]), (liste[0], liste[2]), (liste[1], liste[2])]

    texte += r"Déterminer les longueur  ${}{}$, ${}{}$ et ${}{}$.\\".format(
        liste[0][0], liste[1][0], liste[0][0], liste[2][0], liste[1][0], liste[2][0]
    )
    texte += "\n"
    texte += (
        r"Que pouvez-vous en déduire pour la nature du "
        r"triangle {}{}{} ?".format(*[pt[0] for pt in liste])
    )
    return texte


def wrapp_1(n):
    if n < 0 or isinstance(n, Mul):
        return r"\left({}\right)".format(latex(n))
    else:
        return latex(n)


def corrige_longueur(pointA, pointB):
    texte = r"${0}{1}=\sqrt{{ \left(x_{{{1}}}-x_{{{0}}} \right)^2 +\left(y_{{{1}}}-y_{{{0}}} \right)^2 }}$\\".format(
        pointA[0], pointB[0]
    )
    texte += "\n"
    texte += r"${0}{1}=\sqrt{{ \left({2} - {3} \right)^2 +\left({4} - {5} \right)^2 }}$\\".format(
        pointA[0],
        pointB[0],
        wrapp_1(pointB[1]),
        wrapp_1(pointA[1]),
        wrapp_1(pointB[2]),
        wrapp_1(pointA[2]),
    )
    texte += "\n"
    texte += r"${0}{1}=\sqrt{{ \left({2} \right)^2 +\left({3} \right)^2 }}$\\".format(
        pointA[0],
        pointB[0],
        latex(Add(pointB[1], -pointA[1], evaluate=False)),
        latex(Add(pointB[2], -pointA[2], evaluate=False)),
    )
    texte += "\n"
    texte += r"${0}{1}=\sqrt{{{2}^2 +{3}^2 }}$\\".format(
        pointA[0],
        pointB[0],
        wrapp_1(pointB[1] - pointA[1]),
        wrapp_1(pointB[2] - pointA[2]),
    )
    texte += "\n"
    texte += r"${0}{1}=\sqrt{{ {2} +{3} }}".format(
        pointA[0],
        pointB[0],
        latex((pointB[1] - pointA[1]) ** 2),
        latex((pointB[2] - pointA[2]) ** 2),
    )
    carre = (pointB[1] - pointA[1]) ** 2 + (pointB[2] - pointA[2]) ** 2
    if latex(sqrt(carre)) != r"\sqrt{{{}}}".format(latex(carre)):
        texte += r"=\sqrt{{{}}}".format(latex(carre))
    texte += r"=\mathbf{{{}}}$".format(latex(sqrt(carre)))
    texte += r"\\" + "\n"
    return texte


def corrige_coordonnees_milieu(pointA, pointB, nomMilieu="M"):
    res = r"Le point ${}$ est le milieu de $[{}{}]$.\\".format(
        nomMilieu[0], pointA[0], pointB[0]
    )
    res += "\n"
    res += r"Il a donc pour coordonnées :\\"
    res += "\n"
    res += (
        r"$x_{{{0}}}=\dfrac{{x_{{{1}}}+x_{{{2}}}}}{{2}}$ "
        r"et $y_{{{0}}}=\dfrac{{y_{{{1}}}+y_{{{2}}}}}{{2}}$\\"
    ).format(nomMilieu[0], pointA[0], pointB[0])
    res += "\n"
    res += r"$x_{{{0}}}=\dfrac{{{1}}}{{2}}={2}$ \\".format(
        nomMilieu[0],
        latex(Add(pointA[1], pointB[1], evaluate=False)),
        latex(Rational(pointA[1] + pointB[1], 2)),
    )
    res += "\n"
    res += r"$y_{{{0}}}=\dfrac{{{1}}}{{2}}={2}$ ".format(
        nomMilieu[0],
        latex(Add(pointA[2], pointB[2], evaluate=False)),
        latex(Rational(pointA[2] + pointB[2], 2)),
    )
    res += r"\\" + "\n"
    res += r"$\mathbf{{{}\left({};{}\right)}}$".format(
        nomMilieu[0],
        latex(Rational(pointA[1] + pointB[1], 2)),
        latex(Rational(pointA[2] + pointB[2], 2)),
    )
    res += r"\\" + "\n"
    return res


def corrige_symetrique(pointA, pointB, nomSymetrique):
    res = (
        r"Puisque ${0}$ est le symétrique de ${1}$ par rapport à ${2}$,"
        r" le point ${2}$ est donc le milieu de {3} :\\"
    ).format(
        nomSymetrique[0],
        pointA[0],
        pointB[0],
        format_segment(pointA[0], nomSymetrique[0]),
    )
    res += "\n"
    res += (
        r"$x_{{{0}}}=\dfrac{{x_{{{1}}}+x_{{{2}}}}}{{2}}$ "
        r"et $y_{{{0}}}=\dfrac{{y_{{{1}}}+y_{{{2}}}}}{{2}}$\\"
    ).format(pointB[0], pointA[0], nomSymetrique[0])
    res += "\n"
    res += r"D'une part : ${}=\dfrac{{{}+x_{{{}}}}}{{2}}$ \\".format(
        latex(pointB[1]), latex(pointA[1]), nomSymetrique[0]
    )
    res += "\n"
    res += r"${}\times 2={}+x_{{{}}}$ \\".format(
        latex(pointB[1]), latex(pointA[1]), nomSymetrique[0]
    )
    res += "\n"
    res += r"${}={}+x_{{{}}}$ \\".format(
        latex(pointB[1] * 2), latex(pointA[1]), nomSymetrique[0]
    )
    res += "\n"
    res += r"$x_{{{}}}={}=\mathbf{{{}}}$ \\".format(
        nomSymetrique[0],
        latex(Add(pointB[1] * 2, -pointA[1], evaluate=False)),
        latex(pointB[1] * 2 - pointA[1]),
    )
    res += "\n"
    res += r"D'autre part : ${}=\dfrac{{{}+y_{{{}}}}}{{2}}$ \\".format(
        latex(pointB[2]), latex(pointA[2]), nomSymetrique[0]
    )
    res += "\n"
    res += r"${}\times 2={}+y_{{{}}}$ \\".format(
        latex(pointB[2]), latex(pointA[2]), nomSymetrique[0]
    )
    res += "\n"
    res += r"${}={}+y_{{{}}}$ \\".format(
        latex(pointB[2] * 2), latex(pointA[2]), nomSymetrique[0]
    )
    res += "\n"
    res += r"$y_{{{}}}={}=\mathbf{{{}}}$ \\".format(
        nomSymetrique[0],
        latex(Add(pointB[2] * 2, -pointA[2], evaluate=False)),
        latex(pointB[2] * 2 - pointA[2]),
    )
    res += "\n"
    res += r"$\mathbf{{{}\left({};{}\right)}}$".format(
        nomSymetrique[0],
        latex(pointB[1] * 2 - pointA[1]),
        latex(pointB[2] * 2 - pointA[2]),
    )
    return res


def corrige_nature_triangle(point_A, point_B, point_C):
    res = corrige_triangle_longueurs(point_A, point_B, point_C)
    lignes = []
    longueurs_cotes = longueurs([point_A, point_B, point_C])
    valeurs_longueurs = [l[1] for l in longueurs_cotes]
    if len(set(valeurs_longueurs)) == 1:
        lignes.append(
            "Je constate que ${}={}={}$.".format(
                longueurs_cotes[0][0], longueurs_cotes[1][0], longueurs_cotes[2][0]
            )
        )
        lignes.append(
            "Le triangle ${}{}{}$ est équilatéral.".format(
                point_A[0], point_B[0], point_C[0]
            )
        )
    elif len(set(valeurs_longueurs)) == 2:
        if longueurs_cotes[0][1] == longueurs_cotes[1][1]:
            longueurs_egales = longueurs_cotes[:2]
        elif longueurs_cotes[0][1] == longueurs_cotes[2][1]:
            longueurs_egales = [longueurs_cotes[0], longueurs_cotes[2]]
        else:
            longueurs_egales = longueurs_cotes[1:]
        sommet_principal = (
            set(longueurs_egales[0][0]).intersection(set(longueurs_egales[1][0])).pop()
        )
        lignes.append(
            "Le triangle ${0}{1}{2}$ est isocèle en {3}.".format(
                point_A[0], point_B[0], point_C[0], sommet_principal
            )
        )

    carres = list(map(lambda x: ["{" + x[0] + "}^2", x[1] ** 2], longueurs_cotes))
    carres.sort(key=lambda x: x[1])
    if carres[0][1] + carres[1][1] == carres[2][1]:
        lignes.append(
            r"D'une part : ${0}+{1}={{{2}}}^2+{{{3}}}^2={4}+{5}={6}$.\\".format(
                carres[0][0],
                carres[1][0],
                wrapp_1(sqrt(carres[0][1], evaluate=False)),
                wrapp_1(sqrt(carres[1][1], evaluate=False)),
                carres[0][1],
                carres[1][1],
                carres[0][1] + carres[1][1],
            )
        )
        lignes.append(
            r"D'autre part : ${0}={1}^2={2} $.\\".format(
                carres[2][0], wrapp_1(sqrt(carres[2][1])), carres[2][1]
            )
        )
        sommet_principal = (
            set(carres[0][0][1:-3]).intersection(set(carres[1][0][1:-3])).pop()
        )
        lignes.append(
            (
                r"Je constate que ${0}+{1}={2}$, d'après la "
                r"réciproque du théorème de Pythagore, le triangle ${3}{4}{5}$ "
                r"est rectangle en {6}."
            ).format(
                carres[0][0],
                carres[1][0],
                carres[2][0],
                point_A[0],
                point_B[0],
                point_C[0],
                sommet_principal,
            )
        )

    res += "\n".join(lignes)
    return res


def corrige_triangle_longueurs(pointA, pointB, pointC):
    return corrige_longueurs([pointA, pointB, pointC])


def corrige_longueurs(points):
    res = ""
    for i, point in enumerate(points[:-1]):
        pointA, pointB = points[i : i + 2]
        res += r"Calcul de la longeur ${}{}$ :\\".format(pointA[0], pointB[0])
        res += "\n"
        res += corrige_longueur(pointA, pointB) + "\n"
    if len(points) > 2:
        pointA, pointB = points[0], points[-1]
        res += r"Calcul de la longeur ${}{}$ :\\".format(pointA[0], pointB[0])
        res += "\n"
        res += corrige_longueur(pointA, pointB) + "\n"
    return res


def corrige_centre_cercle_circonscrit(centre, A, B, C):
	res=""
	for point in A, B, C:
		res += corrige_longueur(centre, point)
	return res


def exercice1_DSA():
    """
    Crée un exercice de type triangle rectangle de manière aléatoire.
    """
    triplet_lettres = sample(LETTRES, 3)
    couple_coord = sample([coord for coord in product(range(-5, 6), range(-5, 6))], 2)
    k = choice([-2, 2])
    couplePoint = [
        [lettre] + list(couple_coord[i])
        for i, lettre in enumerate(triplet_lettres[:-1])
    ]
    texte = exercice_triangle_rectangle(
        couplePoint[0], couplePoint[1], triplet_lettres[2], k
    )
    pointAux = rotation(couplePoint[0], pi / 2, couplePoint[1])
    pointC = [triplet_lettres[-1]] + [
        couplePoint[0][1] + k * (pointAux[1] - couplePoint[0][1]),
        couplePoint[0][2] + k * (pointAux[2] - couplePoint[0][2]),
    ]
    correction = corrige_triangle_longueurs(couplePoint[0], couplePoint[1], pointC)

    return texte + "\n" + correction


def questions_type1():
    res = []
    for k in range(2):
        triplet_lettres = sample(LETTRES, 3)
        triplet_coord = sample(
            [coord for coord in product(range(-10, 11), range(-10, 11))], 2
        ) + [()]
        triplet = [
            [lettre] + list(triplet_coord[i])
            for i, lettre in enumerate(triplet_lettres)
        ]
        res.append(triplet)
    return res


def question_type2():
    res = []
    for k in range(2):
        triplet_lettres = sample(LETTRES, 3)
        triplet_coord = sample(
            [
                coord
                for coord in product(
                    fractions_irreductibles(-5, 5), fractions_irreductibles(-5, 5)
                )
            ],
            2,
        ) + [()]
        triplet = [
            [lettre] + list(triplet_coord[i])
            for i, lettre in enumerate(triplet_lettres)
        ]
        res.append(triplet)
    return res


def questions_type3():
    res = []
    for k in range(2):
        couple_lettres = sample(LETTRES, 2)
        couple_coord = sample(
            [coord for coord in product(range(-10, 11), range(-10, 11))], 2
        )
        couple = [
            [lettre] + list(couple_coord[i]) for i, lettre in enumerate(couple_lettres)
        ]
        res.append(couple)
    return res


if __name__ == "__main__":
    conf = configuration_losange_1(["A", 5, 3], "B", "C", "D", [4, 6], 2)
    print(",".join("({1},{2})".format(*point) for point in conf))
    liste_points_2 = [["A", -7, -1], ["B", -3, -5], ["C", 5, 3]]
    print(corrige_longueurs(liste_points_2))
    print(corrige_triangle_longueurs(*liste_points_2))
    print(
        corrige_longueurs(liste_points_2) == corrige_triangle_longueurs(*liste_points_2)
    )
    print(corrige_nature_triangle(*liste_points_2))
    print(corrige_nature_triangle(["C", 0, -1], ["D", 1, 0], ["E", 0, 1]))
    print(corrige_nature_triangle(["G", -1, 0], ["H", 1, 0], ["L", 0, sqrt(3)]))
    print(corrige_symetrique(["M", 1, 3], ["C", 0.5, 2.5], "N"))
    print(corrige_symetrique(["M", 1.0, 3.0], ["C", 0.5, 2.5], "N"))
    # enonce = ur'Soient les points A, B, C et D de coordoonnées :''\n'
    # enonce += ','.join('{0}({1},{2})'.format(*point) for point in conf)
    # print(enonce)
    # print(configuration_carre(['A',0,0],['B',5,0],'C','D'))
    # print(corrige_longueurs([['A',2,0],['B',3,1],['C',0,2]]))
    # print(configuration_carre(['A',1,2],['B',4,4],'C','D'))
    # print(configuration_carre(['A',1,2],['B',4,4],'C','D',False))
    # l = configuration_losange_1(['A',1,2],'B','C','D',(4,6),3)
    # print(l)
    # print(corrige_longueurs(l))
    # S = ['S',Rational(4,3),Rational(-1,3)]
    # M = ['M',1,2]
    # E = ['E',Rational(1,2),0]
    # print(corrige_coordonnees_milieu(S, E, 'K'))
    # K = milieu(S, E, 'K')
    # print(corrige_symetrique(M, K, 'R'))

    # T = ['T',0,Rational(2,3)]
    # U = ['U',1,-Rational(2,3)]
    # R = ['R',Rational(1,3),1]
    # print(corrige_coordonnees_milieu(T,R, 'P'))
    # P = milieu(T,R, 'P')
    # print(corrige_symetrique(U, P, 'R'))
    # A=['A', 3, 3]
    # B=['B', -1, 1]
    # C=['C', 1 , -3]
    # D=['D', 5 , -1]
    # print("")
    # print(corrige_coordonnees_milieu(A,C,'L'))
    # print(corrige_coordonnees_milieu(B,D,'K'))
    # print(corrige_triangle_longueurs(A,B,C))
    # print("")
    # print(exercice_parallelogramme2(A,B,C,'D'))
    # print(exercice1_DSA())
    # A=['A',-1,4]
    # B=['B',3,2]
    # C=['C',-1,-1]
    # print(corrige_triangle_longueurs(A,B,C))

    # T=['T',0,Rational(2,3)]
    # U=['U',1,-Rational(2,3)]
    # R=['R',Rational(1,3),1]
    # exercice2 = exercice_parallelogramme(T,U,R,'N','P')
    # print(exercice2['enonce'])
    # for q in exercice2['questions'] :
    # print(q)
    # M=['M',-1,Rational(1,3)]
    # E=['E',0,-Rational(2,3)]
    # R=['R',Rational(2,3),1]
    # print(exercice_parallelogramme(M,E,R,'S','L'))
    # print(exercice_triangle_rectangle_isocele(M,E,'T'))
    # print(corrige_triangle_longueurs(M,E,R))
    # A=['A',2,3]
    # B=['B',13,1]
    # C=['C',5,7]
    # J=['J',0,1]
    # D=['D',4,-1]
    # print(latex(longueur(A,B)))
    # print(corrige_triangle_longueurs(A,B,C))
    # print('ABC')
    # print(corrige_longueur(A,B))
    # print(corrige_longueur(A,C))
    # print(corrige_longueur(B,C))
    # print('JAD')
    # print(corrige_longueur(J,A))
    # print(corrige_longueur(J,D))
    # print(corrige_longueur(A,D))
    # print("ex 25 :")
    # T=['T',-2.2,1.2]
    # A=['A',-1.2,3.6]
    # C=['C',6,0.6]
    # print(corrige_longueur(T,A))
    # print(corrige_longueur(T,C))
    # print(corrige_longueur(A,C))
    ##print(corrige_longueur(['A',1,5],['B',2,-3]))
    ##print(corrige_symetrique(['A',2,4],['B',5,-2],["A'"]))
    # devoir={'num':1,'classe':'2nde B','date' : '11 septembre 2017'}
    # devoir['eleves']=GT2['eleves']
    # revisions=GT2['eleves']
    # for el in revisions :
    # el['questions milieu'] = questions_type1() + question_type2()
    # el['questions symetrique'] = questions_type1() + question_type2()
    # el['questions longueur'] = questions_type3()
    # liste_questions =[]
    # el['exercices']=\
    # [{'enonce': question_coordonnees_milieu(*q),
    #'correction':corrige_coordonnees_milieu(*q)} \
    # for q in el['questions milieu']]\
    # +[{'enonce': question_symetrique(*q),
    #'correction':corrige_symetrique(*q)} \
    # for q in el['questions symetrique']]\
    # +[{'enonce': question_longueur(*q),
    #'correction':corrige_longueur(*q)} \
    # for q in el['questions longueur']]
    # print(devoir['eleves'][0]['questions milieu'])
    ##compileDocument('testDM2GT1',generer_dm_1_2GT(revisions))
    # compileDocument('testDM2B1',generer_dm(devoir))
    # compileDocument('testDM2Bcorr',generer_correction_dm(devoir))
    # fractions_irreductibles(-5,5)
    # print(fractions_irreductibles(-5,5))

    # print(question_coordonnees_milieu(['A',1,2],['B',-2,3],'M'))
    # print(corrige_coordonnees_milieu(['A',1,2],['B',-2,3],'M'))

    # print(question_symetrique(['A',1,2],['B',-2,3],'F'))
    # for k in range(5) :
    # triplet_lettres = sample(LETTRES,3)
    # triplet_coord = sample([coord for coord in product(range(-10,11),range(-10,11))],2)+[()]
    # triplet=[[lettre] + list(triplet_coord[i]) for i, lettre in enumerate(triplet_lettres)]
    # print(question_coordonnees_milieu(*triplet))
    # for k in range(5) :
    # triplet_lettres = sample(LETTRES,3)
    # triplet_coord = sample([coord for coord in product(range(-10,11),range(-10,11))],2)+[()]
    # triplet=[[lettre] + list(triplet_coord[i]) for i, lettre in enumerate(triplet_lettres)]
    # print(question_symetrique(*triplet))
    # for p in range(5) :
    # triplet_lettres = sample(LETTRES,3)
    # triplet_coord = sample([coord for coord in product(fractions_irreductibles(-5,5),fractions_irreductibles(-5,5))],2)+[()]
    # triplet=[[lettre] + list(triplet_coord[i]) for i, lettre in enumerate(triplet_lettres)]
    # print(question_symetrique(*triplet))
    # for p in range(5) :
    # couple_lettres = sample(LETTRES,2)
    # couple_coord = sample([coord for coord in product(fractions_irreductibles(-5,5),fractions_irreductibles(-5,5))],2)
    # couple=[[lettre] + list(couple_coord[i]) for i, lettre in enumerate(couple_lettres)]
    # print(question_longueur(*couple))
