import os
import os.path
import sys

from sympy import Rational, symbols

dossier_actuel = os.path.realpath(os.curdir)

while os.path.basename(dossier_actuel) != "fundamath":
    dossier_actuel = os.path.dirname(dossier_actuel)
DOSSIER_FUNDAMATH = dossier_actuel

sys.path.append(DOSSIER_FUNDAMATH)

from equation_droites import points_extremes, equation_reduite  # isort:skip
from derivation import equation_tangente  # isort:skip
from sympy_to_latex import latex  # isort:skip

x = symbols("x")
f = x ** 2


def place_point_tikz(point, position="below left", label="x", precision=4):
    return r"""
    \draw ({1},{2}) node {{$\times$}};
    \draw ({1},{2}) node[{3}] {{${0}$}};
    \draw [dashed] ({1},{2}) -- ({1},0);
    \draw ({1}, 0) node[below] {{${4}$}};
    """.format(
        point[0],
        round(point[1], precision),
        round(point[2], precision),
        position,
        label,
    )


def secante_courbe_tikz(x0, h, fct, xmin=-3, xmax=3, ymin=-1, ymax=9, precision=4):
    point_A = ["A", x0, fct.subs(x, x0)]
    point_B = ["B", x0 + h, fct.subs(x, x0 + h)]
    res = place_point_tikz(point_A, precision=precision)
    res += place_point_tikz(point_B, label="x+h", precision=precision)
    ext_1, ext_2 = points_extremes(
        ["D", equation_reduite(point_A, point_B)],
        xmin=xmin,
        xmax=xmax,
        ymin=ymin,
        ymax=ymax,
    )
    res += r"""
    \draw ({0},{1}) -- ({2},{3});
    """.format(
        *map(lambda l: round(l, precision), [ext_1[1], ext_1[2], ext_2[1], ext_2[2]])
    )
    res += r"""
    \draw[color=blue, -{{Latex[length=3mm,width=2mm]}}] ({0},{1}) -- ({2},{1});
    \draw (,{1}) node {\textcolor{blue}{$1$}}
    \draw[color=green, -{{Latex[length=3mm,width=2mm]}}] ({2},{1}) -- ({2},{3});
    """.format(
        *map(
            lambda t: round(t, precision),
            [point_A[1], point_A[2], point_B[1], point_B[2]],
        )
    )
    return res


def trace_tangente_tikz(fct, x0, xmin=-3, xmax=3, ymin=-1, ymax=9, precision=4):
    D = ["T", equation_tangente(fct, x0)]
    ext_1, ext_2 = points_extremes(D, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
    res = ""
    res += r""" 
    \draw[very thick, color = red] ({0},{1}) -- ({2},{3}); 
     """.format(
        *map(lambda t: round(t, precision), [ext_1[1], ext_1[2], ext_2[1], ext_2[2]])
    )
    res += place_point_tikz(["", x0, fct.subs(x, x0)], label=str(x0))
    res += r"""
    \draw[very thick, color=blue, -{{Latex[length=3mm,width=2mm]}}] ({0},{1}) -- ({2},{1});
    \draw[color=blue] ({4} , {1} ) node[below] {{$\textcolor{{blue}}{{1}}$}};
    \draw[very thick, color=green, -{{Latex[length=3mm,width=2mm]}}] ({2},{1}) -- ({2},{3});
    \draw[color=blue] ({2} , {5} ) node[right] {{$\textcolor{{green}}{{{6}}}$}};
    """.format(
        *(
            map(
                lambda t: round(t, precision),
                [
                    x0,
                    fct.subs(x, x0),
                    x0 + 1,
                    fct.subs(x, x0) + (fct.diff(x)).subs(x, x0),
                    x0 + 0.5,
                    fct.subs(x, x0) + 0.5 * (fct.diff(x)).subs(x, x0),
                ],
            )
            + [latex((fct.diff(x)).subs(x, x0))]
        )
    )
    return res


def ligne_derivee(f, liste_x):
    return " & ".join(
        [
            r"\onslide<{}->{{${}$}}".format(k + 2, latex(f.diff().subs(x, val)))
            for k, val in enumerate(liste_x)
        ]
    )


def init_tableau(f, liste_x):
    return r"\begin{{tabular}}{{|*{{{}}}{{c|}}}}".format(len(liste_x) + 1)


def tableau_val_derivee(f, liste_x):
    lignes = [""]
    lignes.append(" & ".join(map(lambda t: "${}$".format(latex(t)), liste_x)))
    res = "\n".join(lignes)
    return res


# f = 1 / x

# # for nb in range(4):
# #     print trace_tangente_tikz(
# #         f, Rational(1, 2) + Rational(1, 2) * nb, xmin=-5, xmax=5, ymin=-5, ymax=5
# #     )

# val_x = [
#     -4,
#     -2,
#     -1,
#     -Rational(1, 2),
#     -Rational(1, 3),
#     4,
#     2,
#     1,
#     Rational(1, 2),
#     Rational(1, 3),
# ]
# val_x.sort()
# for valeur in val_x:
#     print(
#         r"\onslide<>{"
#         "\n" + trace_tangente_tikz(f, valeur, xmin=-4, xmax=4, ymin=-4, ymax=4) + r"}"
#         "\n"
#     )

f2 = x ** 0.5
valeurs_x = [5, 4, 3, 2, 1, Rational(1, 9), Rational(1, 4), Rational(1, 100)]

valeurs_x.sort()
valeurs_x.reverse()
print init_tableau(f2, valeurs_x)
# for compteur, valeur in enumerate(valeurs_x):
#     print(
#         r"\onslide<{}>".format(compteur + 2)
#         + "{\n"
#         + trace_tangente_tikz(f2, valeur, xmin=-0.5, xmax=8, ymin=-0.5, ymax=8)
#         + "}\n"
#     )
