def charnieress_reelles(relation, arrondi=3): 
    valeurs_exactes = solve(Eq(*relation.args))
    valeurs_arrondies = [round(v.evalf(arrondi+2),arrondi) for v in valeurs_exactes if v.is_real]
    valeurs_arrondies.sort()
    return valeurs_arrondies
