#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division
from fractions import Fraction
from subprocess import call
from os import remove, listdir

entete = r"""
\documentclass[11pt]{beamer}
\usetheme{Warsaw}
\usepackage[utf8]{inputenc}
%\usepackage[french]{babel} TROP DE SOUCIS
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{tikz,tkz-tab,tkz-fct}

\setbeamertemplate{enumerate subitem}{(\alph{enumii})}

\author{N.Talabardon}
\title{correction du DS n 7}
%\setbeamercovered{transparent}
%\setbeamertemplate{navigation symbols}{}
%\logo{}
\institute{Lycée C.Lehec}
\date{Jeudi 5 mars 2015}
%\subject{}
\begin{document}
\begin{frame}
\titlepage
\end{frame}
"""
pied = r"""

\end{document}
"""


def fractionTex(frac, inMath=False):
    if frac.denominator == 1:
        string = str(frac.numerator)
    else:
        string = r"\dfrac{0}{1}".format(
            "{" + str(frac.numerator) + "}", "{" + str(frac.denominator) + "}"
        )
    if not inMath:
        string = "$" + string + "$"
    return string


def expressionAffine(a, b, var="x"):
    string = ""
    if a != 0:
        if a == -1:
            string += "-" + var  # var est un caractère
        elif a == 1:
            string += var  # var est un caractère
        else:
            string += str(a) + var
    if b > 0:
        string += "+" + str(b)  # b est un entier
    elif b < 0:
        string += str(b)  # b est un entier
    return string


def Monome(a, var, n):
    string = ""
    if n == 0:
        string += str(a)
    else:
        if (a != 1) and (a != -1):
            string += str(a)
        string += var
        if n > 1:
            string += "^"
            string += str(n)
    return string


def polynomeSnD(a, b, c, var):
    string = "$"
    if a != 0:
        string += Monome(a, var, 2)
    if b < 0:
        string += Monome(b, var, 1)
    if b > 0:
        string += "+"
        string += Monome(b, var, 1)
    if c < 0:
        string += Monome(c, var, 0)
    if c > 0:
        string += "+"
        string += Monome(c, var, 0)
    string += "$"
    return string


def fausseFactorisationParIR(a, b, IR=2):
    pass


def zeroAffine(a, b, var="x"):
    string = r"Recherche de la valeur charnière:" + "\n"
    string += r"\pause" + "\n"
    string += r"\\" + "\n"
    string += "$" + expressionAffine(a, b, var="x") + " = 0$" + "\n"
    string += r"\pause" + "\n"
    string += r"\\" + "\n"
    string += "$" + expressionAffine(a, 0, var="x") + " = " + str(-b) + "$" + "\n"
    string += r"\pause" + "\n"
    string += r"\\" + "\n"
    if a != 1:
        string += (
            "$"
            + expressionAffine(1, 0, var="x")
            + " = \dfrac{"
            + str(-b)
            + "}{"
            + str(a)
            + "}$"
            + "\n"
        )
        string += r"\\" + "\n"
        string += r"\pause" + "\n"
    if -b != Fraction(-b, a).numerator:
        string += (
            "$"
            + expressionAffine(1, 0, var="x")
            + " = "
            + fractionTex(Fraction(-b, a), inMath=True)
            + "$"
            + "\n"
        )
        string += r"\\" + "\n"
        string += r"\pause" + "\n"
    return string


def tableauSigneFonctionAffine(a, b, var="x"):
    string = ""
    string += r"\begin{tikzpicture}" + "\n"
    string += r"\tkzTabInit[lgt=4,espcl=1.5]" + "\n"
    string += "{$%s$" % var + "\n"  # var est un caractère
    string += r"/1," + "\n"
    string += "Signe de $" + expressionAffine(a, b, var) + "$\n"
    string += "/1}\n"
    if a != 0:
        valeurCharniere = Fraction(-b, a)
        string += r"{$-\infty$," + fractionTex(valeurCharniere) + ",$+\infty$}" + "\n"
        string += r"\pause" + "\n"
        if a > 0:
            string += r"\tkzTabLine { ,-,z,+, }" + "\n"
        else:
            string += r"\tkzTabLine { ,+,z,-, }" + "\n"
    else:
        if b > 0:
            pass
        elif b < 0:
            pass
        else:
            pass
    string += r"\end{tikzpicture}" + "\n"
    string += r"\\" + "\n"
    return string


def traceFonctionAffineTikz(a, b):
    xmax = max(1, 1 - 2 * b / a, 1 + 2 * b / a)
    xmin = min(-1, 1 - 2 * b / a, 1 + 2 * b / a)

    ymax = max(1, a * xmin + b, a * xmax + b)
    ymin = min(-1, a * xmin + b, a * xmax + b)

    # échelle choisie pour se rapprocher du format d'un écran de calculatrice :
    xscale = 6 / (xmax - xmin)
    yscale = 3.5 / (ymax - ymin)
    string = (
        "xmin="
        + str(round(xmin, 3))
        + ",xmax="
        + str(round(xmax, 3))
        + ",ymin="
        + str(round(ymin, 3))
        + ",ymax="
        + str(round(ymax, 3))
        + "\n"
    )
    string += (
        r"\begin{tikzpicture}[xscale=%s,yscale=%s]" % (str(xscale), str(yscale)) + "\n"
    )  # PB d'échelle pour les "tirets"
    string += (
        r"\tkzInit[xmin={0},xmax={1},ymin={2},ymax={3}]".format(xmin, xmax, ymin, ymax)
        + "\n"
    )
    # string+=r"\tkzGrid[sub]"
    string += (
        r"\tkzDrawX[label={},tickwd=%s,tickup=%s,tickdn=%s]"
        % (str(0.8), str(0.03 / (yscale)), str(0.03 / (yscale)))
        + "\n"
    )
    string += (
        r"\tkzDrawY[label={},tickwd=%s,ticklt=%s,tickrt=%s]"
        % (str(0.8), str(0.03 / (xscale)), str(0.03 / (xscale)))
        + "\n"
    )
    string += r"\tkzFct[samples=400]{\x,%s*\x+%s}" % (str(a), str(b)) + "\n"
    string += r"\end{tikzpicture}" + "\n"
    return string


def traceProduitDeuxAffines(a, b, c, d):
    def fonct(v):
        return (a * v + b) * (c * v + d)

    e1 = min(-b / a, -d / c)
    e2 = max(-b / a, -d / c)
    if e1 != e2:
        xmin = 2 * e1 - e2
        xmax = 2 * e2 - e1
        centre = (e1 + e2) / 2
        ymin = min(-1, fonct(centre) - 1, xmin)
        ymax = max(1, fonct(centre) + 1, xmax)
    else:
        if a * c > 0:
            xmax = max(1, 1 - 2 * b / a, 1 + 2 * b / a)
            xmin = min(-1, 1 - 2 * b / a, 1 + 2 * b / a)
            ymax = max(1, a * xmin + b, a * xmax + b)
            ymin = -1
        elif a * c < 0:
            xmax = max(1, 1 - 2 * b / a, 1 + 2 * b / a)
            xmin = min(-1, 1 - 2 * b / a, 1 + 2 * b / a)
            ymax = 1
            ymin = min(-1, a * xmin + b, a * xmax + b)
        else:
            pass
    # échelle choisie pour se rapprocher du format d'un écran de calculatrice :
    xscale = 6 / (xmax - xmin)
    yscale = 3.5 / (ymax - ymin)
    string = (
        "xmin="
        + str(round(xmin, 3))
        + ",xmax="
        + str(round(xmax, 3))
        + ",ymin="
        + str(round(ymin, 3))
        + ",ymax="
        + str(round(ymax, 3))
        + "\n"
    )
    string += (
        r"\begin{tikzpicture}[xscale=%s,yscale=%s]" % (str(xscale), str(yscale)) + "\n"
    )
    string += (
        r"\tkzInit[xmin={0},xmax={1},ymin={2},ymax={3}]".format(xmin, xmax, ymin, ymax)
        + "\n"
    )
    # string+=r"\tkzGrid[sub]"
    string += (
        r"\tkzDrawX[label={},tickwd=%s,tickup=%s,tickdn=%s]"
        % (str(0.8), str(0.03 / (yscale)), str(0.03 / (yscale)))
        + "\n"
    )
    string += (
        r"\tkzDrawY[label={},tickwd=%s,ticklt=%s,tickrt=%s]"
        % (str(0.8), str(0.03 / (xscale)), str(0.03 / (xscale)))
        + "\n"
    )
    string += (
        r"\tkzFct[samples=400,domain=%s:%s]{\x,(%s*\x+%s)*(%s*\x+%s)}"
        % (str(e1 - 5), str(e2 + 5), str(a), str(b), str(c), str(d))
        + "\n"
    )
    string += r"\end{tikzpicture}" + "\n"
    return string


def traceQuotientDeuxAffines(a, b, c, d):
    def fonct(v):
        return (a * v + b) / (c * v + d)

    e1 = min(-b / a, -d / c)
    e2 = max(-b / a, -d / c)
    f1 = min(0, b / d)
    f2 = max(0, b / d)
    if e1 != e2:
        xmin = 3 * e1 - 2 * e2
        xmax = 3 * e2 - 2 * e1
        centre = (e1 + e2) / 2
        ymin = min(-1, 4 * f1 - 3 * f2)
        ymax = max(1, 4 * f2 - 3 * f1)
    else:
        xmax = max(1, 1 - 2 * b / a, 1 + 2 * b / a)
        xmin = min(-1, 1 - 2 * b / a, 1 + 2 * b / a)
        ymax = max(1, a * xmin + b, a * xmax + b)
        ymin = min(-1, a * xmin + b, a * xmax + b)
    # échelle choisie pour se rapprocher du format d'un écran de calculatrice :
    xscale = 6 / (xmax - xmin)
    yscale = 3.5 / (ymax - ymin)
    string = (
        "xmin="
        + str(round(xmin, 3))
        + ",xmax="
        + str(round(xmax, 3))
        + ",ymin="
        + str(round(ymin, 3))
        + ",ymax="
        + str(round(ymax, 3))
        + "\n"
    )
    string += (
        r"\begin{tikzpicture}[xscale=%s,yscale=%s]" % (str(xscale), str(yscale)) + "\n"
    )
    string += (
        r"\tkzInit[xmin={0},xmax={1},ymin={2},ymax={3}]".format(xmin, xmax, ymin, ymax)
        + "\n"
    )
    # string+=r"\tkzGrid[sub]"
    string += (
        r"\tkzDrawX[label={},tickwd=%s,tickup=%s,tickdn=%s]"
        % (str(0.8), str(0.03 / (yscale)), str(0.03 / (yscale)))
        + "\n"
    )
    string += (
        r"\tkzDrawY[label={},tickwd=%s,ticklt=%s,tickrt=%s]"
        % (str(0.8), str(0.03 / (xscale)), str(0.03 / (xscale)))
        + "\n"
    )
    string += (
        r"\tkzFct[samples=400]{\x,(%s*\x+%s)/(%s*\x+%s)}"
        % (str(a), str(b), str(c), str(d))
        + "\n"
    )
    string += r"\end{tikzpicture}" + "\n"
    return string


def tableauSigneProduitDeuxAffines(
    a, b, c, d, var="x"
):  # Refactorisation nécessaire pour factoriser le code/raisonnement commun avec tableauSigneFonctionAffine
    string = ""
    string += r"\begin{tikzpicture}" + "\n"
    string += r"\tkzTabInit[lgt=4,espcl=1.5]" + "\n"
    string += "{$%s$" % var + "\n"  # var est un caractère
    string += r"/1," + "\n"
    string += "Signe de $" + expressionAffine(a, b, var) + "$\n"
    string += "/1,\n"
    string += "Signe de $" + expressionAffine(c, d, var) + "$\n"
    string += "/1,\n"
    string += (
        r"Signe de $\left("
        + expressionAffine(a, b, var)
        + r"\right) \left("
        + expressionAffine(c, d, var)
        + r"\right)$"
        + "\n"
    )
    string += "/1}\n"
    if a != 0 and c != 0:
        valCharniere1 = Fraction(-b, a)
        valCharniere2 = Fraction(-d, c)
        if valCharniere1 < valCharniere2:
            string += (
                r"{$-\infty$, "
                + fractionTex(valCharniere1)
                + ", "
                + fractionTex(valCharniere2)
                + ",$+\infty$}"
                + "\n"
            )
            string += r"\pause" + "\n"
            if a > 0:
                string += r"\tkzTabLine { ,-,z,+,t,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,+,z,-,t,-, }" + "\n"
            string += r"\pause" + "\n"
            if c > 0:
                string += r"\tkzTabLine { ,-,t,-,z,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,+,t,+,z,-, }" + "\n"
            string += r"\pause" + "\n"
            if a * c > 0:
                string += r"\tkzTabLine { ,+,z,-,z,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,-,z,+,z,-, }" + "\n"
            string += r"\pause" + "\n"
        elif valCharniere1 > valCharniere2:
            string += (
                r"{$-\infty$,"
                + fractionTex(valCharniere2)
                + ","
                + fractionTex(valCharniere1)
                + ",$+\infty$}"
                + "\n"
            )
            string += r"\pause" + "\n"
            if a > 0:
                string += r"\tkzTabLine { ,-,t,-,z,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,+,t,+,z,-, }" + "\n"
            string += r"\pause" + "\n"
            if c > 0:
                string += r"\tkzTabLine { ,-,z,+,t,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,+,z,-,t,-, }" + "\n"
            string += r"\pause" + "\n"
            if a * c > 0:
                string += r"\tkzTabLine { ,+,z,-,z,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,-,z,+,z,-, }" + "\n"
            string += r"\pause" + "\n"
        else:
            string += r"{$-\infty$," + fractionTex(valCharniere1) + ",$+\infty$}" + "\n"
            string += r"\pause" + "\n"
            if a > 0:
                string += r"\tkzTabLine { ,-,z,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,+,z,-, }" + "\n"
            string += r"\pause" + "\n"
            if c > 0:
                string += r"\tkzTabLine { ,-,z,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,+,z,-, }" + "\n"
            string += r"\pause" + "\n"
            if a * c > 0:
                string += r"\tkzTabLine { ,+,z,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,-,z,-, }" + "\n"
            string += r"\pause" + "\n"
    else:
        pass
    string += r"\end{tikzpicture}" + "\n"
    return string


def tableauSigneQuotientDeuxAffines(a, b, c, d, var="x"):  # Refactorisation nécessaire
    string = ""
    string += r"\begin{tikzpicture}" + "\n"
    string += r"\tkzTabInit[lgt=4,espcl=1.5]" + "\n"
    string += "{$%s$" % var + "\n"  # var est un caractère
    string += r"/1," + "\n"
    string += "Signe de $" + expressionAffine(a, b, var) + "$\n"
    string += "/1,\n"
    string += "Signe de $" + expressionAffine(c, d, var) + "$\n"
    string += "/1,\n"
    string += (
        r"Signe de $\dfrac{"
        + expressionAffine(a, b, var)
        + r"}{"
        + expressionAffine(c, d, var)
        + r"}$"
        + "\n"
    )
    string += "/1}\n"
    if a != 0 and c != 0:
        valCharniere1 = Fraction(-b, a)
        valCharniere2 = Fraction(-d, c)
        if valCharniere1 < valCharniere2:
            string += (
                r"{$-\infty$,"
                + fractionTex(valCharniere1)
                + ","
                + fractionTex(valCharniere2)
                + ",$+\infty$}"
                + "\n"
            )
            string += r"\pause" + "\n"
            if a > 0:
                string += r"\tkzTabLine { ,-,z,+,t,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,+,z,-,t,-, }" + "\n"
            string += r"\pause" + "\n"
            if c > 0:
                string += r"\tkzTabLine { ,-,t,-,z,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,+,t,+,z,-, }" + "\n"
            string += r"\pause" + "\n"
            if a * c > 0:
                string += r"\tkzTabLine { ,+,z,-,d,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,-,z,+,d,-, }" + "\n"
            string += r"\pause" + "\n"
        elif valCharniere1 > valCharniere2:
            string += (
                r"{$-\infty$,"
                + fractionTex(valCharniere2)
                + ","
                + fractionTex(valCharniere1)
                + ",$+\infty$}"
                + "\n"
            )
            string += r"\pause" + "\n"
            if a > 0:
                string += r"\tkzTabLine { ,-,t,-,z,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,+,t,+,z,-, }" + "\n"
            string += r"\pause" + "\n"
            if c > 0:
                string += r"\tkzTabLine { ,-,z,+,t,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,+,z,-,t,-, }" + "\n"
            string += r"\pause" + "\n"
            if a * c > 0:
                string += r"\tkzTabLine { ,+,d,-,z,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,-,d,+,z,-, }" + "\n"
            string += r"\pause" + "\n"
        else:
            string += r"{$-\infty$," + fractionTex(valCharniere1) + ",$+\infty$}" + "\n"
            string += r"\pause" + "\n"
            if a > 0:
                string += r"\tkzTabLine { ,-,z,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,+,z,-, }" + "\n"
            string += r"\pause" + "\n"
            if c > 0:
                string += r"\tkzTabLine { ,-,z,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,+,z,-, }" + "\n"
            string += r"\pause" + "\n"
            if a * c > 0:
                string += r"\tkzTabLine { ,+,d,+, }" + "\n"
            else:
                string += r"\tkzTabLine { ,-,d,-, }" + "\n"
            string += r"\pause" + "\n"
    else:
        pass
    string += r"\end{tikzpicture}" + "\n"
    return string


def correction_DS():
    fichier = open("correction_DS_30_03.tex", "w")
    fichier.write(entete)
    ##### Exercice 1 #####
    fichier.write(r"\begin{frame}" + "\n")
    fichier.write(r"\underline{Exercice 1}" + "\n" + r"\\" + "\n")
    fichier.write(r"\begin{enumerate}" + "\n")
    fichier.write(r"\item{Lire la ou les valeur(s) pour la(les)quelle(s) :}" + "\n")
    fichier.write(r"\begin{enumerate}" + "\n")
    # fichier.write(r"\renewcommand{\labelenumii}{\alph{enumii}}"+'\n')
    fichier.write(r"\item{La fonction n'est pas définie.}" + "\n")
    fichier.write(r"\item{La fonction est éventuellement nulle.}" + "\n")
    fichier.write(r"\end{enumerate}" + "\n")
    fichier.write(r"\item{Établir le tableau de signe de cette fonction.}" + "\n")
    fichier.write(r"\\" + "\n")
    fichier.write(r"\href{ex 1.ggb}{figure}" + "\n")  ####nécessite\usepackage{hyperref}
    fichier.write(r"\pause" + "\n")
    fichier.write(tableauSigneQuotientDeuxAffines(2, -6, 1, 1))
    fichier.write(r"\end{enumerate}" + "\n")
    fichier.write(r"\end{frame}" + "\n")
    ##### Exercice 2 #####
    ## a) ##
    fichier.write(r"\begin{frame}" + "\n")
    fichier.write(r"\underline{Exercice 2}" + "\n" + r"\\" + "\n")
    fichier.write(r"\begin{enumerate}" + "\n")
    fichier.write(r"\setcounter{enumi}{0}" + "\n")
    fichier.write(
        r"\item{Signe de $\left(2 x - 5 \right)\left(6 - 3 x \right)$}" + "\n"
    )
    fichier.write(tableauSigneProduitDeuxAffines(2, -5, -3, 6))
    fichier.write(r"\end{enumerate}" + "\n")
    fichier.write(r"\end{frame}" + "\n")
    ## b) ##
    fichier.write(r"\begin{frame}" + "\n")
    fichier.write(r"\underline{Exercice 2}" + "\n" + r"\\" + "\n")
    fichier.write(r"\begin{enumerate}" + "\n")
    fichier.write(r"\setcounter{enumi}{1}" + "\n")
    fichier.write(r"\item{Signe de $\dfrac{3 x-1}{2 x+7 }$}" + "\n")
    fichier.write(tableauSigneQuotientDeuxAffines(3, -1, 2, 7))
    fichier.write(r"\end{enumerate}" + "\n")
    fichier.write(r"\end{frame}" + "\n")
    ## c) ##
    fichier.write(r"\begin{frame}" + "\n")
    fichier.write(r"\underline{Exercice 2}" + "\n" + r"\\" + "\n")
    fichier.write(r"\begin{enumerate}" + "\n")
    fichier.write(r"\setcounter{enumi}{2}" + "\n")
    fichier.write(r"\item{Signe de $\dfrac{2+x}{\left(x-5 \right)^2}$}" + "\n")
    fichier.write(tableauSigneQuotientDeuxAffines(1, 2, 1, -5))
    fichier.write(r"\end{enumerate}" + "\n")
    fichier.write(r"\end{frame}" + "\n")
    ##### Exercice 3 #####
    ## a) ##
    fichier.write(r"\begin{frame}" + "\n")
    fichier.write(r"\underline{Exercice 3}" + "\n" + r"\\" + "\n")
    fichier.write(r"\begin{enumerate}" + "\n")
    fichier.write(r"\item{$ (3-x)(-x-1) \leqslant 0$}" + r"\\" + "\n")
    fichier.write(r"\pause" + "\n")
    fichier.write(tableauSigneProduitDeuxAffines(-1, 3, -1, -1))
    fichier.write(r"\end{enumerate}" + "\n")
    fichier.write(r"\end{frame}" + "\n")
    ## b) ##
    fichier.write(r"\begin{frame}" + "\n")
    fichier.write(r"\underline{Exercice 3}" + "\n" + r"\\" + "\n")
    fichier.write(r"\begin{enumerate}" + "\n")
    fichier.write(r"\setcounter{enumi}{1}" + "\n")
    fichier.write(r"\item{$\dfrac{7x+5}{11-2x} \leqslant 0$}" + r"\\" + "\n")
    fichier.write(r"\pause" + "\n")
    fichier.write(tableauSigneQuotientDeuxAffines(7, 5, -2, 11))
    fichier.write(r"\end{enumerate}" + "\n")
    fichier.write(r"\end{frame}" + "\n")
    ## c) ##
    fichier.write(r"\begin{frame}" + "\n")
    fichier.write(r"\underline{Exercice 3}" + "\n" + r"\\" + "\n")
    fichier.write(r"\begin{enumerate}" + "\n")
    fichier.write(r"\setcounter{enumi}{2}" + "\n")
    fichier.write(r"\item{$\left( 2 x + 7\right)^2 \leqslant 0$}" + r"\\" + "\n")
    fichier.write(r"\pause" + "\n")
    fichier.write(tableauSigneProduitDeuxAffines(2, 7, 2, 7))
    fichier.write(r"\end{enumerate}" + "\n")
    fichier.write(r"\end{frame}" + "\n")

    ## d) ##
    fichier.write(r"\begin{frame}" + "\n")
    fichier.write(r"\underline{Exercice 3}" + "\n" + r"\\" + "\n")
    fichier.write(r"\begin{enumerate}" + "\n")
    fichier.write(r"\setcounter{enumi}{3}" + "\n")
    fichier.write(
        r"\item{$  x\left( x - 5\right)-\left( x - 5\right)^2 > 0$}" + r"\\" + "\n"
    )

    fichier.write(r"\pause" + "\n")
    fichier.write(r"Factorisation" + r"\\" + "\n")
    fichier.write(
        r"$x \textcolor<3>{red}{\left( x - 5\right)}-\textcolor<3>{red}{\left( x - 5\right)}^2 > 0$"
        + "\n"
    )
    fichier.write(r"\\" + "\n")
    fichier.write(r"\pause" + "\n")
    fichier.write(
        r"$x \textcolor{red}{\left( x - 5\right)}-\left( x - 5\right)\times\textcolor{red}{\left( x - 5\right)} > 0$"
        + "\n"
    )
    fichier.write(r"\\" + "\n")
    fichier.write(r"\pause" + "\n")
    fichier.write(
        r"$\left[ x-\left(x - 5\right)\right]\times   \textcolor{red}{\left( x - 5\right)}> 0$"
        + "\n"
    )
    fichier.write(r"\\" + "\n")
    fichier.write(r"\pause" + "\n")
    fichier.write(
        r"$\left( x-x + 5 \right)\times   \textcolor{red}{\left( x - 5\right)}> 0$"
        + "\n"
    )
    fichier.write(r"\\" + "\n")
    fichier.write(r"\pause" + "\n")
    fichier.write(r"$ 5 \times   \textcolor{red}{\left( x - 5\right)}> 0$" + "\n")
    fichier.write(r"\\" + "\n")
    fichier.write(r"\pause" + "\n")
    fichier.write(tableauSigneProduitDeuxAffines(1, -5, 1, -5))
    fichier.write(r"\end{enumerate}" + "\n")
    fichier.write(r"\end{frame}" + "\n")
    ##### Exercice 4 #####
    fichier.write(r"\begin{frame}" + "\n")
    fichier.write(r"\underline{Exercice 4}" + "\n" + r"\\" + "\n")
    fichier.write(r"Soit $f$ la fonction définie par $f(x)=\dfrac{x- 2}{4-x}$." + "\n")
    fichier.write(r"\\" + "\n")
    fichier.write(r"\begin{enumerate}" + "\n")
    fichier.write(
        r"\item{Tracer la représentation graphique de f sur votre calculatrice. Conjecturer graphiquement les solutions de l'inéquation $f(x)<2$}"
        + "\n"
    )
    fichier.write(r"\href{ex 4.ggb}{figure}" + "\n")
    fichier.write(r"\pause")
    fichier.write(
        r"\item{Justifier que $f(x)-2$ peut aussi s'écrire :$ \dfrac{3 x -10}{4-x}$ }"
        + "\n"
    )
    fichier.write(r"\pause" + "\n")
    fichier.write(
        r"$f(x)-2=\dfrac{x- 2}{4-x}-\dfrac{2}{1}=\pause \dfrac{x- 2}{4-x}-\dfrac{2 \times(4-x)}{1\times(4-x)}$"
        + "\n"
    )
    fichier.write(r"\pause \\" + "\n")
    fichier.write(
        r"$f(x)-2=\pause \dfrac{ x- 2-(8-2 x)}{4-x}=\pause  \dfrac{ x- 2-8+2 x}{4-x}=\pause \dfrac{ 3 x- 10}{4-x}$"
    )
    fichier.write(r"\end{enumerate}" + "\n")
    fichier.write(r"\end{frame}" + "\n")

    fichier.write(r"\begin{frame}" + "\n")
    fichier.write(r"\underline{Exercice 4}" + "\n" + r"\\" + "\n")
    fichier.write(r"\begin{enumerate}" + "\n")
    fichier.write(r"\setcounter{enumi}{2}" + "\n")
    fichier.write(
        r"\item{Vérifier la conjecture proposée au 1) en utilisant un tableau de signe.}"
        + "\n"
    )
    fichier.write(r"\pause " + r"\\" + "\n")
    fichier.write(
        r"$f(x)<2$ équivaut à \pause $f(x)-2<0$, c'est-à-dire à  \pause: $\dfrac{ 3 x- 10}{4-x}<0$ \pause"
        + "\n"
    )
    fichier.write(tableauSigneQuotientDeuxAffines(3, -10, -1, 4))
    fichier.write(r"\end{enumerate}" + "\n")
    fichier.write(r"\end{frame}" + "\n")
    fichier.write(pied)
    fichier.close()
    call(
        [
            "pdflatex",
            "-shell-escape",
            "-synctex=1",
            "-interaction=nonstopmode",
            fichier.name,
        ]
    )
    prenom = fichier.name.split(".")[0]
    nomPdf = prenom + ".pdf"

    for ext in [
        ".aux",
        ".dvi",
        ".out",
        ".ps",
        ".nav",
        ".snm",
        ".toc",
        ".log",
        ".tkzfct.table",
    ]:
        if prenom + ext in listdir("."):
            remove(prenom + ext)

    call(["evince", nomPdf])


if __name__ == "__main__":
    correction_DS()
    ##print polynomeSnD(64,0,-121,'x')
    # questions =[
    # [[4,1],[4,1]],
    # [[8,-11],[8,11]],
    # [[-1,4],[1,10]]
    # ]

    # fichier=open('23_03_correction.tex','w')
    # fichier.write(entete)
    # questions2=[[[5,14],[3, 5]],
    # [[6,0],[1,-1]],
    # [[2,-9],[1,-1]],
    # [[3,-9],[1,-2]],
    # [[-8,-9],[1,1]]]

    # for question in questions2 :
    # exprAff1 = question[0]
    # exprAff2 = question[1]
    # fichier.write(r"\begin{frame}"+'\n')
    # fichier.write(tableauSigneQuotientDeuxAffines(exprAff1[0],exprAff1[1],exprAff2[0],exprAff2[1]))
    ##fichier.write(r"\pause{vérification graphique}:"+'\n')
    ##fichier.write(r"\pause{"+'\n')
    ##fichier.write(traceQuotientDeuxAffines(exprAff1[0],exprAff1[1],exprAff2[0],exprAff2[1]))
    ##fichier.write(r"}"+'\n')
    # fichier.write(r"\end{frame}"+'\n')
    ##for i,question in enumerate(questions) :

    ##fichier.write(r"\begin{frame}"+'\n')
    ##fichier.write(r"\underline{Exercice 39}"+'\n')
    ##fichier.write(r"\\"+'\n')
    ##fichier.write(r"\begin{enumerate}"+'\n')
    ##if i>0 :
    ##fichier.write(r"\setcounter{enumi}{"+str(i)+'}'+'\n')
    ##fichier.write(r"\item"+'\n')
    ##exprAff1 = question[0]
    ##exprAff2 = question[1]
    ###fichier.write(zeroAffine(exprAff1[0],exprAff1[1]))
    ###fichier.write(tableauSigneFonctionAffine(exprAff1[0],exprAff1[1]))
    ###fichier.write(traceFonctionAffineTikz(exprAff1[0],exprAff1[1]))
    ###fichier.write(zeroAffine(exprAff2[0],exprAff2[1]))
    ###fichier.write(tableauSigneFonctionAffine(exprAff2[0],exprAff2[1]))
    ##fichier.write(tableauSigneProduitDeuxAffines(exprAff1[0],exprAff1[1],exprAff2[0],exprAff2[1]))
    ##fichier.write(r"\\"+'\n')
    ###fichier.write(r"\onslide<5->{vérification graphique}:"+'\n')
    ###fichier.write(r"\onslide<6->{"+'\n')
    ####fichier.write(traceProduitDeuxAffines(exprAff1[0],exprAff1[1],exprAff2[0],exprAff2[1]))
    ###fichier.write(r"}"+'\n')
    ##fichier.write(r"\end{enumerate}"+'\n')
    ##fichier.write(r"\end{frame}"+'\n')
    ###fichier.write(r"\begin{frame}"+'\n')
    ###fichier.write(tableauSigneQuotientDeuxAffines(exprAff1[0],exprAff1[1],exprAff2[0],exprAff2[1]))
    ###fichier.write(r"\onslide<5->{vérification graphique}:"+'\n')
    ###fichier.write(r"\onslide<6->{"+'\n')
    ###fichier.write(traceQuotientDeuxAffines(exprAff1[0],exprAff1[1],exprAff2[0],exprAff2[1]))
    ###fichier.write(r"}"+'\n')
    ###fichier.write(r"\end{frame}"+'\n')
    # fichier.write(pied)
    # fichier.close()

    # call(['pdflatex','-shell-escape','-synctex=1','-interaction=nonstopmode', fichier.name])
    # prenom=fichier.name.split('.')[0]
    # nomPdf=prenom+".pdf"

    # for ext in ['.aux','.dvi','.out','.ps','.nav','.snm','.toc','.log','.tkzfct.table'] :
    # if prenom+ext in listdir('.'):
    # remove(prenom+ext)

    # call(['evince',nomPdf])
