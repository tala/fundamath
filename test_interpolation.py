#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

import unittest

import sympy

from .interpolation import *


class interpolationTestCase(unittest.TestCase):
    def setUp(self):
        x = sympy.symbols("x")

    def test_hermite(self):
        p1 = hermite([[0, 0, 0], [1, 1, 2]])
        p2 = x ** 2
        self.assertEqual(p1, p2)

    def test_solve_hpm_val(self):
        hpm = hermiteParMorceauxC1(
            [[-3, 2], [-2, 3, 0], [-1, 0], [0, -2, 0], [2, 0], [3, 5]]
        )
        self.assertEqual(solve_hpm_val(hpm, 0), set([-1, 2]))


if __name__ == "__main__":
    unittest.main()
