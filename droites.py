#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division
from sympy import Rational
from produitScalaire import dessine_droite


def equation_reduite_points(pt1, pt2):
    if pt1[0] == pt2[0]:
        c = pt1[0]
        return [c]
    else:
        m = Rational(pt2[1] - pt1[1], pt2[0] - pt1[0])
        p = pt1[1] - pt1[0] * m
        return [m, p]


def point_intersection(equ1, equ2):
    if len(equ1) == 1:
        x = equ1[0]
        y = equ2[0] * x + equ2[1]
    if len(equ2) == 1:
        x = equ2[0]
        y = equ1[0] * x + equ1[1]
    if len(equ1) == 2 and len(equ2) == 2 and equ1[0] != equ2[0]:
        x = Rational(equ2[1] - equ1[1], equ1[0] - equ2[0])
        y = equ1[0] * x + equ1[1]
    return [x, y]


def parallele_par_point(equation, point):
    if len(equation) == 2:
        m = equation[0]
        p = point[1] - equation[0] * point[0]
        return [m, p]
    else:
        return [point[0]]


def grille(repere, points, echelle=0.5):
    instructions = (
        r"\begin{{tikzpicture}}[scale={}]"
        "\n".format(echelle) + r"\tkzInit[xmin=-5,ymin=-5,xmax=5,ymax=5]"
        "\n"
    )
    for pt in repere:
        instructions += (
            r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(
                pt[1], pt[2], "below left", pt[0]
            )
            + "\n"
        )
    instructions += r"\end{tikzpicture}" "\n"
    return instructions


if __name__ == "__main__":
    print equation_reduite_points([1, 5], [3, 7])
    print equation_reduite_points([1, 5], [3, 5])
    print equation_reduite_points([1, 4], [3, 5])
    print equation_reduite_points([1, 2], [5, 8])
    print point_intersection([1, 2], [-3, 2])
    print point_intersection([-1, -2], [-3, 2])
    print point_intersection([-1], [-3, 2])
    print point_intersection([-1, 4], [2])
    print parallele_par_point([-2, 3], [3, 4])
    print parallele_par_point([-2], [3, 4])
    print grille([["O", 0, 0], ["I", 1, 0], ["J", 1, 1]], [])
