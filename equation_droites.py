#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  equation_droites.py
#
#  Copyright 2017 talabardon <talabardon@talabardon-700T1C>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

from sympy import Eq, Rational, floor, solve, symbols

from in_equations import simplifier
from sympy_to_latex import reformat_latex as latex

x, y, const_c = symbols("x,y,c")


def entier_egal_ou_sup(nb):
    if nb == floor(nb):
        return nb
    else:
        return floor(nb) + 1


def variable(expression):
    return expression.free_symbols.pop()


def presentation_point(P):
    return r"${}\left({};{}\right)$".format(*map(latex, P))


def get_coeff_dir(droite):
    u"""
    Retourne le coefficient directeur de la droite donnée où D est
    donnée par son équation réduite.
    """
    nom_D, equation_D = droite
    expression = equation_D.args[1]
    if expression.is_Number:
        return 0
    else:
        var = variable(expression)
        if expression.args[1] == x:
            return 1
        elif expression.args[1] == -x:
            return -1
        else:
            return expression.args[1].args[0]


def coefficient_directeur(A, B):
    u"""
    Retourne le coefficient directeur de la droite passant par les
    points A et B.
    Hypothèse est faite que ces points n'ont pas la même abscisse.
    """
    return Rational((B[2] - A[2]), (B[1] - A[1]))


def equation_reduite(A, B):
    """
    Renvoie directement l'équation réduite de la droite passant par les
    points A et B."""
    nomA, x_A, y_A = A
    nomB, x_B, y_B = B
    if x_B != x_A:
        m = coefficient_directeur(A, B)
        p = y_A - m * x_A
        return Eq(y, m * x + p)
    else:
        return Eq(x, x_A)


def equation_cartesienne_droite(A, B):
    """
    Renvoie directement une équation cartésienne de la droite passant
    par les points A et B.
    """
    nomA, x_A, y_A = A
    nomB, x_B, y_B = B
    a = y_B - y_A
    b = -(x_B - x_A)
    c = -(a * x_A + b * y_A)
    return Eq(a * x + b * y + c, 0)


def parallele_passant_par(equation, point):
    u"""
    Renvoie directement l'équation réduite de la droite parallèle à
    la droite dont on donne une équation et passant par le point donnés.
    """
    inconnue = equation.args[0]
    if inconnue == x:
        return Eq(x, point[1])
    else:
        expression = equation.args[1]
        expression2 = (
            expression.args[1] + point[2] - expression.args[1].subs(x, point[1])
        )
        return Eq(y, expression2)


# ce qui suit devrait être "factorisé" dans un module à part de mise en
# forme qui permettrait de se passer de sympy


def latex_enveloppe_si_besoin(n):
    if n < 0:
        return ur"\left({}\right)".format(latex(n))
    else:
        return latex(n)


def etapes_calcul_affine(expression, val):
    etapes = []
    if expression.is_Number:
        etapes.append(latex(expression))
    else:
        var = expression.free_symbols.pop()
        if not expression.args:
            etapes.append(latex(expression.subs(var, val)))
        elif expression.is_Mul:
            if expression.args[0] == -1:
                if val < 0:
                    etapes.append(ur"-\left({}\right)".format(latex(val)))
                etapes.append(latex(-val))
            else:
                etapes.append(
                    latex(expression.args[0])
                    + ur"\times "
                    + latex_enveloppe_si_besoin(val)
                )
                etapes.append(latex(expression.subs(var, val)))
        elif expression.is_Add:
            etapes = map(
                lambda s: "{} + {}".format(
                    s, latex_enveloppe_si_besoin(expression.args[0])
                ),
                etapes_calcul_affine(expression.args[1], val),
            )
            etapes.append(latex(expression.subs(var, val)))
    return etapes


def presente_calcul_affine(expression, val):
    return " = ".join(etapes_calcul_affine(expression, val))


# fin de partie à factoriser


def corrige_appartenance_droite(D, P):
    """
    Détaille la démarche déterminant si le point P est sur la droite D
    où D est donnée par son équation réduite.
    """
    nom_D, equation_D = D
    nom_P, x_P, y_P = P
    lignes = []
    if equation_D.args[0] == x:
        if equation_D.args[1] == x_P:
            lignes.append(
                ur"L'abscisse de ${}$ est égale à ${}$\\".format(
                    nom_P, equation_D.args[1]
                )
            )
            lignes.append(
                ur"${}$ appartient donc bien à la droite ${}$".format(nom_P, nom_D)
            )
        else:
            lignes.append(
                ur"L'abscisse de ${}$ n'est pas égale à ${}$\\".format(
                    nom_P, equation_D.args[1]
                )
            )
            lignes.append(
                ur"${}$ n'appartient donc pas à la droite ${}$".format(nom_P, nom_D)
            )
    elif equation_D.args[1].is_Number:
        if equation_D.args[1] == y_P:
            lignes.append(
                ur"L'ordonnée de ${0}$ est bien égale à ${1}$, "
                ur"donc ${0}$ appartient bien à ${2}$".format(
                    nom_P, latex(equation_D.args[1]), nom_D
                )
            )
        else:
            lignes.append(
                ur"L'ordonnée de ${0}$ n'est pas égale à ${1}$, "
                ur"donc ${0}$ n'appartient pas à ${2}$".format(
                    nom_P, latex(equation_D.args[1]), nom_D
                )
            )

    else:
        calculs = presente_calcul_affine(equation_D.args[1], x_P)
        resultat = equation_D.args[1].subs(x, x_P)
        if resultat == y_P:
            lignes.append("$" + calculs + r"$\\")
            lignes.append(
                "Le point {} est sur la droite ${}$.".format(
                    presentation_point(P), nom_D
                )
            )
        else:
            lignes.append(ur"${} \neq {}$\\".format(*map(latex, [calculs, y_P])))
            lignes.append(
                ur"Le point {} n'est donc pas sur la droite ${}$.".format(
                    presentation_point(P), nom_D
                )
            )
    return "\n".join(lignes)


def corrige_coeff_directeur(A, B):
    u"""
    Détaille le calcul du coefficient directeur de la droite passant
    par les points A et B.
    """
    if A[1] == B[1]:
        return (
            ur"Les deux points ont la même abscisse, donc la "
            ur"droite $\left({}{}\right)$ n'a pas de coefficient directeur".format(
                A[0], B[0]
            )
        )
    formule = r"$m = \dfrac{{y_{0}-y_{1}}}{{x_{0}-x_{1}}}".format(B[0], A[0])
    x_B, y_B = map(latex, B[1:3])
    x_A, y_A = map(latex_enveloppe_si_besoin, A[1:3])
    formule += r" = \dfrac{{{}-{}}}{{{}-{}}}".format(y_B, y_A, x_B, x_A)
    Dy = B[2] - A[2]
    Dx = B[1] - A[1]
    expression_1 = r"\dfrac{{{}}}{{{}}}".format(latex(Dy), latex(Dx))
    m = Rational(Dy, Dx)
    expression_2 = latex(m)
    if expression_1 != expression_2:
        formule += " = " + expression_1
    formule += r" = \mathbf{{{}}} ".format(expression_2)
    formule += "$"
    return formule


def corrige_ordonnee_origine(m, P):
    u"""
    Détaille le calcul de l'ordonnée à l'origine la droite de
    coefficient directeur m et passant par le point P.
    """
    nom_P, x_P, y_P = P
    lignes = []
    lignes.append(
        "Je sais que la droite passe par le point "
        ur"${}\left({} ; {}\right)$. "
        ur"Donc ses coordonnées vérifient les équations "
        ur"de la droite.\\".format(*map(latex, P))
    )
    lignes.append(
        u"Je sais également que le coefficient directeur "
        ur"de la droite est ${}$.\\".format(latex(m))
    )
    lignes.append(
        ur"J'obtiens donc l'équation : ${}={} \times {}+p$ "
        ur"où $p$ désigne l'ordonnée à l'origine "
        ur" de la droite.\\".format(
            *map(latex, [y_P, m, latex_enveloppe_si_besoin(x_P)])
        )
    )
    formule_0 = r"{} = {} \times {}+p".format(
        *map(latex, [y_P, m, latex_enveloppe_si_besoin(x_P)])
    )
    equivalences = [formule_0]
    formule_1 = "{} = {}+p".format(*map(latex, [y_P, m * x_P]))
    equivalences.append(formule_1)
    b = latex_enveloppe_si_besoin(m * x_P)
    formule_2 = "p ={}-{}".format(*map(latex, [y_P, b]))
    equivalences.append(formule_2)
    equivalences.append(r"p = \mathbf{{{}}}".format(latex(y_P - m * x_P)))
    lignes.append("$" + r" \iff ".join(equivalences) + ur"$\\")
    equivalences.append(formule_2)
    return "\n".join(lignes)


def corrige_equation_reduite(A, B):
    """
    Corrige en détail la recherche de l'équation réduite de la droite
    passant par les points A et B donnés.
    """
    nomA, x_A, y_A = A
    nomB, x_B, y_B = B
    lignes = []
    if x_A != x_B:
        lignes.append(ur"\begin{itemize}")
        lignes.append(r"\item \underline{Calcul du coefficient directeur :}\\")
        lignes.append(corrige_coeff_directeur(A, B) + r"\\")
        lignes.append(r"\item \underline{" ur"Calcul de l'ordonnée à l'origine :}\\")
        m = coefficient_directeur(A, B)
        lignes.append(corrige_ordonnee_origine(m, A))
        equation = equation_reduite(A, B)
        lignes.append(
            ur"\item $\mathbf{{{}}}$ est donc une équation "
            ur"de la droite $\left({}{}\right)$.".format(latex(equation), A[0], B[0])
        )
        lignes.append(ur"\end{itemize}")
    else:
        lignes.append(
            ur"Je constate que l'abscisse du point ${}$ "
            ur"et celle du point ${}$ sont égales. ".format(nomA, nomB)
        )
        lignes.append(
            ur"\item $x ={2}$ est donc une équation "
            ur"de la droite $({0}{1}$).".format(nomA, nomB, x_A)
        )
    return "\n".join(lignes)


def corrige_equation_parallele(D, P, nom_res="d'"):
    """
    Corrige en détail la recherche de l'équation réduite de la droite
    parallèle à une droite d'équation réduite connue et passant par un
    point connu.
    """
    res = ""
    nom_droite, equation_droite = D
    nom_P, x_P, y_P = P
    if equation_droite.args[0] == x:
        res += (
            ur"La droite ${0}$ est parallèle à la droite ${1}$ qui est "
            ur"parallèle à l'axe des ordonnées. Donc ${0}$ est elle-même "
            ur"parallèle à l'axe des ordonnées. Son équation est donc du "
            ur"type $x=c$ où $c$ est un certain nombre réel.\\"
            "\n".format(nom_res, nom_droite)
        )
        res += (
            ur"D'autre part, le point ${0}$, de coordonnées "
            ur"${0}\left({1} ; {2}\right)$ ".format(*map(latex, P))
        )
        res += ur"appartient à ${}$.\\" "\n".format(nom_res)
        res += ur"Donc l'équation de ${}$ est $x={}$.".format(nom_res, latex(P[1]))
    else:
        m = get_coeff_dir(D)
        res += (
            ur"La droite ${0}$ est parallèle à la droite ${1}$ qui "
            ur"a pour coefficient directeur ${2}$. Donc ${0}$ a elle-même "
            ur"pour coefficent directeur ${2}$.".format(nom_res, nom_droite, latex(m))
        )
        res += corrige_ordonnee_origine(m, P)
        res += ur"Donc l'équation de ${}$ est ${}$.".format(
            nom_res, latex(parallele_passant_par(equation_droite, P))
        )
    return res


def points_sur_droite(D, x_1=None, x_2=None):
    """
    Détaille la recherche de deux points situés sur une droite donnée
    par son équation réduite, aux abscisses indiquées, si besoin.
    """
    nom_D, equation_D = D
    lignes = [
        r"\underline{{Points sur la droite ${}$, "
        ur"d'équation : ${}$}}.\\".format(*map(latex, D))
    ]
    if equation_D.args[0] == x:
        lignes.append(
            ur"Il suffit de choisir deux points d'abscisse "
            ur"${}$.".format(equation_D.args[1])
        )
    else:
        lignes.append(ur"\begin{itemize}")
        lignes.append(r"\item\underline{recherche du premier point :}\\")
        lignes.append(ur"Je choisis comme première abscisse ${}$.".format(latex(x_1)))
        lignes.append(
            r"${}$.\\".format(presente_calcul_affine(equation_D.args[1], x_1))
        )
        lignes.append(
            ur"Le point de coordonnées $\left({} ; {} \right)$"
            ur" est sur la droite ${}$.\\".format(
                *map(latex, (x_1, equation_D.args[1].subs(x, x_1), nom_D))
            )
        )
        lignes.append(u"\\item\\underline{recherche du deuxième point :}" r"\\")
        lignes.append(ur"Je choisis comme deuxième abscisse ${}$.".format(latex(x_2)))
        lignes.append(
            r"${}$.\\".format(presente_calcul_affine(equation_D.args[1], x_2))
        )
        lignes.append(
            ur"Le point de coordonnées $\left({} ; {} \right)$"
            ur" est sur la droite ${}$.".format(
                *map(latex, (x_2, equation_D.args[1].subs(x, x_2), nom_D))
            )
        )
        lignes.append(ur"\end{itemize}")
    return "\n".join(lignes)


def corrige_lecture_graphique_coeff_dir(
    droite, xmin=-2, xmax=5, xstep=1, ymin=-2, ymax=5, ystep=1
):
    lignes = lignes_trace_droite(droite, xmin, xmax, ymin, ymax)
    point_A, point_B = points_pour_coeff_dir(
        droite, xmin, xmax, xstep, ymin, ymax, ystep
    )
    lignes = (
        lignes[:-1] + lignes_decompose_passage(point_A, point_B) + [lignes[-1] + r"\\"]
    )
    if point_A[1] == point_B[1]:
        lignes.append(ur"La droite est parallèle à l'axe des ordonnées.\\")
        lignes.append(
            ur"La droite ${}$ n'a pas de coefficient directeur".format(droite[0])
        )
    else:
        lignes.append(ur"Le coefficient directeur de ${}$ est :\\".format(droite[0]))
        lignes.append(
            ur"$\dfrac{{{}}}{{{}}}=\mathbf{{{}}}$.".format(
                point_B[2] - point_A[2],
                point_B[1] - point_A[1],
                latex(Rational(point_B[2] - point_A[2], point_B[1] - point_A[1])),
            )
        )
    return "\n".join(lignes)


def points_extremes(droite, xmin=-2, xmax=5, ymin=-2, ymax=5):
    nom_D, equation_D = droite
    if equation_D.args[0] == x:
        x_D = equation_D.args[1]
        if xmin <= x_D <= xmax:
            res = (["P_1", x_D, ymin], ["P_2", x_D, ymax])
        else:
            res = ()
    elif equation_D.args[1].is_Number:
        y_D = equation_D.args[1]
        if ymin <= y_D <= ymax:
            res = (["P_1", xmin, y_D], ["P_2", xmax, y_D])
        else:
            res = ()
    else:  # la droite est donc "oblique" et coupe les "bords" en quatre
        # points
        expression = equation_D.args[1]
        points = []
        y_1 = expression.subs(x, xmin)
        if ymin < y_1 < ymax:
            points.append(["P_1", xmin, y_1])
        y_2 = expression.subs(x, xmax)
        if ymin < y_2 < ymax:
            points.append(["P_2", xmax, y_2])
        x_3 = solve(Eq(expression, ymin))[0]
        if xmin <= x_3 <= xmax:
            points.append(["P_3", x_3, ymin])
        x_4 = solve(Eq(expression, ymax))[0]
        if xmin <= x_4 <= xmax:
            points.append(["P_4", x_4, ymax])
        res = tuple(points)
    return res


def points_sur_quadrillage(droite, xmin=-2, xmax=5, xstep=1, ymin=-2, ymax=5, ystep=1):
    """
    Renvoie la liste des points de la droite situés sur le quadrillage
    """
    res = []
    point_A, point_B = points_extremes(droite, xmin, xmax, ymin, ymax)
    nom_D, equation_D = droite
    x_0 = min(point_A[1], point_B[1])
    x_fin = max(point_A[1], point_B[1])
    y_0 = min(point_A[2], point_B[2])
    y_fin = max(point_A[2], point_B[2])
    expression = equation_D.args[1]
    if equation_D.args[0] == x:
        if equation_D.args[1] % 1 == 0:
            res = [
                ["P_" + str(i), equation_D.args[1], y_i]
                for i, y_i in enumerate(
                    xrange(entier_egal_ou_sup(y_0), floor(y_fin) + 1)
                )
            ]
    elif equation_D.args[1].is_Number:
        equation_D.args[1]
        if equation_D.args[1] % 1 == 0:
            res = [
                ["P_" + str(i), x_i, equation_D.args[1]]
                for i, x_i in enumerate(
                    xrange(entier_egal_ou_sup(x_0), floor(x_fin) + 1)
                )
            ]
    else:
        res = [
            ["P_" + str(i), x_i, equation_D.args[1].subs(x, x_i)]
            for i, x_i in enumerate(xrange(entier_egal_ou_sup(x_0), floor(x_fin) + 1))
            if equation_D.args[1].subs(x, x_i) % 1 == 0
        ]
    return res


def points_pour_coeff_dir(droite, xmin=-2, xmax=5, xstep=1, ymin=-2, ymax=5, ystep=1):
    res = []
    liste_initiale = points_sur_quadrillage(
        droite, xmin, xmax, xstep, ymin, ymax, ystep
    )

    if len(liste_initiale) == 2:
        res = liste_initiale
    else:
        print "liste_initiale :"
        print liste_initiale
        res = liste_initiale[:2]
        print res
    return res


def lignes_decompose_passage(point_A, point_B):
    lignes = []
    lignes.append(
        r"\draw[color=blue,dashed,very thick,-latex] ({0},{1}) -- ({2},{1});".format(
            point_A[1], point_A[2], point_B[1], point_B[2]
        )
    )
    lignes.append(
        r"\draw[color=green,dashed,very thick,-latex] ({2},{1}) -- ({2},{3});".format(
            point_A[1], point_A[2], point_B[1], point_B[2]
        )
    )
    print lignes
    return lignes


def lignes_trace_droites(droites, xmin=-2, xmax=5, ymin=-2, ymax=5):
    lignes = [ur"\begin{tikzpicture}[scale=0.5]"]
    lignes.append(
        ur"\tkzInit[xmin={},xmax={},ymin={},ymax={}]".format(xmin, xmax, ymin, ymax)
    )
    lignes.append(r"\tkzGrid \tkzDrawXY")
    lignes.append(r"\draw (0,0) node[below left] {$O$};")
    lignes.append(r"\draw (1,0) node[below] {$1$};")
    lignes.append(r"\draw (0,1) node[right] {$1$};")
    for d in droites:
        point_A, point_B = points_extremes(
            d, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax
        )
        lignes.append(
            r"\draw [very thick] ({},{}) -- ({},{});".format(
                *map(lambda k: round(k, 3), point_A[1:3] + point_B[1:3])
            )
        )
    lignes.append(ur"\end{tikzpicture}")
    return lignes


def trace_droites(droites, xmin=-2, xmax=5, ymin=-2, ymax=5):
    return "\n".join(lignes_trace_droites(droites, xmin, xmax, ymin, ymax))


def lignes_trace_droite(droite, xmin=-2, xmax=5, ymin=-2, ymax=5):
    lignes = [ur"\begin{tikzpicture}[scale=0.5]"]
    lignes.append(
        ur"\tkzInit[xmin={},xmax={},ymin={},ymax={}]".format(xmin, xmax, ymin, ymax)
    )
    lignes.append(r"\tkzGrid \tkzDrawXY")
    lignes.append(r"\draw (0,0) node[below left] {$O$};")
    lignes.append(r"\draw (1,0) node[below] {$1$};")
    lignes.append(r"\draw (0,1) node[right] {$1$};")
    point_A, point_B = points_extremes(droite)
    lignes.append(
        r"\draw [very thick] ({},{}) -- ({},{});".format(
            *map(lambda k: round(k, 3), point_A[1:3] + point_B[1:3])
        )
    )
    lignes.append(ur"\end{tikzpicture}")
    return lignes


def trace_droite(droite, xmin=-2, xmax=5, ymin=-2, ymax=5):
    return "\n".join(lignes_trace_droite(droite, xmin, xmax, ymin, ymax))


def corrige_trace_droite(droite, xmin=-2, xmax=5, ymin=-2, ymax=5, x1=0, x2=3):
    correction = points_sur_droite(droite, x1, x2)
    correction += "\n"
    correction += place_points_sur_droite(droite, xmin, xmax, ymin, ymax, x1, x2)
    correction += place_points_et_droite(droite, xmin, xmax, ymin, ymax, x1, x2)
    return correction


def place_points_sur_droite(droite, xmin=-2, xmax=5, ymin=-2, ymax=5, x1=0, x2=3):
    lignes = [ur"\begin{tikzpicture}[scale=0.5]"]
    lignes.append(
        ur"\tkzInit[xmin={},xmax={},ymin={},ymax={}]".format(xmin, xmax, ymin, ymax)
    )
    lignes.append(r"\tkzGrid \tkzDrawXY")
    lignes.append(r"\draw (0,0) node[below left] {$O$};")
    lignes.append(r"\draw (1,0) node[below] {$1$};")
    lignes.append(r"\draw (0,1) node[right] {$1$};")
    y1 = droite[1].args[1].subs(x, x1)
    lignes.append(r"\draw ({},{}) node {{x}};".format(x1, y1))
    y2 = droite[1].args[1].subs(x, x2)
    lignes.append(r"\draw ({},{}) node {{x}};".format(x2, y2))
    lignes.append(ur"\end{tikzpicture}")
    return "\n".join(lignes)


def place_points_et_droite(droite, xmin=-2, xmax=5, ymin=-2, ymax=5, x1=0, x2=3):
    lignes = [ur"\begin{tikzpicture}[scale=0.5]"]
    lignes.append(
        ur"\tkzInit[xmin={},xmax={},ymin={},ymax={}]".format(xmin, xmax, ymin, ymax)
    )
    lignes.append(r"\tkzGrid \tkzDrawXY")
    lignes.append(r"\draw (0,0) node[below left] {$O$};")
    lignes.append(r"\draw (1,0) node[below] {$1$};")
    lignes.append(r"\draw (0,1) node[right] {$1$};")
    y1 = droite[1].args[1].subs(x, x1)
    lignes.append(r"\draw ({},{}) node {{x}};".format(x1, y1))
    y2 = droite[1].args[1].subs(x, x2)
    lignes.append(r"\draw ({},{}) node {{x}};".format(x2, y2))
    point_A, point_B = points_extremes(droite)
    lignes.append(
        r"\draw [very thick] ({},{}) -- ({},{});".format(
            *map(lambda k: round(k, 3), point_A[1:3] + point_B[1:3])
        )
    )
    lignes.append(ur"\end{tikzpicture}")
    return "\n".join(lignes)


def corrige_intersection(d_1, d_2):
    res = ""
    nom1, equation1 = d_1
    nom2, equation2 = d_2
    if equation1.args[0] == y and equation2.args[0] == y:
        res += (
            ur"Les coordonnées du point d'intersection des droites "
            ur"${}$ et ${}$ doivent vérifier à la fois les deux équations.\\".format(
                nom1, nom2
            )
            + "\n"
        )
        res += (
            ur"En notant $(x,y)$ les coordonnées de ce point, "
            ur"on obtient donc le système :"
        )
        res += ur"""
    $\left\{{
    \begin{{array}}{{l}}
        {}\\
        {}
    \end{{array}}
    \right.$\\
    """.format(
            latex(equation1), latex(equation2)
        )
        equation = Eq(equation1.args[1], equation2.args[1])
        res += ur"On a donc : ${}$\\" "\n".format(latex(equation))
        etapes = simplifier(equation)
        res += "$\phantom{\iff}" + (r"$\\" "\n" r"$\iff ").join(map(latex, etapes[:-1]))
        res += r"$\\" "\n" r"$ \iff \mathbf{{{}}}$\\" "\n".format(latex(etapes[-1]))
        x_I = etapes[-1].args[1]
        res += (
            ur"Pour le calcul de $y$, on utilise indifféremment "
            ur"l'une ou l'autre des équations de droites.\\"
            "\n"
        )
        res += r"$y =" + presente_calcul_affine(equation1.args[1], x_I)
        y_I = equation1.args[1].subs(x, x_I)
        res += (
            ur"Les coordonnées du points d'intersection "
            ur"des droites ${}$ et ${}$ sont $\left({};{}\right)".format(
                nom1, nom2, latex(x_I), latex(y_I)
            )
        )
        res += r"$\\" "\n"
        res += trace_droites([d_1, d_2])
    return res


def corrige_lecture_graphique_equation(droite_d):
    res = ""
    return res


def question_calcul_coeff_dir(point_A, point_B):
    q = {}
    q["consigne"] = (
        ur"Soit les points ${0}$ et ${2}$ de coordonnées "
        ur"respectives {1} et {3}.\\"
        "\n"
        ur"Déterminez, s'il existe, le coefficient "
        ur"directeur de la droite $\left({0}{2}\right)$.".format(
            point_A[0],
            presentation_point(point_A),
            point_B[0],
            presentation_point(point_B),
        )
    )
    q["enonce"] = ""
    q["corrige"] = corrige_coeff_directeur(point_A, point_B)
    return q


def question_lecture_graphique_coeff_dir(D):
    q = {}
    q["consigne"] = (
        ur"Déterminer graphiquement, s'il existe, le "
        ur"coefficient directeur de la droite ${}$ tracée ci-dessous :".format(D[0])
    )
    q["enonce"] = trace_droite(D)
    q["corrige"] = corrige_lecture_graphique_coeff_dir(D)
    return q


def question_point_droite(P, D):
    q = {}
    nom_D, equation_D = D
    presentation_droite = ur"${}$ d'équation ${}$".format(*map(latex, D))
    q["consigne"] = (
        ur"Déterminer si le point {} appartient ou non à "
        ur"la droite {}".format(presentation_point(P), presentation_droite)
    )
    q["enonce"] = ""
    q["corrige"] = corrige_appartenance_droite(D, P)
    return q


#### Pour les premières scientifiques


def corrige_equation_cartesienne_version_longue(A, B):
    v = [B[1] - A[1], B[2] - A[2]]
    texte = (
        r"Le vecteur $\overrightarrow{{{0}{1}}}$ est un vecteur directeur de la droite $\left({0}{1}\right)$.\\".format(
            A[0], B[0]
        )
        + "\n"
    )
    texte += ur"Ce vecteur a pour coordonnées : $\overrightarrow{{{}{}}}\begin{{pmatrix}}{}\\{}\end{{pmatrix}}$.\\".format(
        A[0], B[0], v[0], v[1]
    )
    texte += (
        ur"Soit $M$ un point du plan. Notons ses coordonnées $M(x,y)$. Alors $M$ est sur la droite $({0}{1})$ si et seulement si les vecteurs $\overrightarrow{{{0}M}}$ et $\overrightarrow{{{0}{1}}}$ sont colinéaires.\\".format(
            A[0], B[0]
        )
        + "\n"
    )
    texte += ur"C'est à dire : $M \in ({0}{1}) \iff {2} \left(\right)\times{{}}-{3}\times{{}}=0$".format(
        A[0], B[0], v[0], v[1]
    )
    return texte


def corrige_equation_cartesienne_version_courte_point_vecteur(A, v, nomD=None):
    nomV, Vx, Vy = v
    texte = (
        r"Le vecteur $\overrightarrow{{{}}}\begin{{pmatrix}}{}\\{}\end{{pmatrix}}$ est un vecteur directeur de la droite ${}$.\\".format(
            nomV, Vx, Vy, nomD
        )
        + "\n"
    )
    texte += (
        ur"La droite ${}$ admet donc une équation cartésienne " ur"du type ${}$"
    ).format(nomD, latex(Eq(Vy * x - Vx * y + const_c, 0)))
    return texte


if __name__ == "__main__":
    print (
        points_extremes(
            ["D", equation_reduite(["A", 1.5, 1.5 ** 2], ["B", -1, 1])],
            xmin=-3,
            xmax=3,
            ymin=-1,
            ymax=9,
        )
    )
    print (
        points_extremes(
            ["D", equation_reduite(["A", 1.5, 1.5 ** 2], ["B", 0.5, 0.25])],
            xmin=-3,
            xmax=3,
            ymin=-1,
            ymax=9,
        )
    )
    print (
        points_extremes(
            ["D", equation_reduite(["A", 1.5, 1.5 ** 2], ["B", 1.25, 1.25 ** 2])],
            xmin=-3,
            xmax=3,
            ymin=-1,
            ymax=9,
        )
    )
    sortie_type = """
	\draw[line width=1pt] (3, 6.375) -- (0.3182, -1);
                \draw[dashed] (0.5, 0) -- (0.5,0.25);
                \draw[dashed] (0.5,0.25) -- (0, 0.25);
                \draw (0.5,0.25) node {$\times$};
                \draw (0.5, 0.25) node[above] {$B$};
                \draw (0.5, 0) node[below] {$x+h$};
                \draw (0, 0.25) node[left] {$\left(x+h\right)^2$};        
	"""

    for ligne in lignes_trace_droites(
        [["$(d_1)$", Eq(y, -Rational(2, 3) * x + 2)], ["$(d_1)$", Eq(y, 3 * x - 9)]],
        xmin=-5,
        xmax=5,
        ymin=-5,
        ymax=5,
    ):
        print (ligne)

    for ligne in lignes_trace_droites(
        [["$(d_2)$", Eq(y, -Rational(2, 3) * x + 2)], ["$(d_1)$", Eq(y, 3 * x - 9)]],
        xmin=-5,
        xmax=5,
        ymin=-5,
        ymax=5,
    ):
        print (ligne)

    print (corrige_lecture_graphique_coeff_dir(["$d$", Eq(y, 2 * x - 3)]))
    # print presente_calcul_affine(x,3)
    # print presente_calcul_affine(Eq(y,3).args[1],2)
    # print (-x).args
    # print "## présente_calcul_affine"
    # print '-x,7'
    # print presente_calcul_affine(-x, 7)
    # print '-x,-7'
    # print presente_calcul_affine(-x, -7)
    # print '-3x,-7'
    # print presente_calcul_affine(-3*x, -7)
    # print 'x+4,-7'
    # print presente_calcul_affine(x+4, -7)
    # print '3x + 6, 2'
    # print presente_calcul_affine(3*x+6, 2)
    # print '3x + 6, -2'
    # print presente_calcul_affine(3*x+6, -2)
    # print corrige_appartenance_droite(['D',Eq(y,2*x-3)],['B', 2, 5])
    # print corrige_appartenance_droite(['D',Eq(y,2*x-3)],['B', 2, 1])
    # print points_sur_droite(["D", Eq(y, 2 * x - 3)], 0, 3)
    # print points_extremes(["D", Eq(x, 2)])
    # print points_extremes(["D", Eq(y, 2)])
    # print points_extremes(["D", Eq(y, 2 * x - 1)])
    # print trace_droite(["D", Eq(y, 2 * x - 1)])
    # print points_sur_quadrillage(["D", Eq(x, 2)])
    # print points_sur_quadrillage(["D", Eq(y, -1)])
    # print points_sur_quadrillage(["D", Eq(y, x)])
    # print points_sur_quadrillage(["D", Eq(y, 2 * x - 1)])
    # print points_pour_coeff_dir(["D", Eq(y, 2 * x - 1)])
    # print points_extremes(["D", Eq(y, Rational(2, 3) * x - 1)])
    # print points_sur_quadrillage(["D", Eq(y, Rational(2, 3) * x - 1)])
    # print corrige_lecture_graphique_coeff_dir(["D", Eq(y, -3 * x + 4)])
    # print points_sur_droite(["D", Eq(y, -2 * x + 4)], 0, 3)
    # print trace_droite(["D", Eq(y, -2 * x + 4)])
    # print ""
    # print corrige_trace_droite(["D", Eq(y, -2 * x + 4)])
    # print corrige_intersection(["d_1", Eq(y, -2 * x + 4)], ["d_2", Eq(y, 2 * x - 3)])
    # print corrige_equation_parallele(["d_1", Eq(y, -2 * x + 4)], ["M", 2, 3])
    # print corrige_equation_parallele(["d_1", Eq(x, 4)], ["M", 2, 3])
    # droite_d = ["(d)", Eq(y, 3 * x - 5)]
    # print corrige_appartenance_droite(droite_d, ["A", 2, 5])
    # print corrige_appartenance_droite(droite_d, ["B", 3, 4])
    # print corrige_lecture_graphique_coeff_dir(["(d_4)", Eq(y, -1)])
    # print corrige_coeff_directeur(["A", 2, -3], ["B", -2, 5])
    # print corrige_coeff_directeur(["A", 2, -3], ["C", 2, -1])
    # print corrige_equation_parallele(
    # ["(d)", Eq(y, Rational(1, 3) * x + 2)], ["P", 3, -2], "(d')"
    # )
    # print corrige_lecture_graphique_equation(["(d)", Eq(y, Rational(1, 3) * x + 2)])
    # print "\n".join(
    # lignes_trace_droites(
    # [
    # ["(d_1)", Eq(y, 3 * x + 1)],
    # ["(d_2)", Eq(y, 3)],
    # ["(d_3)", Eq(y, Rational(2, 3) * x - 3)],
    # ["(d_4)", Eq(y, -2 * x + 1)],
    # ]
    # )
    # )
    # print "\n".join(
    # map(
    # lambda l: latex(l[1]),
    # (
    # [
    # ["(d_1)", Eq(y, 3 * x + 1)],
    # ["(d_2)", Eq(y, 3)],
    # ["(d_3)", Eq(y, Rational(2, 3) * x - 3)],
    # ["(d_4)", Eq(y, -2 * x + 1)],
    # ]
    # ),
    # )
    # )
    # print corrige_equation_cartesienne_version_courte_point_vecteur(
    # ["A", 2, 3], ["v", 3, 4]
    # )
    #  print points_sur_droite(['D',Eq(x,-3)], 0, 3)
    # print corrige_equation_reduite(['A', 2, 3], ['B', 2, 5])
    # print equation_reduite(['A', 2, 3], ['B', 2, 5])
    # equation = equation_reduite(['A', 3, 3], ['B', 2, 5])
    # print equation
    # print equation.args[1].args[0]
    # print equation.args[1].args[1]
    # print parallele_passant_par(equation, ['D', 3, 5]),
    # print corrige_coeff_directeur(['A', 3, 3], ['B', 2, 5])
    # print corrige_coeff_directeur(['A', -3, 3], ['B', 2, 5])
    # m = coefficient_directeur(['A', -3, 3], ['B', 2, 5])
    # print corrige_ordonnee_origine(m, ['M', 2, 5])
    # print parallele_passant_par(equation_reduite(['A', -3, 3], ['B', 2, 5]),
    # ['M', 2, 5])
    # equation_reduite(['A', -3, 3], ['B', 2, 5])
    # print corrige_equation_reduite(['A', -3, 3], ['B', 2, 5])
    # print corrige_appartenance_droite([r'\mathcal{D}', Eq(x, 5)], ['A', 5, 3])
    # print corrige_appartenance_droite([r"\mathcal{D}'", Eq(x, 2)], ['A', 5, 3])
    # print corrige_appartenance_droite([r"\mathcal{D}'", Eq(y, 2)], ['A', 5, 2])
    # print corrige_appartenance_droite([r"\mathcal{D}'", Eq(y, 2)], ['A', 5, 3])

    # import sys
    # sys.exit(main(sys.argv))
