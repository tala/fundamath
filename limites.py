#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division
import sympy
from sympy import cos, sin, pi, oo, symbols
from compilation.compilateur import compileDocument
from generateurTex import (
    generer_interro_flash_tex,
    generer_interro_flash_corrigee_tex,
    generer_revision_corrigee_tex,
)
from random import choice

x = symbols("x")
t = symbols("t")


def latex(expr):
    if type(expr) == str:
        return expr
    elif expr == oo:
        return ur"+\infty"
    else:
        return sympy.latex(expr)


def question_limite_reference(fct, x=x, xlim=oo, dir=None):
    q = {}
    q["consigne"] = ur"Déterminer si possible, la limite suivante :"
    q["enonce"] = redige_limite(fct, x, xlim, dir)
    q.update(corrige_limite_reference(fct, x=x, xlim=xlim, dir=dir))
    return q


def corrige_limite_reference(fct, x=x, xlim=oo, dir=None):
    r = {"rappel texte": r"\ "}
    limite_gauche = fct.limit(x, xlim, "-")
    limite_droite = fct.limit(x, xlim, "+")
    if not dir and not limite_gauche == limite_droite:
        corrige = (
            redige_limite(fct, x, xlim, "-", lim=limite_gauche)
            + ", "
            + redige_limite(fct, x, xlim, "+", lim=limite_droite)
            + ".\n"
            + " Donc "
            + redige_limite(fct, x, xlim, None, lim="")
            + u" n'est pas définie."
        )
        r["corrige"] = corrige
    elif not dir:
        lim = latex(fct.limit(x, xlim))
        r["corrige"] = redige_limite(fct, x, xlim, dir, lim)
    else:
        lim = latex(fct.limit(x, xlim, dir))
        r["corrige"] = redige_limite(fct, x, xlim, dir, lim)
    return r


def redige_limite(fct, x=x, xlim=oo, dir=None, lim=""):
    if dir == "-":
        borne = r"{{\substack{{{0} \to {1} \\ x<{1}}}}}".format(latex(x), latex(xlim))
    elif dir == "+":
        borne = r"{{\substack{{{0} \to {1} \\ x>{1}}}}}".format(latex(x), latex(xlim))
    else:
        borne = r"{{{0} \to {1} }}".format(latex(x), latex(xlim))
    if latex(x) not in latex(fct):
        expr = r"{0}({1})".format(latex(fct), latex(x))
    else:
        expr = latex(fct)
    if lim == "":
        formule = r"\lim\limits_{0} {{{1}}}".format(borne, expr)
    else:
        formule = r"\lim\limits_{{{0}}}{{{1}}}={2}".format(
            borne, latex(expr), latex(lim)
        )
    return r"$ " + formule + " $"


def question_limite_vers_asymptote(fct="f", equa=["y", 0]):
    q = {}
    q[
        "consigne"
    ] = ur"Donner, si possible, l'équation d'une droite asymptote à ${{\mathcal{{C}}}}_{}$ :".format(
        fct
    )
    if equa == "infini":
        lim = choice([-oo, oo])
        xlim = choice([-oo, oo])
        dir = None
    elif type(equa[0]) is not str:  # lim x->equa[0] = equa[1] et
        # equa est un tableau de nombres.
        xlim = equa[0]
        lim = equa[1]
        dir = None
    elif equa[0] == "y":
        xlim = choice([-oo, oo])
        lim = equa[1]
        dir = None
    else:  # par élimination l'équation est du type 'x=c'
        xlim = equa[1]
        lim = choice([-oo, oo])
        dir = choice(["-", "+"])
    q["enonce"] = redige_limite(fct, x, xlim, dir, lim)
    q.update(corrige_limite_vers_asymptote(fct, equa, xlim, dir, lim))
    return q


def corrige_limite_vers_asymptote(fct, equa, xlim, dir, lim):
    r = {}
    r["rappel texte"] = redige_limite(fct, x, xlim, dir, lim)
    if equa == "infini" or not type(equa[0]) == str:
        r["corrige"] = (
            ur"On ne peut déduire aucune asymptote pour "
            + ur"${{\mathcal{{C}}}}_{}$.".format(fct)
        )
    else:
        r["corrige"] = ur"On peut en déduire que ${{\mathcal{{C}}}}_{}$".format(
            fct
        ) + ur" admet une asymptote d'équation : ${}={}$".format(equa[0], equa[1])
    return r


def trace_quotient_deux_affines(a, b, c, d):
    def fonct(v):
        return (a * v + b) / (c * v + d)

    xmin = min(-d / c - 3, -1)
    xmax = 10  # FIX IT
    ymin = -10  # FIX IT
    ymax = max(a / c + 3, 1)
    # échelle choisie pour se rapprocher du format d'un écran de calculatrice :
    xscale = 6 / (xmax - xmin)
    yscale = 3.5 / (ymax - ymin)
    string = (
        r"\begin{tikzpicture}[xscale=%s,yscale=%s]" % (str(xscale), str(yscale)) + "\n"
    )
    string += (
        r"\tkzInit[xmin={0},xmax={1},ymin={2},ymax={3}]".format(xmin, xmax, ymin, ymax)
        + "\n"
    )
    # string+=r"\tkzGrid[sub]"
    string += (
        r"\tkzDrawX[label={},tickwd=%s,tickup=%s,tickdn=%s]"
        % (str(0.8), str(0.03 / (yscale)), str(0.03 / (yscale)))
        + "\n"
    )
    string += (
        r"\tkzDrawY[label={},tickwd=%s,ticklt=%s,tickrt=%s]"
        % (str(0.8), str(0.03 / (xscale)), str(0.03 / (xscale)))
        + "\n"
    )
    string += r"\tkzLabelX" + "\n"
    string += r"\tkzLabelY" + "\n"
    string += (
        r"\tkzFct[samples=400,domain={0}:{1}]{{\x,({2}*\x+{3})/({4}*\x+{5})}}".format(
            -d / c, xmax, a, b, c, d
        )
        + "\n"
    )
    xlabel = (-d / c + xmax) / 2
    ylabel = fonct(xlabel)
    string += (
        r"\draw ({0},{1}) node[below right]{{${{\mathcal{{C}}}}_f$}};".format(
            xlabel, ylabel
        )
        + "\n"
    )

    string += r"\draw[blue]({0},{1})--({0},{2});".format(-d / c, ymin, ymax) + "\n"
    string += r"\draw[blue]({0},{2})--({1},{2});".format(xmin, xmax, a / c) + "\n"
    string += r"\end{tikzpicture}" + "\n"
    return string


if __name__ == "__main__":
    revision2 = {
        "classe": "Tale STI-2D",
        "date": "Lundi 2 mai 2016",
        "num": "2",
        "questions": [
            question_limite_reference(x ** 2 + 2 * x - 5),
            question_limite_reference(x ** 2 + 2 * x - 5, xlim=2),
            question_limite_reference((x - 2) / (x + 2), xlim=2),
            question_limite_reference((x - 2) / (x + 2), xlim=2, dir="-"),
            question_limite_reference((x - 2) / (x + 2), xlim=-2),
            question_limite_reference((x - 2) / (x + 2), xlim=-2, dir="-"),
            question_limite_vers_asymptote(),
            question_limite_vers_asymptote(fct="g", equa=["x", 0]),
            question_limite_vers_asymptote(fct="g", equa=["y", 2]),
            question_limite_vers_asymptote(fct="g", equa=[3, 2]),
        ],
    }
    compileDocument("revision2", generer_revision_corrigee_tex(revision2))
