#!/usr/bin/python
# -*- coding: utf-8 -*-
# from sympy import symbols, latex, Eq
# x = symbols('x')

#### Fonction auxiliaires

# def signe(expr, val):
# var = expr.free_symbols.pop()
# res = expr.subs(var, val)
# if res > 0 :
# return '+'
# else :
# return '-'

# def separation(expr,val):
# var = expr.free_symbols.pop()
# res = expr.subs(var, val)
# try :
# if res == 0 :
# return 'z'
# elif res < 0 or res > 0 :
# return 't'
# except :
# return 'd'


# def ligne_signe(expr, valeurs):
# ligne = r"\tkzTabLine { ,"
## problèmes à régler
# ligne += signe(expr, valeurs[0]-1)
# ligne += ','
# ligne += separation(expr, valeurs[0])
# ligne += ','
# for val1, val2 in zip(valeurs, valeurs[1:]) :
# ligne += signe(expr,(val1 + val2)/2)
# ligne += ','
# ligne += separation(expr,val2)
# ligne += ','
# ligne += signe(expr, valeurs[-1]+1)
# ligne += ", }"
# return ligne


# def signe_produit(pdt):
# var = pdt.free_symbols.pop()
# racines = Eq(pdt,0).as_set()
# racines = list(racines)
# racines.sort()
# figure = r'''\begin{tikzpicture}
# \tkzTabInit[lgt=4,espcl=1.5]
#'''
# figure += "{{${}$ /1,".format(latex(var))+'\n'
# for facteur in pdt.args :
# figure += " Signe de ${}$ /1,".format(latex(facteur))+'\n'
# figure +=" Signe de ${}$ /1}}".format(latex(pdt))+'\n'
# figure +="{{$-\infty$, {}, $+\infty$}}".format(
#', '.join([latex(r,mode='inline') for r in racines]))+'\n'
# for facteur in pdt.args :
# figure += r'\pause ' + ligne_signe(facteur,racines)+'\n'
# figure += r'\pause ' + ligne_signe(pdt,racines)+'\n'
# figure += r"\end{tikzpicture}"
# return figure

from sympy import symbols, latex, Eq

### Fonction auxiliaires


def signe(expr, val):
    var = expr.free_symbols.pop()
    res = expr.subs(var, val)
    if res > 0:
        return "+"
    else:
        return "-"


def separation(expr, val):
    var = expr.free_symbols.pop()
    res = expr.subs(var, val)
    if res == 0:
        return "z"
    else:
        return "t"


def ligne_signe(expr, valeurs):
    ligne = r"\tkzTabLine { ,"
    ligne += signe(expr, valeurs[0] - 1)
    ligne += ","
    ligne += separation(expr, valeurs[0])
    ligne += ","
    for val1, val2 in zip(valeurs, valeurs[1:]):
        ligne += signe(expr, (val1 + val2) / 2)
        ligne += ","
        ligne += separation(expr, val2)
        ligne += ","
    ligne += signe(expr, valeurs[-1] + 1)
    ligne += ", }"
    return ligne


def signe_produit(pdt):
    var = pdt.free_symbols.pop()
    racines = Eq(pdt, 0).as_set()
    racines = list(racines)
    racines.sort()
    figure = r"""\begin{tikzpicture}
\tkzTabInit[lgt=4,espcl=1.5]
"""
    figure += "{{${}$ /1,".format(latex(var)) + "\n"
    for facteur in pdt.args:
        figure += " Signe de ${}$ /1,".format(latex(facteur)) + "\n"
    figure += " Signe de ${}$ /1}}".format(latex(pdt)) + "\n"
    figure += (
        "{{$-\infty$, {}, $+\infty$}}".format(
            ", ".join([latex(r, mode="inline") for r in racines])
        )
        + "\n"
    )
    for facteur in pdt.args:
        figure += r"\pause " + ligne_signe(facteur, racines) + "\n"
    figure += r"\pause " + ligne_signe(pdt, racines) + "\n"
    figure += r"\end{tikzpicture}"
    return figure


def main():
    x = symbols("x")
    ex35 = [
        (-2 * x + 3) * (-3 * x - 5),
        (2 * x + 14) * (6 * x + 24),
        (5 * x - 65) * (7 - 2 * x),
        (-3 * x - 72) * (-4 * x - 96),
        (-2 * x + 3) * (-3 * x - 5),
    ]
    for pdt in ex35:
        print r"\begin{frame}"
        print signe_produit(pdt)
        print r"\end{frame}"


if __name__ == "__main__":
    main()
