#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division
from sympy import latex, sqrt, cos, sin, pi

# from math import  cos,sin,pi
from fractions import gcd
from compilation.compilateur import compileDocument
from generateurTex import generer_interro_flash_tex, generer_interro_flash_corrigee_tex
from random import choice, randint, shuffle

from sympy import Rational

# import json

## ATTENTION : - Confusions possibles enttre angles en radians, angles en degrés et angles en "fractions" équivalentes à des radians.
##             - confusions également entre "cos" et "\cos" dans les arguments de fonctions : CHOISIR
denominateursReference = [1, 2, 3, 4, 6]
anglesReference = [[0, 1], [1, 1], [1, 6], [1, 4], [1, 3], [1, 2]]
numerateursSimplifies = {
    "1": [-1, 1],
    "2": [-2, -1, 1, 2],
    "3": [-3, -1, 1, 3],
    "4": [-5, -1, 1, 5],
}

valeursReferences = {
    "cos": {
        "0": 1,
        r"\pi": -1,
        r"\dfrac{\pi}{6}": r"\dfrac{\sqrt{3}}{2}",
        r"\dfrac{\pi}{4}": r"\dfrac{\sqrt{2}}{2}",
        r"\dfrac{\pi}{3}": r"\dfrac{1}{2}",
        r"\dfrac{\pi}{2}": 0,
    },
    "sin": {
        "0": 0,
        r"\pi": 0,
        r"\dfrac{\pi}{6}": r"\dfrac{1}{2}",
        r"\dfrac{\pi}{6}": r"\dfrac{1}{2}",
        r"\dfrac{\pi}{4}": r"\dfrac{\sqrt{2}}{2}",
        r"\dfrac{\pi}{3}": r"\dfrac{\sqrt{3}}{2}",
        r"\dfrac{\pi}{2}": 1,
    },
}


def cos_(x):
    return round(cos(x), 4)


def sin_(x):
    return round(sin(x), 4)


def rad2deg(angle):
    return angle[0] * 180 / angle[1]


def rad2ODF(liste):
    """
    Prépare l'écriture de radians en formule open document.
    """

    if liste[0] == 0:
        frac = "${0}$"
    elif liste[0] == liste[1]:
        frac = "${%pi}$"
    elif liste[0] == -liste[1]:
        frac = "${-%pi}$"
    elif liste[0] == 1:
        frac = "${{%pi}over{" + str(liste[1]) + "}}$"
    elif liste[0] == -1:
        frac = "-${{%pi}{" + str(liste[1]) + "}}$"
    elif liste[1] == -1:  # Probablement inutile
        frac = "${" + str(-liste[0]) + " %pi}$"
    elif liste[1] == 1:
        frac = "${" + str(liste[0]) + "%pi}$"
    elif liste[0] < 0:
        frac = "${- {{" + str(-liste[0]) + "%pi}over{" + str(liste[1]) + "}}}$"
    else:
        frac = "${ {" + str(liste[0]) + "%pi}over{" + str(liste[1]) + "}}$"
    return frac


def rad2tex(liste, contexteMath=False):
    """
    Prépare l'écriture de radians en latex.(extrait modifié de Pyromaths).
    """

    if liste[0] == 0:
        frac = "0"
    elif liste[0] == liste[1]:
        frac = "\\pi"
    elif liste[0] == -liste[1]:
        frac = "-\\pi"
    elif liste[0] == 1:
        frac = "\\dfrac{\\pi}{" + str(liste[1]) + "}"
    elif liste[0] == -1:
        frac = "-\\dfrac{\\pi}{" + str(liste[1]) + "}"
    elif liste[1] == -1:  # Probablement inutile
        frac = str(-liste[0]) + "\\pi"
    elif liste[1] == 1:
        frac = str(liste[0]) + "\\pi"
    elif liste[0] < 0:
        frac = "- \\dfrac{" + str(-liste[0]) + "\\pi}{" + str(liste[1]) + "}"
    else:
        frac = " \\dfrac{" + str(liste[0]) + "\\pi}{" + str(liste[1]) + "}"
    if not contexteMath:
        frac = "$" + frac + "$"
    return frac


def tex2rad(texte):
    if texte[0] == "$":
        contexteMath = False
    else:
        contexteMath = True
    for angle in anglesReference:
        if rad2tex(angle, contexteMath) == texte:
            angleRes = angle
        elif rad2tex([-angle[0], angle[1]], contexteMath) == texte:
            angleRes = [-angle[0], angle[1]]
        elif rad2tex([angle[1] - angle[0], angle[1]], contexteMath) == texte:
            angleRes = [angle[1] - angle[0], angle[1]]
    return angleRes


def formatAngle(numerateur, denominateur, format="latex"):
    if format == "latex":
        if numerateur == 0:
            string = "$0$"
        elif denominateur == 1:
            if numerateur == 1:
                string = r"$\pi$"
            else:
                string = r"${}\pi$".format(numerateur)
        else:
            if numerateur == 1:
                string = r"$\dfrac{{\pi}}{{{0}}}$".format(denominateur)
            else:
                string = r"$\dfrac{{{0}\pi}}{{{1}}}$".format(numerateur, denominateur)
    else:
        pass
    return string


def dessineAngle(point, angle, decalageAngle=0, marque=None):
    if decalageAngle == 0:
        instructions = (
            r"\begin{{scope}}[shift={{({0},{1})}}]".format(point[0], point[1]) + "\n"
        )
    else:
        instructions = (
            r"\begin{{scope}}[shift={{({0},{1})}}, rotate={2}]".format(
                point[0], point[1], decalageAngle
            )
            + "\n"
        )
    # instructions+=r"\draw (0,0) arc ({0}:

    instructions += r"\end{scope}"
    return instructions


def simplifie(angle):
    a = angle[0]
    b = angle[1]
    c = gcd(a, b)
    return [int(a / c), int(b / c)]


def mesure_principale(angle, avec_nb_tours=False):
    """
    Calcule la mesure principale d'un angle en radians ( entre -pi et pi ). Extrait modifié de Pyromaths.
    """
    num = angle[0] % (2 * angle[1])
    k = angle[0] // (2 * angle[1])
    if num > angle[1]:
        num -= 2 * angle[1]
        k = k + 1
    if avec_nb_tours:
        return (simplifie([num, angle[1]]), k)
    else:
        return simplifie([num, angle[1]])


def mesure_positive(angle):
    mes = mesure_principale(angle)
    # Principe est admis que le dénominateur est positif
    if mes[0] < 0:
        mes = [mes[0] + 2 * mes[1], mes[1]]
    return mes


def mesure_negative(angle):
    mes = mesure_principale(angle)
    # Principe est admis que le dénominateur est positif
    if mes[0] > 0:
        mes = [mes[0] - 2 * mes[1], mes[1]]
    return mes


def corrige_anglesSontIlsEgaux(angle1, angle2, methode="calcul"):
    """
    Détaille la prise de décision sur l'égalité de deux angles.
    """
    ### cas des "fractions" non simplifiées à revoir. L'hypothèse est faite que les angles ont même dénominateur.
    corrige = ""
    if methode == "calcul":
        if angle2[0] > 0:
            corrige += (
                r"${0}-{1}=".format(rad2tex(angle1, True), rad2tex(angle2, True)) + "\n"
            )
            corrige += r"\dfrac{{{0}-{1}}}{{{2}}}=".format(
                rad2tex([angle1[0], 1], True), rad2tex([angle2[0], 1], True), angle2[1]
            )
            # corrige+=r"\dfrac{{{0}}}{{{1}}}=".format(angle1[0]-angle2[0],angle2[1])
        else:
            corrige += (
                r"${0}-\left({1}\right)=".format(
                    rad2tex(angle1, True), rad2tex(angle2, True)
                )
                + "\n"
            )
            corrige += r"\dfrac{{{0}-\left({1}\right)}}{{{2}}}=".format(
                rad2tex([angle1[0], 1], True), rad2tex([angle2[0], 1], True), angle2[1]
            )
            # corrige+=r"\dfrac{{{0}}}{{{1}}}=".format(angle1[0]-angle2[0],angle2[1])
        corrige += rad2tex([angle1[0] - angle2[0], angle2[1]], True)
        diff = simplifie([angle1[0] - angle2[0], angle2[1]])
        corrige += "=" + rad2tex(diff, True) + "$" + r"\\" + "\n"
        if diff[1] == 1 and diff[0] % 2 == 0:
            corrige += ur"{} est un multiple de $2\pi$.\\".format(rad2tex(diff)) + "\n"
            corrige += ur"Ces deux mesures correspondent à un même angle."
        else:
            corrige += (
                ur"{} n'est pas un multiple de $2\pi$.\\".format(rad2tex(diff)) + "\n"
            )
            corrige += ur"Ces deux mesures ne correspondent pas à un même angle."
    return corrige


def corrige_mesurePrincipale(angle, methode="approx"):
    """
    Détaille la recherche de la mesure principale d'un angle en radians.
    """
    corrige = r"mesure principale de {}: \\".format(rad2tex(angle)) + "\n"
    if methode == "approx":
        if angle[1] == 1:
            if angle[0] % 2 == 0:
                corrige += (
                    r"{} est un multiple de $2\pi$.\\".format(rad2tex(angle))
                    + r"\\"
                    + "\n"
                )
                corrige += (
                    r"Sa mesure principale est donc $0$.\\".format(rad2tex(angle))
                    + r"\\"
                    + "\n"
                )
            else:
                corrige += (
                    r"{} est multiple de $\pi$, mais pas de $2\pi$.\\".format(
                        rad2tex(angle)
                    )
                    + r"\\"
                    + "\n"
                )
                corrige += (
                    r"Sa mesure principale est donc $\pi$.\\".format(rad2tex(angle))
                    + r"\\"
                    + "\n"
                )
        else:
            if angle[0] / angle[1] == round(angle[0] / angle[1], 2):
                corrige += (
                    r"$\dfrac{{{0}}}{{{1}}}$".format(angle[0], angle[1])
                    + r"= {}".format(round(angle[0] / angle[1], 2))
                    + r"\\"
                    + "\n"
                )
            else:
                corrige += (
                    r"$\dfrac{{{0}}}{{{1}}}".format(angle[0], angle[1])
                    + r"\approx {}$".format(round(angle[0] / angle[1], 2))
                    + r"\\"
                    + "\n"
                )
            k1 = angle[0] // (2 * angle[1])
            k2 = k1 + 1
            corrige += (
                r"{0} est donc compris entre {1} et {2}".format(
                    rad2tex(angle), rad2tex([2 * k1, 1]), rad2tex([2 * k2, 1])
                )
                + r"\\"
                + "\n"
            )

            if angle[0] / angle[1] > 2 * k1 + 1:
                multiple = 2 * k2
                principale = [angle[0] - 2 * k2 * angle[1], angle[1]]
            else:
                multiple = 2 * k1
                principale = [angle[0] - 2 * k1 * angle[1], angle[1]]
            corrige += (
                r"Il est plus proche de {}\\".format(rad2tex([multiple, 1])) + "\n"
            )
            corrige += r"Sa mesure principale est : \\" + "\n"
            if multiple >= 0:
                corrige += (
                    r"$ {0}-{1}={0}-{3}={2}$".format(
                        rad2tex(angle, True),
                        rad2tex([multiple, 1], True),
                        rad2tex(principale, True),
                        rad2tex([multiple * angle[1], angle[1]], True),
                    )
                    + r"\\"
                    + "\n"
                )
            else:
                corrige += (
                    r"$ {0}+{1}={0}+{3}={2}$".format(
                        rad2tex(angle, True),
                        rad2tex([-multiple, 1], True),
                        rad2tex(principale, True),
                        rad2tex([-multiple * angle[1], angle[1]], True),
                    )
                    + r"\\"
                    + "\n"
                )
    else:  # Autres méthodes à prendre en compte, par exemple les soustractions successives sur des valeurs simples.
        pass
    return corrige


def opp(val):
    if val > 0:
        return "-{}".format(val)
    if val < 0:
        return str(-val)
    if val == 0:
        return str(val)


def corrige_valeurTrigo(angle, fonction):
    mesPrinc, k = mesure_principale(angle, avec_nb_tours=True)
    correction = r"$ \{0}\left({1}\right)".format(fonction, rad2tex(angle, True))
    if angle == [0, 1]:
        angleRef = angle
    else:
        angleRef = [1, mesPrinc[1]]
    if mesPrinc[1] in denominateursReference:
        valRef = valeursReferences[fonction][rad2tex(angleRef, True)]
        if angle == angleRef:
            correction += r"={}$".format(valRef)
        elif angle == [-angleRef[0], angleRef[1]]:  # cos-x et sin-x
            if fonction == "cos":
                correction += r"=\{0}\left({1}\right)={2}$".format(
                    fonction, rad2tex(angleRef, True), valRef
                )
            elif fonction == "sin":
                correction += r"=-\{0}\left({1}\right)={2}$".format(
                    fonction, rad2tex(angleRef, True), opp(valRef)
                )
        elif angle == [
            angleRef[1] + angleRef[0],
            angleRef[1],
        ]:  # cos(pi+x) et sin(pi+x)
            correction += r"=\{0}\left(\pi+{1}\right)".format(
                fonction, rad2tex(angleRef, True)
            )
            correction += r"=-\{0}\left({1}\right)={2}$".format(
                fonction, rad2tex(angleRef, True), opp(valRef)
            )
        elif angle == [
            angleRef[0] - angleRef[1],
            angleRef[1],
        ]:  # cos(x-pi) et sin(x-pi)
            correction += r"=\{0}\left({1}-\pi\right)".format(
                fonction, rad2tex(angleRef, True)
            )
            correction += r"=-\{0}\left({1}\right)={2}$".format(
                fonction, rad2tex(angleRef, True), opp(valRef)
            )
        elif angle == [
            angleRef[1] - angleRef[0],
            angleRef[1],
        ]:  # cos(pi-x) et sin(pi-x)
            correction += r"=\{0}\left(\pi-{1}\right)".format(
                fonction, rad2tex(angleRef, True)
            )
            if fonction == "cos":
                correction += r"=-\{0}\left({1}\right)={2}$".format(
                    fonction, rad2tex(angleRef, True), opp(valRef)
                )
            elif fonction == "sin":
                correction += r"=\{0}\left({1}\right)={2}$".format(
                    fonction, rad2tex(angleRef, True), valRef
                )
        else:
            # Rédaction très courte de la recherche de la mesure principale
            if k > 0:
                correction += r"=\{0}\left({1}+{2}\times 2\pi \right)$".format(
                    fonction, rad2tex(mesPrinc, True), k
                )
            elif k < 0:
                correction += r"=\{0}\left({1}-{2}\times 2\pi \right)$".format(
                    fonction, rad2tex(mesPrinc, True), -k
                )
            else:
                correction += r"=\{0}\left({1} \right)$".format(
                    fonction, rad2tex(mesPrinc, True)
                )
            correction += "=" + corrige_valeurTrigo(mesPrinc, fonction)
    else:
        if fonction == "cos":
            correction += r"\approx {}$".format(cos_(mesPrinc[0] * pi / mesPrinc[1]))
        elif fonction == "sin":
            correction += r"\approx {}$".format(sin_(mesPrinc[0] * pi / mesPrinc[1]))
    return correction


def corrige_valeursTrigo(angle):
    return (r"\\" + "\n").join(
        [corrige_valeurTrigo(angle, "cos"), corrige_valeurTrigo(angle, "sin")]
    )


def corrige_equationTrigo(valeur, fonction, inconnue="x"):
    correction = ""
    if fonction == "cos":
        coordonnee = "abscisse"
    elif fonction == "sin":
        coordonnee = u"ordonnée"
    if not isinstance(valeur, str):
        if valeur < -1:
            correction += (
                ur"${0}<-1$, il n'existe donc aucun réel ${1}$ tel que $\{2}({1})={0}$\\".format(
                    valeur, inconnue, fonction
                )
                + "\n"
            )
        elif valeur > 1:
            correction += (
                ur"${0}>1$, il n'existe donc aucun réel ${1}$ tel que $\{2}({1})={0}$\\".format(
                    valeur, inconnue, fonction
                )
                + "\n"
            )
        elif abs(valeur) == 1:
            if fonction == "cos":
                coordonnee = "abscisse"
                if valeur == 1:
                    sol = 0
                else:
                    sol = "\pi"
            elif fonction == "sin":
                coordonnee = u"ordonnée"
                if valeur == 1:
                    sol = r"\dfrac{\pi}{2}"
                else:
                    sol = r"-\dfrac{\pi}{2}"
            correction += (
                ur"Un seul point du cercle trigonométrique a pour {0} ${1}$.\\".format(
                    coordonnee, valeur
                )
                + "\n"
            )
            correction += ur"Les solutions de l'équation sont donc tous les réels ${0}$ tel que \\ $\left{0}={1}+2k\pi$, où $k$ est un entier relatif.".format(
                inconnue, sol
            )
    elif valeur in [k for k in valeursReferences[fonction].itervalues()]:
        for k in valeursReferences[fonction].keys():
            if valeursReferences[fonction][k] == valeur:
                sol = k
        correction += (
            ur"Deux points du cercle trigonométrique ont pour {0} ${1}$.\\".format(
                coordonnee, valeur
            )
            + "\n"
        )
        if fonction == "cos":
            correction += (
                ur"Les solutions de l'équation sont donc tous les réels ${}$ tel que :\\".format(
                    inconnue
                )
                + "\n"
            )
            correction += ur"""$
\begin{{cases}}
{0}  ={1}+2k\pi\\
\text{{ou}}\\
{0}  =-{1}+2k\pi\\
\end{{cases}}
$, où $k$ est un entier relatif.""".format(
                inconnue, sol
            )
        elif fonction == "sin":
            solRad = tex2rad(sol)
            sol1 = sol
            sol2 = rad2tex([solRad[1] - solRad[0], solRad[1]], True)
            correction += (
                ur"Les solutions de l'équation sont donc tous les réels ${}$ tel que :\\".format(
                    inconnue
                )
                + "\n"
            )
            correction += ur"""$
\begin{{cases}}
{0}  ={1}+2k\pi\\
\text{{ou}}\\
{0}  ={2}+2k\pi\\
\end{{cases}}
$, où $k$ est un entier relatif.""".format(
                inconnue, sol1, sol2
            )
    return correction


def corrige_ApplicationPythagore(
    var="x", fonction=r"\sin", valeur=Rational(1 / 2), borneInf=[0, 1], borneSup=[1, 2]
):
    correction = (
        r"$\cos^2 \left( {0} \right) +\sin^2 \left( {0} \right)=1$\\".format(var) + "\n"
    )
    if fonction == r"\sin":
        fonctAlt = "\cos"
    else:
        fonctAlt = "\sin"
    correction += (
        r"Donc : ${0}^2 \left( {2} \right) =1-{1}^2 \left( {2} \right)$\\".format(
            fonctAlt, fonction, var
        )
        + "\n"
    )
    correction += (
        r"Donc : ${0}^2 \left( {2} \right) =1- \left( {1}\right)^2=1-{3} ={4}$\\".format(
            fonctAlt, valeur, var, valeur ** 2, 1 - valeur ** 2
        )
        + "\n"
    )
    correction += (
        r"Comme ${0} \in \left[ {1};{2} \right] $, ${3}\left({0} \right)$ ".format(
            var, rad2tex(borneInf, True), rad2tex(borneSup, True), fonctAlt
        )
        + "\n"
    )
    return correction


def traceAngleSurCercleTrigoPartageTikz(
    angle,
    mode="fixe",
    point="M",
    xmin=-1.5,
    xmax=1.5,
    ymin=-1.5,
    ymax=1.5,
    xscale=1,
    yscale=1,
):
    # BUG : seul les anles positifs semblent être correctement tracés
    numerateur = angle[0]
    denominateur = angle[1]
    instructions = (
        r"\begin{tikzpicture}[xscale=%s,yscale=%s]" % (str(xscale), str(yscale)) + "\n"
    )

    instructions += r"\draw (0,0) circle (1);" + "\n"
    instructions += r"\draw[->]({0},0)--({1},0);".format(xmin, xmax) + "\n"
    instructions += r"\draw[->](0,{0})--(0,{1});".format(ymin, ymax) + "\n"
    instructions += (
        r"\draw (0,0) node[below right] {\tiny  $O$} node {\tiny  $x$};" + "\n"
    )

    instructions += (
        r"\draw (1,0) node[below right] {\tiny $I$} node {\tiny $x$} ;" + "\n"
    )
    instructions += (
        r"\draw (0,1) node[below right] {\tiny $J$} node {\tiny $x$};" + "\n"
    )
    instructions += r"\draw[very thick, -latex](0,0)--(1,0);" + "\n"
    for i in xrange(2 * denominateur):
        angle = i * pi / denominateur
        X = round(cos_(angle), 4)
        Y = round(sin_(angle), 4)
        if i < denominateur / 4:
            position = "right"
        elif i < denominateur / 2:
            position = "above right"
        elif i < 3 * denominateur / 4:
            position = "above left"
        elif i < denominateur:
            position = "left"
        # elif i == denominateur -1:
        # position='below'
        elif i < 5 * denominateur / 4:
            position = "below left"
        elif i < 3 * denominateur / 2:
            position = "below"
        elif i < 7 * denominateur / 4:
            position = "below right"
        elif i == 2 * denominateur:
            position = "above right"
        else:
            position = "below right"

        if i == numerateur:
            style = ""
            label = point
            instructions += (
                r"\draw[very thick,-latex](0,0)--({0},{1});".format(X, Y) + "\n"
            )
        else:
            style = "[blue]"
            label = ""

        instructions += (
            r"\draw {4} ({0},{1})  node {{\tiny $X$}} node[{2}]{{\tiny $ {3} $}};".format(
                X, Y, position, label, style
            )
            + "\n"
        )
        instructions += (
            r"\draw[blue][densely dotted,thick] (0,0) -- ({0},{1});".format(X, Y) + "\n"
        )
        alpha = angle - 0.5 * pi / denominateur
        instructions += (
            r"\draw [blue] ({0},{1})  node[rotate={2}] {{ \tiny //}};".format(
                cos_(alpha), sin_(alpha), alpha * 180 / pi - 70
            )
            + "\n"
        )
        if mode == "pasApas":
            instructions += r"\pause" + "\n"
    instructions += r"\end{tikzpicture}" + "\n"
    return instructions


def traceCercleTrigoPartageTikz(
    denominateur,
    mode="fixe",
    nomsPoints=None,
    xmin=-1.5,
    xmax=1.5,
    ymin=-1.5,
    ymax=1.5,
    xscale=1,
    yscale=1,
):
    instructions = (
        r"\begin{tikzpicture}[xscale=%s,yscale=%s]" % (str(xscale), str(yscale)) + "\n"
    )

    instructions += r"\draw (0,0) circle (1);" + "\n"
    instructions += r"\draw[->]({0},0)--({1},0);".format(xmin, xmax) + "\n"
    instructions += r"\draw[->](0,{0})--(0,{1});".format(ymin, ymax) + "\n"
    instructions += (
        r"\draw (0,0) node[below right] {\tiny  $O$} node {\tiny  $x$};" + "\n"
    )

    instructions += (
        r"\draw (1,0) node[below right] {\tiny $I$} node {\tiny $x$} ;" + "\n"
    )
    instructions += (
        r"\draw (0,1) node[below right] {\tiny $J$} node {\tiny $x$};" + "\n"
    )
    for i in xrange(2 * denominateur):
        angle = i * pi / denominateur
        if i < denominateur / 4:
            position = "right"
        elif i < denominateur / 2:
            position = "above right"
        elif i < 3 * denominateur / 4:
            position = "above left"
        elif i < denominateur:
            position = "left"
        # elif i == denominateur -1:
        # position='below'
        elif i < 5 * denominateur / 4:
            position = "below left"
        elif i < 3 * denominateur / 2:
            position = "below"
        elif i < 7 * denominateur / 4:
            position = "below right"
        elif i == 2 * denominateur:
            position = "above right"
        else:
            position = "below right"

        if nomsPoints == "Auto":
            label = "M_{{{}}}".format(i + 1)
            instructions += (
                r"\draw [blue] ({0},{1})  node {{\tiny $X$}} node[{2}]{{\tiny ${3}$}};".format(
                    cos_(angle), sin_(angle), position, label
                )
                + "\n"
            )
        elif nomsPoints is not None:
            label = nomsPoints[i]
            instructions += (
                r"\draw [blue] ({0},{1})  node {{\tiny $X$}} node[{2}]{{\tiny ${3}$}};".format(
                    cos_(angle), sin_(angle), position, label
                )
                + "\n"
            )
        else:
            instructions += (
                r"\draw [blue] ({0},{1}) ;".format(cos_(angle), sin_(angle)) + "\n"
            )

        instructions += (
            r"\draw[blue][densely dotted,thick] (0,0) -- ({0},{1});".format(
                cos_(angle), sin_(angle)
            )
            + "\n"
        )
        alpha = angle - 0.5 * pi / denominateur
        instructions += (
            r"\draw [blue] ({0},{1})  node[rotate={2}-70] {{ \tiny //}};".format(
                cos_(alpha), sin_(alpha), alpha * 180 / pi
            )
            + "\n"
        )
        if mode == "pasApas":
            instructions += r"\pause" + "\n"
    instructions += r"\end{tikzpicture}" + "\n"
    return instructions


def traceCercleTrigoTikz(
    angle=None, xmin=-1.5, xmax=1.5, ymin=-1.5, ymax=1.5, xscale=1, yscale=1
):
    string = (
        r"\begin{tikzpicture}[xscale=%s,yscale=%s]" % (str(xscale), str(yscale)) + "\n"
    )
    if angle is not None:
        angle = mesure_principale(angle)
        if angle[0] * angle[1] > 0:
            position = "above"
            # string+=r"\draw[very thick,green,->] (0.5,0) arc (0:{}:0.5);".format(rad2deg(angle))+'\n'
            # string+=r"\draw[very thick,red,->] (0.5,0) arc (0:{}:0.5);".format(rad2deg(angle)-360)+'\n'
        else:
            position = "below"
            # string+=r"\draw[very thick,red,->] (0.5,0) arc (0:{}:0.5);".format(rad2deg(angle))+'\n'
            # string+=r"\draw[very thick,green,->] (0.5,0) arc (0:{}:0.5);".format(rad2deg(angle)+360)+'\n'
        if abs(angle[0]) >= angle[1] / 2:
            position += " left"
        else:
            position += " right"
        string += r"\draw[very thick,-latex] (0,0) -- (1,0);"
        angle = angle[0] * pi / angle[1]
        string += (
            r"\draw ({0},{1})  node {{\tiny $x$}};".format(cos_(angle), sin_(angle))
            + "\n"
        )
        string += (
            r"\draw[very thick,-latex] (0,0) -- ({0},{1});".format(
                cos_(angle), sin_(angle)
            )
            + "\n"
        )
        string += (
            r"\draw ({0},{1}) node[{2}] {{\tiny M}};".format(
                cos_(angle), sin_(angle), position
            )
            + "\n"
        )
        # string+=r"\draw[green,->] (0.5,0) arc (0:{}:0.5);".format(angle*180/pi)+'\n'
        # string+=r"\fill[green!20!white](0,0) -- (0.5,0) arc (0:{0}:0.5) -- cycle;".format(angle*180/pi,'M')+'\n'
        # string+=r"\draw[green,->] (0.5,0) arc (0:{0}:0.5);".format(angle*180/pi,'M')+'\n'
        # string+=r"\draw[red,<-] ({0},{1}) arc ({2}:0:0.5);".format(0.5*cos_(angle),0.5*sin_(angle),angle*180/pi-360,'M')+'\n'
        # string+=r"\fill[red!20!white](0,0) -- ({0},{1}) arc ({2}:0:0.5) -- cycle;".format(0.5*cos_(angle),0.5*sin_(angle),angle*180/pi-360,'M')+'\n'
        # string+=r"\draw[red,<-] ({0},{1}) arc ({2}:0:0.5);".format(0.5*cos_(angle),0.5*sin_(angle),angle*180/pi-360,'M')+'\n'
    string += r"\draw[->](0,{0})--(0,{1});".format(ymin, ymax) + "\n"
    string += r"\draw[->]({0},0)--({1},0);".format(xmin, xmax) + "\n"
    string += r"\draw (0,0) node[below right] {\tiny  $O$} node {\tiny  $x$};" + "\n"

    string += r"\draw (1,0) node[below right] {\tiny $I$} node {\tiny $x$} ;" + "\n"
    string += r"\draw (0,1) node[below right] {\tiny $J$} node {\tiny $x$};" + "\n"

    string += r"\draw (0,0) circle (1);" + "\n"
    string += r"\end{tikzpicture}" + "\n"
    return string


def questionLecturePointImage(angle, nomsPoints="Auto"):
    """
    Demande à l'élève de déterminer quel point est l'image d'un angle donné.
    Le cercle est partagé en deux fois le dénominateur arcs égaux et l'angle simplifié.
    """
    q = {}
    a = angle[0]
    b = angle[1]
    c = gcd(a, b)
    angleSimpl = [int(a / c), int(b / c)]
    q["consigne"] = (
        ur"Déterminer le point image de \textbf{{{}}}".format(rad2tex(angleSimpl))
        + "\n"
    )
    q["enonce"] = traceCercleTrigoPartageTikz(angle[1], nomsPoints=nomsPoints) + "\n"
    rang_point = a % (2 * b)
    if nomsPoints == "Auto":
        q["corrige"] = "$M_{{}}$".format(rang_point)
    else:
        q["corrige"] = "${}$".format(nomsPoints[rang_point])
    return q


def questionLectureMesureAngle(angle, positive=True, point="M"):
    q = {}
    if positive:
        signe = "positive"
        q["corrige"] = rad2tex(mesure_positive(angle))
    else:
        signe = u"négative"
        q["corrige"] = rad2tex(mesure_negative(angle))
    q["consigne"] = (
        ur"Donner une mesure \textbf{{{0} en radians}} de l'angle $\left(\overrightarrow{{OI}};\overrightarrow{{O{1}}}\right)$.".format(
            signe, point
        )
        + "\n"
    )
    q["enonce"] = traceAngleSurCercleTrigoPartageTikz(angle, point=point) + "\n"

    return q


def questionLectureMesurePrincipaleAngle(angle, positive=True, point="M"):
    q = {}
    q["consigne"] = (
        ur"Donner \textbf{{la mesure principale}} en radians "
        ur"l'angle "
        ur"$\left(\overrightarrow{{OI}};\overrightarrow{{O{}}}\right)$.".format(point)
    )
    "\n"
    q["enonce"] = traceAngleSurCercleTrigoPartageTikz(angle, point=point) + "\n"
    q["corrige"] = rad2tex(mesure_principale(angle))
    return q


def questionPlacerPointsImagesSurCercleTrace(Angles, NomPoints="Auto"):
    """
    """
    q = {}
    listePointsDefaut = ["A", "B", "C", "D", "E", "F", "G", "H", "K", "L", "M", "N"][
        : len(Angles)
    ]
    listeAngles = [rad2tex(alpha) for alpha in Angles]
    descriptionAngles = ", ".join(listeAngles[:-1]) + " et " + listeAngles[-1]
    if NomPoints == "Auto":
        descriptionPoints = (
            ", ".join(listePointsDefaut[:-1]) + " et " + listePointsDefaut[-1]
        )
    else:
        descriptionPoints = ", ".join(nomPoints[:-1]) + " et " + nomPoints[-1]
    q["consigne"] = (
        r"Placer sur le cercle trigonométrique suivant les points "
        + descriptionPoints
        + r" associés respectivements aux mesures en radians suivantes : "
        + descriptionAngles
        + "."
    )

    q["enonce"] = traceCercleTrigoPartageTikz(12)
    return q


def questionAnglesSontIlsEgaux(angle1, angle2):
    q = {}
    q["consigne"] = r"$x$ et $y$ sont-ils des mesures d'un même angle ?"

    q["enonce"] = r"\center{{$x=${0} et $y=${1}}}".format(
        rad2tex(angle1), rad2tex(angle2)
    )
    return q


def questionDonnerUneAutreMesure(angle):
    q = {}
    q[
        "consigne"
    ] = r"Donner \textbf{autre mesure} de l'angle $\left(\overrightarrow{OI};\overrightarrow{OM}\right)$ "

    q[
        "enonce"
    ] = r"{0} est une mesure de $\left(\overrightarrow{{OI}};\overrightarrow{{OM}}\right)$, proposez-en une autre.".format(
        rad2tex(angle)
    )
    return q


def questionChoisirLaMesurePrincipale(angles):
    q = {}
    q[
        "consigne"
    ] = r"Parmi les mesures suivantes, seule une est la mesure principale d'un angle.\\ Déterminez laquelle "
    radians = [rad2tex(alpha) for alpha in angles]
    q["enonce"] = r"\center{" + ", ".join(radians[:-1]) + " et " + radians[-1] + "}"
    return q


def questionDeterminerLaMesurePrincipale(angle):
    q = {}
    q[
        "consigne"
    ] = r"Déterminez la mesure principale en radians de l'angle dont une mesure est : "
    q["enonce"] = r"\center{{{}}}".format(rad2tex(angle))
    q["corrige"] = rad2tex(mesure_principale(angle))
    return q


def questionLireLeCosinus1(angle):
    q = {}
    q[
        "consigne"
    ] = r"Sur la figure suivante, quel est le \textbf{cosinus} de l'angle $\left(\overrightarrow{{OI}};\overrightarrow{{OM}}\right)$ ?"
    q["enonce"] = traceCercleTrigoTikz(angle)
    return q


def questionLireLeSinus1(angle):
    q = {}
    q[
        "consigne"
    ] = r"Sur la figure suivante, quel est le \textbf{sinus} de l'angle $\left(\overrightarrow{{OI}};\overrightarrow{{OM}}\right)$ ?"
    q["enonce"] = r"\center{" + traceCercleTrigoTikz(angle) + "}"
    return q


def questionCosinus(angle):
    q = {}
    q["consigne"] = r"Donner la valeur de :"
    q["enonce"] = r"\center{{$\cos \left( {}\right)$}}".format(rad2tex(angle, True))
    angle = angle[0] * pi / angle[1]
    q["corrige"] = "${}$".format(latex(cos(angle)))
    return q


def questionSinus(angle):
    q = {}
    q["consigne"] = r"Donner la valeur de :"
    q["enonce"] = r"\center{{$\sin \left( {}\right)$}}".format(rad2tex(angle, True))
    angle = angle[0] * pi / angle[1]
    q["corrige"] = "${}$".format(latex(sin(angle)))
    return q


def questionSigne0(angle, fonction=r"\sin"):
    q = {}
    q["consigne"] = ur"Déterminer le signe de :"
    q["enonce"] = r"\center{{${0} \left( {1}\right)$}}".format(
        fonction, rad2tex(angle, True)
    )
    return q


def questionSigne(var="x", fonction=r"\sin", borneInf=[0, 1], borneSup=[1, 2]):
    q = {}
    q[
        "consigne"
    ] = r"${0} \in \left [ {1};{2}\right]$, déterminer \textbf{{si possible}} le signe de :".format(
        var, rad2tex(borneInf, True), rad2tex(borneSup, True)
    )
    q["enonce"] = r"\center{{${0} \left( {1}\right)$}}".format(fonction, var)
    return q


def questionApplicationPythagore(
    var="x", fonction=r"\sin", valeur=r"\dfrac{1}{2}", borneInf=[0, 1], borneSup=[1, 2]
):
    q = {}
    q[
        "consigne"
    ] = r"${0} \in \left [ {1};{2}\right]$ et ${3}({0})={4} $ déterminer \textbf{{si possible}}:".format(
        var, rad2tex(borneInf, True), rad2tex(borneSup, True), fonction, valeur
    )
    if fonction == r"\sin":
        fonctAlt = r"\cos"
    else:
        fonctAlt = r"\sin"
    q["enonce"] = r"\center{{${0} \left( {1}\right)$}}".format(fonctAlt, var)
    return q


def questionEquationTrigo(valeur, fonction, inconnue="x"):
    q = {}
    q[
        "consigne"
    ] = ur"Déterminer \textbf{{si possible}} les valeurs de ${0}$ telles que : ${0} \in \left ] -\pi;\pi\right]$ et :".format(
        inconnue
    )
    q["enonce"] = r"\center{{${0} \left( {1}\right)}}={2}$".format(
        fonction, inconnue, valeur
    )
    return q


def generer_AnglesEgauxOuPresque(anglesEgaux=True, signeAngle1="+", signeAngle2="+"):
    coeff = 1
    if signeAngle1 == "-":
        coeff = -1
    denom = choice([3, 4, 6])
    numer = coeff * randint(2 * denom + 1, 5 * denom - 1)
    angle1 = simplifie([numer, denom])
    # anglesEgaux=choice([True,False])
    if anglesEgaux:
        angle2 = [numer + 2 * randint(3, 6) * denom, denom]
    else:
        angle2 = [numer + 2 * (randint(3, 6) + 1) * denom, denom]
    angles = [angle1, angle2]
    shuffle(angles)
    return angles


def generer_AnglePourMesPrinc(signeMP="+", signeMA="+"):
    coeff = 1
    if signeMP == "-":
        coeff = -1
    denom = choice([3, 4, 6])
    numer = coeff * choice([1, denom - 1])
    nbTour = randint(3, 8)
    coeff = 1
    if signeMP == "-":
        coeff = -1
    numer = numer + coeff * 2 * denom * nbTour
    # print numer
    angle = [numer, denom]
    return angle


def generer_MesurePrincipale(listeDenom=[3, 4, 6], signe="+"):
    coeff = 1
    if signe == "-":
        coeff = -1
    denom = choice([3, 4, 6])
    numer = coeff * choice([1, denom - 1])
    return [numer, denom]


if __name__ == "__main__":
    pass
    # print r'\underline{exercice 12 p 167 :} \\' + '\n'
    # for mes in [[15,2],[-10,3],[-19,4],[17,6],[22,3],
    # [23,6],[27,4],[47,6],[-19,2]] :
    # print corrige_mesurePrincipale(mes) + r'\\'
    # print r'\underline{exercice 13 p 167 :} \\' + '\n'
    # for mes in [[-5,6],[-7,2],[11,3],[-5,4],[-16,6],[19,2],[19,6],
    # [19,4],[10,6],[4,3]] :
    # print corrige_mesurePrincipale(mes) + r'\\'
    # print r'\underline{exercice 14 p 167 :} \\' + '\n'
    # for mes in [[17,4],[-16,3],[37,6],[28,6]] :
    # print corrige_mesurePrincipale(mes) + r'\\'
    # print r'\underline{exercice 15 p 167 :} \\' + '\n'
    # for mes in [[-7,2],[13,3],[11,4],[23,6],[-11,3],[-15,6]] :
    # print corrige_mesurePrincipale(mes) + r'\\'
    # print r'\underline{exercice 18 p 167 :} \\' + '\n'
    # for mes in [[3,4],[5,4],[-7,4]]:
    # print corrige_valeursTrigo(mes) + r'\\'
    # print r'\underline{exercice 20 p 167 :} \\' + '\n'
    # for mes in [[31,6],[-13,4],[29,3],[35,2]]:
    # print corrige_valeursTrigo(mes) + r'\\'
    # print r'\underline{exercice 30 p 168 :} \\' + '\n'
    # print corrige_equationTrigo(r"\dfrac{1}{2}",'sin')
    # print corrige_equationTrigo(r"-\dfrac{1}{2}",'sin')

    # print corrige_valeurTrigo([3,4],'cos')
    # print corrige_valeurTrigo([27,4],'sin')
    # for i in xrange(10) :
    # mes = generer_MesurePrincipale()
    # print rad2tex(mes)
    # print rad2tex(mesure_negative(mes))
    # print rad2tex(mesure_positive(mes))
    # print ""

    # for i in xrange(6):
    # angles=[generer_AnglePourMesPrinc('+','+'),\
    # generer_AnglePourMesPrinc('-','+'),\
    # generer_AnglePourMesPrinc('+','-')]
    # shuffle(angles)
    # for angle in angles :
    # mes_princ=mesure_principale(angle)
    # print 'mesure principale de '+rad2tex(angle)+' :'
    # print rad2tex(mes_princ)
    # print ""

    # for i in xrange(6):
    # angles=[generer_AnglesEgauxOuPresque(True,'-'),\
    # generer_AnglesEgauxOuPresque(False)]
    # for angle in angles :
    # print rad2tex(angle[0])+"     "+rad2tex(angle[1])
    # print ""
