#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
from sympy import Abs, cos, sqrt, Rational, pi, latex
from compilation.compilateur import compileDocument
from generateurTex import (
    generer_revision_corrigee_tex,
    generer_interro_flash_tex,
    generer_interro_flash_corrigee_tex,
)


def translater(A, v):
    """
    renvoie les coordonnées de l'image de A par la translation
    de vecteur v
    """
    return [A[1] + v[0], A[2] + v[1]]


def vecteur_et_coordonnees(u):
    formule = ur"$\vec {} \begin{{pmatrix}}".format(u[0]) + ur" \\ ".join(
        [latex(val) for val in u[1:]]
    )
    formule += ur" \end{pmatrix}$ "
    return formule


def somme_produit(l1, l2):
    def aux(el):
        if el < 0:
            return ur"\left({}\right)".format(latex(el))
        else:
            return latex(el)

    return " + ".join([latex(e1) + r"\times " + aux(e2) for e1, e2 in zip(l1, l2)])


def vecteur_unitaire(v):
    norme_v = distance(["O", 0, 0], ["M"] + v)
    return [round(v[0] / norme_v, 3), round(v[1] / norme_v, 3)]


def vecteur_orthogonal(v):
    return [-v[1], v[0]]


def somme(u, v):
    return [u[0] + v[0], u[1] + v[1]]


def distance(A, B):
    return sqrt((B[1] - A[1]) ** 2 + (B[2] - A[2]) ** 2)


def produit_scalaire_analytique(u, v):
    if len(u) == 2:
        return u[0] * v[0] + u[1] * v[1]
    elif len(u) == 3:
        return u[0] * v[0] + u[1] * v[1] + u[2] * v[2]


def projete(A, B, C):
    """
    renvoie le projeté orthogonal de C sur (AB)
    (on part du principe que la droite (AB) est
    horizontale ou verticale).
    """
    if est_horizontal(vecteur(A, B)):
        H = ["H", C[1], A[2]]
    else:  # on part du principe que (AB) est vertical
        H = ["H", A[1], C[2]]
    return H


def vecteur(A, B):
    return [B[1] - A[1], B[2] - A[2]]


def sont_colineaires(u, v):
    return u[0] * v[1] - u[1] * v[0] == 0


def sont_orthogonaux(u, v):
    return u[0] * v[0] + u[1] * v[1] == 0


def est_horizontal(u):
    return u[1] == 0


def est_vertical(u):
    return u[0] == 0


def dessine_couple_vecteur_meme_origine(A, B, C, echelle=0.5):
    posA = posB = posC = "below right"
    return (
        r"\begin{{tikzpicture}}[scale={}]".format(echelle)
        + r"\tkzInit[xmin=-5,ymin=-5,xmax=5,ymax=5]"
        + "\n"
        + r"\tkzGrid"
        + "\n"
        + r"\draw[very thick,-latex]({},{})--({},{});".format(A[1], A[2], B[1], B[2])
        + "\n"
        + r"\draw[very thick,-latex]({},{})--({},{});".format(A[1], A[2], C[1], C[2])
        + "\n"
        + r"\draw ({},{}) node {{\tiny  ${{\times}}$}};".format(A[1], A[2])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(A[1], A[2], posA, A[0])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(B[1], B[2], posB, B[0])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(C[1], C[2], posC, C[0])
        + "\n"
        + r"\end{tikzpicture}\\"
        + "\n"
    )


def marque_angle_droit(A, B, C, echelle=0.5):
    posA = posB = posC = "below right"
    u1 = vecteur_unitaire(vecteur(A, B))
    v1 = vecteur_unitaire(vecteur(A, C))
    M1 = translater(A, u1)
    M2 = translater(A, somme(u1, v1))  # ma struture de données est trop
    # inconsistante
    M3 = translater(A, v1)
    instructions = (
        r"\begin{{tikzpicture}}[scale={}]".format(echelle)
        + r"\tkzInit[xmin=-5,ymin=-5,xmax=5,ymax=5]"
        + "\n"
        + r"\tkzGrid"
        + "\n"
        + r"\draw[very thick,-latex]({},{})--({},{});".format(A[1], A[2], B[1], B[2])
        + "\n"
        + r"\draw[very thick,-latex]({},{})--({},{});".format(A[1], A[2], C[1], C[2])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(A[1], A[2], posA, A[0])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(B[1], B[2], posB, B[0])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(C[1], C[2], posC, C[0])
        + "\n"
        + r"\pause"
        + "\n"
        + r"\draw[color=red,thick]({},{})--({},{})--({},{});".format(
            M1[0], M1[1], M2[0], M2[1], M3[0], M3[1]
        )
        + r"\end{tikzpicture}\\"
        + "\n"
    )
    return instructions


def projette(A, B, C, echelle=0.5):
    """
    projette le vecteur AC sur le vecteur AB
    (on part du principe que le vecteur AB est
    horizontal ou vertical).
    """
    posA = posB = posC = posH = "below right"
    instructions = (
        r"\begin{{tikzpicture}}[scale={}]".format(echelle)
        + r"\tkzInit[xmin=-5,ymin=-5,xmax=5,ymax=5]"
        + "\n"
        + r"\tkzGrid"
        + "\n"
        + r"\draw[very thick,-latex]({},{})--({},{});".format(A[1], A[2], B[1], B[2])
        + "\n"
        + r"\draw[very thick,-latex]({},{})--({},{});".format(A[1], A[2], C[1], C[2])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(A[1], A[2], posA, A[0])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(B[1], B[2], posB, B[0])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(C[1], C[2], posC, C[0])
        + "\n"
        + r"\pause"
        + "\n"
    )
    if est_horizontal(vecteur(A, B)):
        instructions += r"\draw[dashed,thick](-5,{0})--(5,{0});".format(A[2])
        H = ["H", C[1], A[2]]
    else:  # AB doit être vertical
        instructions += r"\draw[dashed,thick]({0},-5)--({0},5);".format(A[1])
        H = ["H", A[1], C[2]]
    instructions += (
        r"\pause"
        + "\n"
        + r"\draw[dashed,color=red, thick]({},{})--({},{});".format(
            C[1], C[2], H[1], H[2]
        )
        + "\n"
        + r"\pause"
        + "\n"
        + ur"\draw ({},{}) node[color=red] {{$\times$}};".format(H[1], H[2])
        + r"\draw ({},{}) node[color=red,{}] {{\tiny  ${}$}};".format(
            H[1], H[2], posH, H[0]
        )
        + "\n"
        + r"\pause"
        + "\n"
        + r"\draw[color=red,very thick,-latex]({},{})--({},{});".format(
            A[1], A[2], H[1], H[2]
        )
        + "\n"
        + r"\end{tikzpicture}\\"
        + "\n"
    )
    return instructions


def dessine_droite(
    vecteur, point, style="dashed,thick", xmin=-5, xmax=5, ymin=-5, ymax=5
):
    instructions = ""
    if est_vertical(vecteur):
        instructions += r"\draw[{0}]({1},-5)--({1},5);".format(style, point[1]) + "\n"
    else:
        m = vecteur[1] / vecteur[0]
        y1 = point[2] + m * (-5 - point[1])
        if y1 >= -5 and y1 <= 5:
            x1 = -5
        elif y1 < -5:
            y1 = -5
            x1 = point[1] + (-5 - point[2]) / m
        else:
            y1 = 5
            x1 = point[1] + (5 - point[2]) / m

        y2 = point[2] + m * (5 - point[1])
        if y2 >= -5 and y2 <= 5:
            x2 = 5
        elif y2 < -5:
            y2 = -5
            x2 = point[1] + (-5 - point[2]) / m
        else:
            y2 = 5
            x2 = point[1] + (5 - point[2]) / m
        instructions += (
            r"\draw[{}]({},{})--({},{});".format(style, x1, y1, x2, y2) + "\n"
        )
    return instructions


def decompose(A, B, C, echelle=0.5):
    """
    decompose le vecteur AC en deux vecteurs :
    - un vecteur colineaire avec AB
    - un vecteur orthogonal avec AB
    """
    posA = posB = posC = posH = "below right"
    instructions = (
        r"\begin{{tikzpicture}}[scale={}]".format(echelle)
        + r"\tkzInit[xmin=-5,ymin=-5,xmax=5,ymax=5]"
        + "\n"
        + r"\tkzGrid"
        + "\n"
        + r"\draw[very thick,-latex]({},{})--({},{});".format(A[1], A[2], B[1], B[2])
        + "\n"
        + r"\draw[very thick,-latex]({},{})--({},{});".format(A[1], A[2], C[1], C[2])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(A[1], A[2], posA, A[0])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(B[1], B[2], posB, B[0])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(C[1], C[2], posC, C[0])
        + "\n"
        + r"\pause"
        + "\n"
    )
    u = [b - a for a, b in zip(A[1:], B[1:])]
    v = [c - a for a, c in zip(A[1:], C[1:])]
    uv = sum([el1 * el2 for el1, el2 in zip(u, v)])
    uu = sum([el1 * el2 for el1, el2 in zip(u, u)])
    instructions += dessine_droite(u, A) + r"\pause" + "\n"
    instructions += dessine_droite(vecteur_orthogonal(u), A) + r"\pause" + "\n"
    instructions += dessine_droite(vecteur_orthogonal(u), C) + r"\pause" + "\n"
    H = translater(A, [el * uv / uu for el in u])
    instructions += (
        r"\draw[red,very thick,-latex]({},{})--({},{});".format(A[1], A[2], H[0], H[1])
        + "\n"
        + r"\pause"
        + "\n"
    )
    instructions += dessine_droite(u, C) + r"\pause" + "\n"
    G = translater(C, [-el * uv / uu for el in u])
    instructions += (
        r"\draw[blue,very thick,-latex]({},{})--({},{});".format(A[1], A[2], G[0], G[1])
        + "\n"
        + r"\pause"
        + "\n"
    )
    instructions += r"\end{tikzpicture}\\" + "\n"
    return instructions


def question_calcul_avec_longueurs_et_angle(A, B, C, AB, AC, theta):
    question = {}
    question["consigne"] = (
        u"Déterminer le produit scalaire des vecteurs"
        + ur" $\overrightarrow{{{0}{1}}}$ et $\overrightarrow{{{0}{2}}}$ :".format(
            A, B, C
        )
    )
    question["enonce"] = (
        r"${0}{1}={3}$, ${0}{2}={4}$, "
        + r"$\left(\overrightarrow{{{0}{1}}}"
        + r";\overrightarrow{{{0}{2}}}\right)={5}$."
    ).format(A, B, C, AB, AC, latex(theta))
    question.update(corrige_calcul_avec_longueurs_et_angle(A, B, C, AB, AC, theta))
    return question


def corrige_calcul_avec_longueurs_et_angle(A, B, C, AB, AC, theta):
    reponse = {}
    reponse["rappel du texte"] = (
        r"${0}{1}={3}$, ${0}{2}={4}$, "
        + r"$\left(\overrightarrow{{{0}{1}}}"
        + r";\overrightarrow{{{0}{2}}}\right)={5}$"
    ).format(A, B, C, AB, AC, latex(theta))
    cs = cos(theta)
    pdt = AB * AC * cs
    reponse["corrige"] = (
        ur"$\overrightarrow{{{0}{1}}}"
        + ur".\overrightarrow{{{0}{2}}}"
        + ur"={0}{1} \times {0}{2}\times"
        + ur"\cos\left(\overrightarrow{{{0}{1}}}"
        + ur";\overrightarrow{{{0}{2}}}\right)"
        + ur"={3}\times {4}\times\cos \left({5}\right)"
        + ur"={3}\times {4}\times {6}={7}$\\"
        + "\n"
    ).format(A, B, C, AB, AC, latex(theta), latex(cs), latex(pdt))
    return reponse


def question_calcul_par_projection(A, B, C, echelle=0.5):
    question = {}
    question["consigne"] = (
        u"Déterminer le produit scalaire des vecteurs" + u" représentés ci-dessous :"
    )
    posA = posB = posC = "below right"

    question["enonce"] = (
        r"\begin{{tikzpicture}}[scale={}]".format(echelle)
        + r"\tkzInit[xmin=-5,ymin=-5,xmax=5,ymax=5]"
        + "\n"
        + r"\tkzGrid"
        + "\n"
        + r"\draw[very thick,-latex]({},{})--({},{});".format(A[1], A[2], B[1], B[2])
        + "\n"
        + r"\draw[very thick,-latex]({},{})--({},{});".format(A[1], A[2], C[1], C[2])
        + "\n"
        + r"\draw ({},{}) node {{\tiny  ${{\times}}$}};".format(A[1], A[2])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(A[1], A[2], posA, A[0])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(B[1], B[2], posB, B[0])
        + "\n"
        + r"\draw ({},{}) node[{}] {{\tiny  ${}$}};".format(C[1], C[2], posC, C[0])
        + "\n"
        + r"\end{tikzpicture}\\"
        + "\n"
    )
    question.update(corrige_calcul_par_projection(A, B, C, echelle=0.5))
    return question


def corrige_calcul_par_projection(A, B, C, echelle=0.5):
    reponse = {}
    reponse["rappel texte"] = r"\ "
    if sont_colineaires(vecteur(A, B), vecteur(A, C)):
        AC = distance(A, C)
        AB = distance(A, B)
        reponse["corrige"] = (
            dessine_couple_vecteur_meme_origine(A, B, C)
            + r"\pause"
            + "\n"
            + (
                ur"{{Les vecteurs $\overrightarrow{{{0}{1}}}$"
                + ur" et $\overrightarrow{{{0}{2}}}$ sont colinéaires.}}\\"
                + "\n"
            ).format(A[0], B[0], C[0])
            + "\pause"
            + "\n"
        )
        if produit_scalaire_analytique(vecteur(A, B), vecteur(A, C)) > 0:
            reponse["corrige"] += ur"De plus, ils ont même sens." + (
                ur"Donc $\overrightarrow{{{0}{1}}}"
                + ur".\overrightarrow{{{0}{2}}}={0}{1} \times {0}{2}"
                + ur"={3}\times{4}={5}$\\"
                + "\n"
            ).format(A[0], B[0], C[0], AB, AC, AB * AC)
        else:
            reponse["corrige"] += ur"De plus, ils sont de sens opposés." + (
                ur"Donc $\overrightarrow{{{0}{1}}}"
                + ur".\overrightarrow{{{0}{2}}}=-{0}{1} \times {0}{2}"
                + ur"=-{3}\times{4}=-{5}$\\"
                + "\n"
            ).format(A[0], B[0], C[0], AB, AC, AB * AC)
    elif sont_orthogonaux(vecteur(A, B), vecteur(A, C)):
        reponse["corrige"] = (
            marque_angle_droit(A, B, C)
            + (
                r"\onslide<3->{{Les vecteurs $\overrightarrow{{{0}{1}}}$"
                + r" et $\overrightarrow{{{0}{2}}}$ sont orthogonaux.}}\\"
                + "\n"
            ).format(A[0], B[0], C[0])
            + (
                r"\onslide<4->{{Donc $\overrightarrow{{{0}{1}}}."
                + r"\overrightarrow{{{0}{2}}}=0$}}\\"
                + "\n"
            ).format(A[0], B[0], C[0])
        )

    elif est_horizontal(vecteur(A, B)) or est_vertical(vecteur(A, B)):
        H = projete(A, B, C)
        AH = distance(A, H)
        AB = distance(A, B)
        reponse["corrige"] = (
            projette(A, B, C)
            + r"\onslide<6->{{${0}H={1}$}}\onslide<7->{{ ,${0}{2}={3}$ }}\\".format(
                A[0], AH, B[0], AB
            )
            + "\n"
            + r"\onslide<8->{{Les vecteurs $\overrightarrow{{{}H}}$".format(A[0])
            + r" et $\overrightarrow{{{}{}}}$ ".format(A[0], B[0])
        )
        if produit_scalaire_analytique(vecteur(A, B), vecteur(A, C)) > 0:
            reponse["corrige"] += (
                ur" sont de même sens.}\\"
                + (
                    ur"\onslide<9->{{Donc $\overrightarrow{{{0}{1}}}"
                    + ur".\overrightarrow{{{0}{2}}}"
                ).format(A[0], B[0], C[0])
                + ur"={0}{1} \times {0}{2}={3} \times {4}={5}$}}".format(
                    A[0], B[0], H[0], AB, AH, AB * AH
                )
            )
        else:
            reponse["corrige"] += (
                ur" sont de sens opposés.}\\"
                + (
                    ur"\onslide<9->{{Donc $\overrightarrow{{{0}{1}}}"
                    + ur".\overrightarrow{{{0}{2}}}"
                ).format(A[0], B[0], C[0])
                + ur"=-{0}{1} \times {0}{2}=-{3} \times {4}=-{5}$}}".format(
                    A[0], B[0], H[0], AB, AH, AB * AH
                )
            )
    elif est_horizontal(vecteur(A, C)) or est_vertical(vecteur(A, C)):
        H = projete(A, C, B)
        AH = distance(A, H)
        AC = distance(A, C)
        reponse["corrige"] = (
            projette(A, C, B)
            + r"\onslide<6->{{${0}H={1}$}}\onslide<7->{{ ,${0}{2}={3}$ }}\\".format(
                A[0], AH, C[0], AC
            )
            + r"\onslide<8->{{Les vecteurs $\overrightarrow{{{}H}}$".format(A[0])
            + r" et $\overrightarrow{{{}{}}}$ ".format(A[0], C[0])
        )
        if produit_scalaire_analytique(vecteur(A, B), vecteur(A, C)) > 0:
            reponse["corrige"] += (
                ur" sont de même sens.}\\"
                + (
                    ur"\onslide<9->{{Donc $\overrightarrow{{{0}{1}}}"
                    + ur".\overrightarrow{{{0}{2}}}"
                ).format(A[0], B[0], C[0])
                + ur"={0}{1} \times {0}{2}={3} \times {4}={5}$}}".format(
                    A[0], H[0], C[0], AH, AC, AC * AH
                )
            )
        else:
            reponse["corrige"] += (
                ur" sont de sens opposés.}\\"
                + (
                    ur"\onslide<9->{{Donc $\overrightarrow{{{0}{1}}}"
                    + ur".\overrightarrow{{{0}{2}}}"
                ).format(A[0], B[0], C[0])
                + ur"=-{0}{1} \times {0}{2}=-{3} \times {4}=-{5}$}}".format(
                    A[0], H[0], C[0], AH, AC, AC * AH
                )
            )
    return reponse


def question_calcul_avec_normes_et_angles(u, v, norme_u, norme_v, angle):
    question = {}
    question["consigne"] = (
        u"Déterminer le produit scalaire des vecteurs"
        + ur" $\vec{{{}}}$ et $\vec{{{}}}$ :".format(u, v)
    )
    question["enonce"] = (
        r"$||\vec{{{0}}}||={2}$, $||\vec{{{1}}}||={3}$,"
        r" $\left(\vec{{{0}}};\vec{{{1}}}\right)={4}$."
    ).format(u, v, norme_u, norme_v, latex(angle))
    question.update(corrige_calcul_avec_normes_et_angles(u, v, norme_u, norme_v, angle))
    return question


def corrige_calcul_avec_normes_et_angles(u, v, norme_u, norme_v, angle):
    reponse = {}
    reponse["rappel texte"] = (
        r"$||\vec{{{0}}}||={2}$,"
        r" $||\vec{{{1}}}||={3}$,"
        r" $\left(\vec{{{0}}};\vec{{{1}}}\right)={4}$."
    ).format(u, v, norme_u, norme_v, latex(angle))
    pdt_scalaire = norme_u * norme_v * cos(angle)
    pdt_norme = norme_u * norme_v
    reponse["corrige"] = (
        ur"$\vec{{{0}}}.\vec{{{1}}}"
        + ur"=||\vec{{{0}}}||\times||\vec{{{1}}}||"
        + ur"\times \cos\left(\vec{{{0}}};\vec{{{1}}}\right)"
        + ur"={2}\times {3}\times\cos\left({4}\right)={5}\times {6}={7}$."
    ).format(
        u,
        v,
        latex(norme_u),
        latex(norme_v),
        latex(angle),
        latex(cos(angle)),
        latex(pdt_norme),
        latex(pdt_scalaire),
    )
    return reponse


def question_calcul_avec_coordonnees(u, v):
    question = {}
    question["consigne"] = (
        u"Déterminer le produit scalaire des vecteurs"
        + ur" $\vec {}$ et $\vec {}$ dont les coordonnées".format(u[0], v[0])
        + ur" dans un repère orthonormé sont:"
    )
    question["enonce"] = vecteur_et_coordonnees(u) + " ; " + vecteur_et_coordonnees(v)
    question.update(corrige_calcul_avec_coordonnees(u, v))
    return question


def corrige_calcul_avec_coordonnees(u, v):
    reponse = {}
    lpdt = [e1 * e2 for e1, e2 in zip(u[1:], v[1:])]
    reponse["rappel texte"] = (
        vecteur_et_coordonnees(u) + " ; " + vecteur_et_coordonnees(v)
    )

    def aux(el):
        if el < 0:
            return ur"\left({}\right)".format(latex(el))
        else:
            return latex(el)

    reponse["corrige"] = (
        ur"$\vec {}.\vec {}=".format(u[0], v[0])
        + somme_produit(u[1:], v[1:])
        + "="
        + " + ".join([latex(lpdt[0])] + map(aux, lpdt[1:]))
        + "="
        + latex(sum([e1 * e2 for e1, e2 in zip(u[1:], v[1:])]))
        + "$"
    )
    return reponse


def main():
    pass


interro = {
    "classe": ur"1ère STI-2D",
    "date": "Vendredi 29 avril 2016",
    "num": u"15",
    "questions": [
        {
            "gauche": question_calcul_par_projection(
                ["A", -2, -1], ["B", 2, -1], ["C", -4, -1]
            ),
            "droite": question_calcul_avec_coordonnees(["u", 1, 2], ["v", 3, -2]),
            "temps": 45,
        },
        {
            "gauche": question_calcul_avec_coordonnees(["u", 2, 3], ["v", 3, -2]),
            "droite": question_calcul_par_projection(
                ["A", -2, -1], ["B", 2, -1], ["C", -4, 2]
            ),
            "temps": 45,
        },
        {
            "gauche": question_calcul_avec_normes_et_angles("u", "v", 2, 3, pi / 3),
            "droite": question_calcul_par_projection(
                ["A", -2, -1], ["B", 2, 3], ["C", -2, 2]
            ),
            "temps": 45,
        },
        {
            "gauche": question_calcul_par_projection(
                ["A", -2, -1], ["B", 2, -1], ["C", 0, 2]
            ),
            "droite": question_calcul_par_projection(
                ["A", 3, 1], ["B", 3, 3], ["C", -2, 1]
            ),
            "temps": 45,
        },
        {
            "gauche": question_calcul_par_projection(
                ["A", -2, -1], ["B", 2, 2], ["C", -2, -4]
            ),
            "droite": question_calcul_avec_normes_et_angles("u", "v", 5, 2, 2 * pi / 3),
            "temps": 45,
        },
    ],
}

liste = [
    question_calcul_par_projection(["A", -2, 3], ["B", 2, 3], ["C", 1, -5]),
    question_calcul_par_projection(["A", 0, -1], ["B", -4, 1], ["C", 1, 1]),
    question_calcul_par_projection(["A", -2, -1], ["B", 3, -1], ["C", -4, -1]),
    question_calcul_avec_longueurs_et_angle("D", "E", "F", 3, 2, 3 * pi / 4),
    question_calcul_avec_normes_et_angles("u", "v", 3, 7, pi / 6),
    question_calcul_avec_coordonnees(["u", 1, 2, 3], ["v", 3, -2, Rational(1, 3)]),
]

liste2 = [
    question_calcul_par_projection(["O", -1, 0], ["A", 3, 0], ["B", 2, 1]),
    question_calcul_par_projection(["O", 1, 1], ["A", 4, 1], ["B", -1, -1]),
    question_calcul_par_projection(["O", 1, -1], ["A", 1, 4], ["B", -1, 3]),
    question_calcul_par_projection(["O", 3, 4], ["A", 3, -1], ["B", -1, 4]),
    question_calcul_par_projection(["O", -1, 2], ["A", -1, 3], ["B", 0, -1]),
    question_calcul_par_projection(["O", -1, 1], ["A", 1, -1], ["B", 2, 4]),
]
DS = {
    "classe": ur"1ère STI-2D",
    "date": "Vendredi 29 avril 2016",
    "num": u"15",
    "questions": liste,
}
if __name__ == "__main__":
    print question_calcul_avec_coordonnees(["u", 2, 3], ["v", 4, 5])["enonce"]
    print dessine_droite([0, 2], ["A", 1, -1])
    print dessine_droite([2, -0.5], ["A", -1, 0])
    print dessine_droite([1, 4], ["A", -1, 0])
    print decompose(["A", -2, -2], ["B", 2, 3], ["C", -2, 2])

    # print question_calcul_par_projection(
    # ['A',-2,3],['B',2,3],['C',-2,-2])
    # print question_calcul_avec_longueurs_et_angle('A','B','C',3,2,pi/3)
    # generer_revision_corrigee_tex(main())
    # compileDocument('1STI2D_pdtScal2',
    # generer_revision_corrigee_tex(main()))
    compileDocument('1STI2D_DS',
                    generer_revision_corrigee_tex(DS))
    print(generer_interro_flash_corrigee_tex(interro))
